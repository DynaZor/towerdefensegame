# Tower Defense Game by DynaZor

## Back to [README.md](https://bitbucket.org/DynaZor/towerdefensegame/src/master/)\\[INDEX.md](https://bitbucket.org/DynaZor/towerdefensegame/src/master/INDEX.md)

## Credits & Thanks

A passion project by Shahar DynaZor Alon



### Testing

May Mor Yosef



### Technical

Unity Technologies - Thanks for a great engine with great support and great community!

Jason Weimann @ Unity3D.college - Taught DynaZor the basics and principles of Development

Ilan Tsarfati, God rest his soul - Taught DynaZor the basics, principles, theory, history and discipline of Film Making and the arts



### Assets

SerializableDictionary by Mathiue Le Ber - Under MIT license



### Additional Ideas

DynaZor's mother, Alejandro Bitran, May Mor Yosef, Eugene Malugin



### Interviews

GamesIL.net (05/04/2020) - [Link (Hebrew)](http://gamesil.net/dz-tower-defense/), [Link (English, Google Translate)](https://translate.google.com/translate?sl=iw&tl=en&u=http%3A%2F%2Fgamesil.net%2Fdz-tower-defense%2F)



### Support & Care

DynaZor's family, May Mor Yosef, Eugene Malugin