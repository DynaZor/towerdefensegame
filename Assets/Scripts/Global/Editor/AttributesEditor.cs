﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Events;

public class AttributesEditor
{
	[CustomPropertyDrawer(typeof(ConditionalAttribute))]
	public class ConditionalAttributeDrawer : PropertyDrawer
	{
		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
		{
			ConditionalAttribute attr = attribute as ConditionalAttribute;
			bool conditionMet = property.serializedObject.FindProperty(attr.BoolName).boolValue == attr.EnabledWhen;
			if (attr.PreventDraw && !conditionMet)
			{
				return;
			}
			GUI.enabled = conditionMet;
			EditorGUI.PropertyField(position, property, label, true);
		}
	}

	[CustomPropertyDrawer(typeof(CurrentMenusControllerAttribute))]
	public class MenusControllerDrawer : PropertyDrawer
	{
		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
		{
			Rect rect_objField = new Rect(position);
			rect_objField.width = 0.5f * position.width;

			Rect rect_Button = new Rect(position);
			rect_Button.width = 0.5f * position.width;
			rect_Button.x += rect_objField.width;

			EditorGUI.ObjectField(rect_objField, property, typeof(MenusController));
			if (GUI.Button(rect_Button, new GUIContent("Auto Get", "Auto Reference the Scene's MenusController")))
			{
				property.objectReferenceValue = GameObject.FindObjectOfType<MenusController>();
			}
		}
	}

	[CustomPropertyDrawer(typeof(MenusControllerCanvasesAttribute))]
	public class MenusControllerCanvasesDrawer : PropertyDrawer
	{
		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
		{
			bool MenusControllerIsSet = (property.serializedObject.targetObject as UI_Button_CanvasToggler).MenusControllerIsSet;

			if (MenusControllerIsSet)
			{
				MenusController controller = (property.serializedObject.targetObject as UI_Button_CanvasToggler).currentMenusController;

				string[] canvasNames = new string[controller.Canvases.Count + 1];
				int currentSelected = 0;
				canvasNames[0] = "None\\Other";
				if (canvasNames.Length > 1)
				{
					for (int i = 0; i < canvasNames.Length - 1; i++)
					{
						canvasNames[i + 1] = controller.Canvases[i].gameObject.name;
						if (property.objectReferenceValue == controller.Canvases[i])
						{
							currentSelected = i + 1;
						}
					}
				}

				Rect rect_Dropdown = new Rect(position);

				if (currentSelected == 0)
				{
					Rect rect_objField = new Rect(position);
					rect_objField.width = 0.5f * position.width;
					rect_Dropdown.width = 0.5f * position.width;
					rect_Dropdown.x += rect_objField.width;
					EditorGUI.ObjectField(rect_objField, property, typeof(MenusController));
				}

				int newSelection = EditorGUI.Popup(rect_Dropdown, currentSelected, canvasNames);

				if (newSelection != currentSelected) // Changed
				{
					property.objectReferenceValue = controller.Canvases[newSelection - 1];
				}
			}
			else
			{
				EditorGUI.ObjectField(position, property, typeof(MenusController));
			}

		}
	}
}
