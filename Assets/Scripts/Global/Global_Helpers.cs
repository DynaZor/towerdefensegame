﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
namespace Global_Helpers
{
	public class Pair<T1, T2>
	{
		public T1 Item1 { get; set; }
		public T2 Item2 { get; set; }
	}
}