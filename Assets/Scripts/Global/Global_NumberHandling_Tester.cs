﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Global_NumberHandling_Tester : MonoBehaviour
{
	public uint InputNumberUINT = 0;
	public string OutputStringUINT = "";
	public int InputNumberINT = 0;
	public string OutputStringINT = "";

	private void OnValidate ()
	{
		OutputStringUINT = InputNumberUINT.GetNumberString();
		OutputStringINT = InputNumberINT.GetNumberString();
	}
}