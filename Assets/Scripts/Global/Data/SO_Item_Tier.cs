﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;
using ResourcesSystem;
using System.Collections.Generic;

[System.Serializable, SerializeField, CreateAssetMenu(fileName = "item__Tier", menuName = "Create new Item Tier asset")]
public class SO_Item_Tier : ScriptableObject
{
	public SO_Item_Sprites Sprites = null;
	public ResourcesStack Cost = new ResourcesStack();
	public float Health = 100;
	public float Defence = 0;
	public List<NodeResourceType> NodeNeedTypes = new List<NodeResourceType>();
	public List<float> NodeNeedValues = new List<float>();
	public List<CraftedResourceType> CraftedNeedTypes = new List<CraftedResourceType>();
	public List<float> CraftedNeedValues = new List<float>();
	public List<CraftedResourceType> ProductionTypes = new List<CraftedResourceType>();
	public List<float>  ProductionValues = new List<float>();
	public float PollutionAmount = 0;
	public float ElectricityCapacity = 0;
	public float AttackRange = 0;
	public float AttackPower = 0;
	public SO_AttackTiming attackTiming = null;
}