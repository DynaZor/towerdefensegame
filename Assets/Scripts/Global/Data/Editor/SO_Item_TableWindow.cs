﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.AnimatedValues;
using UnityEngine.Events;
using UnityEditor;
using DynaZor.EditorUtilities.EUEditorOnly;
using ResourcesSystem;
using DynaZor.EditorUtilities;

public class SO_Item_TableWindow : EditorWindow
{
	public static readonly string WindowTitle = "SO_Global_Items Editor";
	#region Data
	public SO_Global_Items Items = null;
	#endregion
	#region GUI Labels
	private GUIContent Label_Item = new GUIContent("Items Asset");
	private GUIContent Label_Ally_Buildings = new GUIContent("Ally_Buildings");
	private GUIContent Label_Ally_Units     = new GUIContent("Ally_Units");
	private GUIContent Label_Ally_Bases     = new GUIContent("Ally_Bases");
	private GUIContent Label_Enemy_Units    = new GUIContent("Enemy_Units");
	#endregion
	#region GUI Styles
	private GUIStyle toolbarButton_Active;
	private GUIStyle general_alternateColor;
	private GUIStyle tier_alternateColor;
	private GUIStyle foldoutHeader_Bold;
	private GUIStyle foldoutHeader_Bold_alternateColor;
	private GUIStyle floatField_AlignRight;
	private static float _button_MiniButton_Width = 18f;
	private GUILayoutOption button_MiniButton_Width = GUILayout.Width(_button_MiniButton_Width);
	private Texture texture_Transparent;
	private Color transparentWhite = new Color(1f,1f,1f,0.5f);
	private Color transparentGray = new Color(0.5f,0.5f,0.5f,0.5f);
	private Color transparentBlack = new Color(0f,0f,0f,0.5f);
	#endregion
	#region UI State
	private bool needToSetup = true;

	public int currentListIndex = 0;

	private Vector2 ListDataScrollView_position = new Vector2();
	private List<AnimBool> listFoldoutState = new List<AnimBool>();
	private bool ShowQuickToggles = true;

	private int filter_results = 0;
	private int currentFilterIndex = 0;
	private AgesSystem.Age filter_age = AgesSystem.Age.Stone;
	private bool filter_bool = true;
	private string filter_string = "";
	private int filter_id = 0;

	private bool preview_LargeSprites = false;
	private float preview_SpriteWidth
	{
		get
		{
			if (preview_LargeSprites)
			{
				return 128f;
			}
			return 32f;
		}

	}
	#endregion
	#region Editor Integration
	#region Open Blank Window
	[MenuItem("TowerDefence/SO_Global_Items Editor")]
	private static void OpenBlankEditor ()
	{
		var window = EditorWindow.GetWindow<SO_Item_TableWindow>(false, WindowTitle, true);
		if (Selection.activeObject != null && Selection.activeObject.GetType() == typeof(SO_Global_Items))
		{
			window.Items = (SO_Global_Items) Selection.activeObject;
		}
	}
	#endregion
	#region Open SO_Global_Items
	[MenuItem("Assets/TowerDefence/SO_Global_Items Editor")]
	private static void LoadSelectedIntoEditor ()
	{
		var window = EditorWindow.GetWindow<SO_Item_TableWindow>(false, WindowTitle, true);
		window.Items = (SO_Global_Items) Selection.activeObject;
	}
	[MenuItem("Assets/TowerDefence/SO_Global_Items Editor", true)]
	private static bool LoadSelectedIntoEditor_Validation ()
	{
		try
		{
			SO_Global_Items a = (SO_Global_Items) Selection.activeObject;
		}
		catch
		{
			return false;
		}
		return true;
	}
	#endregion
	#region Add Items To Lists
	private static EditorWindow lastFocusedWindow = null;

	[MenuItem("Assets/Add to List/Ally_Bases")]
	private static void AddItemToList_Ally_Bases ()
	{
		var window = EditorWindow.GetWindow<SO_Item_TableWindow>(false, WindowTitle, true);
		window.Items.Ally_Bases.Add((SO_Item)Selection.activeObject);
	}
	[MenuItem("Assets/Add to List/Ally_Buildings")]
	private static void AddItemToList_Ally_Buildings ()
	{
		var window = EditorWindow.GetWindow<SO_Item_TableWindow>(false, WindowTitle, true);
		window.Items.Ally_Buildings.Add((SO_Item) Selection.activeObject);
	}
	[MenuItem("Assets/Add to List/Ally_Units")]
	private static void AddItemToList_Ally_Units ()
	{
		var window = EditorWindow.GetWindow<SO_Item_TableWindow>(false, WindowTitle, true);
		window.Items.Ally_Units.Add((SO_Item) Selection.activeObject);
	}
	[MenuItem("Assets/Add to List/Enemy_Units")]
	private static void AddItemToList_Enemy_Units ()
	{
		var window = EditorWindow.GetWindow<SO_Item_TableWindow>(false, WindowTitle, true);
		window.Items.Enemy_Units.Add((SO_Item) Selection.activeObject);
	}

	[MenuItem("Assets/Add to List/Ally_Bases", true)]
	[MenuItem("Assets/Add to List/Ally_Buildings", true)]
	[MenuItem("Assets/Add to List/Ally_Units", true)]
	[MenuItem("Assets/Add to List/Enemy_Units", true)]
	private static bool AddItemToList_Validation ()
	{
		lastFocusedWindow = EditorWindow.focusedWindow;
		try
		{
			SO_Item a = (SO_Item) Selection.activeObject;
			if (a == null)
			{
				return false;
			}
			EditorWindow.FocusWindowIfItsOpen(typeof(SO_Item_TableWindow));
			if (EditorWindow.focusedWindow.GetType() != typeof(SO_Item_TableWindow))
			{
				return false;
			}
			var window = EditorWindow.GetWindow<SO_Item_TableWindow>(false, WindowTitle, false);
			if (window.Items == null)
			{
				lastFocusedWindow.Focus();
				return false;
			}
			lastFocusedWindow.Focus();
		}
		catch
		{
			lastFocusedWindow.Focus();
			return false;
		}
		lastFocusedWindow.Focus();
		return true;
	}
	#endregion
	#endregion
	#region Lifecycle
	private void __Setup ()
	{
		toolbarButton_Active = new GUIStyle(EditorStyles.toolbarButton);
		toolbarButton_Active.fontStyle = FontStyle.Bold;
		Texture2D temp_texture_Transparent = new Texture2D(1,1);
		temp_texture_Transparent.SetPixel(0, 0, new Color(0, 0, 0, 0));
		texture_Transparent = (Texture) temp_texture_Transparent;

		string path = AssetDatabase.GetAssetPath(Items);
		SO_Global_Items items1 = (SO_Global_Items) AssetDatabase.LoadAssetAtPath(path, typeof(SO_Global_Items));
		Items = items1;

		Color[] alternateTex_general_pix = new Color[1]{
			new Color(0.9f, 0.9f, 0.9f) 
		};
		Texture2D alternateTex_general = new Texture2D(1, 1);
		alternateTex_general.SetPixels(alternateTex_general_pix);
		alternateTex_general.filterMode = FilterMode.Point;
		alternateTex_general.Apply();

		Color[] alternateTex_tier_pix = new Color[1]{
			new Color (0.8f, 0.8f, 0.8f)
		};
		Texture2D alternateTex_tier = new Texture2D(1, 1);
		alternateTex_tier.SetPixels (alternateTex_tier_pix);
		alternateTex_tier.filterMode = FilterMode.Point;
		alternateTex_tier.Apply ();

		general_alternateColor = new GUIStyle();
		general_alternateColor.normal.background = alternateTex_general;

		tier_alternateColor = new GUIStyle ();
		tier_alternateColor.normal.background = alternateTex_tier;


		foldoutHeader_Bold = new GUIStyle(EditorStyles.foldoutHeader);
		foldoutHeader_Bold.fontStyle = FontStyle.Bold;
		foldoutHeader_Bold.fixedWidth = 0;
		foldoutHeader_Bold.stretchWidth = true;

		foldoutHeader_Bold_alternateColor = new GUIStyle(EditorStyles.foldoutHeader);
		foldoutHeader_Bold_alternateColor.fontStyle = FontStyle.Bold;
		foldoutHeader_Bold_alternateColor.fixedWidth = 0;
		foldoutHeader_Bold_alternateColor.stretchWidth = true;

		floatField_AlignRight = new GUIStyle(EditorStyles.numberField);
		floatField_AlignRight.alignment = TextAnchor.MiddleRight;

		needToSetup = false;
	}
	#region Setup Helpers
	private void Setup ()
	{
		EditorCoroutine.start(_Setup());
	}
	private IEnumerator _Setup ()
	{
		bool needToWait = true;
		while (needToWait)
		{
			try
			{
				if (EditorStyles.toolbarButton != null)
				{
					needToWait = false;
				}
			}
			catch
			{
				needToWait = true;
			}
			yield return null;
		}

		__Setup();
	}
	private void OnEnable ()
	{
		if (EditorApplication.isCompiling)
		{
			needToSetup = true;
			return;
		}

		if (needToSetup)
			Setup();
	}
	#endregion
	#endregion
	#region GUI Draw
	#region OnGUI
	#region Base
	private void OnGUI ()
	{
		if (EditorApplication.isPlayingOrWillChangePlaymode)
		{
			return;
		}
		if (EditorApplication.isCompiling)
		{
			needToSetup = true;
			Repaint();
			return;
		}
		else
		{
			if (needToSetup)
			{
				Setup();
				Repaint();
				return;
			}
		}

		DrawItemsObjectField();

		if (Items != null)
		{
			if (focusedWindow == this)
			{
				if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.S))
				{
					SaveAll();
				}
			}
			using (var changeCheck = new EditorGUI.ChangeCheckScope())
			{
				EditorGUIUtility.fieldWidth = 100f;
				EditorGUIUtility.labelWidth = 100f;
				DrawTabToolbar();
				DrawFilterToolbar();
				filter_results = 0;
				var list = currentList();
				using (var ListDataScrollView = new EditorGUILayout.ScrollViewScope(ListDataScrollView_position))
				{
					ListDataScrollView_position = ListDataScrollView.scrollPosition;
					UpdateListFoldoutState(list.Count);
					for (int listIndex = 0; listIndex < list.Count; listIndex++)
					{
						if (DrawListItem(list, listIndex))
						{
							filter_results++;
						}
					}
					DrawAddItemButton();
				}

				if (changeCheck.changed)
				{
					UpdateItemIDs(list);
					EditorUtility.SetDirty(Items);
				}
			}
		}
		Repaint();
	}
	#endregion
	#endregion
	#region GUI Draw - Methods
	#region Items
	private bool DrawListItem (List<SO_Item> list, int listIndex)
	{
		#region Filters
		if (currentFilterIndex != 0)
		{
			switch (currentFilterIndex)
			{
				case 1:
					if (filter_age != list[listIndex].age)
						return false;
					break;
				case 2:
					if (filter_bool != list[listIndex].Movable)
						return false;
					break;
				case 3:
					if (filter_bool != list[listIndex].Buyable)
						return false;
					break;
				case 4:
					if (filter_bool != list[listIndex].CanAttack)
						return false;
					break;
				case 5:
					if (filter_bool != list[listIndex].Demolishable)
						return false;
					break;
				case 6:
					if (!list[listIndex].ItemName.Contains(filter_string))
						return false;
					break;
				case 7:
					if (listIndex != filter_id)
						return false;
					break;
			}
		}
		#endregion
		using (var c = new EditorGUI.ChangeCheckScope())
		{
			using (new EditorGUILayout.VerticalScope(AlternateStyle(listIndex)))
			{
				#region Item Header
				using (new EditorGUILayout.HorizontalScope())
				{
					string label = "[" + listIndex + " \\ ?] Empty";
					if (list[listIndex] != null)
					{
						label = "[" + listIndex + " \\ " + list[listIndex].ItemID.ToString() + "] '" + list[listIndex].ItemName + "' (" + list[listIndex].Tiers.Count + ")";
						listFoldoutState[listIndex].target = EditorGUILayout.Foldout(listFoldoutState[listIndex].target, label, true, AlternateStyle_foldoutBold(listIndex));
						if (ShowQuickToggles)
						{
							GUILayout.FlexibleSpace();
							EditorGUIUtility.labelWidth = 1f;
							EditorGUIUtility.fieldWidth = 90f;
							list[listIndex].age = (AgesSystem.Age) EditorGUILayout.EnumPopup(GUIContent.none, list[listIndex].age);
							EditorGUIUtility.fieldWidth = 10f;
							list[listIndex].Movable = EditorGUILayout.ToggleLeft(GUIContent.none, list[listIndex].Movable);
							var typeRect = GUILayoutUtility.GetLastRect();
							GUI.Label(typeRect, new GUIContent("", "Movable"));
							list[listIndex].Buyable = EditorGUILayout.ToggleLeft(GUIContent.none, list[listIndex].Buyable);
							typeRect = GUILayoutUtility.GetLastRect();
							GUI.Label(typeRect, new GUIContent("", "Buyable"));
							list[listIndex].Demolishable = EditorGUILayout.ToggleLeft(GUIContent.none, list[listIndex].Demolishable);
							typeRect = GUILayoutUtility.GetLastRect();
							GUI.Label(typeRect, new GUIContent("", "Demolishable"));
							list[listIndex].CanAttack = EditorGUILayout.ToggleLeft(GUIContent.none, list[listIndex].CanAttack);
							typeRect = GUILayoutUtility.GetLastRect();
							GUI.Label(typeRect, new GUIContent("", "Can Attack"));
							EditorGUIUtility.fieldWidth = 100f;
							EditorGUIUtility.labelWidth = 100f;
						}
					}
					else
					{
						EditorGUILayout.PrefixLabel(label, EditorStyles.boldLabel);
					}

					list[listIndex] = EditorGUILayout.ObjectField(GUIContent.none, list[listIndex], typeof(SO_Item), false) as SO_Item;

					using (new EditorGUILayout.HorizontalScope(GUILayout.Width((_button_MiniButton_Width + 5f) * 3f + 2f)))
					{
						GUILayout.FlexibleSpace();
						if (listIndex > 0)
						{
							if (GUILayout.Button(new GUIContent("^", "Move Item Up The List"), EditorStyles.miniButton, button_MiniButton_Width))
							{
								Helpers.MoveItemInList(list, listIndex, listIndex - 1);
								UpdateItemIDs(list);
								Repaint();
							}
						}
						if (listIndex != list.Count - 1)
						{
							if (GUILayout.Button(new GUIContent("V", "Move Item Down The List"), EditorStyles.miniButton, button_MiniButton_Width))
							{
								Helpers.MoveItemInList(list, listIndex, listIndex + 1);
								UpdateItemIDs(list);
								Repaint();
							}
						}
						GUILayout.Space(2f);
						if (GUILayout.Button(new GUIContent("-", "Remove Item from list\nRight Click: Delete Item Asset"), EditorStyles.miniButton, button_MiniButton_Width))
						{
							if (Event.current.type == EventType.ContextClick)
							{
								AssetDatabase.MoveAssetToTrash(AssetDatabase.GetAssetPath(list[listIndex]));
								UpdateItemIDs(list);
								Repaint();
							}
							list.RemoveAt(listIndex);
						}
					}
				}
				#endregion
				#region Item Details
				if (list[listIndex] != null)
				{
					using (var f = new EditorGUILayout.FadeGroupScope(listFoldoutState[listIndex].faded))
					{
						if (f.visible)
						{
							EditorHelpers.EditorUI.DrawUILine(transparentWhite, 1, 0);
							DrawListItemDetails(list, listIndex);
						}
					}
				}
				#endregion
			}
			if (c.changed)
			{
				if (list[listIndex])
				{
					EditorUtility.SetDirty(list[listIndex]);
				}
			}
		}
		#region UI
		EditorHelpers.EditorUI.DrawUILine(transparentGray, 1, -4);
		#endregion
		return true;
	}
	private void DrawListItemDetails (List<SO_Item> list, int listIndex)
	{
		using (var changeCheck = new EditorGUI.ChangeCheckScope())
		{
			using (new EditorGUILayout.VerticalScope(AlternateStyle(listIndex)))
			{
				using (var h = new EditorGUILayout.HorizontalScope())
				{
					using (new EditorGUILayout.VerticalScope())
					{
						list[listIndex].ItemName = EditorGUILayout.DelayedTextField("Name", list[listIndex].ItemName);
						list[listIndex].age = (AgesSystem.Age) EditorGUILayout.EnumPopup("Age", list[listIndex].age);
						list[listIndex].Movable = EditorGUILayout.Toggle("Movable", list[listIndex].Movable);
					}
					using (new EditorGUILayout.VerticalScope())
					{
						list[listIndex].Buyable = EditorGUILayout.Toggle("Buyable", list[listIndex].Buyable);
						list[listIndex].Demolishable = EditorGUILayout.Toggle("Demolishable", list[listIndex].Demolishable);
						list[listIndex].CanAttack = EditorGUILayout.Toggle("Can Attack", list[listIndex].CanAttack);
					}
					using (new EditorGUILayout.VerticalScope())
					{
						using (new EditorGUILayout.HorizontalScope())
						{
							EditorGUILayout.LabelField("Index:", GUILayout.Width(50f));
							EditorGUILayout.SelectableLabel(listIndex.ToString(), EditorStyles.numberField, GUILayout.Width(50f), GUILayout.Height(EditorGUIUtility.singleLineHeight));
						}
						using (new EditorGUILayout.HorizontalScope())
						{
							EditorGUILayout.LabelField("ID:", GUILayout.Width(50f));
							EditorGUILayout.SelectableLabel(list[listIndex].ItemID.ToString(), EditorStyles.numberField, GUILayout.Width(50f), GUILayout.Height(EditorGUIUtility.singleLineHeight));
						}
						using (new EditorGUILayout.HorizontalScope())
						{
							EditorGUILayout.LabelField("Cat:", GUILayout.Width(50f));
							EditorGUILayout.SelectableLabel(list[listIndex].ItemCat.ToString(), EditorStyles.numberField, GUILayout.Width(50f), GUILayout.Height(EditorGUIUtility.singleLineHeight));
						}
					}
					GUILayout.FlexibleSpace();
				}

				if (list[listIndex].Tiers.Count > 0)
				{
					for (int i = 0; i < list[listIndex].Tiers.Count; i++)
					{
						DrawTier(list, listIndex, i);
					}
				}
				else
				{
					DrawItemTierAddButton(list, listIndex);
				}
			}
			if (changeCheck.changed)
			{
				EditorUtility.SetDirty(list[listIndex]);
				string itemName = "Item_" + list[listIndex].age.ToString() + "_" + list[listIndex].ItemName;
				AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(list[listIndex]), itemName);
				if (list[listIndex].Tiers != null && list[listIndex].Tiers.Count > 0)
				{
					for (int i = 0; i < list[listIndex].Tiers.Count; i++)
					{
						if (list[listIndex].Tiers[i] != null)
						{
							EditorUtility.SetDirty(list[listIndex].Tiers[i]);
							string tierName = itemName + "_Tier" + i;
							AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(list[listIndex].Tiers[i]), tierName);
							if (list[listIndex].Tiers[i].Sprites != null)
							{
								EditorUtility.SetDirty(list[listIndex].Tiers[i].Sprites);
								string spriteName = tierName + "_Sprites";
								AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(list[listIndex].Tiers[i].Sprites), spriteName);
							}
						}
					}
				}
				Repaint();
			}
		}
	}
	private void DrawTier (List<SO_Item> list, int listIndex, int i)
	{
		using (var cTier = new EditorGUI.ChangeCheckScope())
		{
			using (new EditorGUILayout.VerticalScope(AlternateStyleTier(i)))
			{
				using (new EditorGUILayout.HorizontalScope())
				{
					using (new EditorGUILayout.VerticalScope()) // Basic Details
					{
						using (new EditorGUILayout.HorizontalScope())
						{
							DrawItemTierRemoveButton(list, listIndex, i);
							DrawItemTierAddButton(list, listIndex);
							EditorGUILayout.LabelField("Tier " + i.ToString(), EditorStyles.boldLabel);
							if (!preview_LargeSprites)
							{
								GUILayout.FlexibleSpace();
								list[listIndex].Tiers[i] = (SO_Item_Tier) EditorGUILayout.ObjectField(list[listIndex].Tiers[i], typeof(SO_Item_Tier), false);
							}
						}

						if (list[listIndex].Tiers[i] != null)
						{
							using (new EditorGUILayout.HorizontalScope())
							{
								using (new EditorGUILayout.VerticalScope())
								{
									EditorGUILayout.LabelField("Cost", GUILayout.Width(60f));
								}
								using (new EditorGUILayout.VerticalScope())
								{
									EditorGUILayout.LabelField("Crafted Resources");
									DrawResources(list[listIndex].Tiers[i].Cost.CraftedResourceTypes, list[listIndex].Tiers[i].Cost.CraftedResourceAmounts);
								}
								using (new EditorGUILayout.VerticalScope())
								{
									EditorGUILayout.LabelField("Node Resources");
									DrawResources(list[listIndex].Tiers[i].Cost.NodeResourceTypes, list[listIndex].Tiers[i].Cost.NodeResourceAmounts);
								}
							}
							EditorHelpers.EditorUI.DrawUILine(transparentWhite, 1, 1);
							using (new EditorGUILayout.HorizontalScope())
							{
								using (new EditorGUILayout.VerticalScope())
								{
									list[listIndex].Tiers[i].Health = EditorGUILayout.DelayedFloatField("Health", list[listIndex].Tiers[i].Health, floatField_AlignRight, GUILayout.Width(200f));
									list[listIndex].Tiers[i].Defence = EditorGUILayout.DelayedFloatField("Defence", list[listIndex].Tiers[i].Defence, floatField_AlignRight, GUILayout.Width(200f));
									list[listIndex].Tiers[i].ElectricityCapacity = EditorGUILayout.DelayedFloatField("ElectricityCapacity", list[listIndex].Tiers[i].ElectricityCapacity, floatField_AlignRight, GUILayout.Width(200f));
								}
								using (new EditorGUILayout.VerticalScope())
								{
									list[listIndex].Tiers[i].AttackPower = EditorGUILayout.DelayedFloatField("AttackPower", list[listIndex].Tiers[i].AttackPower, floatField_AlignRight, GUILayout.Width(200f));
									list[listIndex].Tiers[i].AttackRange = EditorGUILayout.DelayedFloatField("AttackRange", list[listIndex].Tiers[i].AttackRange, floatField_AlignRight, GUILayout.Width(200f));
								}
							}
						}
					} // Basic Details

					using (new EditorGUILayout.VerticalScope()) // Tier Asset
					{
						if (list[listIndex].Tiers[i] != null)
						{
							if (preview_LargeSprites)
							{
								list[listIndex].Tiers[i] = (SO_Item_Tier) EditorGUILayout.ObjectField(list[listIndex].Tiers[i], typeof(SO_Item_Tier), false);
							}
							if (!preview_LargeSprites) // Small Sprites
							{
								list[listIndex].Tiers[i].Sprites = (SO_Item_Sprites) EditorGUILayout.ObjectField("Sprites", list[listIndex].Tiers[i].Sprites, typeof(SO_Item_Sprites), false);
								if (list[listIndex].Tiers[i].Sprites != null)
								{
									using (var c = new EditorGUI.ChangeCheckScope())
									{
										using (new EditorGUILayout.HorizontalScope())
										{
											EditorGUILayout.PrefixLabel("Active");
											list[listIndex].Tiers[i].Sprites.Active = (Texture2D) EditorGUILayout.ObjectField(list[listIndex].Tiers[i].Sprites.Active, typeof(Texture2D), false);
											var _rect = EditorGUILayout.GetControlRect(false, preview_SpriteWidth);
											EditorGUI.DrawTextureTransparent(_rect, list[listIndex].Tiers[i].Sprites.Active, ScaleMode.ScaleToFit, 1f, -1f);
											DrawTexturePreviewButton(_rect, list[listIndex].Tiers[i].Sprites.Active);
										}
										using (new EditorGUILayout.HorizontalScope())
										{
											EditorGUILayout.PrefixLabel("Producing");
											list[listIndex].Tiers[i].Sprites.Producing = (Texture2D) EditorGUILayout.ObjectField(list[listIndex].Tiers[i].Sprites.Producing, typeof(Texture2D), false);
											var _rect = EditorGUILayout.GetControlRect(false, preview_SpriteWidth);
											EditorGUI.DrawTextureTransparent(_rect, list[listIndex].Tiers[i].Sprites.Producing, ScaleMode.ScaleToFit, 1f, -1f);
											DrawTexturePreviewButton(_rect, list[listIndex].Tiers[i].Sprites.Producing);
										}
										using (new EditorGUILayout.HorizontalScope())
										{
											EditorGUILayout.PrefixLabel("InActive");
											list[listIndex].Tiers[i].Sprites.InActive = (Texture2D) EditorGUILayout.ObjectField(list[listIndex].Tiers[i].Sprites.InActive, typeof(Texture2D), false);
											var _rect = EditorGUILayout.GetControlRect(false, preview_SpriteWidth);
											EditorGUI.DrawTextureTransparent(_rect, list[listIndex].Tiers[i].Sprites.InActive, ScaleMode.ScaleToFit, 1f, -1f);
											DrawTexturePreviewButton(_rect, list[listIndex].Tiers[i].Sprites.InActive);
										}
										if (c.changed)
										{
											EditorUtility.SetDirty(list[listIndex].Tiers[i].Sprites);
										}
									}
								}
								else
								{
									if (GUILayout.Button(new GUIContent("Create", "Create a Sprites Asset"), EditorStyles.miniButton))
									{
										string path = Items.GetPath(currentListIndex) + "/SO_Item_Tier_Sprites";
										while (AssetDatabase.LoadAssetAtPath(path + ".asset", typeof(ScriptableObject)) != null)
										{
											path += "_";
										}
										SO_Item_Sprites newItem = ScriptableObject.CreateInstance(typeof(SO_Item_Sprites)) as SO_Item_Sprites;
										AssetDatabase.CreateAsset(newItem, path + ".asset");
										list[listIndex].Tiers[i].Sprites = newItem;
									}
								}
							}
						}
						else
						{
							using (new EditorGUILayout.HorizontalScope())
							{
								if (GUILayout.Button(new GUIContent("Create", "Auto create Tier Asset and reference it"), EditorStyles.miniButton, GUILayout.Width(50f)))
								{
									string path = Items.GetPath(currentListIndex) + "/SO_Item_Tier";
									while (AssetDatabase.LoadAssetAtPath(path + ".asset", typeof(ScriptableObject)) != null)
									{
										path += "_";
									}

									SO_Item_Tier newItem = ScriptableObject.CreateInstance(typeof(SO_Item_Tier)) as SO_Item_Tier;
									AssetDatabase.CreateAsset(newItem, path + ".asset");
									list[listIndex].Tiers[i] = newItem;
								}
								list[listIndex].Tiers[i] = (SO_Item_Tier) EditorGUILayout.ObjectField(list[listIndex].Tiers[i], typeof(SO_Item_Tier), false);
							}
						}
					} // Tier Asset

					GUILayout.FlexibleSpace();
				}

				EditorHelpers.EditorUI.DrawUILine(transparentWhite, 1, 1);
				using (new EditorGUILayout.VerticalScope(GUILayout.Width(550f))) // Tier Asset
				{
					if (list[listIndex].Tiers[i] != null)
					{
						if (preview_LargeSprites) // Large Sprites
						{
							list[listIndex].Tiers[i].Sprites = (SO_Item_Sprites) EditorGUILayout.ObjectField("Sprites", list[listIndex].Tiers[i].Sprites, typeof(SO_Item_Sprites), false);
							if (list[listIndex].Tiers[i].Sprites != null)
							{
								using (new EditorGUILayout.HorizontalScope(GUILayout.Width(550f)))
								{
									using (new EditorGUILayout.VerticalScope())
									{
										EditorGUILayout.PrefixLabel("Active");
										list[listIndex].Tiers[i].Sprites.Active = (Texture2D) EditorGUILayout.ObjectField(list[listIndex].Tiers[i].Sprites.Active, typeof(Texture2D), false);
										if (list[listIndex].Tiers[i].Sprites.Active != null)
										{
											var _rect = EditorGUILayout.GetControlRect(false, preview_SpriteWidth);
											EditorGUI.DrawTextureTransparent(_rect, list[listIndex].Tiers[i].Sprites.Active, ScaleMode.ScaleToFit, 1f, -1f);
											DrawTexturePreviewButton(_rect, list[listIndex].Tiers[i].Sprites.Active);
										}
									}
									using (new EditorGUILayout.VerticalScope())
									{
										EditorGUILayout.PrefixLabel("Producing");
										list[listIndex].Tiers[i].Sprites.Producing = (Texture2D) EditorGUILayout.ObjectField(list[listIndex].Tiers[i].Sprites.Producing, typeof(Texture2D), false);
										if (list[listIndex].Tiers[i].Sprites.Producing != null)
										{
											var _rect = EditorGUILayout.GetControlRect(false, preview_SpriteWidth);
											EditorGUI.DrawTextureTransparent(_rect, list[listIndex].Tiers[i].Sprites.Producing, ScaleMode.ScaleToFit, 1f, -1f);
											DrawTexturePreviewButton(_rect, list[listIndex].Tiers[i].Sprites.Producing);
										}
									}
									using (new EditorGUILayout.VerticalScope())
									{
										EditorGUILayout.PrefixLabel("InActive");
										list[listIndex].Tiers[i].Sprites.InActive = (Texture2D) EditorGUILayout.ObjectField(list[listIndex].Tiers[i].Sprites.InActive, typeof(Texture2D), false);
										if (list[listIndex].Tiers[i].Sprites.InActive != null)
										{
											var _rect = EditorGUILayout.GetControlRect(false, preview_SpriteWidth);
											EditorGUI.DrawTextureTransparent(_rect, list[listIndex].Tiers[i].Sprites.InActive, ScaleMode.ScaleToFit, 1f, -1f);
											DrawTexturePreviewButton(_rect, list[listIndex].Tiers[i].Sprites.InActive);
										}
									}
								}
							}
							else
							{
								if (GUILayout.Button(new GUIContent("Create", "Create a Sprites Asset"), EditorStyles.miniButton))
								{
									string path = Items.GetPath(currentListIndex) + "/SO_Item_Tier_Sprites";
									while (AssetDatabase.LoadAssetAtPath(path + ".asset", typeof(ScriptableObject)) != null)
									{
										path += "_";
									}
									SO_Item_Sprites newItem = ScriptableObject.CreateInstance(typeof(SO_Item_Sprites)) as SO_Item_Sprites;
									AssetDatabase.CreateAsset(newItem, path + ".asset");
									list[listIndex].Tiers[i].Sprites = newItem;
								}
							}
						}

						using (new EditorGUILayout.HorizontalScope(GUILayout.Width(550f))) // Resources
						{
							EditorGUIUtility.fieldWidth = 70f;
							using (new EditorGUILayout.VerticalScope())
							{
								EditorGUILayout.LabelField("Node Needs");
								DrawResources(list[listIndex].Tiers[i].NodeNeedTypes, list[listIndex].Tiers[i].NodeNeedValues);
							}
							using (new EditorGUILayout.VerticalScope())
							{
								EditorGUILayout.LabelField("Crafted Needs");
								DrawResources(list[listIndex].Tiers[i].CraftedNeedTypes, list[listIndex].Tiers[i].CraftedNeedValues);
							}
							using (new EditorGUILayout.VerticalScope())
							{
								EditorGUILayout.LabelField("Production");
								DrawResources(list[listIndex].Tiers[i].ProductionTypes, list[listIndex].Tiers[i].ProductionValues);
							}
							EditorGUIUtility.fieldWidth = 100f;
							EditorGUIUtility.labelWidth = 100f;
						} // Resources
					}
				}
			}
			if (cTier.changed)
			{
				if (list[listIndex].Tiers[i] != null)
				{
					EditorUtility.SetDirty(list[listIndex].Tiers[i]);
				}
			}
		}
	}
	private void DrawResources<T> (List<T> resourceTypes, List<float> resourceAmounts) where T : System.Enum
	{
		EditorGUIUtility.fieldWidth = 70f;
		using (new EditorGUILayout.VerticalScope(GUILayout.Width(170f)))
		{
			for (int index = 0; index < resourceAmounts.Count; index++)
			{
				using (new EditorGUILayout.HorizontalScope())
				{
					resourceTypes[index] = (T) EditorGUILayout.EnumPopup(resourceTypes[index]);
					resourceAmounts[index] = EditorGUILayout.DelayedFloatField(resourceAmounts[index], floatField_AlignRight, GUILayout.Width(55f));
					if (GUILayout.Button(new GUIContent("-", "Remove Resource"), EditorStyles.miniButton, button_MiniButton_Width))
					{
						resourceTypes.RemoveAt(index);
						resourceAmounts.RemoveAt(index);
					}
				}
			}
			using (new EditorGUILayout.HorizontalScope())
			{
				GUILayout.FlexibleSpace();
				if (GUILayout.Button(new GUIContent("+", "Add Resource"), EditorStyles.miniButton, button_MiniButton_Width))
				{
					resourceTypes.Add((T) System.Enum.GetValues(typeof(T)).GetValue(0));
					resourceAmounts.Add(0f);
				}
			}
		}
		EditorGUIUtility.fieldWidth = 100f;
	}
	#endregion
	#region Toolbars
	private void DrawTabToolbar ()
	{
		using (new EditorGUILayout.HorizontalScope(EditorStyles.toolbar))
		{
			if (GUILayout.Button(Label_Ally_Buildings, currentListIndex == 0 ? toolbarButton_Active : EditorStyles.toolbarButton))
			{
				currentListIndex = 0;
			}
			if (GUILayout.Button(Label_Ally_Units, currentListIndex == 1 ? toolbarButton_Active : EditorStyles.toolbarButton))
			{
				currentListIndex = 1;
			}
			if (GUILayout.Button(Label_Ally_Bases, currentListIndex == 2 ? toolbarButton_Active : EditorStyles.toolbarButton))
			{
				currentListIndex = 2;
			}
			if (GUILayout.Button(Label_Enemy_Units, currentListIndex == 3 ? toolbarButton_Active : EditorStyles.toolbarButton))
			{
				currentListIndex = 3;
			}

			GUILayout.FlexibleSpace();

			if (GUILayout.Button(new GUIContent("Big Textures", "On: 128px Previews\nOff: 32px Previews"), preview_LargeSprites ? toolbarButton_Active : EditorStyles.toolbarButton))
			{
				preview_LargeSprites = !preview_LargeSprites;
			}
			ShowQuickToggles = GUILayout.Toggle(ShowQuickToggles, GUIContent.none);
			var typeRect = GUILayoutUtility.GetLastRect();
			GUI.Label(typeRect, new GUIContent("", "Show Quick Toggles"));
			bool allFoldoutsOpen = listFoldoutState.TrueForAll(x => x.value == true);
			if (GUILayout.Button(GUIContent.none, allFoldoutsOpen? EditorStyles.toolbarDropDown : EditorStyles.toolbarPopup, GUILayout.Width(20f)))
			{
				FoldoutsChangeAll(!allFoldoutsOpen);
			}
			typeRect = GUILayoutUtility.GetLastRect();
			GUI.Label(typeRect, new GUIContent("", "Expand\\Collapse All"));
			if (GUILayout.Button("Save", EditorStyles.toolbarButton))
			{
				SaveAll();
			}
		}
	}

	private void SaveAll ()
	{
		UpdateItemIDs(Items.Ally_Bases);
		UpdateItemIDs(Items.Ally_Buildings);
		UpdateItemIDs(Items.Ally_Units);
		UpdateItemIDs(Items.Enemy_Units);
		List<List<SO_Item>> lists = new List<List<SO_Item>>()
				{
					Items.Ally_Bases, Items.Ally_Buildings, Items.Ally_Units, Items.Enemy_Units
				};
		for (int l = 0; l < lists.Count; l++)
		{
			for (int i = 0; i < lists[l].Count; i++)
			{
				if (lists[l][i] != null)
				{
					EditorUtility.SetDirty(lists[l][i]);
					for (int t = 0; t < lists[l][i].Tiers.Count; t++)
					{
						if (lists[l][i].Tiers[t] != null)
						{
							EditorUtility.SetDirty(lists[l][i].Tiers[t]);
							if (lists[l][i].Tiers[t].Sprites != null)
							{
								EditorUtility.SetDirty(lists[l][i].Tiers[t].Sprites);
							}
						}
					}
				}
			}
		}

		AssetDatabase.SaveAssets();
	}

	private void DrawFilterToolbar ()
	{
		using (new EditorGUILayout.HorizontalScope(EditorStyles.toolbar))
		{
			if (GUILayout.Button("No Filter", currentFilterIndex == 0 ? toolbarButton_Active : EditorStyles.toolbarButton))
			{
				currentFilterIndex = 0;
			}
			if (GUILayout.Button("Age", currentFilterIndex == 1 ? toolbarButton_Active : EditorStyles.toolbarButton))
			{
				currentFilterIndex = 1;
			}
			if (GUILayout.Button("Movable", currentFilterIndex == 2 ? toolbarButton_Active : EditorStyles.toolbarButton))
			{
				currentFilterIndex = 2;
			}
			if (GUILayout.Button("Buyable", currentFilterIndex == 3 ? toolbarButton_Active : EditorStyles.toolbarButton))
			{
				currentFilterIndex = 3;
			}
			if (GUILayout.Button("Can Attack", currentFilterIndex == 4 ? toolbarButton_Active : EditorStyles.toolbarButton))
			{
				currentFilterIndex = 4;
			}
			if (GUILayout.Button("Demolishable", currentFilterIndex == 5 ? toolbarButton_Active : EditorStyles.toolbarButton))
			{
				currentFilterIndex = 5;
			}
			if (GUILayout.Button("Name", currentFilterIndex == 6 ? toolbarButton_Active : EditorStyles.toolbarButton))
			{
				currentFilterIndex = 6;
			}
			if (GUILayout.Button("ID", currentFilterIndex == 7 ? toolbarButton_Active : EditorStyles.toolbarButton))
			{
				currentFilterIndex = 7;
			}
			GUILayout.FlexibleSpace();
			if (currentFilterIndex != 0)
			{
				using (new EditorGUILayout.HorizontalScope(EditorStyles.toolbar))
				{
					float prevWidth = EditorGUIUtility.labelWidth;
					EditorGUIUtility.labelWidth = 50f;
					if (currentFilterIndex == 1)
					{
						filter_age = (AgesSystem.Age) EditorGUILayout.EnumPopup("Show", filter_age);
					}
					else if (currentFilterIndex < 6)
					{
						filter_bool = EditorGUILayout.Toggle("Show", filter_bool);
					}
					else if (currentFilterIndex == 6)
					{
						filter_string = EditorGUILayout.TextField(GUIContent.none, filter_string, GUILayout.MinWidth(150f), GUILayout.MaxWidth(350f));
						if (GUILayout.Button("Clear", EditorStyles.toolbarButton, GUILayout.Width(50f)))
						{
							filter_string = "";
						}
					}
					else if (currentFilterIndex == 7)
					{
						filter_id = EditorGUILayout.IntField(GUIContent.none, filter_id, GUILayout.MinWidth(150f), GUILayout.MaxWidth(350f));
					}
					EditorGUIUtility.labelWidth = prevWidth;
				}
			}
			GUILayout.FlexibleSpace();
			GUILayout.Label("Showing " + filter_results.ToString() + "/" + currentList().Count);
		}
	}

	private void DrawItemsObjectField ()
	{
		using (new EditorGUILayout.HorizontalScope())
		{
			Items = EditorGUILayout.ObjectField(Label_Item, Items, typeof(SO_Global_Items), false) as SO_Global_Items;
		}
	}
	#endregion
	#region Buttons
	private void DrawAddItemButton ()
	{
		using (new EditorGUILayout.HorizontalScope())
		{
			GUILayout.FlexibleSpace();
			if (GUILayout.Button(new GUIContent("+", "Add new Item entry to the list\nRight Click: Create Item Asset"), EditorStyles.miniButton, button_MiniButton_Width))
			{
				if (Event.current.type == EventType.ContextClick)
				{
					string path = Items.GetPath(currentListIndex) + "/SO_Item_";
					while (AssetDatabase.LoadAssetAtPath(path + ".asset", typeof(ScriptableObject)) != null)
					{
						path += "_";
					}

					SO_Item newItem = ScriptableObject.CreateInstance(typeof(SO_Item)) as SO_Item;
					AssetDatabase.CreateAsset(newItem, path + ".asset");
					currentList().Add(newItem);
					UpdateItemIDs(currentList());
				}
				else
				{
					currentList().Add(null);
					UpdateItemIDs(currentList());
				}
				Repaint();
			}
			GUILayout.FlexibleSpace();
		}
	}

	private void DrawTexturePreviewButton (Rect _rect, Texture2D texture)
	{
		if (GUI.Button(_rect, texture_Transparent, GUIStyle.none))
		{
			var window = EditorWindow.GetWindow<Texture_PreviewWindow>(false, "Texture Preview", true);
			window.texture = texture;
		}
	}

	private void DrawItemTierRemoveButton (List<SO_Item> list, int listIndex, int i)
	{
		if (GUILayout.Button(new GUIContent("-", "Remove this Tier from the item's Tier list\nRight Click: Delete Tier Asset"), EditorStyles.miniButton, button_MiniButton_Width))
		{
			if (Event.current.type == EventType.ContextClick)
			{
				AssetDatabase.MoveAssetToTrash(AssetDatabase.GetAssetPath(list[listIndex].Tiers[i]));
			}
			list[listIndex].Tiers.RemoveAt(i);
			this.Repaint ();
		}
	}

	private void DrawItemTierAddButton (List<SO_Item> list, int listIndex)
	{
		if (GUILayout.Button(new GUIContent("+", "Add a new Tier to the item's Tier list\nRight Click: Create Tier Asset"), EditorStyles.miniButton, button_MiniButton_Width))
		{
			if (Event.current.type == EventType.ContextClick)
			{
				string path = Items.GetPath(currentListIndex) + "/SO_Item_Tier";
				while (AssetDatabase.LoadAssetAtPath(path + ".asset", typeof(ScriptableObject)) != null)
				{
					path += "_";
				}

				SO_Item_Tier newItem = ScriptableObject.CreateInstance(typeof(SO_Item_Tier)) as SO_Item_Tier;
				AssetDatabase.CreateAsset(newItem, path + ".asset");
				list[listIndex].Tiers.Add(newItem);
			}
			else
			{
				list[listIndex].Tiers.Add(null);
			}
		}
	}
	#endregion
	#endregion
	#endregion
	#region Helpers
	private void UpdateItemIDs (List<SO_Item> list)
	{
		for (int i = 0; i < list.Count; i++)
		{
			if (list[i] != null)
			{
				int listsIncludingThisItem = 0;
				string lists = "";
				if (Items.Ally_Bases.Contains(list[i]))
				{
					listsIncludingThisItem++;
					lists += "Ally_Bases ";
				}
				if (Items.Ally_Buildings.Contains(list[i]))
				{
					listsIncludingThisItem++;
					lists += "Ally_Buildings ";
				}
				if (Items.Ally_Units.Contains(list[i]))
				{
					listsIncludingThisItem++;
					lists += "Ally_Units ";
				}
				if (Items.Enemy_Units.Contains(list[i]))
				{
					listsIncludingThisItem++;
					lists += "Enemy_Units ";
				}
				if (listsIncludingThisItem > 1)
					Debug.LogWarning("SO_Item_TableWindow - Item exists in more than one list!\n" + lists, this);
				list[i].ItemID = i + 1;
				list[i].ItemCat = currentListIndex;
				EditorUtility.SetDirty(list[i]);
			}
		}
	}
	private GUIStyle AlternateStyle (int listIndex)
	{
		return listIndex % 2 > 0 ? GUIStyle.none : general_alternateColor;
	}

	private GUIStyle AlternateStyleTier (int tierIndex)
	{
		return tierIndex % 2 > 0 ? GUIStyle.none : tier_alternateColor;
	}

	private GUIStyle AlternateStyle_foldoutBold (int listIndex)
	{
		return EditorStyles.foldout;//listIndex % 2 > 0 ? foldoutHeader_Bold_alternateColor : foldoutHeader_Bold;
	}

	private void FoldoutsChangeAll (bool desiredState)
	{
		for (int i = 0; i < listFoldoutState.Count; i++)
		{
			listFoldoutState[i].target = desiredState;
		}
	}

	private void UpdateListFoldoutState (int desiredCount)
	{
		int currentCount = listFoldoutState.Count;
		if (currentCount < desiredCount)
		{
			int needed = desiredCount - currentCount;
			for (int i = 0; i < needed; i++)
			{
				listFoldoutState.Add(new AnimBool(false));
				listFoldoutState[currentCount + i].valueChanged.AddListener(new UnityAction(base.Repaint));
			}
		}
	}

	private List<SO_Item> currentList ()
	{
		switch (currentListIndex)
		{
			case 0:
				return Items.Ally_Buildings;
			case 1:
				return Items.Ally_Units;
			case 2:
				return Items.Ally_Bases;
			case 3:
				return Items.Enemy_Units;
			default:
				currentListIndex = 0;
				return Items.Ally_Buildings;
		}
	}
	#endregion
}