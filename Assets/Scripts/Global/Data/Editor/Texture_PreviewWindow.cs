﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;
using UnityEditor;

public class Texture_PreviewWindow : EditorWindow
{
	#region Data
	public Texture2D texture = null;
	#endregion
	#region GUI Styles
	private GUIStyle toolbarButton_Active = new GUIStyle(EditorStyles.toolbarButton);
	#endregion
	#region UI State
	private bool useTextureSize = false;
	private float currentSize = 256f;

	private Vector2 scrollPos = new Vector2();
	#endregion
	#region Lifecycle
	private void OnLostFocus ()
	{
		this.Close();
		return;
	}

	private void OnEnable ()
	{
		if (EditorApplication.isCompiling)
		{
			this.Close();
			return;
		}
		toolbarButton_Active = new GUIStyle(EditorStyles.toolbarButton);
		toolbarButton_Active.fontStyle = FontStyle.Bold;
	}
	#endregion
	#region GUI Draw
	private void OnGUI ()
	{
		using (new EditorGUILayout.HorizontalScope(EditorStyles.toolbar))
		{
			EditorGUILayout.LabelField(new GUIContent(texture.name, texture.name));
			GUILayout.FlexibleSpace();
			currentSize = EditorGUILayout.Slider(currentSize, 8f, 2048f);
			if (GUILayout.Button("1:1", useTextureSize ? toolbarButton_Active : EditorStyles.toolbarButton))
			{
				useTextureSize = !useTextureSize;
			}
		}

		using (var scrollview = new EditorGUILayout.ScrollViewScope(scrollPos))
		{
			using (new EditorGUILayout.VerticalScope(GUILayout.Height(useTextureSize ? texture.height : currentSize),
				GUILayout.Width(useTextureSize ? texture.width : currentSize)))
			{
				var _rect = EditorGUILayout.GetControlRect(false, useTextureSize? texture.height : currentSize);
				EditorGUI.DrawTextureTransparent(_rect, texture, ScaleMode.ScaleToFit);
			}
		}
	}
	#endregion
}