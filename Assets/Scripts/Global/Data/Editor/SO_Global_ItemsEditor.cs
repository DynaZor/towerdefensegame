﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.AnimatedValues;

[CustomEditor(typeof(SO_Global_Items))]
[CanEditMultipleObjects]
public class SO_Global_ItemsEditor : Editor
{
	#region UI States
	private AnimBool bool_Debug = new AnimBool(false);
	#endregion
	#region GUI Draw
	public override void OnInspectorGUI ()
	{
		serializedObject.Update();
		SO_Global_Items so = (SO_Global_Items)serializedObject.targetObject;
		if (GUILayout.Button("Ally Buildings"))
		{
			if (so.Ally_Buildings == null)
			{
				so.Ally_Buildings = new List<SO_Item>();
				serializedObject.Update();
			}
			ShowTableWindow(0);
		}

		if (GUILayout.Button("Ally Units"))
		{
			if (so.Ally_Units == null)
			{
				so.Ally_Units = new List<SO_Item>();
				serializedObject.Update();
			}
			ShowTableWindow(1);
		}

		if (GUILayout.Button("Ally Bases"))
		{
			if (so.Ally_Bases == null)
			{
				so.Ally_Bases = new List<SO_Item>();
				serializedObject.Update();
			}
			ShowTableWindow(2);
		}

		if (GUILayout.Button("Enemy Units"))
		{
			if (so.Enemy_Units == null)
			{
				so.Enemy_Units = new List<SO_Item>();
				serializedObject.Update();
			}
			ShowTableWindow(3);
		}

		bool_Debug.target = EditorGUILayout.Foldout(bool_Debug.target, "Debug");

		using (var debugFoldout = new EditorGUILayout.FadeGroupScope(bool_Debug.faded))
		{
			EditorGUI.indentLevel++;
			if (debugFoldout.visible)
			{
				base.OnInspectorGUI();
			}
		}

		if (serializedObject.hasModifiedProperties)
			serializedObject.ApplyModifiedProperties();

		if (bool_Debug.speed != 0f)
			Repaint();
	}
	#endregion
	#region Methods
	public void ShowTableWindow (int listIndex)
	{
		var window = EditorWindow.GetWindow<SO_Item_TableWindow>(false, SO_Item_TableWindow.WindowTitle, true);
		window.Items = serializedObject.targetObject as SO_Global_Items;
		window.currentListIndex = listIndex;
	}
	#endregion
}