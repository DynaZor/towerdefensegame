﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;

[SerializeField, CreateAssetMenu(fileName = "UIColors_", menuName = "Create new UI Colors asset")]
public class SO_UIColors : ScriptableObject
{
	[Header("Text")]
	public Color Game_UI_Text_Normal;
	public Color Game_UI_Text_Alert;

	[Header("Grid Node Selection Colors")]
	[Range(0,255)] public int Game_Grid_Hover_Power;
	[Range(0,255)] public int Game_Grid_Selected_Power;
	public Color Game_Grid_Selection_Normal_Free;
	public Color Game_Grid_Selection_Normal_Enemy;
	public Color Game_Grid_Selection_Normal_Ally;
	public Color Game_Grid_Selection_Normal_AllyMovable;
	public Color Game_Grid_Selection_Action_Free;
	public Color Game_Grid_Selection_Action_Blocked;
}