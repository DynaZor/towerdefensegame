﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;
using AgesSystem;
using System.Collections.Generic;

[System.Serializable, SerializeField, CreateAssetMenu(fileName = "item_", menuName = "Create new Item asset")]
public class SO_Item : ScriptableObject
{
	public int ItemCat = 0;
	public int ItemID = 0;
	public string ItemName = "Unnamed Item";
	public Age age = Age.Stone;
	public bool Buyable = false;
	public bool Movable = false;
	public bool Demolishable = true;
	public bool CanAttack = false;
	public List<SO_Item_Tier> Tiers = new List<SO_Item_Tier>();
}