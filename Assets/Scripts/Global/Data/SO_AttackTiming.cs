﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;

[System.Serializable, SerializeField, CreateAssetMenu(fileName = "AttackTiming_", menuName = "Create new Attack Timing asset")]
public class SO_AttackTiming : ScriptableObject
{
	public bool[] Attack = new bool[12];
	public float[] Modifier = new float[12];
}