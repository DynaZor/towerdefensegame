﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;
using AgesSystem;

[SerializeField, CreateAssetMenu(fileName = "enemy_", menuName = "Create new Enemy asset")]
public class SO_Enemy : ScriptableObject
{
	public string EnemyName;
	public Age age;
	public float Health = 100;
	public float Defence = 0;
	public float AttackRange = 0;
	public float AttackPower = 0;
	public SO_AttackTiming attackTiming = null;
	public Texture2D[] Sprites;
	public int RewardAge = 10;
}