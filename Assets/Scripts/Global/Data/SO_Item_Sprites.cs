﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;

[CreateAssetMenu(fileName = "item__Tier_Sprites", menuName = "Create new Item Sprites asset")]
public class SO_Item_Sprites : ScriptableObject
{
	public Texture2D Active;
	public Texture2D InActive;
	public Texture2D Producing;
}