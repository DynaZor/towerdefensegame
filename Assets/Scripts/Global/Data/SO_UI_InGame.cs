﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;

[SerializeField, CreateAssetMenu(fileName = "UI_InGame_", menuName = "Create new UI_InGame asset")]
public class SO_UI_InGame : ScriptableObject
{
	public Sprite Button;
	public Sprite SmallButton;
	public Sprite Panel_Top;
	public Sprite Panel_Info;
	public Sprite Panel_Menu;
	public Sprite Demolish;

	public enum UIElement
	{
		Button, SmallButton, Panel_Top, Panel_Info, Panel_Menu, Demolish
	}
}