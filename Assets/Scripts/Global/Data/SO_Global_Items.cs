﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using System.Collections.Generic;
using UnityEngine;

[System.Serializable, SerializeField, CreateAssetMenu(fileName = "Global_Items_", menuName = "Create new Global_Items asset")]
public class SO_Global_Items : ScriptableObject
{
	public enum ItemType
	{
		Ally_Buildings, Ally_Units, Ally_Bases, Enemy_Units
	}

	public string AssetPath_Ally_Buildings = "Assets/Data/Items/Ally_Buildings";
	public string AssetPath_Ally_Units     = "Assets/Data/Items/Ally_Units";
	public string AssetPath_Ally_Bases     = "Assets/Data/Items/Ally_Bases";
	public string AssetPath_Enemy_Units    = "Assets/Data/Items/Enemy_Units";

	public List<SO_Item> Ally_Buildings = new List<SO_Item>();
	public List<SO_Item> Ally_Units		= new List<SO_Item>();
	public List<SO_Item> Ally_Bases		= new List<SO_Item>();
	public List<SO_Item> Enemy_Units	= new List<SO_Item>();

	public SO_Item GetItemSO (int ItemID)
	{
		string id = ItemID.ToString();
		int listIndex = ItemID % 10;
		int itemIndex = int.Parse(id.Remove(id.Length - 1));
		try
		{
			switch ((ItemType) listIndex)
			{
				case ItemType.Ally_Bases:
					return Ally_Bases[itemIndex];
				case ItemType.Ally_Buildings:
					return Ally_Buildings[itemIndex];
				case ItemType.Ally_Units:
					return Ally_Units[itemIndex];
				case ItemType.Enemy_Units:
					return Enemy_Units[itemIndex];
				default:
					return null;
			}
		}
		catch
		{
			Debug.LogWarning("SO_Global_Items - GetItemSO - Failed to get item through ItemID.\nItemID: " + ItemID + 
				"\nList: " + listIndex + " (" + ((ItemType) listIndex).ToString() + ")\nItem: " + itemIndex, this);
			return null;
		}
	}
	public SO_Item GetItemSO (ItemType type, int index)
	{
		try
		{
			switch (type)
			{
				case ItemType.Ally_Bases:
					return Ally_Bases[index];
				case ItemType.Ally_Buildings:
					return Ally_Buildings[index];
				case ItemType.Ally_Units:
					return Ally_Units[index];
				case ItemType.Enemy_Units:
					return Enemy_Units[index];
				default:
					return null;
			}
		}
		catch
		{
			Debug.LogWarning("SO_Global_Items - GetItemSO - Failed to return item\nMaybe index was out of range.\nRequested item:\n" + type.ToString() + " at " + index.ToString(), this);
			return null;
		}
	}

	public void SetData (SO_Global_Items Items)
	{
		AssetPath_Ally_Buildings = Items.AssetPath_Ally_Buildings;
		AssetPath_Ally_Units = Items.AssetPath_Ally_Units;
		AssetPath_Ally_Bases = Items.AssetPath_Ally_Bases; 
		AssetPath_Enemy_Units = Items.AssetPath_Enemy_Units;

		Ally_Buildings = Items.Ally_Buildings;
		Ally_Units = Items.Ally_Units;
		Ally_Bases = Items.Ally_Bases;
		Enemy_Units = Items.Enemy_Units;
	}

	public string GetPath (int currentListIndex)
	{
		switch (currentListIndex)
		{
			case 0:
				return AssetPath_Ally_Buildings;
			case 1:
				return AssetPath_Ally_Units;
			case 2:
				return AssetPath_Ally_Bases;
			case 3:
				return AssetPath_Enemy_Units;
			default:
				return AssetPath_Ally_Buildings;
		}
	}

	public int[] GetAllyMaxBuildingsSizeArray ()
	{
		List<int> tempList = new List<int>();
		int numAges = System.Enum.GetNames(typeof(AgesSystem.Age)).Length;
		for (int i = 0; i < numAges; i++)
		{
			//!! 1Alpha ItemsDB: Separate Buildings, Walls, Units, Towers
			//!! 1Alhpa ItemsDB: After separation, update MaxBuildingsCount System
			tempList.Add(Ally_Buildings.FindAll(x => x.age == (AgesSystem.Age) i).Count);
		}
		return tempList.ToArray();
	}
}