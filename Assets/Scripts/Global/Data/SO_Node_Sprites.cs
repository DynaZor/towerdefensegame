﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;

[SerializeField, CreateAssetMenu(fileName = "NodeSprites_", menuName = "Create new Node Sprites asset")]
public class SO_Node_Sprites : ScriptableObject
{
	public Texture2D GroundDry;
	public Texture2D GroundWater;
	public Texture2D GroundFertility;
	public Texture2D Vegetation;
	public Texture2D Pollution;
}