﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
// For Reference: https://en.wikipedia.org/wiki/Names_of_large_numbers
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Global_NumberHandling
{
	public static string GetNumberString (this float var, uint maxDigits = 4)
	{
		string result = ((uint) Mathf.Abs(var)).GetNumberString(maxDigits);
		if (var < 0)
		{
			result.Insert(0, "-");
		}
		return result;
	}
	public static string GetNumberString (this int var, uint maxDigits = 4)
	{
		string result = ((uint) Mathf.Abs(var)).GetNumberString(maxDigits);
		if (var < 0)
		{
			result.Insert(0, "-");
		}
		return result;
	}

	public static string GetNumberString (this uint var, uint maxDigits = 4)
	{
		string numberString = var.ToString();
		int lengthOfNumber = numberString.Length;
		string result;
		bool needSuffix = lengthOfNumber > maxDigits;
		if (!needSuffix)
		{
			result = numberString;
		}
		else // if (needPrefix)
		{
			result = numberString.Substring(0, (int) maxDigits);
		}

		if (lengthOfNumber > 3)
		{
			int separatorIndex = (lengthOfNumber - 1) % 3 + 1;
			if (separatorIndex > 0)
			{
				result = result.Insert(separatorIndex, needSuffix? "." : ",");
			}
			else if (needSuffix)
			{
				result = result.Substring(0, (int)maxDigits - 1).Insert(0, "0.");
			}

			if (needSuffix)
			{
				string unitChar;
				if (lengthOfNumber < 7)
				{
					unitChar = "K";
				}
				else if (lengthOfNumber >= 7 && lengthOfNumber < 10)
				{
					unitChar = "M";
				}
				else if (lengthOfNumber >= 10 && lengthOfNumber < 13)
				{
					unitChar = "B";
				}
				else if (lengthOfNumber >= 13 && lengthOfNumber < 16)
				{
					unitChar = "T";
				}
				else if (lengthOfNumber >= 16 && lengthOfNumber < 19)
				{
					unitChar = "q";
				}
				else if (lengthOfNumber >= 19 && lengthOfNumber < 22)
				{
					unitChar = "Q";
				}
				else if (lengthOfNumber >= 22 && lengthOfNumber < 25)
				{
					unitChar = "s";
				}
				else if (lengthOfNumber >= 25 && lengthOfNumber < 28)
				{
					unitChar = "S";
				}
				else if (lengthOfNumber >= 28 && lengthOfNumber < 31)
				{
					unitChar = "o";
				}
				else if (lengthOfNumber >= 31 && lengthOfNumber < 34)
				{
					unitChar = "n";
				}
				else
				{
					unitChar = "..";
				}

				result += unitChar;
			}
		}
		
		return result;
	}
}