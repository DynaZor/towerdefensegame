﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;

public class Global_DeactivateOnAwake : MonoBehaviour
{
	private void Awake ()
	{
		gameObject.SetActive(false);
		Destroy(this);
	}
}