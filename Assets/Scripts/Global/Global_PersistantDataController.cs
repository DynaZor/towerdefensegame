﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!

using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Global_PersistantDataController : MonoBehaviour
{
    void Start()
    {
        if (SceneManager.sceneCount == 1)
        {
            var op = SceneManager.LoadSceneAsync(Global_SceneNumbers.MainMenu, LoadSceneMode.Additive);
            StartCoroutine(SetMainMenuSceneActive(op));
        }
    }

    private IEnumerator SetMainMenuSceneActive (AsyncOperation operation)
    {
        while (!operation.isDone)
        {
            yield return null;
        }
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(Global_SceneNumbers.MainMenu));
    }
}