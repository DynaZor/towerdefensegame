﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
namespace AgesSystem
{
	public enum Age
	{
		Stone,
		Glory,
		Malignant,
		Mended
	}
}