﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
namespace ResourcesSystem
{
	public enum CraftedResourceType
	{
		Food,
		Tech,
		Age,
		Electricity
	}

	public enum NodeResourceType
	{
		GroundFertility,
		Vegetation,
		GroundWater,
		Pollution
	}

	public static class ResourceTypeExtensions
	{
		public static bool IsRenewable (this NodeResourceType nodeResourceType)
		{
			if (Gameplay_Values.Instance.RenewableResources.Contains(nodeResourceType))
				return true;
			return false;
		}
	}
}