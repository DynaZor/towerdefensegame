﻿namespace ItemStatuses
{
	public enum ItemStatus
	{
		InActive, Moving, 
		WaitingToMove,
		Attacking, TookDamage, Producing, 
		LowHealth50,
		LowHealth25, LowHealth10,
		Healing,
		NotEnoughGNDRes,
		NotEnoughCraftedRes,
		Upgraded 
	}
}