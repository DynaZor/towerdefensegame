﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!

public interface IItem_Actions
{
	void TakeDamage (float damageAmount);
}