﻿public enum ItemType
{
	Base,
	Factory,
	Tower,
	Wall,
	Unit,
	EnemyOnly
}