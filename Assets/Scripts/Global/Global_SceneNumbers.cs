﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!

using UnityEngine;

public class Global_SceneNumbers : MonoBehaviour
{
	#region Singleton
	public static Global_SceneNumbers Instance;
	private void Awake ()
	{
		MakeSingleton();
	}

	private void MakeSingleton ()
	{
		if (Instance != null)
			Debug.Log("Global_SceneNumbers - More than one instance!\n" +
				"Registered Instance: " + Instance.gameObject.name +
				"This Instance: " + gameObject.name, this);
		else
			Instance = this;
	}
	#endregion

	[Header("Scene Numbers")]
	[SerializeField] private int persistant = 0;
	[SerializeField] private int mainMenu = 1;
	[SerializeField] private int loader = 2;
	[SerializeField] private int inGame = 3;
	public int _Persistant => persistant;
	public int _MainMenu => mainMenu;
	public int _Loader => loader;
	public int _InGame => inGame;

	public static int Persistant => Instance._Persistant;
	public static int MainMenu => Instance._MainMenu;
	public static int Loader => Instance._Loader;
	public static int InGame => Instance._InGame;
}