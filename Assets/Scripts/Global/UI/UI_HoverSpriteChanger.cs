﻿// Code written by Shahar DynaZor Alon Use of this code outside of this project
// without Shahar's permission is prohibited!
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UI_HoverSpriteChanger : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	[SerializeField] private Image thisImage = null;
	[SerializeField] private Sprite Sprite_HoverOn = null;
	[SerializeField] private Sprite Sprite_HoverOff = null;
	public void Hover_On ()
	{
		thisImage.sprite = Sprite_HoverOn;
	}
	public void Hover_Off ()
	{
		thisImage.sprite = Sprite_HoverOff;
	}

	public void OnPointerEnter (PointerEventData eventData)
	{
		Hover_On();
	}

	public void OnPointerExit (PointerEventData eventData)
	{
		Hover_Off();
	}
}