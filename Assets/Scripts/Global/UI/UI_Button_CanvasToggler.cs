﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class UI_Button_CanvasToggler : MonoBehaviour
{
	[Header("References")]
	[SerializeField, CurrentMenusController] public MenusController currentMenusController = null;
	[SerializeField, MenusControllerCanvases] private Canvas controlledCanvas = null;
#if UNITY_EDITOR
	public bool DoReferenceScriptToButton = false;
#endif
	public bool MenusControllerIsSet => currentMenusController != null;
	[Header("Settings")]
	[SerializeField] public bool Toggler = false;
	[SerializeField, Conditional("Toggler", false)] private bool targetState = true;

#if UNITY_EDITOR
	private void OnValidate ()
	{
		if (DoReferenceScriptToButton)
		{
			DoReferenceScriptToButton = false;
			ReferenceScriptToButton();
		}
	}

	private void ReferenceScriptToButton ()
	{
		Button thisButton = GetComponent<Button>();
		if (thisButton != null)
		{
			if (thisButton.onClick.GetPersistentEventCount() > 0)
			{
				for (int i = 0; i < thisButton.onClick.GetPersistentEventCount(); i++)
				{
					if (thisButton.onClick.GetPersistentTarget(i) == this)
					{
						if (thisButton.onClick.GetPersistentMethodName(i) == "Button_ClickEvent")
						{
							return;
						}
					}
				}
			}
			// Else add
			UnityEditor.Events.UnityEventTools.AddPersistentListener(thisButton.onClick, new UnityAction(Button_ClickEvent));
			UnityEditor.EditorUtility.SetDirty(thisButton);
		}
	}
#endif

	public void Button_ClickEvent ()
	{
		if (Toggler)
		{
			controlledCanvas.enabled = !controlledCanvas.enabled;
		}
		else
		{
			controlledCanvas.enabled = targetState;
		}
	}
}