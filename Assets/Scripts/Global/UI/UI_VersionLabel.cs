﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;
using TMPro;
using DynaZor.EditorUtilities.EUScriptableObjects;

public class UI_VersionLabel : MonoBehaviour
{
	[Header("References")]
	[SerializeField] private TextMeshProUGUI VersionLabel = null;
	
	[SerializeField] private AppSettings AppSettings = null;

	private void Start()
    {
		VersionLabel.text = "v" + AppSettings.GenerateVersionString(AppSettings, false) + " " + AppSettings.currentState.ToString();
	}
}