﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!

using UnityEngine;
using System.Collections.Generic;
using System.Collections.ObjectModel;

public abstract class MenusController : MonoBehaviour
{

	[Header("Canvases - Disabled On Load")]
	[SerializeField] private Canvas[] canvases = System.Array.Empty<Canvas>();
	public IReadOnlyList<Canvas> Canvases => new ReadOnlyCollection<Canvas>(canvases);

	private void Start () // After everything loads
	{
		Start_DisableCanvases();
		StartAdditional();
	}

	public abstract void StartAdditional ();

	public void ToggleCanvas(int canvasIndex)
	{
		if (canvases.Length > canvasIndex)
		{
			canvases[canvasIndex].enabled = !canvases[canvasIndex].enabled;
		}
	}

	public void SetCanvas(int canvasIndex, bool state)
	{
		if (canvases.Length > canvasIndex)
		{
			canvases[canvasIndex].enabled = state;
		}
	}

	private void Start_DisableCanvases ()
	{
		for (int i = 0; i < canvases.Length; i++)
		{
			canvases[i].enabled = false;
		}
	}
}