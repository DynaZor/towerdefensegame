﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using System.Collections;
using System.Collections.Generic;
using DynaZor.EditorUtilities;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
[ExecuteInEditMode()]
public class UI_Effect_Image_DropShadow : MonoBehaviour
{
	[Header("Image DropShadow Settings")]
	[SerializeField] private Image mainImage = null;
	[SerializeField] private Color shadowColorAndAlpha = Color.black;
	[SerializeField] private float shadowLength = 2f;
	[Space()]
	[SerializeField] private bool applyNow = false;
	[SerializeField] private bool autoUpdate = false;
	private bool debugAll = false;

	private Image shadowImage;

	private Rect? lastMainImageRect = null;
	private bool needToApply = false;

	private void OnValidate ()
	{
		if (applyNow)
		{
			applyNow = false;
			needToApply = true;
		}
	}

	private void Update ()
	{
		if (needToApply)
		{
			needToApply = false;

			ApplyShadowSettings();
		}
	}

	private void ApplyShadowSettings ()
	{
		if (shadowImage == null)
		{
			shadowImage = GetComponent<Image>();
		}

		if (shadowImage != null)
		{
			if (mainImage != null)
			{
				if (this.name != mainImage.name + "_Shadow")
				{
					this.name = mainImage.name + "_Shadow";
				}
				lastMainImageRect = new Rect(mainImage.rectTransform.rect);
				shadowImage.color = shadowColorAndAlpha;
				shadowImage.sprite = mainImage.sprite;
				shadowImage.fillCenter = false;
				shadowImage.pixelsPerUnitMultiplier = mainImage.pixelsPerUnitMultiplier;
				shadowImage.preserveAspect = mainImage.preserveAspect;
				shadowImage.type = mainImage.type;
				shadowImage.rectTransform.MatchRectTransform(mainImage.rectTransform);
				shadowImage.rectTransform.pivot = new Vector2(0.5f, 1f);
				shadowImage.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, shadowImage.rectTransform.rect.height + shadowLength);
				if (debugAll)
					Debug.Log("Image_DropShadow: Updated", this);
#if UNITY_EDITOR
				if (!UnityEditor.EditorApplication.isPlaying)
				{
					UnityEditor.EditorUtility.SetDirty(this);
				}
#endif	
			}
			else
			{
				Debug.Log("Image_DropShadow: No mainImage selected (=null)", this);
			}
		}
	}

	private void OnGUI ()
	{
		if (autoUpdate && shadowImage != null && mainImage != null)
		{
			bool update = !lastMainImageRect.Equals(mainImage.rectTransform.rect)
				|| shadowImage.sprite != mainImage.sprite
				|| shadowImage.color != shadowColorAndAlpha
				|| shadowImage.pixelsPerUnitMultiplier != mainImage.pixelsPerUnitMultiplier
				|| shadowImage.preserveAspect != mainImage.preserveAspect
				|| shadowImage.type != mainImage.type;
			if (update)
				needToApply = true;
		}
	}
}