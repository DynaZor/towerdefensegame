﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!

using UnityEngine;

public class ConditionalAttribute : PropertyAttribute
{
	public string BoolName;
	public bool EnabledWhen;
	public bool PreventDraw;
	/// <summary>
	/// If the referenced Public Bool (by name) is true, the element will be enabled.
	/// </summary>
	/// <param name="conditionBoolName">Name of the condition's Public Bool.</param>
	public ConditionalAttribute (string conditionBoolName, bool enabledWhen, bool preventDraw = false)
	{
		BoolName = conditionBoolName;
		EnabledWhen = enabledWhen;
		PreventDraw = preventDraw;
	}
}

public class CurrentMenusControllerAttribute : PropertyAttribute
{
	public CurrentMenusControllerAttribute ()
	{
	}
}
public class MenusControllerCanvasesAttribute : PropertyAttribute
{
	public MenusControllerCanvasesAttribute ()
	{
	}
}