﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class UI_MainMenu_Manager : MenusController
{
	[Header("References")]
	[SerializeField] private Camera MainMenuCamera = null;
	[SerializeField] private TMP_Dropdown difficulty_DropDown = null;

	// Private Data
	private Coroutine currentProcess;

	#region Public Actions
	public override void StartAdditional () // Additional Start functions
	{
		StartCoroutine(Setup_DifficultiesDropDown());
		SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(Global_SceneNumbers.MainMenu));
	}

	public void Button_NewGame ()
	{
		if (currentProcess == null)
			currentProcess = StartCoroutine(_NewGame());
	}

	public void Button_Quit ()
	{
		Debug.Log("Quitting Game (Called from MainMenu UI_Manager)");
		Global_ApplicationActions.Quit();
	}
	#endregion

	#region Private Actions
	private void _Setup_DifficultiesDropDown ()
	{
		List<DifficultySetting> diff = Difficulty_Manager.Instance.Difficulties.Difficulties;
		Debug.Log(diff.Count + " Difficulties");
		List<TMP_Dropdown.OptionData> options = new List<TMP_Dropdown.OptionData>();
		for (int i = 0; i < diff.Count; i++)
		{
			options.Add(new TMP_Dropdown.OptionData(diff[i].Name));
		}
		difficulty_DropDown.options = options;
	}

	private IEnumerator Setup_DifficultiesDropDown ()
	{
		while (Difficulty_Manager.Instance == null)
			yield return null;
		_Setup_DifficultiesDropDown();
	}

	private IEnumerator _NewGame ()
	{
		var operation = SceneManager.LoadSceneAsync(Global_SceneNumbers.Loader, LoadSceneMode.Additive);
		while (!operation.isDone)
			yield return null;
		MainMenuCamera.enabled = false;
		while (GameLoader.Instance == null)
			yield return null;
		Difficulty_Manager.Instance.SetDifficulty(difficulty_DropDown.value);
		GameLoader.Instance.NewGame(Difficulty_Manager.CurrentDifficulty);
	}
	#endregion
}