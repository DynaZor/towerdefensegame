﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Game_Speed : MonoBehaviour
{
	#region Singleton
	public static Game_Speed Instance;
	private void MakeSingleton ()
	{
		if (Instance != null)
			Debug.Log("Game_Speed - More than one instance!\n" +
				"Registered Instance: " + Instance.gameObject.name +
				"This Instance: " + gameObject.name, this);
		else
			Instance = this;
	}
	#endregion

	// Types
	public enum Speed
	{
		Pause, Normal, Fast, SuperFast
	}

	// Data
	private Speed _currentSpeed = Speed.Pause;
	public Speed CurrentSpeed
	{
		get
		{
			return _currentSpeed;
		}
		private set
		{
			_currentSpeed = value;
			ApplySpeed();
		}
	}
	private Speed lastSpeed = Speed.Pause;

	// Communication
	public delegate void TimeSpeedUpdatedHandler (Speed newSpeed);
	public event TimeSpeedUpdatedHandler TimeSpeedUpdatedEvent = delegate { };

	// References
	private Gameplay_Values Gameplay_Values;

	#region Setup
	private void Awake ()
	{
		MakeSingleton();
		StartCoroutine(Setup());
	}

	private IEnumerator Setup ()
	{
		while (Gameplay_Values.Instance == null)
			yield return null;
		Gameplay_Values = Gameplay_Values.Instance;
	}
	#endregion

	#region Public Actions
	public void SetSpeed (Speed wantedSpeed)
	{
		lastSpeed = CurrentSpeed;
		CurrentSpeed = wantedSpeed;
	}

	public void Game_Pause()
	{
		lastSpeed = CurrentSpeed;
		CurrentSpeed = Speed.Pause;
	}

	public void Game_UnPause ()
	{
		CurrentSpeed = lastSpeed;
	}
	#endregion

	#region Private Actions
	private void ApplySpeed ()
	{
		switch (_currentSpeed)
		{
			case Speed.Pause:
				Time.timeScale = 0f;
				break;

			case Speed.Normal:
			default:
				Time.timeScale = Gameplay_Values.TimeSpeedNormal;
				break;

			case Speed.Fast:
				Time.timeScale = Gameplay_Values.TimeSpeedFast;
				break;

			case Speed.SuperFast:
				Time.timeScale = Gameplay_Values.TimeSpeedSuperFast;
				break;
		}
		TimeSpeedUpdatedEvent.Invoke(_currentSpeed);
	}
	#endregion
}