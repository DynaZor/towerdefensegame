﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_Clock : MonoBehaviour
{
	#region Singleton
	public static Game_Clock Instance;
	private void MakeSingleton ()
	{
		if (Instance != null)
			Debug.Log("Game_Clock - More than one instance!\n" +
				"Registered Instance: " + Instance.gameObject.name +
				"This Instance: " + gameObject.name, this);
		else
			Instance = this;
	}
	#endregion

	// Communication

	public delegate void UIEventHandler ();
	public event UIEventHandler UIUpdateCallEvent = delegate { };

	public delegate void TickEventHandler (int currentTick);

	public event TickEventHandler Tick_Group1 = delegate { };
	public readonly int NumTicks_Group1 = 4;

	public event TickEventHandler Tick_Group2 = delegate { };
	public readonly int NumTicks_Group2 = 2;

	public event TickEventHandler Tick_Group3 = delegate { };
	public readonly int NumTicks_Group3 = 2;

	public event TickEventHandler Tick_Group4 = delegate { };
	public readonly int NumTicks_Group4 = 4;

	public event TickEventHandler Tick_Group5 = delegate { };
	public readonly int NumTicks_Group5 = 4;

	public event TickEventHandler Tick_Group6 = delegate { };
	public readonly int NumTicks_Group6 = 4;

	private Dictionary<int, int>[] assignedTicks = new Dictionary<int, int>[6];

	// Private Data
	private Coroutine ClockCoroutine;

	// Public Data
	public uint CurrentTime = 0;
	public int CurrentTickGroup = 0;
	public int CurrentTick = 0;
	public int CurrentTickOfGroup = 0;

	#region Setup
	private void Awake ()
	{
		MakeSingleton();
		for (int i = 0; i < assignedTicks.Length; i++)
		{
			assignedTicks[i] = new Dictionary<int, int>();
		}
		for (int i = 1; i <= 4; i++)
		{
			if (NumTicks_Group1 >= i)
			{
				assignedTicks[0].Add(i, 0);
			}
			if (NumTicks_Group2 >= i)
			{
				assignedTicks[1].Add(i, 0);
			}
			if (NumTicks_Group3 >= i)
			{
				assignedTicks[2].Add(i, 0);
			}
			if (NumTicks_Group4 >= i)
			{
				assignedTicks[3].Add(i, 0);
			}
			if (NumTicks_Group5 >= i)
			{
				assignedTicks[4].Add(i, 0);
			}
			if (NumTicks_Group6 >= i)
			{
				assignedTicks[5].Add(i, 0);
			}
		}
		CurrentTime = 0; //!! 1Alpha On Game Load: needs to be updated
	}


	#endregion

	public void StartClock ()
	{
		if (ClockCoroutine != null)
			StopClock();
		ClockCoroutine = StartCoroutine(Clock());
	}

	public void StopClock ()
	{
		StopCoroutine(ClockCoroutine);
		ClockCoroutine = null;
	}

	#region Loop
	private IEnumerator Clock ()
	{
		int ticks = 
			NumTicks_Group1 + NumTicks_Group2 + NumTicks_Group3 + NumTicks_Group4 + NumTicks_Group5 + NumTicks_Group6;
		float cycle = Gameplay_Values.Instance.TimeCycleLength;
		float tickLength = cycle / ticks;
		int currentTick = 1;
		while (true)
		{
			yield return new WaitForSeconds(tickLength);

			UnityEngine.Profiling.Profiler.BeginSample("Game_Clock: Invoke Tick " + currentTick);

			InvokeRelevantTickGroup(currentTick);

			if (currentTick % 2 == 0) // each second tick
				UIUpdateCallEvent.Invoke();

			UnityEngine.Profiling.Profiler.EndSample();
			currentTick++;
			if (currentTick == ticks + 1)
			{
				currentTick = 1;
				CurrentTime++;
			}
			CurrentTick = currentTick;
		}
	}
	#endregion

	#region Public Actions
	public int GetTickAssignment (int GroupNumber)
	{
		GroupNumber--;
		int possibleTicks = assignedTicks[GroupNumber].Keys.Count;
		int resultTickAssigned = -1;
		int resultTick = -1;
		for (int i = 1; i <= possibleTicks; i++)
		{
			if (resultTickAssigned == -1 || assignedTicks[GroupNumber][i] <= resultTickAssigned)
			{
				resultTick = i;
				resultTickAssigned = assignedTicks[GroupNumber][i];
			}
		}
		assignedTicks[GroupNumber][resultTick] = assignedTicks[GroupNumber][resultTick] + 1;
		return resultTick;
	}
	#endregion

	#region Private Actions
	private void InvokeRelevantTickGroup (int currentTick)
	{
		int countedTicks = NumTicks_Group1;
		if (currentTick <= countedTicks)
		{
			UnityEngine.Profiling.Profiler.BeginSample("Game_Clock: Invoke Group 1: Tick " + currentTick, this);
			CurrentTickGroup = 1;
			CurrentTickOfGroup = currentTick;
			Tick_Group1.Invoke(currentTick);
			UnityEngine.Profiling.Profiler.EndSample();
			return; 
		}

		countedTicks += NumTicks_Group2;
		if (currentTick <= countedTicks)
		{
			UnityEngine.Profiling.Profiler.BeginSample("Game_Clock: Invoke Group 2: Tick " + currentTick, this);
			CurrentTickGroup = 2;
			CurrentTickOfGroup = currentTick - (countedTicks - NumTicks_Group2);
			Tick_Group2.Invoke(CurrentTickOfGroup);
			UnityEngine.Profiling.Profiler.EndSample();
			return;
		}

		countedTicks += NumTicks_Group3;
		if (currentTick <= countedTicks)
		{
			UnityEngine.Profiling.Profiler.BeginSample("Game_Clock: Invoke Group 3: Tick " + currentTick, this);
			CurrentTickGroup = 3;
			CurrentTickOfGroup = currentTick - (countedTicks - NumTicks_Group3);
			Tick_Group3.Invoke(CurrentTickOfGroup);
			UnityEngine.Profiling.Profiler.EndSample();
			return;
		}

		countedTicks += NumTicks_Group4;
		if (currentTick <= countedTicks)
		{
			UnityEngine.Profiling.Profiler.BeginSample("Game_Clock: Invoke Group 4: Tick " + currentTick, this);
			CurrentTickGroup = 4;
			CurrentTickOfGroup = currentTick - (countedTicks - NumTicks_Group4);
			Tick_Group4.Invoke(CurrentTickOfGroup);
			UnityEngine.Profiling.Profiler.EndSample();
			return;
		}

		countedTicks += NumTicks_Group5;
		if (currentTick <= countedTicks)
		{
			UnityEngine.Profiling.Profiler.BeginSample("Game_Clock: Invoke Group 5: Tick " + currentTick, this);
			CurrentTickGroup = 5;
			CurrentTickOfGroup = currentTick - (countedTicks - NumTicks_Group5);
			Tick_Group5.Invoke(CurrentTickOfGroup);
			UnityEngine.Profiling.Profiler.EndSample();
			return;
		}

		countedTicks += NumTicks_Group6;
		if (currentTick <= countedTicks)
		{
			UnityEngine.Profiling.Profiler.BeginSample("Game_Clock: Invoke Group 6: Tick " + currentTick, this);
			CurrentTickGroup = 6;
			CurrentTickOfGroup = currentTick - (countedTicks - NumTicks_Group6);
			Tick_Group6.Invoke(CurrentTickOfGroup);
			UnityEngine.Profiling.Profiler.EndSample();
			return;
		}
	}
	#endregion
}