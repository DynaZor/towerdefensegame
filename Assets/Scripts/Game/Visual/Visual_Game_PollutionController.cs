﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using System.Collections.Generic;
using UnityEngine;

public class Visual_Game_PollutionController : GlobalTextureController
{
	#region Private Actions
	public override Texture2D GenerateMap ()
	{
		int maxPollution = (int)Gameplay_Values.Instance.NodeResourceRecoveryMaxPollution;
		Texture2D texture = new Texture2D(GridSizeX, GridSizeY)
		{
			anisoLevel = 0,
			filterMode = FilterMode.Point,
			wrapMode = TextureWrapMode.Clamp
		};

		for (int Y = 0; Y < GridSizeY; Y++)
		{
			for (int X = 0; X < GridSizeX; X++)
			{
				float pollution = Game_Grid.Nodes[X,Y].Stats.NodeResources[ResourcesSystem.NodeResourceType.Pollution] / maxPollution;
				pollution = Mathf.Clamp01(pollution);
				texture.SetPixel(X, Y, new Color (pollution, 0, 0));
			}
		}
		texture.Apply();
		return texture;
	}
	#endregion
}