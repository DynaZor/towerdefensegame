﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using System.Collections;
using UnityEngine;

public abstract class GlobalTextureController : MonoBehaviour
{
	public Texture2D currentTexture = null;
#if UNITY_EDITOR
	[Header("Test")]
	[SerializeField] bool Editor_Update = false;
#endif

	[Header("References")]
	public MeshRenderer meshRenderer = null;

	// Private Data
	public MaterialPropertyBlock materialPropertyBlock = null;
	public Game_Grid Game_Grid;
	public bool Ready = false;
	public int GridSizeX => Game_Grid.GridSizeX;
	public int GridSizeY => Game_Grid.GridSizeY;


	// Timing
	private int Tick_Group = 3;
	private int Tick_Update = 0;

	#region Setup
	private void Start ()
	{
		Ready = false;
		StartCoroutine(Setup());
	}

	private IEnumerator Setup ()
	{
		while (Game_Grid.Instance == null)
			yield return null;
		Game_Grid = Game_Grid.Instance;

		while (Game_Clock.Instance == null)
			yield return null;
		Tick_Update = Game_Clock.Instance.GetTickAssignment(Tick_Group);
		Game_Clock.Instance.Tick_Group3 += TickUpdate;

		while (Game_Grid.Ready == false)
			yield return null;

		Ready = true;
		IssueUpdate();
	}
	#endregion
	#region Loops
	private void TickUpdate (int currentTick)
	{
		if (currentTick == Tick_Update)
		{
			IssueUpdate();
		}
	}

	public void IssueUpdate (bool skipChecks = false)
	{
		if (skipChecks || (Ready && Game_Grid.Ready))
		{
			UnityEngine.Profiling.Profiler.BeginSample("Update Map - " + this.gameObject.name);
			ApplyNewMap(GenerateMap());
			UnityEngine.Profiling.Profiler.EndSample();
		}
	}
	#endregion
	#region Private Actions
	public abstract Texture2D GenerateMap ();
	public void ApplyNewMap (Texture2D map)
	{
		CheckAndGenerateMissingData();
		materialPropertyBlock.SetTexture("_DataTex", map);
		materialPropertyBlock.SetInt("_GridSizeX", GridSizeX);
		materialPropertyBlock.SetInt("_GridSizeY", GridSizeY);
		meshRenderer.SetPropertyBlock(materialPropertyBlock);
		meshRenderer.transform.localScale = new Vector3(GridSizeX, GridSizeY, 1f);

#if UNITY_EDITOR
		currentTexture = map;
#endif
	}
	public void CheckAndGenerateMissingData ()
	{
		if (materialPropertyBlock == null)
			materialPropertyBlock = new MaterialPropertyBlock();
	}
	#endregion
	#region Editor Only
#if UNITY_EDITOR
	private void OnValidate ()
	{
		if (!UnityEditor.EditorApplication.isPlaying)
		{
			if (Editor_Update && currentTexture != null)
			{
				Editor_Update = false;
				IssueUpdate(true);
			}
		}
	}
#endif
	#endregion
}