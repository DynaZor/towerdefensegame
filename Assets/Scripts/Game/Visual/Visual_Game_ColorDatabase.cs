﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;

public class Visual_Game_ColorDatabase : MonoBehaviour
{
	#region Singleton
	public static Visual_Game_ColorDatabase Instance;
	private void Awake ()
	{
		if (Instance != null)
			Debug.Log("Visual_Game_ColorDatabase - More than one instance!\n" +
				"Registered Instance: " + Instance.gameObject.name +
				"This Instance: " + gameObject.name, this);
		else
			Instance = this;
	}
	#endregion

	public SO_UIColors UI_Colors;
}