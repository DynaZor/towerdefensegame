﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

public class Visual_Game_NodeController : GlobalTextureController
{
	#region Singleton
	public static Visual_Game_NodeController Instance;
	public void MakeSingleton ()
	{
		if (Instance != null)
		{
			if (Instance != this)
			{
				Debug.Log("Visual_Game_NodeController - More than one instance!\n" +
					"Registered Instance: " + Instance.gameObject.name +
					"This Instance: " + gameObject.name, this);
			}
		}
		else
			Instance = this;
	}
	#endregion

	#region Public Data
	public bool Preparation_ItemTexturesSet = false;
	public bool Preparation_NodeTexturesSet = false;
	public bool Preparation_SelectionsColorTextureSet = false;
	#endregion
	#region Private Data
	private Texture2D datatex2 = null;
	private Texture2D tex_dataPerFrame = null;
	private bool needToUpdateDataTex = true;
	private bool updateCalledThisFrame = false;
	private int numAllyMovable;

	private byte emptyByte = new byte();

	public Vector2Int? hoveredNode { get; private set; }
	public Vector2Int? SelectedNode { get; private set; }
	#endregion
	private void Awake ()
	{
		Preparation_ItemTexturesSet = false;
		Preparation_NodeTexturesSet = false;
		Preparation_SelectionsColorTextureSet = false;
		MakeSingleton();
		StartCoroutine(Setup());
	}

	private IEnumerator Setup ()
	{
		while (Game_Grid.Instance == null)
			yield return null;
		Game_Grid.GridItemsUpdatedEvent_ItemsQueueFinished += SetNeedUpdate;
		Game_Grid.GridItemsUpdatedEvent += SetNeedUpdate;
		Game_Grid.GridItemsUpdatedEvent_ForceReRender += ForceGenerateMap;

		while (Visual_Game_CameraController.Instance == null)
			yield return null;
		Visual_Game_CameraController.MouseClickEvent += SetSelectedNode;
	}

	private void OnDestroy ()
	{
		Game_Grid.GridItemsUpdatedEvent -= SetNeedUpdate;
	}
	#region Loops
	private void SetNeedUpdate ()
	{
		needToUpdateDataTex = true;
	}
	private void Update ()
	{
		updateCalledThisFrame = false;
		//StartCoroutine(CallUpdatePerFrameMaps());
	}
	#region Loop Methods
	private IEnumerator CallUpdatePerFrameMaps ()
	{
		yield return null;
		UpdatePerFrameMaps();
	}
	public void UpdatePerFrameMaps (bool Force = false)
	{
		//UnityEngine.Profiling.Profiler.BeginSample("Update Per Frame Map - " + this.gameObject.name);
		if (Force || (Ready && Preparation_ItemTexturesSet && Preparation_NodeTexturesSet && Preparation_SelectionsColorTextureSet))
		{
			if (tex_dataPerFrame == null)
			{
				tex_dataPerFrame = new Texture2D(GridSizeX, GridSizeY, TextureFormat.RGBA32, false, true)
				{
					filterMode = FilterMode.Point,
					wrapMode = TextureWrapMode.Clamp,
					anisoLevel = 0
				};
			}

			GetHoveredNode();

			UI_Game_Grid.GridUI_States GridUI_State = UI_Game_Grid.Instance.GridUI_State;
			bool[,] visible = Game_Grid.Instance.Nodes_GetGrid(Game_Grid.CheckType.Visible);
			bool[,] buildable = Game_Grid.Instance.Nodes_GetGrid(Game_Grid.CheckType.Buildable);

			Color32[] dataPF_colors = new Color32[GridSizeX * GridSizeY];
			Game_Node currentNode;
			int currentPixel = 0;
			for (int Y = 0; Y < GridSizeY; Y++)
			{
				for (int X = 0; X < GridSizeX; X++)
				{
					currentNode = Game_Grid.NodeAt(new Vector2Int(X, Y));
					int type = 0;
					int selectionPower = 0;
					int visibility = 0;
					bool selectedNode = false;
					// R: Type
					//	0 - Not Selected
					//	1 - Game_Grid_Selection_Normal_Free
					//	2 - Game_Grid_Selection_Normal_Ally
					//  3 - Game_Grid_Selection_Normal_AllyMovable
					//	4 - Game_Grid_Selection_Normal_Enemy
					//	5 - Game_Grid_Selection_Action_Free
					//	6 - Game_Grid_Selection_Action_Blocked
					// G: Alpha Power 0-255
					// B; Visible 0/1
					if (visible[X, Y])
					{
						visibility = 254;
						switch (GridUI_State)
						{
							case UI_Game_Grid.GridUI_States.Normal:
								if (!currentNode.IsBlocked)
								{
									type = 10;
								}
								else if (currentNode.Item != null)
								{
									if (currentNode.Item.Data.Movable)
									{
										type = 30;
									}
									else
									{
										type = 20;
									}
								}
								else
								{
									type = 40;
								}
								break;

							case UI_Game_Grid.GridUI_States.MovingItem:
							case UI_Game_Grid.GridUI_States.BuildingItem:
								if (buildable[X, Y])
								{
									type = 50;
								}
								else
								{
									type = 60;
								}
								break;

							case UI_Game_Grid.GridUI_States.Demolish:
								if (currentNode.Item != null && currentNode.Item.Item_Stats != null && currentNode.Item.Item_Stats.Demolishable)
								{
									type = 50;
								}
								else
								{
									type = 60;
								}
								break;
						}

						if (SelectedNode != null && X == SelectedNode.Value.x && Y == SelectedNode.Value.y)
						{
							selectedNode = true;
							selectionPower = (byte) Visual_Game_ColorDatabase.Instance.UI_Colors.Game_Grid_Selected_Power;
						}
					}

					if (!selectedNode && hoveredNode != null && X == hoveredNode.Value.x && Y == hoveredNode.Value.y)
					{
						selectionPower = (byte) Visual_Game_ColorDatabase.Instance.UI_Colors.Game_Grid_Hover_Power;
					}

					type++;
					selectionPower++;
					visibility++;

					dataPF_colors[currentPixel] =
						new Color32(Convert.ToByte(type), Convert.ToByte(selectionPower), Convert.ToByte(visibility), Convert.ToByte((int) 1));
					currentPixel++;
				}
			}
			tex_dataPerFrame.SetPixels32(dataPF_colors);
			tex_dataPerFrame.Apply();
			materialPropertyBlock.SetTexture("_DataTex_PerFrame", tex_dataPerFrame);

			if (needToUpdateDataTex && !updateCalledThisFrame)
			{
				ApplyNewMap(GenerateMap());
				needToUpdateDataTex = false;
			}
			else
			{
				meshRenderer.SetPropertyBlock(materialPropertyBlock);
			}
		}
		//UnityEngine.Profiling.Profiler.EndSample();
	} // Called from Game_Grid after every Update

	private void GetHoveredNode ()
	{
		//!! 0POC Mouse Control: Move to Respective script
		if (!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()) // If no UI is blocking
		{
			//!! 0POC Mouse Control: Move to Respective script
			// Check if mouse is inside screen
			Rect screenRect = new Rect(0,0, Screen.width, Screen.height);
			if (!screenRect.Contains(Input.mousePosition))
				return;

			// Get world position of mouse
			Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			int currentX = Mathf.FloorToInt(mousePos.x + (0.5f * GridSizeX));
			int currentY = Mathf.FloorToInt(mousePos.y + (0.5f * GridSizeY));
			Vector2Int nodePos = new Vector2Int(currentX, currentY);

			if (Game_Grid.DoesNodeExistAt(nodePos))
			{
				hoveredNode = nodePos;
				return;
			}
		}
		hoveredNode = null;
	}
	#endregion
	#endregion
	#region Private Actions
	private void ForceGenerateMap ()
	{
		ApplyNewMap(GenerateMap());
		UpdatePerFrameMaps(true);
		meshRenderer.SetPropertyBlock(materialPropertyBlock);
	}
	public override Texture2D GenerateMap ()
	{
		//UnityEngine.Profiling.Profiler.BeginSample("Update Map - " + this.gameObject.name);

		CheckAndGenerateMissingData();

		updateCalledThisFrame = true;
		needToUpdateDataTex = false;

#if UNITY_EDITOR
		if (!UnityEditor.EditorApplication.isPlaying)
		{
			FindObjectOfType<Gameplay_Values>().MakeSingleton();
			Preparation_ItemTexturesSet = false;
			Preparation_NodeTexturesSet = false;
		}
#endif
		if (!Preparation_NodeTexturesSet)
		{
			Preparation_SetNodeTextures(GridSizeX, GridSizeY);
		}
		if (!Preparation_ItemTexturesSet)
		{
			Preparation_SetItemTextures(GridSizeX, GridSizeY);
		}
		if (!Preparation_SelectionsColorTextureSet)
		{
			Preparation_SetSelectionsColorTexture();
		}

		int maxAmount = (int)Difficulty_Manager.CurrentDifficulty.Node_Resources_Base_Main;

		Texture2D tex_data1 = new Texture2D(GridSizeX, GridSizeY, TextureFormat.RGBA32, false, true)
		{
			anisoLevel = 0,
			filterMode = FilterMode.Point,
			wrapMode = TextureWrapMode.Clamp
		};
		if (datatex2 == null)
		{
			datatex2 = new Texture2D(GridSizeX, GridSizeY, TextureFormat.RGBA32, false, true)
			{
				anisoLevel = 0,
				filterMode = FilterMode.Point,
				wrapMode = TextureWrapMode.Clamp
			};
		}


#if UNITY_EDITOR
		if (UnityEditor.EditorApplication.isPlaying)
		{
#endif
			Color32[] datatex2_bytes = new Color32[GridSizeX * GridSizeY];
			Color[] datatex_colors = new Color[GridSizeX * GridSizeY];
			bool[,] visible = Game_Grid.Nodes_GetGrid(Game_Grid.CheckType.Visible);
			int pixelCount = 0;
			for (int Y = 0; Y < GridSizeY; Y++)
			{
				for (int X = 0; X < GridSizeX; X++)
				{
					float GroundFertility = 0f;
					float GroundWater = 0f;
					float Vegetation = 0f;

					if (visible[X, Y])
					{
						GroundFertility = Game_Grid.Nodes[X, Y].Stats.NodeResources[ResourcesSystem.NodeResourceType.GroundFertility] / maxAmount;
						GroundFertility = Mathf.Clamp01(GroundFertility);

						GroundWater = Game_Grid.Nodes[X, Y].Stats.NodeResources[ResourcesSystem.NodeResourceType.GroundWater] / maxAmount;
						GroundWater = Mathf.Clamp01(GroundWater);

						Vegetation = Game_Grid.Nodes[X, Y].Stats.NodeResources[ResourcesSystem.NodeResourceType.Vegetation] / maxAmount;
						Vegetation = Mathf.Clamp01(Vegetation);
					}
					datatex_colors[pixelCount] = new Color(GroundFertility, GroundWater, Vegetation, 1f);
					datatex2_bytes[pixelCount] = GenerateItemID(X, Y);
					pixelCount++;
				}
			}

			tex_data1.SetPixels(datatex_colors);
			datatex2.SetPixels32(datatex2_bytes);
#if UNITY_EDITOR
		}
#endif

		tex_data1.Apply();
		datatex2.Apply();
		materialPropertyBlock.SetTexture("_DataTex2", datatex2);
		//UnityEngine.Profiling.Profiler.EndSample();
		UpdatePerFrameMaps();
		//StartCoroutine(DelayedUpdateCalledPF());
		return tex_data1;
	}

	//private IEnumerator DelayedUpdateCalledPF ()
	//{
	//	yield return null;
	//	updateCalledThisFrame = false;
	//}

	private Color32 GenerateItemID (int X, int Y)
	{
		UnityEngine.Profiling.Profiler.BeginSample("GenerateItemID - " + this.gameObject.name);
		Color32 result;
		if (Game_Grid.Nodes[X, Y] != null && Game_Grid.Nodes[X, Y].Item != null && Game_Grid.Nodes[X, Y].Item.Data != null 
			&& !Game_Grid.Nodes[X, Y].Item.Data.Movable && Game_Grid.Nodes[X, Y].Item.Item_Stats != null)
		{
			byte ItemStatus = (byte)((int)Game_Grid.Nodes[X, Y].Item.Item_Stats.ItemStatus + 1);
			byte ItemTier = (byte)(Game_Grid.Nodes[X, Y].Item.Item_Stats.ItemTier + 1);
			byte ID;
			byte Cat;
			if (Game_Grid.Nodes[X, Y].Item.Data.ItemID == 0)
			{
				Debug.Log(Game_Grid.Nodes[X, Y].Item.Data.name + " - " + Game_Grid.Nodes[X, Y].Item.Data.ItemID);
				ID = (byte) 0;
				Cat = (byte) 0;
			}
			else
			{
				int cat = Game_Grid.Nodes[X, Y].Item.Data.ItemCat == 2 ? 10 : 0;
				Cat = (byte) cat;
				int id = int.Parse(Game_Grid.Nodes[X, Y].Item.Data.ItemID.ToString());
				if (cat == 1)
					id -= numAllyMovable;
				//id++;
				ID = (byte) id;
			}
			result = new Color32(ItemStatus, ItemTier, ID, Cat);
		}
		else
		{
			result = new Color32(emptyByte, emptyByte, emptyByte, emptyByte);
		}
		UnityEngine.Profiling.Profiler.EndSample();
		return result;
	}
	private void Preparation_SetSelectionsColorTexture ()
	{
		//	0 - Not Selected
		//	10 - Game_Grid_Selection_Normal_Free
		//	20 - Game_Grid_Selection_Normal_Ally
		//	30 - Game_Grid_Selection_Normal_AllyMovable
		//	40 - Game_Grid_Selection_Normal_Enemy
		//	50 - Game_Grid_Selection_Action_Free
		//	60 - Game_Grid_Selection_Action_Blocked
		//	70 - Not Selected
		Texture2D tex_var = new Texture2D(8, 1, TextureFormat.RGB24, false, true)
		{
			filterMode = FilterMode.Point,
			wrapMode = TextureWrapMode.Clamp,
			anisoLevel = 0
		};

		var db = Visual_Game_ColorDatabase.Instance.UI_Colors;
		Color[] colors = new Color[8]
		{
			Color.black,
			db.Game_Grid_Selection_Normal_Free,
			db.Game_Grid_Selection_Normal_Ally,
			db.Game_Grid_Selection_Normal_AllyMovable,
			db.Game_Grid_Selection_Normal_Enemy,
			db.Game_Grid_Selection_Action_Free,
			db.Game_Grid_Selection_Action_Blocked,
			Color.black
		};

		tex_var.SetPixels(colors);
		tex_var.Apply();
		materialPropertyBlock.SetTexture("_SelectionColors", tex_var);
		Preparation_SelectionsColorTextureSet = true;
	}
	private void Preparation_SetItemTextures (int GridSizeX, int GridSizeY)
	{
		//UnityEngine.Profiling.Profiler.BeginSample("Preparation_SetItemTextures - " + this.gameObject.name);
#if UNITY_EDITOR
		if (UnityEditor.EditorApplication.isPlaying)
		{
#endif
			var items = Game_Item_Manager.ItemsDB;

			int numStatuses = Enum.GetNames(typeof(Item_Stats.ItemStatusTypes)).Length;
			int width = items.Ally_Bases[0].Tiers[0].Sprites.Active.width;
			Debug.Log("Single Texture Width = " + width, this);
			// Count all items * tiers per category
			// Add all sprites

			int aBa_count = items.Ally_Bases.Count;
			int aBa_tiers_count = 0;
			List<Color32> aBa_numTiers = new List<Color32>();
			List<Color> aBa_textures = new List<Color>();

			for (int i = aBa_count - 1; i >= 0; i--)
			{
				int tierCount = items.Ally_Bases[i].Tiers.Count;
				aBa_numTiers.Add(new Color32((byte) tierCount, emptyByte, emptyByte, emptyByte));
				for (int tier = tierCount - 1; tier >= 0; tier--)
				{
					if (items.Ally_Bases[i].Tiers[tier].Sprites == null)
					{
						Debug.LogError("Sprite Asset missing on " + items.Ally_Bases[i].ItemName + " ("
							+ items.Ally_Bases[i].ItemID + ") , Tier " + tier, items.Ally_Bases[i].Tiers[tier]);
					}
					if (items.Ally_Bases[i].Tiers[tier].Sprites.Active == null)
					{
						Debug.LogError("'Active' Sprite Asset missing on " + items.Ally_Bases[i].ItemName + " ("
							+ items.Ally_Bases[i].ItemID + ") , Tier " + tier, items.Ally_Bases[i].Tiers[tier]);
					}
					if (items.Ally_Bases[i].Tiers[tier].Sprites.InActive == null)
					{
						Debug.LogError("'InActive' Sprite Asset missing on " + items.Ally_Bases[i].ItemName + " ("
							+ items.Ally_Bases[i].ItemID + ") , Tier " + tier, items.Ally_Bases[i].Tiers[tier]);
					}
					if (items.Ally_Bases[i].Tiers[tier].Sprites.Producing == null)
					{
						Debug.LogError("'Producing' Sprite Asset missing on " + items.Ally_Bases[i].ItemName + " ("
							+ items.Ally_Bases[i].ItemID + ") , Tier " + tier, items.Ally_Bases[i].Tiers[tier]);
					}
					aBa_tiers_count++;
					List<Color> arrangedPixels = new List<Color>(width * numStatuses);
					for (int pixel = 0; pixel < width; pixel++)
					{
						arrangedPixels.AddRange(items.Ally_Bases[i].Tiers[tier].Sprites.InActive.GetPixels(0, pixel, width, 1));
						arrangedPixels.AddRange(items.Ally_Bases[i].Tiers[tier].Sprites.Active.GetPixels(0, pixel, width, 1));
						arrangedPixels.AddRange(items.Ally_Bases[i].Tiers[tier].Sprites.Producing.GetPixels(0, pixel, width, 1));
					}
					aBa_textures.AddRange(arrangedPixels);
				}
			}

			var staticBuildings = items.Ally_Buildings.FindAll(x => !x.Movable);
			int aBu_count = staticBuildings.Count;
			numAllyMovable = items.Ally_Buildings.Count - aBu_count;

			int aBu_tiers_count = 0;
			List<Color32> aBu_numTiers = new List<Color32>();
			List<Color> aBu_textures = new List<Color>();

			for (int i = aBu_count - 1; i >= 0; i--)
			{
				int tierCount = staticBuildings[i].Tiers.Count;
				if (tierCount > 0)
				{
					aBu_numTiers.Add(new Color32((byte) tierCount, emptyByte, emptyByte, emptyByte));
					for (int tier = tierCount - 1; tier >= 0; tier--)
					{
						if (staticBuildings[i].Tiers[tier].Sprites == null)
						{
							Debug.LogError("Sprite Asset missing on " + staticBuildings[i].ItemName + " ("
								+ staticBuildings[i].ItemID + ") , Tier " + tier, staticBuildings[i].Tiers[tier]);
						}
						if (staticBuildings[i].Tiers[tier].Sprites.Active == null)
						{
							Debug.LogError("'Active' Sprite Asset missing on " + staticBuildings[i].ItemName + " ("
								+ staticBuildings[i].ItemID + ") , Tier " + tier, staticBuildings[i].Tiers[tier]);
						}
						if (staticBuildings[i].Tiers[tier].Sprites.InActive == null)
						{
							Debug.LogError("'InActive' Sprite Asset missing on " + staticBuildings[i].ItemName + " ("
								+ staticBuildings[i].ItemID + ") , Tier " + tier, staticBuildings[i].Tiers[tier]);
						}
						if (staticBuildings[i].Tiers[tier].Sprites.Producing == null)
						{
							Debug.LogError("'Producing' Sprite Asset missing on " + staticBuildings[i].ItemName + " ("
								+ staticBuildings[i].ItemID + ") , Tier " + tier, staticBuildings[i].Tiers[tier]);
						}
						aBu_tiers_count++;
						List<Color> arrangedPixels = new List<Color>(width * numStatuses);
						for (int pixel = 0; pixel < width; pixel++)
						{
							arrangedPixels.AddRange(staticBuildings[i].Tiers[tier].Sprites.InActive.GetPixels(0, pixel, width, 1));
							arrangedPixels.AddRange(staticBuildings[i].Tiers[tier].Sprites.Active.GetPixels(0, pixel, width, 1));
							arrangedPixels.AddRange(staticBuildings[i].Tiers[tier].Sprites.Producing.GetPixels(0, pixel, width, 1));
						}
						aBu_textures.AddRange(arrangedPixels);
					}
				}
			}
			int numTierMax = Mathf.Max(aBa_count, aBu_count);
			int numTierMin = Mathf.Min(aBa_count, aBu_count);
			Texture2D ally_Bases =
				new Texture2D(width * numStatuses, width * aBa_tiers_count, TextureFormat.ARGB32, false, true)
				{
					filterMode = FilterMode.Point,
					wrapMode = TextureWrapMode.Clamp,
					anisoLevel = 0
				};
			Texture2D ally_Buildings =
				new Texture2D(width * numStatuses, width * aBu_tiers_count, TextureFormat.ARGB32, false, true)
				{
					filterMode = FilterMode.Point,
					wrapMode = TextureWrapMode.Clamp,
					anisoLevel = 0
				};
			
			Texture2D ally_Info =
				new Texture2D(numTierMax, 2, TextureFormat.R8, false, true)
				{
					filterMode = FilterMode.Point,
					wrapMode = TextureWrapMode.Clamp,
					anisoLevel = 0
				};

			for (int i = numTierMin - 1; i < numTierMax; i++)
			{
				if (aBa_numTiers.Count < numTierMax)
				{
					aBa_numTiers.Add(new Color32());
				}
				if (aBu_numTiers.Count < numTierMax)
				{
					aBu_numTiers.Add(new Color32());
				}
			}
			List<Color32> all_NumTiers = new List<Color32>();
			aBu_numTiers.Reverse();
			aBa_numTiers.Reverse();
			all_NumTiers.AddRange(aBu_numTiers);
			all_NumTiers.AddRange(aBa_numTiers);

			ally_Info.SetPixels32(all_NumTiers.ToArray());

			ally_Bases.SetPixels(aBa_textures.ToArray()); // Bases
			ally_Buildings.SetPixels(aBu_textures.ToArray()); // Buildings

			ally_Info.Apply();
			ally_Bases.Apply();
			ally_Buildings.Apply();

			int numAllItemVars = aBa_tiers_count + aBu_tiers_count;

			materialPropertyBlock.SetTexture("_Ally_Info", ally_Info);

			materialPropertyBlock.SetVector("_IntDataPack2", 
				new Vector4(aBa_tiers_count, aBu_tiers_count, numStatuses, numAllItemVars));

			//materialPropertyBlock.SetInt("_Ally_Info_NumItems", numAllItemVars);
			//materialPropertyBlock.SetInt("_ItemTextures_AllyBases_NumTotalVars", aBa_tiers_count);
			//materialPropertyBlock.SetInt("_ItemTextures_AllyBuildings_NumTotalVars", aBu_tiers_count);
			//materialPropertyBlock.SetInt("_Ally_Info_NumStatuses", numStatuses);

			materialPropertyBlock.SetTexture("_ItemTextures_AllyBases", ally_Bases);
			materialPropertyBlock.SetTexture("_ItemTextures_AllyBuildings", ally_Buildings);
#if UNITY_EDITOR
		}
#endif
		Preparation_ItemTexturesSet = true;
		//UnityEngine.Profiling.Profiler.EndSample();
	}
	private void Preparation_SetNodeTextures (int GridSizeX, int GridSizeY)
	{
		//UnityEngine.Profiling.Profiler.BeginSample("Preparation_SetNodeTextures - " + this.gameObject.name);
#if UNITY_EDITOR
		if (!UnityEditor.EditorApplication.isPlaying)
		{
			FindObjectOfType<Visual_Game_SpriteDatabase>().MakeSingleton();
		}
#endif
		var spriteSets = Visual_Game_SpriteDatabase.Instance.NodeSpritesSets;

		int numVars = spriteSets.Length;
		float varMultSend = Mathf.Pow(10f, Mathf.Ceil(Mathf.Log10(numVars)));
		float varMultLocal = 1f / varMultSend;

		Texture2D tex_var = new Texture2D(GridSizeX, GridSizeY, TextureFormat.RGBA32, false, true)
		{
			filterMode = FilterMode.Point,
			wrapMode = TextureWrapMode.Clamp,
			anisoLevel = 0
		};

		int width = spriteSets[0].GroundDry.width;

		Texture2DArray vars_GroundDry = new Texture2DArray(width, width, numVars, TextureFormat.RGB24, true, true)
		{
			filterMode = FilterMode.Point,
			wrapMode = TextureWrapMode.Clamp,
			anisoLevel = 0
		};
		Texture2DArray vars_GroundFertility = new Texture2DArray(width, width, numVars, TextureFormat.RGBA32, true, true)
		{
			filterMode = FilterMode.Point,
			wrapMode = TextureWrapMode.Clamp,
			anisoLevel = 0
		};
		Texture2DArray vars_GroundWater = new Texture2DArray(width, width, numVars, TextureFormat.RGBA32, true, true)
		{
			filterMode = FilterMode.Point,
			wrapMode = TextureWrapMode.Clamp,
			anisoLevel = 0
		};
		Texture2DArray vars_Vegetation = new Texture2DArray(width, width, numVars, TextureFormat.RGBA32, true, true)
		{
			filterMode = FilterMode.Point,
			wrapMode = TextureWrapMode.Clamp,
			anisoLevel = 0
		};

		// Variations
		for (int i = 0; i < numVars; i++)
		{
			if (spriteSets.Any(x => x.GroundDry.width != width || x.GroundFertility.width != width || x.GroundWater.width != width || x.Vegetation.width != width))
			{
				Debug.LogWarning("Visual_Game_NodeController - Not all node textures are the same width!", spriteSets[i]);
				return;
			}
			vars_GroundDry.SetPixels(spriteSets[i].GroundDry.GetPixels(), i, 0);
			vars_GroundFertility.SetPixels(spriteSets[i].GroundFertility.GetPixels(), i, 0);
			vars_GroundWater.SetPixels(spriteSets[i].GroundWater.GetPixels(), i, 0);
			vars_Vegetation.SetPixels(spriteSets[i].Vegetation.GetPixels(), i, 0);
		}
		vars_GroundDry.Apply();
		vars_GroundFertility.Apply();
		vars_GroundWater.Apply();
		vars_Vegetation.Apply();

		// Variation Map
		for (int y = 0; y < GridSizeY; y++)
		{
			for (int x = 0; x < GridSizeX; x++)
			{
				// Variation
				float GD = UnityEngine.Random.Range(0, numVars) * varMultLocal;
				float GF = UnityEngine.Random.Range(0, numVars) * varMultLocal;
				float GW = UnityEngine.Random.Range(0, numVars) * varMultLocal;
				float Vg = UnityEngine.Random.Range(0, numVars) * varMultLocal;
				tex_var.SetPixel(x, y, new Color(GD, GF, GW, Vg));
			}
		}
		tex_var.Apply();

		materialPropertyBlock.SetVector("_IntDataPack1", new Vector4(GridSizeX, GridSizeY, varMultSend, 0f));

		//materialPropertyBlock.SetFloat("_NodeVarBase", varMultSend);
		materialPropertyBlock.SetTexture("_NodeVarTex", tex_var);
		materialPropertyBlock.SetTexture("_NodeTextures_DryGround", vars_GroundDry);
		materialPropertyBlock.SetTexture("_NodeTextures_Fertility", vars_GroundFertility);
		materialPropertyBlock.SetTexture("_NodeTextures_GroundWater", vars_GroundWater);
		materialPropertyBlock.SetTexture("_NodeTextures_Vegetation", vars_Vegetation);

		meshRenderer.SetPropertyBlock(materialPropertyBlock);
		Preparation_NodeTexturesSet = true;
		//UnityEngine.Profiling.Profiler.EndSample();
	}
	//!! 0POC Mouse Control: Move to Respective script
	private void SetSelectedNode (int mouseBTN)
	{
		if (mouseBTN == 0)
		{
			if (Ready)
			{
				//UpdatePerFrameMaps(true);
				SelectedNode = hoveredNode;
			}
		}
	}
	#endregion

	#region Public Actions
	public void ClearSelectedNode ()
	{
		SelectedNode = null;
		if (Ready)
			UpdatePerFrameMaps(true);
	}
	#endregion
}