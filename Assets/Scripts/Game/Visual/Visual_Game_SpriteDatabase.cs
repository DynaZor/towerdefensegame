﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using System.Collections.Generic;
using ItemStatuses;
using UnityEngine;

public class Visual_Game_SpriteDatabase : MonoBehaviour
{
	#region Singleton
	public static Visual_Game_SpriteDatabase Instance;
	private void Awake ()
	{
		MakeSingleton();
	}

	public void MakeSingleton ()
	{
#if UNITY_EDITOR
		if (UnityEditor.EditorApplication.isPlaying)
		{
#endif
			if (Instance != null)
				Debug.Log("Visual_Game_SpriteDatabase - More than one instance!\n" +
					"Registered Instance: " + Instance.gameObject.name +
					"This Instance: " + gameObject.name, this);
			else
				Instance = this;
#if UNITY_EDITOR
		}
		else
		{
			Instance = this;
		}
#endif
	}
	#endregion
	#region Node Sprite Sets
	[Header("GameWorld: Node Sprite Sets")]
	public SO_Node_Sprites[] NodeSpritesSets;

	public SO_Node_Sprites GetRandomNodeSpritesSet ()
	{
		return NodeSpritesSets[Random.Range(0, NodeSpritesSets.Length)]; // 0 [inclusive] - length [exclusive]
	}
	#endregion
	#region UI Sprites
	[Space()]
	[Header("UI Sprites: Item Action Icons")]
	//!! 0POC UI Sprites: Spread to dedicated SOs when crowded
	public Sprite UI_Item_Action_Move_Active;
	public Sprite UI_Item_Action_Move_InActive;
	public Sprite UI_Item_Action_Upgrade_Possible;
	public Sprite UI_Item_Action_Upgrade_ImPossible;
	public Sprite UI_Item_Action_Dismantle_HoverOn;
	public Sprite UI_Item_Action_Dismantle_HoverOff;
	public Sprite[] UI_Age = new Sprite[4];

	[Space()]
	[Header("UI Sprites: Node Resource Icons")]
	public Sprite UI_Resource_GFertility;
	public Sprite UI_Resource_GFertility_InActive;
	public Sprite UI_Resource_GFertility_Main;
	public Sprite UI_Resource_GWater;
	public Sprite UI_Resource_GWater_InActive;
	public Sprite UI_Resource_GWater_Main;
	public Sprite UI_Resource_GVeg;
	public Sprite UI_Resource_GVeg_InActive;
	public Sprite UI_Resource_GVeg_Main;

	[Space()]
	[Header("UI Sprites: Crafted Resource Icons")]
	public Sprite UI_Resource_CFood;
	public Sprite UI_Resource_CFood_InActive;
	public Sprite UI_Resource_CElect;
	public Sprite UI_Resource_CElect_InActive;
	public Sprite UI_Resource_CTech;
	public Sprite UI_Resource_CTech_InActive;

	#endregion
	#region Item Status Icons
	[Header("UI Sprites: Item Status Icons (GameWorld)")]
	public ItemStatusIconDictionary ItemStatusIcons = new ItemStatusIconDictionary();
	#endregion
}
