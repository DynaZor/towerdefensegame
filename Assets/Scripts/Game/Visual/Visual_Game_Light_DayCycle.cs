﻿// Code modofied by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;

public class Visual_Game_Light_DayCycle : MonoBehaviour
{
	[Header("DayCycle Settings")]
	[SerializeField] private float DayLengthInSeconds = 30f;
	[SerializeField] private Color[] LightColors = new Color[0];
	[Header("References")]
	[SerializeField] private Light SunLight = null;

	// Private Data
	private Color[] loopedLightColors;
	private float colorLength;
	private float currentTime = 0f;

	#region Setup
	private void Awake ()
	{
		Setup();
	}

	private void Setup ()
	{
		if (SunLight == null)
			SunLight = GetComponent<Light>();
		colorLength = DayLengthInSeconds / LightColors.Length;
		loopedLightColors = new Color[LightColors.Length + 1];
		LightColors.CopyTo(loopedLightColors, 0);
		loopedLightColors[loopedLightColors.Length - 1] = loopedLightColors[0];
	}
	#endregion

	#region Loops
	private void Update ()
	{
		CalculateLightColorAndSet();
	}

	private void CalculateLightColorAndSet ()
	{
		currentTime += Time.deltaTime;
		while (currentTime >= DayLengthInSeconds)
			currentTime -= DayLengthInSeconds;

		float dayTime = currentTime / DayLengthInSeconds;
		float colorValue = dayTime * LightColors.Length;
		int prevColorIndex = Mathf.FloorToInt(colorValue);
		int nextColorIndex = Mathf.CeilToInt(colorValue);
		if (prevColorIndex == nextColorIndex)
			nextColorIndex = 0;
		float TValue = colorValue - prevColorIndex;
		Color currentColor = Color.Lerp(loopedLightColors[prevColorIndex], loopedLightColors[nextColorIndex], TValue);

		SetColor(currentColor);
	}

	private void SetColor(Color col)
	{
		SunLight.color = col;
	}
	#endregion
}