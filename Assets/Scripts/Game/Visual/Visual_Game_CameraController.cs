﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine.U2D;

public class Visual_Game_CameraController : MonoBehaviour, IMouseControllable
{
	#region Singleton
	public static Visual_Game_CameraController Instance;
	private void MakeSingleton ()
	{
		if (Instance != null && Instance != this)
		{
			Debug.Log("Visual_Game_CameraController - More than one instance!\n" +
			"Registered Instance: " + Instance.gameObject.name +
			"This Instance: " + gameObject.name, this);
		}
		Instance = this;
	}
	#endregion

	private bool Ready = false;
	private Coroutine setupCoroutine = null;

	#region Vars - Mouse
	[Header("Mouse Settings")]
	[SerializeField] private float maxMSForClickRelease = 100f;
	[SerializeField] private float maxDistancePercentForClickRelease = 2.5f;
	[SerializeField] private int maxDistancePixelsForClickRelease = 7;

	public static bool LastFrameClick { get; private set; }
	public delegate void MouseClickEventHandler (int mouseButton);
	public static MouseClickEventHandler MouseClickEvent = delegate { };

	#region Vars - Mouse - Zoom
	[Header("Mouse - Zoom")]
	[SerializeField] private float cameraZOffset = -10f;
	[SerializeField] private int startCameraZoomStep = 0;
	[SerializeField] private List<int> cameraZoomStepList = new List<int>();
	//[SerializeField] private List<CameraZoomStep> cameraZoomStepListPhysical = new List<CameraZoomStep>();
	[SerializeField] private PixelPerfectCamera pixelPerfectCamera = null;
	//[SerializeField] private float cameraZoomMinSize = 1f;
	//[SerializeField] private float cameraZoomMaxSize = 10f;
	#endregion
	#region Vars - Mouse - Panning
	private Vector3 pan_hit_position = Vector3.zero;
	private Vector3 pan_current_mousePosition = Vector3.zero;
	private Vector3 pan_hit_camera_position = Vector3.zero;
	private Vector3 pan_target_camera_position;
	private bool pan_move = false;
	private bool thisFramePanMove = false;
	private bool lastFramePanMove = false;
	private bool thisFrameMouseDown = false;
	private bool lastFrameMouseDown = false;
	private bool thisFrameMouseUp = false;
	private bool ignoreMouseDrag = true;
	private float timeMouseDown = 0f;
	private float timeMouseUp = 0f;
	private float timeSinceMouseDown = 0f;
	private Vector2 posMouseDown;
	private Vector2 posMouseUp;
	#endregion
	#endregion
	#region Vars - Touch
	[Header("Touch")]
	private Vector2 StartPosition;
	private Vector2 DragStartPosition;
	private Vector2 DragNewPosition;
	private Vector2 Finger0Position;
	private float DistanceBetweenFingers;
	private bool isZooming;
	#endregion

	// References
	private Camera Camera;
	private Game_Grid Game_Grid;

	#region Setup
	private void OnEnable ()
	{
		MakeSingleton();
		Camera = GetComponent<Camera>();
		if (setupCoroutine != null)
			StopCoroutine(setupCoroutine);
		setupCoroutine = StartCoroutine(Setup());
	}

	private void OnDisable ()
	{
		Ready = false;
	}

	private IEnumerator Setup ()
	{
		Ready = false;
		while (Game_Grid.Instance == null)
			yield return null;
		Game_Grid = Game_Grid.Instance;
		bool[] tempBools = new bool[3];
		while (!tempBools[2])
		{
			yield return null;
			if (!tempBools[0])
				tempBools[0] = Item_PlayerBase.Instance != null;
			else
			{
				if (!tempBools[1])
					tempBools[1] = Item_PlayerBase.Instance.Node != null;
				else
					tempBools[2] = Item_PlayerBase.Instance.Node.NodeWorldPosSet;
			}
		}

		transform.position = new Vector3(
			Item_PlayerBase.Instance.Node.NodeWorldPosition.x,
			Item_PlayerBase.Instance.Node.NodeWorldPosition.y,
			cameraZOffset);
		pixelPerfectCamera.assetsPPU = cameraZoomStepList[Mathf.Clamp(startCameraZoomStep, 0, cameraZoomStepList.Count - 1)];

		Game_Control_Mouse.RegisterControllable(this);

		ignoreMouseDrag = false;
		lastFramePanMove = false;
		lastFrameMouseDown = false;
		LastFrameClick = false;
		Ready = true;
	}
	#endregion

	#region Loop
	private void Update ()
	{
		if (Ready)
		{
#if UNITY_STANDALONE || UNITY_EDITOR
			
#else
		Touch();
#endif
		}
	}
	#endregion

	#region Touch // UNTESTED!
	private void Touch () // UNTESTED!
	{
		if (Input.touchCount == 0 && isZooming)
		{
			isZooming = false;
		}

		if (Input.touchCount == 1)
		{
			if (!isZooming)
			{
				if (Input.GetTouch(0).phase == TouchPhase.Moved)
				{
					Vector2 NewPosition = GetWorldPosition();
					Vector2 PositionDifference = NewPosition - StartPosition;
					Camera.transform.Translate(-PositionDifference);
				}
				StartPosition = GetWorldPosition();
			}
		}
		else if (Input.touchCount == 2)
		{
			if (Input.GetTouch(1).phase == TouchPhase.Moved)
			{
				isZooming = true;

				DragNewPosition = GetWorldPositionOfFinger(1);
				Vector2 PositionDifference = DragNewPosition - DragStartPosition;

				if (Vector2.Distance(DragNewPosition, Finger0Position) < DistanceBetweenFingers)
					Camera.orthographicSize += (PositionDifference.magnitude);

				if (Vector2.Distance(DragNewPosition, Finger0Position) >= DistanceBetweenFingers)
					Camera.orthographicSize -= (PositionDifference.magnitude);

				DistanceBetweenFingers = Vector2.Distance(DragNewPosition, Finger0Position);
			}
			DragStartPosition = GetWorldPositionOfFinger(1);
			Finger0Position = GetWorldPositionOfFinger(0);
		}
	}

	private Vector2 GetWorldPosition ()
	{
		Vector2 newPos = Camera.ScreenToWorldPoint(Input.mousePosition);
		newPos.x = Mathf.Clamp(newPos.x, Game_Grid.GridSizeX * -0.5f, Game_Grid.GridSizeX * 0.5f);
		newPos.y = Mathf.Clamp(newPos.y, Game_Grid.GridSizeY * -0.5f, Game_Grid.GridSizeY * 0.5f);
		return newPos;
	}

	private Vector2 GetWorldPositionOfFinger (int FingerIndex)
	{
		return Camera.ScreenToWorldPoint(Input.GetTouch(FingerIndex).position);
	}
	#endregion

	#region Mouse
	private void Mouse_CameraZoom ()
	{
		Vector2 mouseScrollDelta = Input.mouseScrollDelta;
		if (mouseScrollDelta != Vector2.zero)
		{
			if (mouseScrollDelta.y != 0)
			{
				int stepCount = cameraZoomStepList.Count;

				int currentStepIndex = stepCount - 1;

				if (cameraZoomStepList.Contains(pixelPerfectCamera.assetsPPU))
					currentStepIndex = cameraZoomStepList.IndexOf(pixelPerfectCamera.assetsPPU);
				if (mouseScrollDelta.y < 0)
				{
					//Camera.orthographicSize = Mathf.Clamp((Camera.orthographicSize + step), cameraZoomMinSize, cameraZoomMaxSize);
					int newIndex = Mathf.Clamp(currentStepIndex + 1, 0, stepCount - 1);
					if (newIndex != currentStepIndex)
						pixelPerfectCamera.assetsPPU = cameraZoomStepList[newIndex];
				}
				else //if (mouseScrollDelta.y > 0)
				{
					//Camera.orthographicSize = Mathf.Clamp((Camera.orthographicSize - step), cameraZoomMinSize, cameraZoomMaxSize);
					int newIndex = Mathf.Clamp(currentStepIndex - 1, 0, stepCount - 1);
					if (newIndex != currentStepIndex)
						pixelPerfectCamera.assetsPPU = cameraZoomStepList[newIndex];
				}
			}
		}
	}

	private bool MouseClickWithinRange ()
	{
		// Pixel Distance
		float distance = math.distance(posMouseDown, posMouseUp);

		// Screen Relative Distance
		//int2 screenRes = new int2(Screen.width, Screen.height);
		//float2 _posMouseDown = new float2(posMouseDown.x / screenRes.x, posMouseDown.y / screenRes.y);
		//float2 _posMouseUp = new float2(posMouseUp.x / screenRes.x, posMouseUp.y / screenRes.y);
		//float _distance = math.distance(_posMouseDown, _posMouseUp);

		if (distance <= maxDistancePixelsForClickRelease) // && _distance <= maxDistancePercentForClickRelease * 0.01f)
		{
			return true;
		}
		return false;
	}

	private void Mouse_CameraPan ()
	{
		thisFrameMouseDown = Game_Control_Mouse.MouseButtonDown0;
		if (thisFrameMouseDown)
		{
			posMouseDown = Game_Control_Mouse.mousePosition.xy;
			timeMouseDown = Time.unscaledTime;
		}

		thisFrameMouseUp = Game_Control_Mouse.MouseButtonUp0;
		if (thisFrameMouseUp)
		{
			timeMouseUp = Time.unscaledTime;
			posMouseUp = Game_Control_Mouse.mousePosition.xy;
			timeSinceMouseDown = (timeMouseUp - timeMouseDown) * 1000f;
		}

		if ((thisFrameMouseDown && thisFrameMouseUp) ||
			(thisFrameMouseUp && timeSinceMouseDown <= maxMSForClickRelease))
		{
			if (MouseClickWithinRange())
			{
				MouseClickEvent.Invoke(0);
				LastFrameClick = true;
				return;
			}
		}

		if (ignoreMouseDrag)
		{
			pan_move = false;
			return;
		}

		if (thisFrameMouseDown)
		{
			pan_hit_position = Game_Control_Mouse.mousePosition.xyz;
			pan_hit_camera_position = transform.position;
		}

		if (Input.GetMouseButton(0))
		{
			pan_current_mousePosition = Game_Control_Mouse.mousePosition.xyz;
			LeftMouseDrag();
			pan_move = true;
		}

		if (pan_move)
		{
			transform.position = pan_target_camera_position;
			pan_move = false; // stop moving
		}
		else
		{
			thisFramePanMove = false;
		}

		if (lastFrameMouseDown && !thisFrameMouseDown && !thisFramePanMove) // Click
		{
			MouseClickEvent.Invoke(0);
			LastFrameClick = true;
		}
		else
		{
			LastFrameClick = false;
		}

		lastFrameMouseDown = thisFrameMouseDown;
		lastFramePanMove = thisFramePanMove;
		lastFramePanMove = thisFramePanMove;
	}

	private void LeftMouseDrag ()
	{
		thisFramePanMove = true;
		pan_current_mousePosition.z = pan_hit_position.z = pan_hit_camera_position.z = cameraZOffset;
		// Get direction of movement.
		Vector3 pan_direction = Camera.ScreenToWorldPoint(pan_current_mousePosition) - Camera.ScreenToWorldPoint(pan_hit_position);
		// Invert direction to move with the mouse.
		pan_direction *= -1f;

		pan_target_camera_position = pan_hit_camera_position + pan_direction;
		pan_target_camera_position.x = Mathf.Clamp(pan_target_camera_position.x, Game_Grid.GridSizeX * -0.5f, Game_Grid.GridSizeX * 0.5f);
		pan_target_camera_position.y = Mathf.Clamp(pan_target_camera_position.y, Game_Grid.GridSizeY * -0.5f, Game_Grid.GridSizeY * 0.5f);
		pan_target_camera_position.z = cameraZOffset;
	}

	private Vector3 RoundToPixel (Vector3 input, int resolution)
	{
		input = math.round(input * resolution) / resolution;
		return input;
	}

	public void GetMouseData ()
	{
		if (Ready)
			Update_Mouse();
	}

	private void Update_Mouse ()
	{
		if (Game_Control_Mouse.IsInScreenSpace)
		{
			if (Game_Control_Mouse.MouseButtonUp0)
			{
				ignoreMouseDrag = false;
			}

			if (Game_Control_Mouse.IsOverUI)
			{
				if (Game_Control_Mouse.MouseButtonDown0 || Game_Control_Mouse.MouseButtonDown1)
				{
					ignoreMouseDrag = true;
				}
			}
			else
			{
				Mouse_CameraPan();
				Mouse_CameraZoom();
			}
		}
		else
		{
			ignoreMouseDrag = true;
		}
	}

	[Serializable]
	public class CameraZoomStep
	{
		public int PixelsPerUnit;

		public CameraZoomStep (int ppu)
		{
			PixelsPerUnit = ppu;
		}

		//public float MinSize;
		//public float Step;

		//public CameraZoomStep(int step, int minSize)
		//{
		//	Step = step;
		//	MinSize = minSize;
		//}
	}
	#endregion
}