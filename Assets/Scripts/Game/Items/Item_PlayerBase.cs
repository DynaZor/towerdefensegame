﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using System.Collections;
using UnityEngine;

public class Item_PlayerBase : MonoBehaviour
{
	public static Item_PlayerBase Instance { get; private set; }
	public Game_Node Node { get; private set; }

	private void Awake ()
	{
		Instance = this;
		Debug.Log("playerBase set Instance");
		StartCoroutine(Setup());
	}

	private IEnumerator Setup ()
	{
		while (Node == null)
		{
			while (GetComponent<Item_Information>() == null)
				yield return null;
			while (GetComponent<Item_Information>().Node == null)
				yield return null;
			Node = GetComponent<Item_Information>().Node;
			yield return null;
		}
		Debug.Log("playerBase got Node");
	}

	public void BaseDestroyed ()
	{
		UI_Game_Manager.Instance.UIAction_GameOver();
	}
}