﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;
using AgesSystem;
using ResourcesSystem;
using System.Collections;
using System.Collections.Generic;

public class Item_Stats : MonoBehaviour
{
	public enum ItemStatusTypes
	{
		InActive, Active, Producing
	}

	public ItemStatusTypes ItemStatus
	{
		get
		{
			if (ItemActive)
			{
				if (Information.Item_Production.ProducedLastCycle)
				{
					return ItemStatusTypes.Producing;
				}
				return ItemStatusTypes.Active;
			}
			return ItemStatusTypes.InActive;
		}
	}
	// Properties
	public string ItemName { get; private set; }
	public int ItemTier { get; private set; }
	public bool Movable { get; private set; }
	public bool Demolishable { get; private set; }
	public Age Age { get; private set; }

	// Health
	public float MaxHealth { get; private set; }
	public float CurrentHealth { get; private set; }

	// Defence
	public float Defence { get; private set; }

	// Offense
	public bool CanAttack { get; private set; }
	public float AttackPower { get; private set; }
	public float AttackRange { get; private set; }
	public SO_AttackTiming AttackTiming { get; private set; }

	// Capacity
	public float ElectricityCapacity { get; private set; }

	// States
	public bool Ready { get; private set; }
	public bool ItemActive { get; private set; }

	// Communication
	public delegate void ItemStateUpdatedHandler (bool itemState);
	public event ItemStateUpdatedHandler ItemStateUpdatedEvent = delegate { };

	public delegate void HealthChangedHandler (float health);
	public event HealthChangedHandler HealthChangedEvent = delegate { };
	public delegate void HasTargetChangedHandler ();
	public event HasTargetChangedHandler HasTargetChangedEvent = delegate { };
	//!! 1Alpha Item Stats: Finish Implementing ProdState Update (Invoker)
	public delegate void ItemStateProductionUpdatedHandler (bool prodState);
	public event ItemStateProductionUpdatedHandler ItemStateProductionUpdatedEvent = delegate { };
	//!! 1Alpha Item Stats: Finish Implementing Needs(Not)Met Update (Invoker)
	public delegate void ItemStateNeedsUpdatedHandler (bool needsState, NodeResourceType[] nodeNeedsNotMet, CraftedResourceType[] craftedNeedsNotMet);
	public event ItemStateNeedsUpdatedHandler ItemStateNeedsUpdatedEvent = delegate { };

	// References
	public Item_Information Information { get; private set; }

	#region Setup
	private void Awake ()
	{
		ItemActive = false;
		Ready = false;
		StartCoroutine(Setup());
	}

	private IEnumerator Setup ()
	{
		while (GetComponent<Item_Information>() == null)
			yield return null;
		Information = GetComponent<Item_Information>();

		while (!Information.Ready)
			yield return null;

		ItemActive = true;
	}

	public void SetStats (SO_Item SO_Item, int tier, bool init)
	{
		if (Information.Item_Production == null)
		{
			StartCoroutine(SetStatsWaiter(SO_Item, tier, init));
		}
		else
		{
			ItemTier = tier;
			ItemName = SO_Item.ItemName;
			Movable = SO_Item.Movable;
			Demolishable = SO_Item.Demolishable;
			Age = SO_Item.age;
			MaxHealth = SO_Item.Tiers[tier].Health;
			ElectricityCapacity = SO_Item.Tiers[tier].ElectricityCapacity;
			if (init)
				CurrentHealth = MaxHealth;
			Defence = SO_Item.Tiers[tier].Defence;
			CanAttack = SO_Item.CanAttack;
			if (CanAttack)
			{
				AttackPower = SO_Item.Tiers[tier].AttackPower;
				AttackRange = SO_Item.Tiers[tier].AttackRange;
				AttackTiming = SO_Item.Tiers[tier].attackTiming;
			}

			if (SO_Item.Tiers[tier].ProductionTypes.Count > 0)
			{
				Information.Item_Production.SetProductionValues(SO_Item.Tiers[tier].ProductionTypes, SO_Item.Tiers[tier].ProductionValues);
			}
			Information.Item_Production.SetPollutionAmount(SO_Item.Tiers[tier].PollutionAmount);
			Ready = true;
		}
	}

	private IEnumerator SetStatsWaiter (SO_Item SO_Item, int tier, bool init)
	{
		while (GetComponent<Item_Production>() == null)
			yield return null;
		SetStats(SO_Item, tier, init);
	}

	private void OnDestroy ()
	{
		ItemStateUpdatedEvent = delegate { };
	}
	#endregion

	#region Public Actions
	public void Health_Set (float amount)
	{
		CurrentHealth = Mathf.Clamp(amount, 0, MaxHealth);
		HealthChangedEvent.Invoke(CurrentHealth);
	}

	public void Health_Subtract (float amount)
	{
		CurrentHealth = Mathf.Clamp(CurrentHealth - amount, 0f, MaxHealth);
		HealthChangedEvent.Invoke(CurrentHealth);
	}

	public void Health_Add (float amount)
	{
		CurrentHealth = Mathf.Clamp(CurrentHealth + amount, 0f, MaxHealth);
		HealthChangedEvent.Invoke(CurrentHealth);
	}

	public void EnableItem ()
	{
		if (Ready)
		{
			ItemActive = true;
			ItemStateUpdatedEvent.Invoke(ItemActive);
		}
		else
			StartCoroutine(DelayedEnableItem());
	}

	private IEnumerator DelayedEnableItem ()
	{
		while (!Ready)
			yield return null;
		EnableItem();
	}

	public void DisableItem ()
	{
		ItemActive = false;
		ItemStateUpdatedEvent.Invoke(ItemActive);
	}

	#region Events
	public void HasTargetChanged ()
	{
		HasTargetChangedEvent.Invoke();
	}
	#endregion
	#endregion

	#region Calculators
	public void DismantleValue (out List<CraftedResourceType> craftedResourceTypes, out List<float> amounts)
	{
		craftedResourceTypes = new List<CraftedResourceType>(1)
		{
			CraftedResourceType.Age
		};
		amounts = new List<float>(1)
		{
			10f
		};
	}
	#endregion
}