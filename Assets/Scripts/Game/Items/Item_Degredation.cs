﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;
using System.Collections;

public class Item_Degredation : MonoBehaviour
{
	// Private Data
	private int degredationTimer;
	private int degredationTimerStart;

	// References
	private Item_Information Information;

	#region Setup
	private void Awake ()
	{
		StartCoroutine(Setup());
	}

	private IEnumerator Setup ()
	{
		while (GetComponent<Item_Information>() == null)
			yield return null;
		Information = GetComponent<Item_Information>();

		degredationTimerStart = Gameplay_Values.Instance.ItemDegradeTimerLength;
		ResetDegredationTimer();
	}
	#endregion

	#region Public Actions
	public void NeedsMet (bool response)
	{
		if (response == true)
			ResetDegredationTimer();
		else
			CheckIfDegradationShouldStart();
	}
	#endregion

	#region Private Actions
	private void CheckIfDegradationShouldStart ()
	{
		degredationTimer--;
		if (degredationTimer <= 0)
		{
			Degrade();
			ResetDegredationTimer();
		}
	}

	private void Degrade ()
	{
		float degredationDamage = Information.Item_Stats.MaxHealth * (Gameplay_Values.Instance.ItemDegradePercentBase / 100f);
		Information.Item_Actions.TakeDamage(degredationDamage);
	}

	private void ResetDegredationTimer ()
	{
		degredationTimer = degredationTimerStart;
	}
	#endregion
}