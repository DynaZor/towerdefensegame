﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using ResourcesSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item_Information : MonoBehaviour
{
	// Public Data
	public SO_Item Data { get; private set; }

	public Item_Stats Item_Stats { get; private set; }
	public Item_Actions Item_Actions { get; private set; }
	public Item_Needs Item_Needs { get; private set; }
	public Item_Production Item_Production { get; private set; }
	public Item_Degredation Item_Degredation { get; private set; }
	public Item_Sprites Item_Sprites { get; private set; }

	public Game_Node_Stats Node_Stats { get; private set; }
	public Game_Node Node { get; private set; }


	public Game_ResourceQueue Global_ResourceQueue { get; private set; }

	// States
	public bool Ready { get; private set; }

	#region Setup
	private void Awake ()
	{
		Ready = false;
		StartCoroutine(Setup());
	}

	private IEnumerator Setup ()
	{
		while (Data == null)
			yield return null;
		while (Node_Stats == null)
			yield return null;

		while (GetComponent<Item_Stats>() == null)
			yield return null;
		Item_Stats = GetComponent<Item_Stats>();

		while (GetComponent<Item_Actions>() == null)
			yield return null;
		Item_Actions = GetComponent<Item_Actions>();

		while (GetComponent<Item_Needs>() == null)
			yield return null;
		Item_Needs = GetComponent<Item_Needs>();

		while (GetComponent<Item_Degredation>() == null)
			yield return null;
		Item_Degredation = GetComponent<Item_Degredation>();

		while (GetComponent<Item_Production>() == null)
			yield return null;
		Item_Production = GetComponent<Item_Production>();

		while (GetComponent<Item_Sprites>() == null)
			yield return null;
		Item_Sprites = GetComponent<Item_Sprites>();

		while (Node_Stats == null)
			yield return null;

		while (Game_ResourceQueue.Instance == null)
			yield return null;
		Global_ResourceQueue = Game_ResourceQueue.Instance;

		if (Data.Movable)
		{
			Item_Sprites.SetSprites(GetCurrentTier());
		}

		Ready = true;
	}

	public void SetItemSO (SO_Item itemSO)
	{
		Data = itemSO;
	}

	public void SetNode (Game_Node_Stats node_stats)
	{
		Node_Stats = node_stats;
		Node = node_stats.Node;
	}
	#endregion

	#region Tiers Information
	public SO_Item_Tier GetCurrentTier ()
	{
		return Data.Tiers[Item_Stats.ItemTier];
	}

	public List<SO_Item_Tier> Tiers ()
	{
		if (Data != null)
			return Data.Tiers;
		Debug.Log("Item_Information - '" + gameObject.name + "' - Tiers requested but Item field is null!", this);
		return null;
	}

	public SO_Item_Tier Tier (int tierNumber)
	{
		if (Data != null)
			if (Data.Tiers.Count > tierNumber)
				return Data.Tiers[tierNumber];
			else
				Debug.Log("Item_Information - '" + gameObject.name + "' - Tier #" + tierNumber + " requested but Item does not contain enough tiers!\n" +
					"Item_SO: " + Data.name, this);
		Debug.Log("Item_Information - '" + gameObject.name + "' - Tiers requested but Item field is null!", this);
		return null;
	}

	public int NumberOfTiers ()
	{
		return Tiers().Count;
	}
	#endregion
}