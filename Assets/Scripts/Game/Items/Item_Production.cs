﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;
using ResourcesSystem;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class Item_Production : MonoBehaviour
{
	// Public Data
	public List<CraftedResourceType> ProductionTypes { get; private set; }
	public List<float> ProductionValues { get; private set; }
	public float PollutionAmount { get; private set; }

	public bool ProducedLastCycle { get; private set; }
	
	// References
	private Item_Information Information;
	private Game_Player_Resources Player_Resources;

	#region Setup
	private void Awake ()
	{
		StartCoroutine(Setup());
	}

	private IEnumerator Setup ()
	{
		while (GetComponent<Item_Information>() == null)
			yield return null;
		Information = GetComponent<Item_Information>();

		while (Game_Player_Resources.Instance == null)
			yield return null;
		Player_Resources = Game_Player_Resources.Instance;

		ProducedLastCycle = false;
	}

	public void SetProductionValues (List<CraftedResourceType> productionTypes, List<float> productionValues)
	{
		ProductionTypes = productionTypes;
		ProductionValues = productionValues;
	}

	public void SetPollutionAmount (float amount)
	{
		PollutionAmount = amount;
	}
	#endregion

	#region Public Actions
	public void Produce (bool response)
	{
		if (response == true && ProductionTypes != null && ProductionTypes.Count() > 0)
			Player_Resources.Gain(ProductionTypes, ProductionValues);

		ProducedLastCycle = response;
		Information.Node_Stats.GetPolluted(PollutionAmount);
	}
	#endregion
}