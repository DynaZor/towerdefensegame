﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;
using System.Collections;

public class Item_Sprites : MonoBehaviour
{
	// Private Data
	private Texture2D Texture_Active;
	private Texture2D Texture_InActive;
	private Texture2D Texture_Producing;
	private Texture2D Texture_Electricity;
	private bool SpritesSet = false;
	private bool Ready = false;
	private bool last_Active;

	// Timing
	private int Tick_Producing = 0;

	// References
	[SerializeField] private MeshRenderer Render_Main = null;
	private Item_Information Information;

	#region Setup
	private void Awake ()
	{
		StartCoroutine(Setup());
	}

	private IEnumerator Setup ()
	{
		while (GetComponent<Item_Information>() == null)
			yield return null;
		Information = GetComponent<Item_Information>();
	}

	private void OnDestroy ()
	{
		if (Ready)
		{
			Game_Clock.Instance.Tick_Group6 -= TickUpdate;
			Information.Item_Needs.NeedsMetValueUpdated -= UpdateActiveState;
		}
	}
	#endregion

	#region Loops
	private void TickUpdate (int currentTick)
	{ 
		if (currentTick == Tick_Producing)
		{
			UpdateProducingState();
		}
	}
	#endregion

	#region Public Actions
	public void SetSprites (SO_Item_Tier currentTier)
	{
		Texture_Active = currentTier.Sprites.Active;
		Texture_InActive = currentTier.Sprites.InActive;
		Texture_Producing = currentTier.Sprites.Producing;
		UpdateActiveState(true);
		SpritesSet = true;
		Information.Item_Needs.NeedsMetValueUpdated += UpdateActiveState;
		Tick_Producing = Game_Clock.Instance.GetTickAssignment(6);
		Game_Clock.Instance.Tick_Group6 += TickUpdate;
		Ready = true;
	}
	#endregion

	#region Private Actions
	private void UpdateActiveState(bool itemActive)
	{
		if (itemActive & Render_Main.material.mainTexture != Texture_Producing)
		{
			Render_Main.material.mainTexture = Texture_Active;
		}
		else if (!itemActive)
		{
			Render_Main.material.mainTexture = Texture_InActive;
		}
		last_Active = itemActive;
	}

	private void UpdateProducingState ()
	{
		if (Information.Item_Production.ProducedLastCycle)
			Render_Main.material.mainTexture = Texture_Producing;
	}
	#endregion
}