﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;
using System.Collections;
using UnityEditor;

public class Item_Inspector : MonoBehaviour
{
#if UNITY_EDITOR
	private Item_Information Item_Information;

	private bool Ready = false;

	private void Awake ()
	{
		StartCoroutine(Setup());
	}

	private IEnumerator Setup ()
	{
		while (GetComponent<Item_Information>() == null)
			yield return null;
		Item_Information = GetComponent<Item_Information>();

		Ready = true;
	}

	[CustomEditor(typeof(Item_Inspector))]
	public class Item_InspectorEditor : Editor
	{
		private bool propertiesFoldout = true;
		private bool needsFoldout = true;
		private bool productionFoldout = true;
		private bool combatFoldout = true;

		public override void OnInspectorGUI ()
		{
			if (EditorApplication.isPlaying && ((Item_Inspector) target).Ready)
			{
				Item_Inspector me = (Item_Inspector)target;

				EditorGUILayout.Vector2IntField("Position", new Vector2Int(me.Item_Information.Node.GridPosX, me.Item_Information.Node.GridPosY));
				EditorGUILayout.ToggleLeft("Active", me.Item_Information.Item_Stats.ItemActive);
				EditorGUILayout.LabelField("Item Name", me.Item_Information.Item_Stats.ItemName);
				EditorGUILayout.LabelField("Item Tier", me.Item_Information.Item_Stats.ItemTier.ToString());
				GUI.enabled = me.Item_Information.Item_Stats.ItemTier != me.Item_Information.Tiers().Count - 1;
				if (GUILayout.Button("Upgrade"))
				{
					me.Item_Information.Item_Actions.Upgrade();
				}
				GUI.enabled = true;
				EditorGUILayout.LabelField("Age", me.Item_Information.Item_Stats.Age.ToString());

				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField("Health", me.Item_Information.Item_Stats.CurrentHealth.ToString());
				EditorGUILayout.LabelField("Max", me.Item_Information.Item_Stats.MaxHealth.ToString());
				EditorGUILayout.EndHorizontal();


				propertiesFoldout = EditorGUILayout.BeginFoldoutHeaderGroup(propertiesFoldout,
					"Properties");
				if (propertiesFoldout)
				{
					EditorGUILayout.ToggleLeft("Movable", me.Item_Information.Item_Stats.Movable);
					EditorGUILayout.ToggleLeft("Demolishable", me.Item_Information.Item_Stats.Demolishable);
				}
				EditorGUILayout.EndFoldoutHeaderGroup();

				needsFoldout = EditorGUILayout.BeginFoldoutHeaderGroup(needsFoldout, "Needs");
				if (needsFoldout)
				{
					EditorGUILayout.ToggleLeft("Needs Met", me.Item_Information.Item_Needs.NeedsMet);
					for (int i = 0; i < me.Item_Information.Item_Needs.NodeNeedTypes.Count; i++)
					{
						EditorGUILayout.LabelField(me.Item_Information.Item_Needs.NodeNeedTypes[i].ToString(),
							me.Item_Information.Item_Needs.NodeNeedValues[i].ToString());
					}
					for (int i = 0; i < me.Item_Information.Item_Needs.CraftedNeedTypes.Count; i++)
					{
						EditorGUILayout.LabelField(me.Item_Information.Item_Needs.CraftedNeedTypes[i].ToString(),
							me.Item_Information.Item_Needs.CraftedNeedValues[i].ToString());
					}
				}
				EditorGUILayout.EndFoldoutHeaderGroup();

				productionFoldout = EditorGUILayout.BeginFoldoutHeaderGroup(productionFoldout, "Production");
				if (productionFoldout)
				{
					if (me.Item_Information.Item_Production.ProductionTypes != null)
						for (int i = 0; i < me.Item_Information.Item_Production.ProductionTypes.Count; i++)
						{
							EditorGUILayout.LabelField(me.Item_Information.Item_Production.ProductionTypes[i].ToString(),
								me.Item_Information.Item_Production.ProductionValues[i].ToString());
						}
				}
				EditorGUILayout.EndFoldoutHeaderGroup();

				combatFoldout = EditorGUILayout.BeginFoldoutHeaderGroup(combatFoldout, "Combat");
				if (combatFoldout)
				{
					EditorGUILayout.LabelField("Defence", me.Item_Information.Item_Stats.Defence.ToString());
					EditorGUILayout.ToggleLeft("Can Attack", me.Item_Information.Item_Stats.CanAttack);

					if (me.Item_Information.Item_Stats.CanAttack)
					{
						EditorGUILayout.LabelField("Attack Power", me.Item_Information.Item_Stats.AttackPower.ToString());
						EditorGUILayout.LabelField("Attack Range", me.Item_Information.Item_Stats.AttackRange.ToString());

						EditorGUILayout.BeginHorizontal();
						GUILayout.Label("Attack Timing");
						GUILayout.FlexibleSpace();
						for (int i = 0; i < 12; i++)
						{
							GUILayout.Toggle(me.Item_Information.Item_Stats.AttackTiming.Attack[i], "");
						}
						EditorGUILayout.EndHorizontal();
					}
				}
				EditorGUILayout.EndFoldoutHeaderGroup();
				EditorUtility.SetDirty(target);
			}
			else
			{
				GUILayout.Label("Information about the item will be shown here in Play mode");
			}
		}
	}
#endif
}