﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;
using System.Collections;
using UnityEditor;

public class Item_Tester : MonoBehaviour
{
#if UNITY_EDITOR
	[Header("Tests")]
	public bool Upgrade = false;

	[Header("References")]
	public SO_Item SO_Item;
	public Game_Node_Stats Grid_Node_Stats;

	private Item_Actions Item_Actions;

	#region Setup
	private void Awake ()
	{
		StartCoroutine(Setup());
	}

	private IEnumerator Setup ()
	{
		yield return new WaitUntil(() => GetComponent<Item_Actions>() != null);
		Item_Actions = GetComponent<Item_Actions>();
		yield return new WaitUntil(() => GetComponent<Item_Information>() != null);
		yield return new WaitUntil(() => GetComponent<Item_Stats>() != null);
		yield return new WaitUntil(() => GetComponent<Item_Needs>() != null);
		yield return new WaitUntil(() => GetComponent<Item_Degredation>() != null);
		yield return new WaitUntil(() => GetComponent<Item_Information>() != null);
		yield return new WaitUntil(() => GetComponent<Item_Production>() != null);

		GetComponent<Item_Information>().SetNode(Grid_Node_Stats);
		GetComponent<Item_Information>().SetItemSO(SO_Item);

	}
	#endregion

	private void OnValidate ()
	{
		if (EditorApplication.isPlaying)
		{
			if (Upgrade)
			{
				Item_Actions.Upgrade();
				Upgrade = false;
			}
		}
	}
#endif
}