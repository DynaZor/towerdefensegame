﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;
using ResourcesSystem;
using System.Collections.Generic;

public class Item_Needs : MonoBehaviour
{
	// Data
	public List<NodeResourceType> NodeNeedTypes { get; private set; }
	public List<float> NodeNeedValues { get; private set; }
	public List<CraftedResourceType> CraftedNeedTypes { get; private set; }
	public List<float> CraftedNeedValues { get; private set; }

	// State
	public bool NeedsMet { get; private set; }
	public List<NodeResourceType> NeedsNotMet { get; private set; }

	// Communications
	public delegate void NeedsMetValueUpdatedHandler (bool needsMet);
	public event NeedsMetValueUpdatedHandler NeedsMetValueUpdated = delegate { };

	#region Setup
	public void SetNodeNeeds (List<NodeResourceType> resourceTypes, List<float> needValues)
	{
		NodeNeedTypes = resourceTypes;
		NodeNeedValues = needValues;
	}

	public void SetCraftedNeeds (List<CraftedResourceType> resourceTypes, List<float> needValues)
	{
		CraftedNeedTypes = resourceTypes;
		CraftedNeedValues = needValues;
	}

	private void OnDestroy ()
	{
		NeedsMetValueUpdated = delegate { };
	}
	#endregion

	#region Loops
	public void SetNeedsMet (bool response, List<NodeResourceType> needsNotMet = null)
	{
		NeedsMet = response;
		NeedsMetValueUpdated.Invoke(response);
		NeedsNotMet = needsNotMet;
	}
	#endregion
}