﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;
using ResourcesSystem;
using System.Collections.Generic;
using System.Linq;

public class Game_Item_Manager : MonoBehaviour
{
	#region Singleton
	public static Game_Item_Manager Instance;
	public void MakeSingleton ()
	{
		if (Instance != null)
			Debug.Log("Game_Item_Manager - More than one instance!\n" +
				"Registered Instance: " + Instance.gameObject.name +
				"This Instance: " + gameObject.name, this);
		else
			Instance = this;
	}
	#endregion

	public static SO_Global_Items ItemsDB { get; private set; }

	[SerializeField] private SO_Global_Items _itemsDB = null;
	public List<Item_Actions> Items { get; private set; }

	#region Setup
	private void Awake ()
	{
		MakeSingleton();
		Items = new List<Item_Actions>();
		ItemsDB = _itemsDB;
	}
	#endregion

	#region Public Actions
	#region Item List Management
	public void ItemRegister (Item_Actions item)
	{
		if (!Items.Contains(item))
		{
			Items.Add(item);
		}
	}

	public void ItemDestroyed (Item_Actions item)
	{
		if (Items.Contains(item))
		{
			Items.Remove(item);
		}
		Game_Player_Resources.Instance.CheckForResourcesExceedingCapacity();
	}
	#endregion

	public float GetElectricityCapacity ()
	{
		float capacity = 0;
		for (int i = 0; i < Items.Count; i++)
		{
			if (Items[i].Information.Item_Stats.ItemActive)
				capacity += Items[i].Information.Item_Stats.ElectricityCapacity;
		}
		return capacity;
	}
	#endregion


}