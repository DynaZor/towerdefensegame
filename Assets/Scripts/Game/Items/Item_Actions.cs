﻿// Code written by Shahar DynaZor Alon Use of this code outside of this project
// without Shahar's permission is prohibited!
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ResourcesSystem;
using UnityEngine;

//using System.Linq;

public class Item_Actions : MonoBehaviour, IItem_Actions
{
	#region Timing
	private int Tick_Heal = 0;
	private int Tick_Move = 0;
	private int Tick_Attack = 0;
	private int Tick_AskResources = 0;

	private static readonly float timefor_Damage = 3f;
	private static readonly float timefor_Heal = 5f;
	private static readonly float timefor_Attack = 4f;
	private static readonly float timefor_Upgraded = 5f;
	#endregion Timing

	#region Data

	// Public Data
	public bool IsMoving => !IsWaiting && !IsAttackingWithoutMoving && path.Count > 0;

	public bool IsAttackingWithoutMoving { get; private set; }
	public bool IsAttacking { get; private set; }
	public bool IsWaiting { get; private set; }
	public bool HasTarget { get; private set; }
	public bool TookDamage
	{
		get
		{
			return Time.time - lastTookDamage < timefor_Damage;
		}
	}
	public bool Healed
	{
		get
		{
			return Time.time - lastHealed < timefor_Heal;
		}
	}
	public bool Attacked
	{
		get
		{
			return Time.time - lastAttacked < timefor_Attack;
		}
	}
	public bool Upgraded
	{
		get
		{
			return Time.time - lastUpgraded < timefor_Upgraded;
		}
	}

	private float lastTookDamage = -100f;
	private float lastHealed = -100f;
	private float lastAttacked = -100f;
	private float lastUpgraded = -100f;
	private Coroutine updateTookDamage = null;
	private Coroutine updateHealed = null;
	private Coroutine updateAttacked = null;
	private Coroutine updateUpgraded = null;

	private IEnumerator UpdateInfoDelay_damage ()
	{
		yield return new WaitForSeconds(timefor_Damage);
		ItemStatusUpdated.Invoke();
	}
	private IEnumerator UpdateInfoDelay_heal ()
	{
		yield return new WaitForSeconds(timefor_Heal);
		ItemStatusUpdated.Invoke();
	}
	private IEnumerator UpdateInfoDelay_attack ()
	{
		yield return new WaitForSeconds(timefor_Attack);
		ItemStatusUpdated.Invoke();
	}
	private IEnumerator UpdateInfoDelay_upgrade ()
	{
		yield return new WaitForSeconds(timefor_Upgraded);
		ItemStatusUpdated.Invoke();
	}

	// Private Data
	private bool Ready = false;

	private Queue<Game_Node> path = new Queue<Game_Node>();
	private int pathScore;
	private bool needsNewPath = false;
	private Game_Node targetNode;

	#endregion Data

	#region Communications

	public delegate void ItemStatusUpdateCaller ();
	public event ItemStatusUpdateCaller ItemStatusUpdated = delegate { };

	#endregion

	#region References

	public Item_Information Information { get; private set; }

	#endregion References

	#region Setup

	private void Awake ()
	{
		StartCoroutine(Setup());
	}

	private IEnumerator Setup ()
	{
		while (GetComponent<Item_Information>() == null)
			yield return null;
		Information = GetComponent<Item_Information>();

		while (Information.Data == null)
			yield return null;

		bool waitFor = ItemSetup();

		while (Game_Clock.Instance == null)
			yield return null;
		Tick_Move = Game_Clock.Instance.GetTickAssignment(2);
		Tick_Attack = Game_Clock.Instance.GetTickAssignment(3);
		Tick_AskResources = Game_Clock.Instance.GetTickAssignment(4);
		Tick_Heal = Game_Clock.Instance.GetTickAssignment(6);
		Game_Clock.Instance.Tick_Group2 += TickUpdate_Group2;
		Game_Clock.Instance.Tick_Group3 += TickUpdate_Group3;
		Game_Clock.Instance.Tick_Group4 += TickUpdate_Group4;
		Game_Clock.Instance.Tick_Group6 += TickUpdate_Group6;

		Information.Item_Stats.HealthChangedEvent += CheckIfDestroyed;
		Ready = true;
	}

	private bool ItemSetup ()
	{
		Information.Item_Needs.SetNodeNeeds(Information.Data.Tiers[0].NodeNeedTypes, Information.Data.Tiers[0].NodeNeedValues);
		Information.Item_Needs.SetCraftedNeeds(Information.Data.Tiers[0].CraftedNeedTypes, Information.Data.Tiers[0].CraftedNeedValues);
		Information.Item_Stats.SetStats(Information.Data, 0, true);
		Information.Item_Sprites.SetSprites(Information.Data.Tiers[0]);
		Information.Item_Stats.EnableItem();
		Game_Item_Manager.Instance.ItemRegister(this);
		return true;
	}

	private void OnDestroy ()
	{
		if (Ready)
		{
			Game_Clock.Instance.Tick_Group2 -= TickUpdate_Group2;
			Game_Clock.Instance.Tick_Group3 -= TickUpdate_Group3;
			Game_Clock.Instance.Tick_Group4 -= TickUpdate_Group4;
			Game_Clock.Instance.Tick_Group6 -= TickUpdate_Group6;
		}
	}

	#endregion Setup

	#region Loops

	private void TickUpdate_Group2 (int currentTick)
	{
		if (Information.Item_Stats.ItemActive)
		{
			if (Tick_Move == currentTick)
			{
				MoveNextStep();
			}
		}
	}

	private void TickUpdate_Group3 (int currentTick)
	{
		if (Information.Item_Stats.ItemActive)
		{
			if (Tick_Attack == currentTick && Information.Item_Stats.CanAttack)
			{
				CheckForEnemiesInRange();
			}
		}
	}

	private void TickUpdate_Group4 (int currentTick)
	{
		if (Information.Item_Stats.ItemActive)
		{
			if (Tick_AskResources == currentTick)
			{
				AskForResources();
			}
		}
	}

	private void TickUpdate_Group6 (int currentTick)
	{
		if (Information.Item_Stats.ItemActive)
		{
			if (Tick_Heal == currentTick)
			{
				Heal();
			}
		}
	}

	private void AskForResources ()
	{
		ResourcesRequest resourceRequest = new ResourcesRequest(this, Information.Node_Stats,
			Information.Item_Needs.NodeNeedTypes, Information.Item_Needs.NodeNeedValues,
			Information.Item_Needs.CraftedNeedTypes, Information.Item_Needs.CraftedNeedValues);

		resourceRequest.SendToQueue();
	}

	public void GotResources (bool response)
	{
		Information.Item_Needs.SetNeedsMet(response);
		Information.Item_Degredation.NeedsMet(response);
		Information.Item_Production.Produce(response);
	}

	#endregion Loops

	#region Self Actions

	private void GridUpdated ()
	{
		if (path != null && path.Count > 0)
		{
			CheckForBetterPath();
		}
	}

	public void SetTarget (Game_Node targetNode)
	{
		UnityEngine.Profiling.Profiler.BeginSample("Item_Actions: SetTarget");
		if (this.targetNode != targetNode)
		{
			if (Information == null || Information.Node == null || Game_PathFinder.Instance == null)
			{
				StartCoroutine(delayedSetTarget(targetNode));
			}

			this.targetNode = targetNode;
			if (targetNode != null)
			{
				FindPath(out Vector2Int[] newPath, out int newPathScore);
				ApplyPath(newPath, newPathScore);
			}
			else
			{
				ClearPath();
			}

			HasTarget = this.targetNode != null;

			Information.Item_Stats.HasTargetChanged();
		}

		ItemStatusUpdated.Invoke();

		UnityEngine.Profiling.Profiler.EndSample();
	}

	private void MoveNextStep ()
	{
		UnityEngine.Profiling.Profiler.BeginSample("Item_Actions: MoveNextStep");
		if (path == null)
		{
			path = new Queue<Game_Node>();
			needsNewPath = true;
		}

		if (targetNode == null)
		{
			goto update;
		}

		if (needsNewPath)
		{
			needsNewPath = false;
			SetTarget(path.ToArray()[path.Count - 1]);
		}

		if (path.Count > 0)
		{
			CheckForBetterPath();
			Game_Node node = path.ToArray()[0];
			if (node.Enemy == null && node.Item == null)
			{
				MoveInitiate();
				IsWaiting = false;
				goto update;
			}
			else if (node.Enemy != null)
			{
				IsAttacking = true;
				IsAttackingWithoutMoving = true;
				IsWaiting = false;
				goto update;
			}
			else // if Ally meeting Ally
				IsWaiting = true;
			IsAttacking = false;
		}
		else
		{
			targetNode = null;
		}
		IsAttackingWithoutMoving = false;

		update:
		ItemStatusUpdated.Invoke();
		UnityEngine.Profiling.Profiler.EndSample();
	}

	/*private bool PathIsBlocked ()
	{
	}*/

	private void MoveInitiate ()
	{
		Game_Node nextNode = path.Dequeue();
		Vector3 newPos = Game_Grid.Instance.WorldPointFromNode(nextNode.GridPosX, nextNode.GridPosY, true);
		transform.position = newPos;
		Information.Node.SetItem(null);
		nextNode.SetItem(Information);
		Information.SetNode(nextNode.Stats);
	}

	public bool? UpgradePossible ()
	{
		int currentTier = Information.Item_Stats.ItemTier;
		int nextTier = currentTier + 1;
		if (nextTier < Information.NumberOfTiers())
		{
			return Information.Tiers()[nextTier].Cost.PlayerCanBuy();
		}
		else
		{
			return null;
		}
	}

	public void Upgrade ()
	{
		int currentTier = Information.Item_Stats.ItemTier;
		int nextTier = currentTier + 1;
		if (nextTier < Information.NumberOfTiers())
		{
			Information.Item_Stats.SetStats(Information.Data, nextTier, false);
			Information.Item_Needs.SetNodeNeeds(Information.Data.Tiers[nextTier].NodeNeedTypes, Information.Data.Tiers[nextTier].NodeNeedValues);
			Information.Item_Needs.SetCraftedNeeds(Information.Data.Tiers[nextTier].CraftedNeedTypes, Information.Data.Tiers[nextTier].CraftedNeedValues);
			Information.Item_Sprites.SetSprites(Information.Data.Tiers[nextTier]);

			lastUpgraded = Time.time;
			if (updateUpgraded != null)
				StopCoroutine(updateUpgraded);
			updateUpgraded = StartCoroutine(UpdateInfoDelay_upgrade());
			ItemStatusUpdated.Invoke();
		}
		else
		{
			Debug.Log("Item_Actions - " + gameObject.name + " - Requested Upgrade but next tier doesn't exist", this);
		}
		UI_Game_InfoPanel_Item.Instance.Event_ItemUpdgraded(Information.Item_Stats);
	}

	public void Dismantle ()
	{
		Information.Item_Stats.DismantleValue(out List<CraftedResourceType> types, out List<float> amounts);
		Game_Player_Resources.Instance.Gain(types, amounts);
		GetDestroyed();
	}

	public void GetDestroyed ()
	{
		if (GetComponent<Item_PlayerBase>() != null)
		{
			GetComponent<Item_PlayerBase>().BaseDestroyed();
		}

		Information.Node_Stats.Node.ItemDestroyed();
		Game_Item_Manager.Instance.ItemDestroyed(this);
		Destroy(gameObject);
	}

	#endregion Self Actions

	#region Health Actions
	public void TakeDamage (float damageAmount)
	{
		Information.Item_Stats.Health_Subtract(damageAmount);

		lastTookDamage = Time.time;
		if (updateTookDamage != null)
			StopCoroutine(updateTookDamage);
		updateTookDamage = StartCoroutine(UpdateInfoDelay_damage());
		ItemStatusUpdated.Invoke();
	}

	private void CheckIfDestroyed (float health)
	{
		if (health <= 0f)
			GetDestroyed();
	}

	public void Heal ()
	{   // Assuming item is Active and Needs Met for this refresh cycle
		if (Information.Item_Needs.NeedsMet && Information.Item_Stats.CurrentHealth < Information.Item_Stats.MaxHealth)
		{
			float currentHealth = Information.Item_Stats.CurrentHealth;
			float healRate = Gameplay_Values.Instance.ItemHealBaseRate;
			float maxHealth = Information.Item_Stats.MaxHealth;

			Information.Item_Stats.Health_Add(healRate);
			lastHealed = Time.time;
			if (updateHealed != null)
				StopCoroutine(updateHealed);
			updateHealed = StartCoroutine(UpdateInfoDelay_heal());
			ItemStatusUpdated.Invoke();
		}
	}

	#endregion Health Actions

	#region Combat Actions

	private void CheckForEnemiesInRange ()
	{
		var gg = Game_Grid.Instance;
		UnityEngine.Profiling.Profiler.BeginSample("Item_Actions: CheckForAlliesInRange");
		if (path.Count > 0 && path.ToArray()[0].Enemy != null) // Check for enemy in next move tile
		{
			AttackEnemy(path.ToArray()[0].Enemy.Enemy_Actions);
			IsAttacking = true;
		}
		else // Check for enemies in range
		{
			List<Game_Enemy_Information> enemiesInRange = new List<Game_Enemy_Information>();
			List<Game_Node> nodes = gg.GetNodesFromPositions(gg.GetSurroundingNodePositions(Information.Node.NodePos, 1, false));
			for (int i = 0; i < nodes.Count; i++)
			{
				if (nodes[i].Enemy != null)
				{
					enemiesInRange.Add(nodes[i].Enemy);
				}
			}

			// If found enemies in range
			if (enemiesInRange.Count > 0)
			{
				// Check for closest enemy to base
				IsAttacking = true;
				int closestEnemyToBase_dist = 9999;
				int closestEnemyToBase_index = 9999;
				for (int i = 0; i < enemiesInRange.Count; i++)
				{
					int dist = Mathf.Abs(enemiesInRange[i].Node.GridPosX - Item_PlayerBase.Instance.Node.GridPosX);
					dist += Mathf.Abs(enemiesInRange[i].Node.GridPosY - Item_PlayerBase.Instance.Node.GridPosY);
					if (dist < closestEnemyToBase_dist)
					{
						closestEnemyToBase_dist = dist;
						closestEnemyToBase_index = i;
					}
				}
				// Attack closest enemy to base
				AttackEnemy(enemiesInRange[closestEnemyToBase_index].Enemy_Actions);

				goto end;
			}
		}
		IsAttacking = false;

		end:
		UnityEngine.Profiling.Profiler.EndSample();
	}

	private void AttackEnemy (Game_Enemy_Actions enemy)
	{
		enemy.TakeDamage(Information.Item_Stats.AttackPower);

		lastAttacked = Time.time;
		if (updateAttacked != null)
			StopCoroutine(updateAttacked);
		updateAttacked = StartCoroutine(UpdateInfoDelay_attack());
		ItemStatusUpdated.Invoke();
	}

	#endregion Combat Actions

	#region PF Helper Methods

	private void FindPath (out Vector2Int[] newPath, out int newPathScore)
	{
		Vector2Int currentPos = Information.Node.NodePos;
		Vector2Int targetPos = targetNode.NodePos;
		newPath = Game_PathFinder.FindPath(currentPos, targetPos, 0, out newPathScore);
		if (newPath.Contains(Information.Node.NodePos))
		{
			var temp = newPath.ToList();
			temp.Remove(Information.Node.NodePos);
			newPath = temp.ToArray();
		}
	}

	private void CheckForBetterPath ()
	{
		FindPath(out Vector2Int[] newPath, out int newPathScore);
		if (newPathScore < pathScore)
		{
			ApplyPath(newPath, newPathScore);
		}
	}

	private void ClearPath ()
	{
		if (path != null)
		{
			if (path.Count > 0)
			{
				while (path.Count > 0)
				{
					path.Dequeue().ItemsUpdatedEvent -= GridUpdated;
				}
			}
			path.Clear();
		}
		else
		{
			path = new Queue<Game_Node>();
		}
	}

	private void ApplyPath (Vector2Int[] newPath, int newPathScore)
	{
		ClearPath();
		for (int i = newPath.Length - 1; i >= 0; i--)
		{
			Game_Node node = Game_Grid.Instance.NodeAt(newPath[i]);
			node.ItemsUpdatedEvent += GridUpdated;
			path.Enqueue(node);
		}
		pathScore = newPathScore;
	}

	private IEnumerator delayedSetTarget (Game_Node targetNode)
	{
		Debug.Log("Delaying setting target", this);
		while (Information == null)
			yield return null;
		while (Information.Node == null)
			yield return null;
		while (Game_PathFinder.Instance == null)
			yield return null;

		SetTarget(targetNode);
	}

	#endregion PF Helper Methods
}