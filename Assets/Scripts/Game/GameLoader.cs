﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class GameLoader : MonoBehaviour
{
	#region Singleton
	public static GameLoader Instance;
	private void MakeSingleton ()
	{
		if (Instance != null)
			Debug.Log("GameLoader - More than one instance!\n" +
				"Registered Instance: " + Instance.gameObject.name +
				"This Instance: " + gameObject.name, this);
		else
			Instance = this;
	}
	#endregion

	[Header("Loader Settings")]
	[SerializeField] private string NewGameText = "New Game";
	[SerializeField] private string LoadingGameText = "Saved Game";

	[Header("Progress Bar and Text Settings")]
	[SerializeField] private float changeRate = 0.1f;
	[SerializeField] private int loadingPercent_GridGeneration = 25;
	private float loadingPercent_GridGenerationFloat => loadingPercent_GridGeneration * 0.01f;
	private float loadingPercent_SceneLoadFloat => 1f - loadingPercent_GridGenerationFloat;

	[Header("Fadeout Settings")]
	[SerializeField] private float fadeoutLengthInSeconds = 1.5f;
	[SerializeField] private float fadeinLengthInSeconds = 1.5f;

	[Header("References")]
	[SerializeField] private Animator[] Animators = System.Array.Empty<Animator>();
	[SerializeField] private TextMeshProUGUI progressText = null;
	[SerializeField] private TextMeshProUGUI typeText = null;
	[SerializeField] private Slider slider = null;
	[SerializeField] private Canvas canvas = null;
	//[SerializeField] private Animator backgroundImageAnimator = null;
	[SerializeField] private Camera LoaderCamera = null;

	// Private Data
	private AsyncOperation operation;
	private Coroutine updateProgressBarCoroutine;

	#region Setup
	private void Awake ()
	{
		MakeSingleton();
		//DontDestroyOnLoad(gameObject);
		canvas.gameObject.SetActive(false);
	}
	#endregion

	#region Public Actions
	public void NewGame (DifficultySetting difficulty)
	{
		typeText.text = NewGameText;
		StartCoroutine(BeginLoad(true, difficulty));
	}

	public void LoadGame(/*Game Data*/)
	{
		typeText.text = LoadingGameText;
		StartCoroutine(BeginLoad(false));
	}
	#endregion

	#region Private Actions
	private IEnumerator BeginLoad (bool NewGame, DifficultySetting difficulty = null/*, GameData data = null*/)
	{
		canvas.gameObject.SetActive(true);
		UpdateProgressUI(0);
		var lastActiveScene = SceneManager.GetActiveScene();
		SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(Global_SceneNumbers.Loader));
		if (Game_Speed.Instance != null)
		{
			Game_Speed.Instance.SetSpeed(Game_Speed.Speed.Pause);
		}
		else
		{
			Time.timeScale = 0f;
		}

		yield return StartCoroutine(Fade(true));
		SceneManager.UnloadSceneAsync(lastActiveScene);
		LoaderCamera.tag = "MainCamera";
		operation = SceneManager.LoadSceneAsync(Global_SceneNumbers.InGame, LoadSceneMode.Additive);

		while (!operation.isDone)
		{
			UpdateProgressUI(operation.progress * loadingPercent_SceneLoadFloat);
			yield return null;
		}
		UpdateProgressUI(operation.progress * loadingPercent_SceneLoadFloat);

		while (Game_Manager.Instance == null)
			yield return null;

		LoaderCamera.tag = "Untagged";
		Game_Manager.Instance.GameWorld_3DCamera.gameObject.SetActive(true);
		SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(Global_SceneNumbers.InGame));

		while (Game_Grid.Instance == null)
			yield return null;
		Game_Grid.Loading_GridCreationEvent += UpdateProgressUI_GridGeneration;
		if (NewGame)
		{
			Game_Grid.Instance.NewGame();
		}
		else
		{
			Game_Grid.Instance.LoadGame();
		}
		while (!Game_Grid.Instance.Ready)
			yield return null;
		Game_Grid.Loading_GridCreationEvent -= UpdateProgressUI_GridGeneration;
		while (Game_Speed.Instance == null)
			yield return null;
		operation = null;
		Game_Speed.Instance.SetSpeed(Game_Speed.Speed.Pause);

		while (Game_Player_Resources.Instance == null)
			yield return null;

		while (Difficulty_Manager.Instance == null)
			yield return null;

		if (NewGame)
		{
			Game_Player_Resources.Instance.NewGame();
			difficulty.Player_StartingResources.PlayerGain();

			Difficulty_Manager.Instance.SetDifficulty(difficulty);

			Game_Manager.Instance.SetCurrentAge(AgesSystem.Age.Stone);
		}
		else
		{
			Game_Player_Resources.Instance.LoadData(null);
		}

		// 0POC Make loading smoother - Node Controller Loading
		while (Visual_Game_NodeController.Instance == null)
			yield return null;
		Visual_Game_NodeController.Instance.IssueUpdate();
		while (!Visual_Game_NodeController.Instance.Preparation_ItemTexturesSet 
			|| !Visual_Game_NodeController.Instance.Preparation_NodeTexturesSet 
			|| !Visual_Game_NodeController.Instance.Preparation_SelectionsColorTextureSet)
			yield return null;
		yield return null;
		UpdateProgressUI(1f);
		yield return null;

		yield return StartCoroutine(Fade(false));
		SceneManager.UnloadSceneAsync(Global_SceneNumbers.Loader);
	}

	private void UpdateProgressUI (float progress)
	{
		if (updateProgressBarCoroutine != null)
			StopCoroutine(updateProgressBarCoroutine);
		updateProgressBarCoroutine = StartCoroutine(SmoothProgressBar(progress));
	}

	private void UpdateProgressUI_GridGeneration (float progress)
	{
		UpdateProgressUI(loadingPercent_SceneLoadFloat + (progress * loadingPercent_GridGenerationFloat));
	}

	private IEnumerator SmoothProgressBar(float targetPercent)
	{
		float currentPercent = slider.value / slider.maxValue;
		float change = (targetPercent - currentPercent) * changeRate;
		while (currentPercent <= targetPercent)
		{
			currentPercent += change;
			if (currentPercent > targetPercent)
				currentPercent = targetPercent;
			slider.value = (int) (currentPercent * slider.maxValue);
			progressText.text = (int) (currentPercent * 100f) + "%";
			yield return null;
		}
	}

	private void FadeOut ()
	{
		for (int i = 0; i < Animators.Length; i++)
		{
			Animators[i].SetTrigger("DoneLoading");
		}
	}

	private void FadeIn()
	{
		for (int i = 0; i < Animators.Length; i++)
		{
			Animators[i].SetTrigger("StartLoading");
		}
	}

	private IEnumerator Fade(bool fadeIn, bool setInActive = false)
	{
		if (fadeIn)
			canvas.gameObject.SetActive(true);
		
		if (fadeIn)
			FadeIn();
		else
			FadeOut();

		yield return new WaitForSecondsRealtime(fadeIn ? fadeinLengthInSeconds : fadeoutLengthInSeconds);

		if (setInActive && !fadeIn)
			canvas.gameObject.SetActive(false);
	}
	#endregion
}