﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using System.Collections;
using UnityEngine;
using AgesSystem;

public class Game_Manager : MonoBehaviour
{
	#region Singleton
	public static Game_Manager Instance;
	private void Awake ()
	{
		if (Instance != null)
			Debug.Log("Game_Manager - More than one instance!\n" +
				"Registered Instance: " + Instance.gameObject.name +
				"This Instance: " + gameObject.name, this);
		else
			Instance = this;
	}
	#endregion

	// Age
	public Age CurrentAge { get; private set; }
	public delegate void CurrentAgeChangedHandler (Age newAge);
	public event CurrentAgeChangedHandler CurrentAgeChangedEvent = delegate { };

	[Header("Public References")]
	public Camera GameWorld_3DCamera = null;

	#region Setup
	private void Start ()
	{
		StartCoroutine(Setup());
	}

	private IEnumerator Setup ()
	{
		while (Game_Clock.Instance == null)
			yield return null;
		Game_Clock.Instance.StartClock();
	}
	#endregion

	public void SetCurrentAge (Age newAge)
	{
		CurrentAge = newAge;
		CurrentAgeChangedEvent.Invoke(newAge);
	}
}