﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!

using UnityEngine;

public class Difficulty_Manager : MonoBehaviour
{
	#region Singleton
	public static Difficulty_Manager Instance;
	private void MakeSingleton ()
	{
		if (Instance != null)
			Debug.Log("Difficulty_Manager - More than one instance!\n" +
				"Registered Instance: " + Instance.gameObject.name +
				"This Instance: " + gameObject.name, this);
		else
			Instance = this;
	}
	#endregion

	public static DifficultySetting CurrentDifficulty { get; private set; }
	public Gameplay_Difficulties Difficulties;

	#region Setup
	private void Awake ()
	{
		MakeSingleton();
	}
	#endregion

	#region Public Actions
	public void SetDifficulty(int index)
	{
		CurrentDifficulty = Difficulties.Difficulties[index];
	}

	public void SetDifficulty (DifficultySetting difficulty)
	{
		CurrentDifficulty = difficulty;
	}
	#endregion
}