﻿// Code written by Shahar DynaZor Alon Use of this code outside of this project
// without Shahar's permission is prohibited!
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Events;

public class Game_Control_Mouse : MonoBehaviour
{
	#region Singleton
	public static Game_Control_Mouse Instance;
	private void MakeSingleton ()
	{
		if (Instance != null)
			Debug.Log("Game_Control_Mouse - More than one instance!\n" +
				"Registered Instance: " + Instance.gameObject.name +
				"This Instance: " + gameObject.name, this);
		else
			Instance = this;
	}
	#endregion

	#region Types
	public enum MouseClick
	{
		Left, Right, Wheel, ScrollUp, ScrollDown
	}
	#endregion

	#region Events
	/// MouseClick type
	/// bool isOverUI
	/// int2 Position (Pixels)
	/// float2 Position (Relative)
	public static event Action<MouseClick, bool, int2, float2> OnMouse_Click;
	public static event Action<MouseClick, bool, int2, float2> OnMouse_HoldStart;
	public static event Action<MouseClick, bool, int2, float2> OnMouse_Hold;
	public static event Action<MouseClick, bool, int2, float2> OnMouse_HoldEnd;

	/// MouseClick type
	/// int2 overNode
	public static event Action<MouseClick, Vector2Int?> OnMouse_Grid_Click;
	public static event Action<MouseClick, Vector2Int?> OnMouse_Grid_HoldStart;
	public static event Action<MouseClick, Vector2Int?> OnMouse_Grid_Hold;
	public static event Action<MouseClick, Vector2Int?> OnMouse_Grid_HoldEnd;
	#endregion

	#region Private Data
	private int2 screenSize;
	private Rect screenRect;

	#region Communication
	private static List<IMouseControllable> controllables = new List<IMouseControllable>();
	#endregion

	#region Basic Mouse Data
	#region This Frame
	public static float3 mousePosition { get; private set; }
	public static float2 mousePositionRelative { get; private set; }
	// Mouse Place
	public static bool IsInScreenSpace { get; private set; }
	public static bool IsOverUI { get; private set; }
	public static bool IsOverGrid { get; private set; }
	// Mouse Down this frame
	public static bool MouseButtonDown0  { get; private set; } // Left
	public static bool MouseButtonDown1  { get; private set; } // Right
	public static bool MouseButtonDown2  { get; private set; } // Wheel
	// Mouse HoldDown this frame		  
	public static bool MouseButtonHold0  { get; private set; } // Left
	public static bool MouseButtonHold1  { get; private set; } // Right
	public static bool MouseButtonHold2  { get; private set; } // Wheel
	// Mouse Up this frame
	public static bool MouseButtonUp0  { get; private set; } // Left
	public static bool MouseButtonUp1  { get; private set; } // Right
	public static bool MouseButtonUp2  { get; private set; } // Wheel
	// Mouse Wheel this frame
	public static Vector2 MouseScrollDelta;
	public static bool MouseScrollUp { get; private set; }
	public static bool MouseScrollDown { get; private set; }
	#endregion

	#region Last Frame
	private float3  last_mousePosition;
	private float2  last_mousePositionRelative;
	// Mouse Place
	private bool    last_isInScreenSpace = false;
	private bool    last_isOverUI = false;
	private bool    last_isOverGrid = false;
	// Mouse Down last frame
	private bool    last_MouseButtonDown0 = false; // Left
	private bool    last_MouseButtonDown1 = false; // Right
	private bool    last_MouseButtonDown2 = false; // Wheel
												   // Mouse HoldDown last frame
	private bool    last_MouseButtonHold0 = false; // Left
	private bool    last_MouseButtonHold1 = false; // Right
	private bool    last_MouseButtonHold2 = false; // Wheel
												   // Mouse Up last frame
	private bool    last_MouseButtonUp0 = false; // Left
	private bool    last_MouseButtonUp1 = false; // Right
	private bool    last_MouseButtonUp2 = false; // Wheel
												 // Mouse Wheel this frame
	private Vector2 last_MouseScrollDelta;
	private bool last_MouseScrollUp = false;
	private bool last_MouseScrollDown = false;
	#endregion
	#endregion

	#region Advanced Mouse Data
	Vector3 MouseDrag_HitPosition;
	Vector2Int? MouseOverNode;
	#endregion

	#endregion

	private void Awake ()
	{
		screenRect = new Rect(0, 0, Screen.width, Screen.height);
		MakeSingleton();
	}

	private void Update ()
	{
		UnityEngine.Profiling.Profiler.BeginSample("Control_Mouse");

		SetLastFrame();

		SetCurrentFrame();

		if (IsInScreenSpace)
		{
			if (!IsOverUI)
			{
				if (MouseScrollDown)
					OnMouse_Grid_Click?.Invoke(MouseClick.ScrollDown, MouseOverNode);
				else if (MouseScrollUp)
					OnMouse_Grid_Click?.Invoke(MouseClick.ScrollUp, MouseOverNode);

				if (IsOverGrid)
				{
					// Drag for Camera Pan
					// Click Node
					// Differentiate Click and Drag
				}
			}
		}

		for (int i = 0; i < controllables.Count; i++)
		{
			controllables[i].GetMouseData();
		}

		UnityEngine.Profiling.Profiler.EndSample();
	}

	#region Private Actions
	private void SetCurrentFrame ()
	{
		// Update Mouse Position
		mousePosition = Input.mousePosition;
		// Update current screenRect
		screenSize = new int2(Screen.width, Screen.height);
		screenRect.width = screenSize.x;
		screenRect.height = screenSize.y;
		IsInScreenSpace = screenRect.Contains(mousePosition);
		// Update Mouse Position Relative
		mousePositionRelative = mousePosition.xy / screenSize;

		if (!IsInScreenSpace)
		{
			AllFalse();
		}
		else
		{
			// Update Mouse over UI
			IsOverUI = UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject();
			// Update Mouse Over Grid
			if (!IsOverUI)
			{
				GetHoveredNode();
			}
			else
			{
				IsOverGrid = false;
			}

			// Update Mouse Buttons Down this frame
			MouseButtonDown0 = Input.GetMouseButtonDown(0);
			MouseButtonDown1 = Input.GetMouseButtonDown(1);
			MouseButtonDown2 = Input.GetMouseButtonDown(2);
			// Update Mouse Buttons Hold Down this frame
			MouseButtonHold0 = Input.GetMouseButton(0);
			MouseButtonHold1 = Input.GetMouseButton(1);
			MouseButtonHold2 = Input.GetMouseButton(2);
			// Update Mouse Buttons Hold Down this frame
			MouseButtonUp0 = Input.GetMouseButtonUp(0);
			MouseButtonUp1 = Input.GetMouseButtonUp(1);
			MouseButtonUp2 = Input.GetMouseButtonUp(2);
			// Update Mouse Scroll
			MouseScrollDelta = Input.mouseScrollDelta;
			if (MouseScrollDelta != Vector2.zero)
			{
				if (MouseScrollDelta.y != 0)
				{
					if (MouseScrollDelta.y > 0)
					{
						MouseScrollDown = true;
					}
					else if (MouseScrollDelta.y < 0)
					{
						MouseScrollUp = true;
					}
				}
			}
		}
	}

	private void GetHoveredNode ()
	{
		Vector3 mousePos = Camera.main.ScreenToWorldPoint(mousePosition);
		int currentX = Mathf.FloorToInt(mousePos.x + (0.5f * Game_Grid.Instance.GridSizeX));
		int currentY = Mathf.FloorToInt(mousePos.y + (0.5f * Game_Grid.Instance.GridSizeY));
		Vector2Int nodePos = new Vector2Int(currentX, currentY);

		if (Game_Grid.Instance.DoesNodeExistAt(nodePos))
		{
			IsOverGrid = true;
			MouseOverNode = nodePos;
		}
		else
		{
			IsOverGrid = false;
			MouseOverNode = null;
		}
	}

	private void SetLastFrame ()
	{
		last_mousePosition = mousePosition;
		last_mousePositionRelative = mousePositionRelative;

		last_isInScreenSpace = IsInScreenSpace;
		last_isOverUI = IsOverUI;
		last_isOverGrid = IsOverGrid;

		last_MouseButtonDown0 = MouseButtonDown0;
		last_MouseButtonDown1 = MouseButtonDown1;
		last_MouseButtonDown2 = MouseButtonDown2;

		last_MouseButtonHold0 = MouseButtonHold0;
		last_MouseButtonHold1 = MouseButtonHold1;
		last_MouseButtonHold2 = MouseButtonHold2;

		last_MouseButtonUp0 = MouseButtonUp0;
		last_MouseButtonUp1 = MouseButtonUp1;
		last_MouseButtonUp2 = MouseButtonUp2;

		last_MouseScrollDelta = MouseScrollDelta;
		last_MouseScrollUp = MouseScrollUp;
		last_MouseScrollDown = MouseScrollDown;
	}

	private void AllFalse ()
	{
		IsOverUI = false;
		last_isOverGrid = false;

		MouseButtonDown0 = false;
		MouseButtonDown1 = false;
		MouseButtonDown2 = false;

		MouseButtonHold0 = false;
		MouseButtonHold1 = false;
		MouseButtonHold2 = false;

		MouseButtonUp0 = false;
		MouseButtonUp1 = false;
		MouseButtonUp2 = false;

		MouseScrollDelta = Vector2.zero;
		MouseScrollUp = false;
		MouseScrollDown = false;

		MouseOverNode = null;
	}
	#endregion

	#region Public Actions
	public static void RegisterControllable (IMouseControllable item)
	{
		if (item != null)
		{
			if (!controllables.Contains(item))
			{
				controllables.Add(item);
			}
		}
	}

	public static void UnregisterControllable (IMouseControllable item)
	{
		if (item != null)
		{
			if (controllables.Contains(item))
			{
				controllables.Remove(item);
			}
		}

	}
	#endregion
}

public interface IMouseControllable
{
	void GetMouseData ();
}