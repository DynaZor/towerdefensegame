﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using ResourcesSystem;
using System.Collections;
using UnityEngine;

public class Game_Enemy_Information : MonoBehaviour
{
	// Public Data
	public SO_Enemy Data { get; private set; }

	public Game_Enemy_Actions Enemy_Actions = null;
	public Game_Enemy_Stats Enemy_Stats = null;

	public Game_Node_Stats Node_Stats { get; private set; }
	public Game_Node Node { get; private set; }

	// States
	public bool Ready { get; private set; }

	#region Setup
	private void Awake ()
	{
		Ready = false;
		StartCoroutine(Setup());
	}

	private IEnumerator Setup ()
	{
		while (Node_Stats == null)
			yield return null;
		while (Data == null)
			yield return null;

		while (GetComponent<Game_Enemy_Actions>() == null)
			yield return null;
		Enemy_Actions = GetComponent<Game_Enemy_Actions>();
		
		Ready = true;
	}

	public void SetEnemySO (SO_Enemy enemySO)
	{
		Data = enemySO;
		Enemy_Stats.SetInformation(this);
		Enemy_Stats.SetStats(Data, true);
	}

	public void SetNode (Game_Node_Stats node_stats)
	{
		Node_Stats = node_stats;
		Node = node_stats.Node;
		Node.SetEnemy(this);
	}
	#endregion
}