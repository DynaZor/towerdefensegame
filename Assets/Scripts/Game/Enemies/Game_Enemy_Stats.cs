﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;
using AgesSystem;
using ResourcesSystem;
using System.Collections;

public class Game_Enemy_Stats : MonoBehaviour
{
	// Properties
	public string Name { get; private set; }
	public Age Age { get; private set; }

	// Health
	public float MaxHealth { get; private set; }
	private float currentHealth;
	public float CurrentHealth
	{
		get
		{
			return currentHealth;
		}
		set
		{
			currentHealth = value;
			HealthChangedEvent.Invoke(value);
		}
	}

	// Defence
	public float Defence { get; private set; }

	// Offense
	public float AttackPower { get; private set; }
	public float AttackRange { get; private set; }
	public SO_AttackTiming AttackTiming { get; private set; }

	// Communication
	public delegate void HealthChangedHandler (float health);
	public event HealthChangedHandler HealthChangedEvent = delegate { };

	// States
	public bool Ready { get; private set; }

	// References
	public Game_Enemy_Information Information { get; private set; }

	#region Setup
	private void Awake ()
	{
		Ready = false;
		StartCoroutine(Setup());
	}

	private IEnumerator Setup ()
	{
		while (GetComponent<Game_Enemy_Information>() == null)
			yield return null;
		Information = GetComponent<Game_Enemy_Information>();

		while (!Information.Ready)
			yield return null;
	}

	public void SetStats (SO_Enemy SO_Enemy, bool init)
	{
		Name = SO_Enemy.EnemyName;
		Age = SO_Enemy.age;
		MaxHealth = SO_Enemy.Health;
		if (init)
			CurrentHealth = MaxHealth;
		Defence = SO_Enemy.Defence;
		AttackPower = SO_Enemy.AttackPower;
		AttackRange = SO_Enemy.AttackRange;
		AttackTiming = SO_Enemy.attackTiming;

		Ready = true;
	}

	public void SetInformation(Game_Enemy_Information information)
	{
		Information = information;
	}
	#endregion
}