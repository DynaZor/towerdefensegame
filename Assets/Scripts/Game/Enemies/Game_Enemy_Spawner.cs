﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using DynaZor.EditorUtilities;
using System.Linq;

public class Game_Enemy_Spawner : MonoBehaviour
{
	#region Singleton
	public static Game_Enemy_Spawner Instance;
	private void MakeSingleton ()
	{
		if (Instance != null)
			Debug.Log("Game_Enemy_Spawner - More than one instance!\n" +
				"Registered Instance: " + Instance.gameObject.name +
				"This Instance: " + gameObject.name, this);
		else
			Instance = this;
	}
	#endregion

	[Header("Enemy Spawner Settings")]
	[SerializeField] private Game_Enemy_Information EnemyPrefab = null; // Change to Game_Enemy when exists

	[Header("References")]
	[SerializeField] private GameObject EnemiesParent = null;
	[SerializeField] private SO_Enemy SO_Enemy = null; //!! 0POC Enemy Spawner: Finish Planning

	// Private Data
	private bool Ready = false;
	private readonly int actionTick_TimeAdvance = 2;

	private DifficultySetting CurrentDifficulty { get { return Difficulty_Manager.CurrentDifficulty; } }
	private uint CurrentTime = 0;
	private uint lastWave = 0;
	public uint WaveNumber { get; private set; }
	public uint NextWave { get; private set; }

	private Game_Node currentSpawnNode = null;
	private Game_Node lastSpawnNode = null;

	private Direction? currentDirection = null;
	private Direction? lastDirection = null;

	private bool permChange = true;
	[System.Flags]
	private enum Direction
	{
		Top, Bottom, Left, Right
	}
	

	#region Setup
	private void Awake ()
	{
		MakeSingleton();
	}

	private void OnEnable () // To enable component disabling
	{
		StartCoroutine(Setup());
	}

	private void Update () // To enable component disabling
	{}

	private IEnumerator Setup ()
	{
		while (Difficulty_Manager.Instance == null)
			yield return null;

		while (Game_Clock.Instance == null)
			yield return null;

		Game_Clock.Instance.Tick_Group4 += TickUpdate;

		while (UI_Game_InfoPanel_Wave.Instance == null)
			yield return null;
		//!! 1Alpha GameLoad: EnemySpanwer: SetNextWaveIn according to save data
		UI_Game_InfoPanel_Wave.SetNextWaveIn(CurrentDifficulty.Enemy_Wave_First_Tick, CurrentDifficulty.Enemy_Wave_First_Tick);

		Ready = true;
	}

	private void OnDestroy ()
	{
		if (Ready)
		{
			Game_Clock.Instance.Tick_Group4 -= TickUpdate;
		}
	}
	private void OnDisable ()
	{
		if (Ready)
		{
			Game_Clock.Instance.Tick_Group4 -= TickUpdate;
		}
	}
	#endregion

	#region Loops
	private void TickUpdate (int currentTick)
	{
		if (currentTick == actionTick_TimeAdvance)
		{
			TimeAdvanceAndCheckForNextWave();
		}
	}
	#endregion

	#region Private Actions
	private void TimeAdvanceAndCheckForNextWave ()
	{
		CurrentTime++;
		if (WaveNumber == 0)
		{
			NextWave = CurrentDifficulty.Enemy_Wave_First_Tick;
			lastWave = 0;
		}

		uint timeLeft = NextWave - (CurrentTime - lastWave);

		if (timeLeft == 0)
		{
			UI_Game_InfoPanel_Wave.SetWaveNow();
			CheckForDirectionChangeAndThenSpawnEnemy ();
			lastWave = CurrentTime;
			WaveNumber++;
			NextWave = (uint) Mathf.RoundToInt(CurrentDifficulty.Enemy_Wave_BaseTicks * Mathf.Pow(CurrentDifficulty.Enemy_Wave_TickMultiplier, WaveNumber));
		}
		else
		{
			UI_Game_InfoPanel_Wave.SetNextWaveIn(timeLeft, NextWave);
		}
	}

	private void CheckForDirectionChangeAndThenSpawnEnemy ()
	{
		if (!permChange && lastSpawnNode != currentSpawnNode)
		{
			currentSpawnNode = lastSpawnNode;
			currentDirection = lastDirection;
		}
		else if (currentSpawnNode == null || currentDirection == null || RollDirectionChange())
		{
			ChangeDirectionChangeSpawnNodeAndSpawnEnemy();
			return;
		}
		SpawnEnemy();
	}

	private void SpawnEnemy ()
	{
		UnityEngine.Profiling.Profiler.BeginSample("Enemy_Spawner: SpawnEnemy");
		if (Game_Grid.Instance.CheckNode(currentSpawnNode, Game_Grid.CheckType.Blocked)?? true)
		{
			ChangeSpawnNodeAndSpawnEnemy();
			return;
		}
		Game_Enemy_Information enemy = 
			Instantiate(EnemyPrefab, Game_Grid.Instance.WorldPointFromNode(currentSpawnNode.GridPosX, currentSpawnNode.GridPosY, true), 
			new Quaternion(), EnemiesParent.transform).GetComponent<Game_Enemy_Information>();
		enemy.SetNode(currentSpawnNode.Stats);
		//!! 0POC EnemySpawner: Finish implementing Enemy Waves with Difficulty
		enemy.SetEnemySO(SO_Enemy);
		enemy.Enemy_Actions.SetTarget(Item_PlayerBase.Instance.Node);
		if (!permChange)
			currentSpawnNode = lastSpawnNode;
		UnityEngine.Profiling.Profiler.EndSample();
	}

	private bool RollDirectionChange ()
	{
		int RandomNumber = UnityEngine.Random.Range(0, 101);
		//!! 0POC EnemySpawner: Finish implementing Enemy Waves with Difficulty
		if (RandomNumber > CurrentDifficulty.Enemy_Direction_TempChangeChance * 100f)
		{
			permChange = RandomNumber > CurrentDifficulty.Enemy_Direction_PermChangeChance * 100f;
			return true;
		}
		else
			return false;
	}

	private void ChangeDirectionChangeSpawnNodeAndSpawnEnemy ()
	{
		int RandomNumber= UnityEngine.Random.Range(0, 5);
		lastDirection = currentDirection;
		currentDirection = (Direction) RandomNumber;
		ChangeSpawnNodeAndSpawnEnemy();
	}

	private void ChangeSpawnNodeAndSpawnEnemy ()
	{

		int GridSizeX = Game_Grid.Instance.GridSizeX;
		int GridSizeY = Game_Grid.Instance.GridSizeY;

		if (!permChange)
		{
			if (!Game_Grid.Instance.CheckNode(currentSpawnNode, Game_Grid.CheckType.Exist) ?? false)
			{
				permChange = true;
			}
			else if (Game_Grid.Instance.CheckNode(currentSpawnNode, Game_Grid.CheckType.Enemy) ?? false)
			{
				permChange = false;
			}
			else if (Game_Grid.Instance.CheckNode(currentSpawnNode, Game_Grid.CheckType.AllyMovable) ?? false)
			{
				permChange = false;
			}
			else if (Game_Grid.Instance.CheckNode(currentSpawnNode, Game_Grid.CheckType.Ally) ?? false)
			{
				permChange = true;
			}
			else if (Game_Grid.Instance.CheckNode(currentSpawnNode, Game_Grid.CheckType.Blocked) ?? true)
			{
				permChange = true;
			}
		}

		Direction BottomAndTop = Direction.Bottom | Direction.Top;
		Direction RightAndLeft = Direction.Right | Direction.Left;

		int nodeX;
		int nodeY;

		if (currentSpawnNode != null)
		{
			nodeX = currentSpawnNode.GridPosX;
			nodeY = currentSpawnNode.GridPosY;
		}
		else
		{
			nodeX = RightAndLeft.HasFlag(currentDirection) ?
				(currentDirection == Direction.Left ? 0 : GridSizeX - 1) : 
				UnityEngine.Random.Range(0, GridSizeX);
			nodeY = BottomAndTop.HasFlag(currentDirection) ?
				(currentDirection == Direction.Top ? 0 : GridSizeY - 1) : 
				UnityEngine.Random.Range(0, GridSizeY);
		}
		int newNodeX = nodeX;
		int newNodeY = nodeY;
		int tries = 0;

		while (Game_Grid.Instance.CheckNodeAt(new Vector2Int(newNodeX, newNodeY), Game_Grid.CheckType.Blocked) ?? true)
		{
			List<Vector2Int> checkedList = new List<Vector2Int>();
			List<Vector2Int> openList = new List<Vector2Int>
			{
				new Vector2Int(newNodeX, newNodeY)
			};

			while (openList.Count > 0)
			{
				Vector2Int cachedPos = openList[0];

				if (tries > 20)
				{
					ChangeDirectionChangeSpawnNodeAndSpawnEnemy();
					return;
				}

				if (Game_Grid.Instance.CheckNodeAt(cachedPos, Game_Grid.CheckType.Blocked)?? true)
				{
					checkedList.Add(cachedPos);

					List<Vector2Int> circle = Game_Grid.Instance.GetSurroundingNodePositionsProgressive(cachedPos, 1);
					openList.AddRange(circle.Except(checkedList));
					openList.RemoveAll(x => x == cachedPos);
					openList = openList.Distinct().ToList();
					tries++;
				}
				else
				{
					newNodeX = cachedPos.x;
					newNodeY = cachedPos.y;
					break;
				}
			}

			if (openList.Count == 0)
				return;
		}

		if (permChange)
			lastSpawnNode = currentSpawnNode;
		currentSpawnNode = Game_Grid.Instance.Nodes[newNodeX, newNodeY];
		SpawnEnemy();
	}
	#endregion
}