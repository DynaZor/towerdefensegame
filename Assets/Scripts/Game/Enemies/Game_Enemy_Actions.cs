﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DynaZor.EditorUtilities;
using System.Linq;
//using System.Linq;

public class Game_Enemy_Actions : MonoBehaviour, IItem_Actions
{
	// Public Data
	public bool IsMoving => !IsWaiting && !IsAttackingWithoutMoving && path.Count > 0;
	public bool IsAttackingWithoutMoving = false;
	public bool IsWaiting = false;

	// Timing
	private int Tick_Move = 0;
	private int Tick_Attack = 0;

	// Private Data
	private bool Ready = false;
	private Queue<Game_Node> path = new Queue<Game_Node>();
	private int pathScore = int.MaxValue;
	private bool needsNewPath = false;
	private Game_Node targetNode;
	private bool alternateCheckForBetterPath = false;
	private bool? lastVisible = null;

	// References
	public Game_Enemy_Information Information { get; private set; }
	public Game_Enemy_Stats Stats { get; private set; }

	#region Setup
	private void Awake ()
	{
		StartCoroutine(Setup());
	}

	private IEnumerator Setup ()
	{
		while (GetComponent<Game_Enemy_Information>() == null)
			yield return null;
		Information = GetComponent<Game_Enemy_Information>();

		while (GetComponent<Game_Enemy_Stats>() == null)
			yield return null;
		Stats = GetComponent<Game_Enemy_Stats>();

		while (Information.Data == null)
			yield return null;
		
		while (Game_Clock.Instance == null)
			yield return null;

		Tick_Move = Game_Clock.Instance.GetTickAssignment(1);
		Tick_Attack = Game_Clock.Instance.GetTickAssignment(3);

		bool waitFor = EnemySetup();
		Game_Clock.Instance.Tick_Group1 += TickUpdate_Move;
		Game_Clock.Instance.Tick_Group3 += TickUpdate_Attack;

		Game_Grid.GridDataUpdate_Visibility += GridVisibilityChanged;
		Information.Enemy_Stats.HealthChangedEvent += CheckIfDestroyed;
		Ready = true;
	}

	private bool EnemySetup ()
	{
		//Game_Item_Manager.Instance.ItemRegister(this);
		return true;
	}

	private void OnDestroy ()
	{
		if (Ready)
		{
			if (Game_Clock.Instance != null)
			{
				Game_Clock.Instance.Tick_Group1 -= TickUpdate_Move;
				Game_Clock.Instance.Tick_Group3 -= TickUpdate_Attack;
			}
			Game_Grid.GridDataUpdate_Visibility -= GridVisibilityChanged;
		}
	}
	#endregion

	#region Loops
	private void TickUpdate_Move (int currentTick)
	{
		if (Ready && Tick_Move == currentTick)
		{
			MoveNextStep();
		}
	}

	private void TickUpdate_Attack (int currentTick)
	{
		if (Ready && Tick_Attack == currentTick)
		{
			CheckForAlliesInRange();
		}
	}
	#endregion

	#region Self Actions
	private void GridVisibilityChanged ()
	{
		CheckVisibility();
	}

	private void CheckVisibility ()
	{
		bool? newState = Game_Grid.Instance.CheckNode(Information.Node, Game_Grid.CheckType.Visible) ?? false;
		if (newState != lastVisible)
		{
#if UNITY_EDITOR
			GetComponent<MeshRenderer>().material.color =
				(Game_Grid.Instance.CheckNode(Information.Node, Game_Grid.CheckType.Visible) ?? false) ? Color.white : new Color(1f, 0f, 0f, 0.3f);
#else // If Player
			GetComponent<MeshRenderer>().enabled = Game_Grid.Instance.CheckNode(Information.Node, Game_Grid.CheckType.Visible) ?? false;
#endif
		}
	}

	private void PathItemsUpdated ()
	{
		if (path != null && path.Count > 0)
		{
			CheckForBetterPath();
		}
	}

	public void SetTarget (Game_Node targetNode)
	{
		UnityEngine.Profiling.Profiler.BeginSample("Enemy_Actions: SetTarget");
		Debug.Log("Set Target", this);
		this.targetNode = targetNode;
		if (Information == null || Information.Node == null || Game_PathFinder.Instance == null)
		{
			StartCoroutine(DelayedSetTarget(targetNode));
		}

		this.targetNode = targetNode;

		FindPath(out Vector2Int[] newPath, out int newPathScore);

		ApplyPath(newPath, newPathScore);
		Debug.Log("Set Target End", this);
		UnityEngine.Profiling.Profiler.EndSample();
	}

	private void FindPath (out Vector2Int[] newPath, out int newPathScore)
	{
		Debug.Log("Find Path", this);
		Vector2Int currentPos = Information.Node.NodePos;
		Vector2Int targetPos = targetNode.NodePos;
		newPath = Game_PathFinder.FindPath(currentPos, targetPos, 1, out newPathScore);
		if (newPath.Contains(Information.Node.NodePos))
		{
			var temp = newPath.ToList();
			temp.Remove(Information.Node.NodePos);
			newPath = temp.ToArray();
		}
	}

	private void MoveNextStep ()
	{
		Debug.Log("Move Next Step checks, path left " + path.Count, this);
		//UnityEngine.Profiling.Profiler.BeginSample("Enemy_Actions: MoveNextStep");
		if (path == null)
		{
			needsNewPath = true;
			path = new Queue<Game_Node>();
			pathScore = int.MaxValue;
		}

		if (targetNode == null)
		{
			needsNewPath = true;
			pathScore = int.MaxValue;
			targetNode = Item_PlayerBase.Instance.Node;
		}

		if (needsNewPath)
		{
			needsNewPath = false;
			SetTarget(targetNode);
		}

		if (path.Count > 0)
		{
			if (path.ToArray()[0] == targetNode)
			{
				Debug.Log("NextNodeIsTarget", this);
				return;
			}
			else if (path.ToArray()[0] == Information.Node)
			{
				Debug.Log("NextNodeIsSameAsCurrent", this);
			}

			if (alternateCheckForBetterPath)
				CheckForBetterPath();

			alternateCheckForBetterPath.FlipBoolValue();

			Game_Node node = path.ToArray()[0];
			if (!(Game_Grid.Instance.CheckNode(node, Game_Grid.CheckType.Blocked) ?? true))
			{
				Debug.Log("Should initiate move", this);
				MoveInitiate();
				IsWaiting = false;
				return;
			}
			else if (node.Item != null)
			{
				Debug.Log("Next move is enemy, waiting to attack", this);
				IsAttackingWithoutMoving = true;
				IsWaiting = false;
				return;
			}
			else // if Enemy meeting Enemy
			{
				Debug.Log("Next move is same", this);
				IsWaiting = true;
			}
		}
		IsAttackingWithoutMoving = false;
		//UnityEngine.Profiling.Profiler.EndSample();
	}

	private void CheckForBetterPath ()
	{
		FindPath(out Vector2Int[] newPath, out int newPathScore);
		if (newPathScore < pathScore)
		{
			ApplyPath(newPath, newPathScore);
		}
	}

	private void ApplyPath (Vector2Int[] newPath, int newPathScore)
	{
		ClearPath();
		for (int i = newPath.Length - 1; i >= 0 ; i--)
		{
			Game_Node node = Game_Grid.Instance.NodeAt(newPath[i]);
			node.ItemsUpdatedEvent += PathItemsUpdated;
			path.Enqueue(node);
		}
		pathScore = newPathScore;
	}

	private void ClearPath ()
	{
		if (path != null)
		{
			if (path.Count > 0)
			{
				while (path.Count > 0)
				{
					path.Dequeue().ItemsUpdatedEvent -= PathItemsUpdated;
				}
			}
			path.Clear();
		}
		else
		{
			path = new Queue<Game_Node>();
		}
	}

	private void MoveInitiate ()
	{
		Game_Node nextNode = path.Dequeue();
		Vector3 newPos = Game_Grid.Instance.WorldPointFromNode(nextNode.GridPosX, nextNode.GridPosY, true);
		transform.position = newPos;
		Information.Node.SetEnemy(null);
		nextNode.SetEnemy(Information);
		Information.SetNode(nextNode.Stats);
		CheckVisibility();
		Debug.Log("Move Initiated", this);
	}

	private IEnumerator DelayedSetTarget(Game_Node targetNode)
	{
		Debug.Log("Delaying setting target", this);
		while (Information == null)
			yield return null;
		while (Information.Node == null)
			yield return null;
		while (Game_PathFinder.Instance == null)
			yield return null;

		SetTarget(targetNode);
	}

	public void GetDestroyed ()
	{
		Information.Node_Stats.Node.ItemDestroyed();
		//Game_Item_Manager.Instance.ItemDestroyed(this);
		Destroy(gameObject);
	}
	#endregion

	#region Health Actions
	public void TakeDamage (float damageAmount)
	{
		Information.Enemy_Stats.CurrentHealth -= damageAmount;
	}

	private void CheckIfDestroyed (float health)
	{
		if (health <= 0f)
			GetDestroyed();
	}
	#endregion

	#region Combat Actions
	private void CheckForAlliesInRange ()
	{
		var gg = Game_Grid.Instance;
		UnityEngine.Profiling.Profiler.BeginSample("Enemy_Actions: CheckForAlliesInRange");
		if (path.Count > 0 && path.ToArray()[0].Item != null)
		{
			Debug.Log("Enemy_Actions: Enemy in path\n" + path.ToArray()[0].NodePos.ToString(), this);
			path.ToArray()[0].Item.Item_Actions.TakeDamage(Stats.AttackPower);
		}
		else
		{
			List<Game_Node> nodes = gg.GetNodesFromPositions(gg.GetSurroundingNodePositions(Information.Node.NodePos, 1, false));
			for (int i = 0; i < nodes.Count; i++)
			{
				if (nodes[i].Item != null)
				{
					Debug.Log("Enemy_Actions: Enemy in range\n" + nodes[i].NodePos.ToString(), this);
					nodes[i].Item.Item_Actions.TakeDamage(Stats.AttackPower);
					UnityEngine.Profiling.Profiler.EndSample();
					return;
				}
			}
		}
		UnityEngine.Profiling.Profiler.EndSample();
	}
	#endregion
}