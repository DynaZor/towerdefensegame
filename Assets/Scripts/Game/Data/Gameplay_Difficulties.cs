﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!

using UnityEngine;
using System.Collections.Generic;
using ResourcesSystem;

[System.Serializable, CreateAssetMenu(fileName = "Difficulties_", menuName = "Create new Difficulties asset")]
public class Gameplay_Difficulties : ScriptableObject
{
	public List<DifficultySetting> Difficulties = new List<DifficultySetting>();
}

[System.Serializable]
public class DifficultySetting
{
	#region General Data
	public string Name = "Unnamed"; // 0
	#endregion
	#region Player
	public ResourcesStack Player_StartingResources = new ResourcesStack(); // 1
	#endregion
	#region Nodes
	public float Node_Resources_Base_Main = 10000f;  // 2
	public float Node_Resources_Base_Secondary = 0f;	// 3

	public float Node_Resources_StartMod_Main = 0.4f; // 4
	public float Node_Resources_StartMod_Secondary = 0f; // 5

	public float Node_Resources_Recovery_BaseMod = 0.001f; // 6
	public float Node_Resources_Recovery_MaxPollution = 100f; // 7

	public float Node_Pollution_MaxPerTile = 200f; // 8
	public float Node_Pollution_SpreadPerTick = 0.01f; // 9
	#endregion
	#region Enemies
	public uint Enemy_Wave_First_Tick = 50; // 10
	public uint Enemy_Wave_BaseTicks = 465; // 11
	public float Enemy_Wave_TickMultiplier = 0.991f; // 12
	public uint Enemy_Wave_NumEnemies_Base = 6; // 13
	public float Enemy_Wave_NumEnemies_Multiplier = 1.1f; // 14

	public uint Enemy_AgeUp_MinWaves = 10; // 15
	public float Enemy_AgeUp_ChanceBase = 0.0008f; // 16

	public float Enemy_Direction_PermChangeChance { get; internal set; }
	public float Enemy_Direction_TempChangeChance { get; internal set; }
	#endregion
}