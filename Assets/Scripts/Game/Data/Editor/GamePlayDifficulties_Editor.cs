﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ResourcesSystem;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine.Events;
using DynaZor.EditorUtilities.EUEditorOnly;

public class GamePlayDifficulties_Editor : EditorWindow
{
	public static readonly string WindowTitle = "Gameplay_Difficulties Editor";
	#region Data
	public Gameplay_Difficulties Holder = null;
	#endregion
	#region Editor Integration
	#region Open Blank Window
	[MenuItem("TowerDefence/Gameplay_Difficulties Editor")]
	private static void OpenBlankEditor ()
	{
		var window = EditorWindow.GetWindow<GamePlayDifficulties_Editor>(false, WindowTitle, true);
		if (Selection.activeObject != null && Selection.activeObject.GetType() == typeof(Gameplay_Difficulties))
		{
			window.Holder = (Gameplay_Difficulties) Selection.activeObject;
		}
	}
	#endregion
	#region Open Gameplay_Difficulties
	[MenuItem("Assets/TowerDefence/Gameplay_Difficulties Editor")]
	private static void LoadSelectedIntoEditor ()
	{
		var window = EditorWindow.GetWindow<GamePlayDifficulties_Editor>(false, WindowTitle, true);
		window.Holder = (Gameplay_Difficulties) Selection.activeObject;
	}
	[MenuItem("Assets/TowerDefence/Gameplay_Difficulties Editor", true)]
	private static bool LoadSelectedIntoEditor_Validation ()
	{
		try
		{
			Gameplay_Difficulties a = (Gameplay_Difficulties) Selection.activeObject;
		}
		catch
		{
			return false;
		}
		return true;
	}
	#endregion
	#endregion
	#region UI State
	private bool needToSetup = true;

	public int currentListIndex = 0;

	private Vector2 ListDataScrollView_position = new Vector2();
	private Vector2 HeadersScrollView_position = new Vector2();

	private List<AnimBool> listFoldoutState = new List<AnimBool>();

	private float startY_Headers;
	private float startY_List;
	#endregion
	#region GUI Styles
	private GUIStyle general_alternateColor;
	private GUIStyle general_alternateColor2;
	private GUIStyle floatField_AlignRight;
	private GUIStyle headerStyle;
	private GUIStyle headerStyle_Bold;
	private Color transparentWhite = new Color(1f,1f,1f,0.5f);
	private Color transparentGray = new Color(0.5f,0.5f,0.5f,0.5f);
	private Color transparentBlack = new Color(0f,0f,0f,0.5f);
	#endregion
	#region Sizes
	private float totalWidth;
	private readonly float elementSpacing = 5f;
	private readonly float headerHeight = 2f * EditorGUIUtility.singleLineHeight;
	private readonly float[] headerWidths = new float[17]
	{
		100f, // 0
		135f, // 1
		55f, // 2
		55f, // 3
		55f, // 4
		55f, // 5
		55f, // 6
		55f, // 7
		55f, // 8
		55f, // 9
		55f, // 10
		55f, // 11
		55f, // 12
		55f, // 13
		55f, // 14
		55f, // 15
		55f, // 16
	};
	private readonly string[] headerTitles = new string[17]
	{
		"Name", // 0
		"StartRes", // 1
		"Base_Main", // 2
		"Base_Sec", // 3
		"StartMod_Main", // 4
		"StartMod_Sec", // 5
		"Rec_BaseMod", // 6
		"Rec_MaxPoll", // 7
		"Poll_MaxPerTile", // 8
		"Poll_SpreadPerTick", // 9
		"FirstWave_Tick", // 10
		"Wave_BaseTicks", // 11
		"Wave_TickMult", // 12
		"Wave_#Enemies_Base", // 13
		"Wave_#Enemies_Mult", // 14
		"AgeUp_MinWaves", // 15
		"AgeUp_ChanceBase", // 16
	};
	private readonly int[] headerCats = new int[4]
	{
		1,
		1,
		8,
		7
	};
	private readonly string[] headerCatTitles = new string[4]
	{
		"General",
		"Player",
		"Node",
		"Enemy"
	};
	private readonly float removeItem = 18f;
	#endregion
	#region LifeCycle
	private void OnEnable ()
	{
		if (EditorApplication.isCompiling || EditorApplication.isPlayingOrWillChangePlaymode)
		{
			needToSetup = true;
			return;
		}

		if (needToSetup)
			Setup();
	}
	private void __Setup ()
	{
		Color[] alternateTex_general_pix = new Color[1]{
			new Color(0.9f, 0.9f, 0.9f)
		};
		Texture2D alternateTex_general = new Texture2D(1, 1);
		alternateTex_general.SetPixels(alternateTex_general_pix);
		alternateTex_general.filterMode = FilterMode.Point;
		alternateTex_general.Apply();
		Color[] alternateTex_general_pix2 = new Color[1]{
			transparentGray
		};
		Texture2D alternateTex_general2 = new Texture2D(1, 1);
		alternateTex_general2.SetPixels(alternateTex_general_pix2);
		alternateTex_general2.filterMode = FilterMode.Point;
		alternateTex_general2.Apply();

		general_alternateColor = new GUIStyle(GUIStyle.none);
		general_alternateColor.normal.background = alternateTex_general;
		general_alternateColor.margin = new RectOffset();
		general_alternateColor.padding = new RectOffset();
		general_alternateColor.border = new RectOffset();
		general_alternateColor.contentOffset = Vector2.zero;
		general_alternateColor.clipping = TextClipping.Clip;
		general_alternateColor.overflow = new RectOffset();
		general_alternateColor.stretchWidth = false;

		general_alternateColor2 = new GUIStyle(GUIStyle.none);
		general_alternateColor2.normal.background = alternateTex_general2;
		general_alternateColor2.margin = new RectOffset();
		general_alternateColor2.padding = new RectOffset();
		general_alternateColor2.border = new RectOffset();
		general_alternateColor2.contentOffset = Vector2.zero;
		general_alternateColor2.clipping = TextClipping.Clip;
		general_alternateColor2.overflow = new RectOffset();
		general_alternateColor2.stretchWidth = false;

		headerStyle = new GUIStyle(EditorStyles.label);
		headerStyle.fontStyle = FontStyle.Normal;
		headerStyle.fontSize = 9;
		headerStyle.margin = new RectOffset();
		headerStyle.padding = new RectOffset();
		headerStyle.border = new RectOffset();
		headerStyle.contentOffset = Vector2.zero;
		headerStyle.clipping = TextClipping.Clip;
		headerStyle.overflow = new RectOffset();
		headerStyle.stretchWidth = false;
		headerStyle.wordWrap = true;

		headerStyle_Bold = new GUIStyle(EditorStyles.boldLabel);
		headerStyle_Bold.fontSize = 15;
		headerStyle_Bold.margin = new RectOffset();
		headerStyle_Bold.padding = new RectOffset();
		headerStyle_Bold.border = new RectOffset();
		headerStyle_Bold.contentOffset = Vector2.zero;
		headerStyle_Bold.clipping = TextClipping.Clip;
		headerStyle_Bold.overflow = new RectOffset();
		headerStyle_Bold.stretchWidth = false;

		floatField_AlignRight = new GUIStyle(EditorStyles.numberField)
		{
			alignment = TextAnchor.MiddleRight,
			margin = new RectOffset(),
			padding = new RectOffset(),
			contentOffset = Vector2.zero,
			clipping = TextClipping.Clip,
			overflow = new RectOffset(),
			stretchWidth = false
		};

		// totalWidth
		totalWidth = elementSpacing * 2f + removeItem;
		for (int i = 0; i < headerWidths.Length; i++)
		{
			totalWidth += headerWidths[i] + elementSpacing;
		}

		needToSetup = false;
	}
	#region Setup
	private void Setup ()
	{
		EditorCoroutine.start(_Setup());
	}
	private IEnumerator _Setup ()
	{
		bool needToWait = true;
		while (needToWait)
		{
			try
			{
				if (EditorStyles.toolbarButton != null)
				{
					needToWait = false;
				}
			}
			catch
			{
				needToWait = true;
			}
			yield return null;
		}

		__Setup();
	}
	#endregion
	#endregion
	#region GUI
	#region OnGUI
	private void OnGUI ()
	{
		if (EditorApplication.isPlayingOrWillChangePlaymode)
		{
			needToSetup = true;
			Repaint();
			return;
		}
		if (EditorApplication.isCompiling)
		{
			needToSetup = true;
			Repaint();
			return;
		}
		else
		{
			if (needToSetup)
			{
				Setup();
				Repaint();
				return;
			}
		}
		using (new EditorGUILayout.HorizontalScope())
		{
			DrawHolderObjectField();
			GUILayout.FlexibleSpace();
			if (Holder != null)
				DrawSaveButton();
		}

		if (Holder != null)
		{
			if (focusedWindow == this)
			{
				if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.S))
				{
					SaveAll();
				}
			}
			startY_Headers = 0f;
			using (var changeCheck = new EditorGUI.ChangeCheckScope())
			{
				EditorGUIUtility.fieldWidth = 100f;
				EditorGUIUtility.labelWidth = 100f;
				UpdateListFoldoutState(headerCats.Length);
				using (var HeadersScrollView = 
					new EditorGUILayout.ScrollViewScope(HeadersScrollView_position, GUIStyle.none, GUIStyle.none, GUILayout.Height(EditorGUIUtility.singleLineHeight * 2f + 8f)))
				{
					DrawHeaders();
				}

				startY_List = EditorGUIUtility.singleLineHeight * 2f + 8f;

				using (var ListDataScrollView = new EditorGUILayout.ScrollViewScope(ListDataScrollView_position, true, false))
				{
					ListDataScrollView_position = ListDataScrollView.scrollPosition;
					HeadersScrollView_position = new Vector2(ListDataScrollView_position.x, 0);

					float currentY = startY_List;
					for (int listIndex = 0; listIndex < Holder.Difficulties.Count; listIndex++)
					{
						DrawListItem(Holder.Difficulties, listIndex, currentY, out float addY, out float itemWidth);
						currentY += addY;
					}
					DrawAddItemButton();
				}


				if (changeCheck.changed)
				{
					EditorUtility.SetDirty(Holder);
				}
			}
		}
		Repaint();
	}

	private void DrawSaveButton ()
	{
		if (GUILayout.Button("Save", EditorStyles.toolbarButton))
		{
			SaveAll();
		}
	}
	#endregion
	private void DrawHeaders ()
	{
		EditorGUIUtility.fieldWidth = 1f;
		EditorGUIUtility.labelWidth = 1f;
		using (new EditorGUILayout.HorizontalScope(GUILayout.Width(totalWidth), GUILayout.ExpandWidth(false)))
		{
			GUILayout.FlexibleSpace();
			float currentX = elementSpacing;
			for (int cat = 0; cat < headerCats.Length; cat++)
			{
				int titleStart = 0;
				for (int i = 0; i < cat; i++)
				{
					titleStart += headerCats[i];
				}
				//using (new EditorGUILayout.VerticalScope((titleStart % 2 == 0) ? general_alternateColor2 : GUIStyle.none, GUILayout.Width(headerWidths[titleStart]), GUILayout.ExpandWidth(false)))
				{
					Rect rect_Main = new Rect(currentX, startY_Headers, headerWidths[titleStart], EditorGUIUtility.singleLineHeight);
					Rect rect_Sec = new Rect(currentX, startY_Headers + EditorGUIUtility.singleLineHeight, headerWidths[titleStart], headerHeight);
					//EditorGUILayout.LabelField(headerCatTitles[cat], headerStyle_Bold, GUILayout.Width(headerWidths[titleStart]), GUILayout.ExpandWidth(false));
					EditorGUI.LabelField(rect_Main, headerCatTitles[cat], headerStyle_Bold);
					if (listFoldoutState[cat].value)
					{
						//EditorGUILayout.LabelField(headerTitles[titleStart], headerStyle, GUILayout.Width(headerWidths[titleStart]), GUILayout.ExpandWidth(false));
						EditorGUI.LabelField(rect_Sec, new GUIContent(headerTitles[titleStart], headerTitles[titleStart]), headerStyle);
					}
					currentX += headerWidths[titleStart] + elementSpacing;
				}
				if (listFoldoutState[cat].value)
				{
					for (int i = 1; i < headerCats[cat]; i++)
					{
						Rect rect_Sec = new Rect(currentX, startY_Headers + EditorGUIUtility.singleLineHeight, headerWidths[titleStart + i], headerHeight);
						//using (
						//	new EditorGUILayout.VerticalScope(((titleStart + i) % 2 == 0) ? general_alternateColor2 : GUIStyle.none, GUILayout.Width(headerWidths[titleStart + i]), GUILayout.ExpandWidth(false)))
						{
							//GUILayout.Space(EditorGUIUtility.singleLineHeight);
							//EditorGUIUtility.labelWidth = headerWidths[titleStart + i];
							//EditorGUILayout.LabelField(headerTitles[titleStart + i], headerStyle, GUILayout.Width(headerWidths[titleStart + i]), GUILayout.ExpandWidth(false));
							EditorGUI.LabelField(rect_Sec, new GUIContent(headerTitles[titleStart + i], headerTitles[titleStart + i]), headerStyle);
						}
						currentX += headerWidths[titleStart + i] + elementSpacing;
					}
				}
			}
		}
		EditorHelpers.EditorUI.DrawUILine(transparentGray, 2, -4);
		EditorGUIUtility.fieldWidth = 100f;
		EditorGUIUtility.labelWidth = 100f;
	}

	private void DrawAddItemButton ()
	{
		using (new EditorGUILayout.HorizontalScope())
		{
			GUILayout.Space(ListDataScrollView_position.x + (position.width * 0.5f));
			if (GUILayout.Button(new GUIContent("+", "Create new Difficulty"), EditorStyles.miniButton))
			{
				Holder.Difficulties.Add(new DifficultySetting());
				EditorUtility.SetDirty(Holder);
				Repaint();
			}
			GUILayout.FlexibleSpace();
		}
	}

	private void DrawListItem (List<DifficultySetting> difficulties, int listIndex, float startY, out float MySizeY, out float MySizeX)
	{
		bool alternate = listIndex % 2 == 0;
		float currentX = elementSpacing;
		using (new EditorGUILayout.VerticalScope(alternate? GUIStyle.none : general_alternateColor, GUILayout.Width(totalWidth)))
		{
			MySizeY = 0f;
			MySizeX = 0f;
			for (int cat = 0; cat < headerCats.Length; cat++)
			{
				int titleStart = 0;
				for (int i = 0; i < cat; i++)
				{
					titleStart += headerCats[i];
				}
				if (listFoldoutState[cat].value)
				{
					for (int i = 0; i < headerCats[cat]; i++)
					{
						using (new EditorGUILayout.HorizontalScope())
						{
							DrawItemVar(difficulties, listIndex, titleStart + i, startY, currentX, out float lastSizeY, out float lastSizeX);
							MySizeY = Mathf.Max(MySizeY, lastSizeY);
							currentX += lastSizeX + elementSpacing;
							GUILayout.FlexibleSpace();
						}
					}
				}
			}
			DrawItemRemoveButton(startY, currentX, difficulties, listIndex);
			MySizeX = currentX + removeItem;
		}
		EditorHelpers.EditorUI.DrawUILine(transparentGray, 1, -4);
	}

	private void DrawItemRemoveButton (float startY, float currentX, List<DifficultySetting> difficulties, int listIndex)
	{
		Rect removeRect = new Rect(currentX, startY, removeItem, EditorGUIUtility.singleLineHeight);
		if (GUI.Button(removeRect, new GUIContent("-", "Remove Item"), EditorStyles.miniButton))
		{
			difficulties.RemoveAt(listIndex);
			Repaint();
		}
	}

	private void DrawItemVar (List<DifficultySetting> difficulties, int listIndex, int i, float startY, float startX, out float MySizeY, out float MySizeX)
	{
		EditorGUIUtility.fieldWidth = 1f;
		EditorGUIUtility.labelWidth = 1f;
		bool alternate = i % 2 == 0;
		float itemWidth = headerWidths[i];
		MySizeX = itemWidth;
		float itemHeight = EditorGUIUtility.singleLineHeight;
		MySizeY = itemHeight;
		Rect rect = new Rect(startX, startY, itemWidth, itemHeight);
		//using (new EditorGUILayout.HorizontalScope(alternate? general_alternateColor2 : GUIStyle.none, GUILayout.Width(itemWidth), GUILayout.ExpandWidth(false), GUILayout.Height(EditorGUIUtility.singleLineHeight * 3f)))
		{
			switch (i)
			{
				case 0:
					difficulties[listIndex].Name =
						//EditorGUILayout.DelayedTextField(difficulties[listIndex].Name, GUILayout.Width(itemWidth), GUILayout.ExpandWidth(false));
						EditorGUI.DelayedTextField(rect, difficulties[listIndex].Name);
					break;
				case 1:
					DrawResourcesStack(difficulties[listIndex].Player_StartingResources, i, rect, out float mysizeadd);
					MySizeY = mysizeadd;
					break;
				case 2:
					difficulties[listIndex].Node_Resources_Base_Main =
						//EditorGUILayout.DelayedFloatField(difficulties[listIndex].Node_Resources_Base_Main, floatField_AlignRight, GUILayout.Width(itemWidth), GUILayout.ExpandWidth(false));
						EditorGUI.DelayedFloatField(rect, difficulties[listIndex].Node_Resources_Base_Main, floatField_AlignRight);
					break;
				case 3:
					difficulties[listIndex].Node_Resources_Base_Secondary =
						//EditorGUILayout.DelayedFloatField(difficulties[listIndex].Node_Resources_Base_Secondary, floatField_AlignRight, GUILayout.Width(itemWidth), GUILayout.ExpandWidth(false));
						EditorGUI.DelayedFloatField(rect, difficulties[listIndex].Node_Resources_Base_Secondary, floatField_AlignRight);
					break;
				case 4:
					difficulties[listIndex].Node_Resources_StartMod_Main =
						//EditorGUILayout.DelayedFloatField(difficulties[listIndex].Node_Resources_StartMod_Main, floatField_AlignRight, GUILayout.Width(itemWidth), GUILayout.ExpandWidth(false));
						EditorGUI.DelayedFloatField(rect, difficulties[listIndex].Node_Resources_StartMod_Main, floatField_AlignRight);
					break;
				case 5:
					difficulties[listIndex].Node_Resources_StartMod_Secondary =
						//EditorGUILayout.DelayedFloatField(difficulties[listIndex].Node_Resources_StartMod_Secondary, floatField_AlignRight, GUILayout.Width(itemWidth), GUILayout.ExpandWidth(false));
						EditorGUI.DelayedFloatField(rect, difficulties[listIndex].Node_Resources_StartMod_Secondary, floatField_AlignRight);
					break;
				case 6:
					difficulties[listIndex].Node_Resources_Recovery_BaseMod =
						//EditorGUILayout.DelayedFloatField(difficulties[listIndex].Node_Resources_Recovery_BaseMod, floatField_AlignRight, GUILayout.Width(itemWidth), GUILayout.ExpandWidth(false));
						EditorGUI.DelayedFloatField(rect, difficulties[listIndex].Node_Resources_Recovery_BaseMod, floatField_AlignRight);
					break;
				case 7:
					difficulties[listIndex].Node_Resources_Recovery_MaxPollution =
						//EditorGUILayout.DelayedFloatField(difficulties[listIndex].Node_Resources_Recovery_MaxPollution, floatField_AlignRight, GUILayout.Width(itemWidth), GUILayout.ExpandWidth(false));
						EditorGUI.DelayedFloatField(rect, difficulties[listIndex].Node_Resources_Recovery_MaxPollution, floatField_AlignRight);
					break;
				case 8:
					difficulties[listIndex].Node_Pollution_MaxPerTile =
						//EditorGUILayout.DelayedFloatField(difficulties[listIndex].Node_Pollution_MaxPerTile, floatField_AlignRight, GUILayout.Width(itemWidth), GUILayout.ExpandWidth(false));
						EditorGUI.DelayedFloatField(rect, difficulties[listIndex].Node_Pollution_MaxPerTile, floatField_AlignRight);
					break;
				case 9:
					difficulties[listIndex].Node_Pollution_SpreadPerTick =
						//EditorGUILayout.DelayedFloatField(difficulties[listIndex].Node_Pollution_SpreadPerTick, floatField_AlignRight, GUILayout.Width(itemWidth), GUILayout.ExpandWidth(false));
						EditorGUI.DelayedFloatField(rect, difficulties[listIndex].Node_Pollution_SpreadPerTick, floatField_AlignRight);
					break;
				case 10:
					difficulties[listIndex].Enemy_Wave_First_Tick =
						//EditorGUILayout.DelayedIntField(difficulties[listIndex].Enemy_Wave_First_Tick, floatField_AlignRight, GUILayout.Width(itemWidth), GUILayout.ExpandWidth(false));
						(uint)EditorGUI.DelayedIntField(rect, (int)difficulties[listIndex].Enemy_Wave_First_Tick, floatField_AlignRight);
					break;
				case 11:
					difficulties[listIndex].Enemy_Wave_BaseTicks =
						//EditorGUILayout.DelayedIntField(difficulties[listIndex].Enemy_Wave_BaseTicks, floatField_AlignRight, GUILayout.Width(itemWidth), GUILayout.ExpandWidth(false));
						(uint)EditorGUI.DelayedIntField(rect, (int)difficulties[listIndex].Enemy_Wave_BaseTicks, floatField_AlignRight);
					break;
				case 12:
					difficulties[listIndex].Enemy_Wave_TickMultiplier =
						//EditorGUILayout.DelayedFloatField(difficulties[listIndex].Enemy_Wave_TickMultiplier, floatField_AlignRight, GUILayout.Width(itemWidth), GUILayout.ExpandWidth(false));
						EditorGUI.DelayedFloatField(rect, difficulties[listIndex].Enemy_Wave_TickMultiplier, floatField_AlignRight);
					break;
				case 13:
					difficulties[listIndex].Enemy_Wave_NumEnemies_Base =
						//EditorGUILayout.DelayedIntField(difficulties[listIndex].Enemy_Wave_NumEnemies_Base, floatField_AlignRight, GUILayout.Width(itemWidth), GUILayout.ExpandWidth(false));
						(uint)EditorGUI.DelayedIntField(rect, (int)difficulties[listIndex].Enemy_Wave_NumEnemies_Base, floatField_AlignRight);
					break;
				case 14:
					difficulties[listIndex].Enemy_Wave_NumEnemies_Multiplier =
						//EditorGUILayout.DelayedFloatField(difficulties[listIndex].Enemy_Wave_NumEnemies_Multiplier, floatField_AlignRight, GUILayout.Width(itemWidth), GUILayout.ExpandWidth(false));
						EditorGUI.DelayedFloatField(rect, difficulties[listIndex].Enemy_Wave_NumEnemies_Multiplier, floatField_AlignRight);
					break;
				case 15:
					difficulties[listIndex].Enemy_AgeUp_MinWaves =
						//EditorGUILayout.DelayedIntField(difficulties[listIndex].Enemy_AgeUp_MinWaves, floatField_AlignRight, GUILayout.Width(itemWidth), GUILayout.ExpandWidth(false));
						(uint)EditorGUI.DelayedIntField(rect, (int)difficulties[listIndex].Enemy_AgeUp_MinWaves, floatField_AlignRight);
					break;
				case 16:
					difficulties[listIndex].Enemy_AgeUp_ChanceBase =
						//EditorGUILayout.DelayedFloatField(difficulties[listIndex].Enemy_AgeUp_ChanceBase, floatField_AlignRight, GUILayout.Width(itemWidth), GUILayout.ExpandWidth(false));
						EditorGUI.DelayedFloatField(rect, difficulties[listIndex].Enemy_AgeUp_ChanceBase, floatField_AlignRight);
					break;
				default:
					break;
			}
			//GUILayout.FlexibleSpace();
		}
		EditorGUIUtility.fieldWidth = 0f;
		EditorGUIUtility.labelWidth = 0f;
	}

	private void DrawResourcesStack (ResourcesStack resStack, int headerIndex, Rect rect, out float MySizeY)
	{
		//using (new EditorGUILayout.VerticalScope(GUILayout.Width(headerWidths[headerIndex]), GUILayout.ExpandWidth(false)))
		MySizeY = 0f;
		float elementHeight = 15f;
		float currentY = rect.y;
		{
			if (resStack.CraftedResourceAmounts.Count == 0)
			{
				Rect rect_Label = new Rect(rect.x, rect.y, rect.width, elementHeight);
				EditorGUI.LabelField(rect_Label, "No Res");
				MySizeY += elementHeight;
				currentY += elementHeight;
			}
			else
			{
				for (int i = 0; i < resStack.CraftedResourceAmounts.Count; i++)
				{
					//using (new EditorGUILayout.HorizontalScope(GUILayout.Width(headerWidths[headerIndex]), GUILayout.ExpandWidth(false)))
					{
						Rect rect_Type = new Rect(rect.x, currentY, 60f, elementHeight);
						Rect rect_Amount = new Rect(rect.x + rect_Type.width, currentY, 50f, elementHeight);
						Rect rect_Btn = new Rect(rect_Amount.x + rect_Amount.width, currentY, 18f, elementHeight);
						resStack.CraftedResourceTypes[i] =
							//(CraftedResourceType) EditorGUILayout.EnumPopup(resStack.CraftedResourceTypes[i], GUILayout.Width(60f));
							(CraftedResourceType) EditorGUI.EnumPopup(rect_Type, resStack.CraftedResourceTypes[i]);
						resStack.CraftedResourceAmounts[i] =
							//EditorGUILayout.DelayedFloatField(resStack.CraftedResourceAmounts[i], floatField_AlignRight, GUILayout.Width(50f));
							EditorGUI.DelayedFloatField(rect_Amount, resStack.CraftedResourceAmounts[i], floatField_AlignRight);
						if (
							//GUILayout.Button(new GUIContent("-", "Remove Resource"), EditorStyles.miniButtonRight, GUILayout.Width(18f)))
							GUI.Button(rect_Btn, new GUIContent("-", "Remove Resource"), EditorStyles.miniButtonRight))
						{
							resStack.CraftedResourceAmounts.RemoveAt(i);
							resStack.CraftedResourceTypes.RemoveAt(i);
							Repaint();
						}
						MySizeY += elementHeight;
						currentY += elementHeight;
						//GUILayout.FlexibleSpace();
					}
				}
			}
			//using (new EditorGUILayout.HorizontalScope(GUILayout.Width(headerWidths[headerIndex])))
			{
				Rect rect_AddBtn = new Rect(rect.x + headerWidths[headerIndex] * 0.3f, currentY, 18f, elementHeight);
				//GUILayout.FlexibleSpace();
				if (
					//GUILayout.Button(new GUIContent("+", "Add Resource"), EditorStyles.miniButton))
					GUI.Button(rect_AddBtn, new GUIContent("+", "Add Resource"), EditorStyles.miniButton))
				{
					resStack.CraftedResourceAmounts.Add(0f);
					resStack.CraftedResourceTypes.Add(CraftedResourceType.Food);
					Repaint();
				}
				MySizeY += elementHeight;
				//GUILayout.FlexibleSpace();
			}
		}
	}

	private void DrawHolderObjectField ()
	{
		using (var c = new EditorGUI.ChangeCheckScope())
		{
			Holder = EditorGUILayout.ObjectField("Difficulties Asset", Holder, typeof(Gameplay_Difficulties), false, GUILayout.Width(500f)) as Gameplay_Difficulties;
			if (c.changed)
			{
				//needToSetup = true;
			}
		}
	}


	#endregion
	private void SaveAll ()
	{
		EditorUtility.SetDirty(Holder);
		AssetDatabase.SaveAssets();
	}
	private void UpdateListFoldoutState (int desiredCount)
	{
		int currentCount = listFoldoutState.Count;
		if (currentCount < desiredCount)
		{
			int needed = desiredCount - currentCount;
			for (int i = 0; i < needed; i++)
			{
				listFoldoutState.Add(new AnimBool(true));
				listFoldoutState[currentCount + i].valueChanged.AddListener(new UnityAction(Repaint));
			}
		}
	}
}