﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using System.Collections.Generic;
using UnityEngine;
using ResourcesSystem;

public class Gameplay_Values : MonoBehaviour
{
	#region Singleton
	public static Gameplay_Values Instance;
	private void Awake ()
	{
		MakeSingleton();
	}

	public void MakeSingleton ()
	{
		if (Instance != null)
			Debug.Log("Gameplay_Values - More than one instance!\n" +
				"Registered Instance: " + Instance.gameObject.name +
				"This Instance: " + gameObject.name, this);
		else
			Instance = this;
	}
	#endregion

	[Header("Time")]
	public int TimeTicksPerCycle = 4;
	public float TimeCycleLength = 2f;
	public float TimeSpeedNormal = 1f;
	public float TimeSpeedFast = 2f;
	public float TimeSpeedSuperFast = 5f;

	[Header("Economy")]
	public float EconomyResourceValue = 100f;

	[Header("Items")]
	public float ItemHealBaseRate = 10f;
	
	public int ItemDegradeTimerLength = 10;
	public float ItemDegradePercentBase = 5f;

	[Header("Nodes")]
	[Header("_Generation")]
	public float NodeResourceGenerationBaseValue = 1500f;
	public float NRG_Base => NodeResourceGenerationBaseValue;
	public float NodeResourceGenerationMainResourceMinPercent = 85f;
	public float NRG_MainMin01 => NodeResourceGenerationMainResourceMinPercent * 0.01f;
	public float NodeResourceGenerationMainResourceMaxPercent = 100f;
	public float NRG_MainMax01 => NodeResourceGenerationMainResourceMaxPercent * 0.01f;
	public float NodeResourceGenerationOtherResourcesMinPercent = 20f;
	public float NRG_OtherMin01 => NodeResourceGenerationOtherResourcesMinPercent * 0.01f;
	public float NodeResourceGenerationOtherResourcesMaxPercent = 50f;
	public float NRG_OtherMax01 => NodeResourceGenerationOtherResourcesMaxPercent * 0.01f;
	public float NodeResourceGenerationStartValueMinPercent = 50f;
	public float NRG_StartMin01 => NodeResourceGenerationStartValueMinPercent * 0.01f;
	public float NodeResourceGenerationStartValueMaxPercent = 85f;
	public float NRG_StartMax01 => NodeResourceGenerationStartValueMaxPercent * 0.01f;
	[Header("_Recovery")]
	public float NodeResourceRecoveryPercent = 3f;
	public float NodeResourceRecoveryMaxPollution = 95f;
	public float NodePollutionRecoveryRate = 0.5f;

	[Header("Resource Types"), SerializeField]
	public List<NodeResourceType> RenewableResources = new List<NodeResourceType>();
}