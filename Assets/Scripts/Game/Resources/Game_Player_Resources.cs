﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;
using ResourcesSystem;
using System.Collections.Generic;
using System.Linq;

public class Game_Player_Resources : MonoBehaviour
{
	#region Singleton
	public static Game_Player_Resources Instance;
	private void Awake ()
	{
		if (Instance != null)
			Debug.Log("Game_Player_Resources - More than one instance!\n" +
				"Registered Instance: " + Instance.gameObject.name +
				"This Instance: " + gameObject.name, this);
		else
			Instance = this;
	}
	#endregion

	// Data
	private Dictionary<CraftedResourceType, float> craftedResources = new Dictionary<CraftedResourceType, float>
	{
		{ CraftedResourceType.Age, 0f },
		{ CraftedResourceType.Food, 0f },
		{ CraftedResourceType.Tech, 0f },
		{ CraftedResourceType.Electricity, 0f }
	};

	private Dictionary<CraftedResourceType, float> craftedResourcesCapacity = new Dictionary<CraftedResourceType, float>
	{
		//{ CraftedResourceType.Age, 0f },
		//{ CraftedResourceType.Food, 0f },
		//{ CraftedResourceType.Tech, 0f },
		{ CraftedResourceType.Electricity, 0f }
	};

	// Communication
	public delegate void PlayerResourcesUpdatedHandler (Dictionary<CraftedResourceType, float> newValues);
	public event PlayerResourcesUpdatedHandler PlayerResourcesUpdated = delegate { };

	#region Data Handling
	public bool LoadData (Dictionary<CraftedResourceType, float> _craftedResources)
	{
		try
		{
			var resourcesToGet = craftedResources.Keys.ToArray();
			for (int i = 0; i < resourcesToGet.Length; i++)
			{
				craftedResources[resourcesToGet[i]] = _craftedResources[resourcesToGet[i]];
			}
			return true;
		}
		catch
		{
			return false;
		}
	}

	public Dictionary<CraftedResourceType, float> SaveData ()
	{
		return craftedResources;
	}
	#endregion

	#region Public Actions
	#region Internal Resource Handling
	public void NewGame ()
	{
		craftedResources = new Dictionary<CraftedResourceType, float>
		{
			{ CraftedResourceType.Age, 0f },
			{ CraftedResourceType.Food, 0f },
			{ CraftedResourceType.Tech, 0f },
		{ CraftedResourceType.Electricity, 0f }
		};
		PlayerResourcesUpdated.Invoke(craftedResources);
	}

	public void NewGame (Dictionary<CraftedResourceType, float> startingResources)
	{
		craftedResources = startingResources;
		PlayerResourcesUpdated.Invoke(craftedResources);
	}
	#endregion
	#region Public Resource Handling
	public bool Holds (List<CraftedResourceType>resourceType, List<float> amount)
	{
		bool holds = true;
		for (int i = 0; holds == true && i < resourceType.Count; i++)
		{
			holds &= Holds(resourceType[i], amount[i]);
		}
		return holds;
	}

	public bool Holds (CraftedResourceType resourceType, float amount)
	{
		if (craftedResources[resourceType] >= amount)
			return true;
		return false;
	}

	public bool Use (List<CraftedResourceType> resourceType, List<float>amount)
	{
		bool success = true;
		for (int i = 0; success == true && i < resourceType.Count; i++)
		{
			success &= Use(resourceType[i], amount[i]);
		}
		return success;
	}

	public bool Use (CraftedResourceType resourceType, float amount)
	{
		if (Holds(resourceType, amount))
		{
			craftedResources[resourceType] -= amount;
			PlayerResourcesUpdated.Invoke(craftedResources);
			return true;
		}
		return false;
	}

	public void Gain (List<CraftedResourceType> resourceType, List<float> amount)
	{
		for (int i = 0; i < resourceType.Count; i++)
		{
			Gain(resourceType[i], amount[i]);
		}
	}

	public void Gain (CraftedResourceType resourceType, float amount)
	{
		craftedResources[resourceType] += amount;
		if (resourceType == CraftedResourceType.Electricity)
			CheckForResourcesExceedingCapacity();
		PlayerResourcesUpdated.Invoke(craftedResources);
	}
	#endregion
	#endregion

	#region Private Actions
	public void CheckForResourcesExceedingCapacity ()
	{
		craftedResourcesCapacity[CraftedResourceType.Electricity] =  Game_Item_Manager.Instance.GetElectricityCapacity();
		craftedResources[CraftedResourceType.Electricity] = Mathf.Clamp(craftedResources[CraftedResourceType.Electricity], 0, craftedResourcesCapacity[CraftedResourceType.Electricity]);
	}
	#endregion
}