﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ResourcesSystem
{
	public class Game_ResourceQueue : MonoBehaviour
	{
		#region Singleton
		public static Game_ResourceQueue Instance;
		private void MakeSingleton ()
		{
			if (Instance != null)
				Debug.Log("Game_ResourceQueue - More than one instance!\n" +
					"Registered Instance: " + Instance.gameObject.name +
					"This Instance: " + gameObject.name, this);
			else
				Instance = this;
		}
		#endregion

		// Private Data
		private bool Ready = false;
		private Queue<ResourcesRequest> Requests = new Queue<ResourcesRequest>();
		// References
		private Game_Player_Resources Game_Player_Resources;
		#region Setup
		private void Awake ()
		{
			MakeSingleton();
			StartCoroutine(Setup());
		}

		private IEnumerator Setup ()
		{
			while (Game_Player_Resources.Instance == null)
				yield return null;
			Game_Player_Resources = Game_Player_Resources.Instance;

			while (Game_Clock.Instance == null)
				yield return null;
			Game_Clock.Instance.Tick_Group5 += RunQueue;
			Ready = true;
		}

		private void OnDestroy ()
		{
			if (Ready)
			{
				Game_Clock.Instance.Tick_Group5 -= RunQueue;
			}
		}
		#endregion
		#region Loops
		private void RunQueue (int currentTick)
		{
			int requestToHandle = Requests.Count / Game_Clock.Instance.NumTicks_Group5;
			for (int i = 0; i < requestToHandle; i++)
			{
				HandleRequest();
			}

			// If there are still requests left after last tick, process them now
			if (currentTick == Game_Clock.Instance.NumTicks_Group5)
				while (Requests.Count > 0)
				{
					HandleRequest();
				}
		}

		private void HandleRequest ()
		{
			ResourcesRequest currentRequest = Requests.Dequeue();
			if (currentRequest.Grid_Node_Stats.CheckResources(currentRequest.NodeResourceTypes, currentRequest.NodeResourceAmounts)
				&&
				Game_Player_Resources.Holds(currentRequest.CraftedResourceTypes, currentRequest.CraftedResourceAmounts))
			{
				Game_Player_Resources.Use(currentRequest.CraftedResourceTypes, currentRequest.CraftedResourceAmounts);
				currentRequest.Grid_Node_Stats.GiveResources(currentRequest.NodeResourceTypes, currentRequest.NodeResourceAmounts, false);
				currentRequest.Item_Actions.GotResources(true);
			}
			else
			{
				currentRequest.Item_Actions.GotResources(false);
			}
		}
		#endregion
		#region Public Actions
		public void AddRequest (ResourcesRequest resourceRequest)
		{
			Requests.Enqueue(resourceRequest);
		}
		#endregion
	}

	/// <summary>
	///	  ResourceRequest Class is used to make a request from the Player's Resources for an Item. <br/>
	///   Use the provided constructor to make a new request.<br/>
	///   (For buying\selling items refer to ResourcesCost)
	/// </summary>
	public class ResourcesRequest
	{
		public Item_Actions Item_Actions;
		public Game_Node_Stats Grid_Node_Stats;
		public List<NodeResourceType> NodeResourceTypes;
		public List<float> NodeResourceAmounts;
		public List<CraftedResourceType> CraftedResourceTypes;
		public List<float> CraftedResourceAmounts;

		public ResourcesRequest (Item_Actions item_Actions, Game_Node_Stats grid_Node_Stats,
			List<NodeResourceType>nodeResourceTypes, List<float>nodeResourceAmounts,
			List<CraftedResourceType> craftedResourceTypes, List<float>craftedResourceAmounts)
		{
			Item_Actions = item_Actions;
			Grid_Node_Stats = grid_Node_Stats;
			NodeResourceTypes = nodeResourceTypes;
			NodeResourceAmounts = nodeResourceAmounts;
			CraftedResourceTypes = craftedResourceTypes;
			CraftedResourceAmounts = craftedResourceAmounts;
		}

		public void SendToQueue ()
		{
			Game_ResourceQueue.Instance.AddRequest(this);
		}
	}

	/// <summary>
	/// ResourcesCost Class is used to make define a cost for buying\selling an item.<br />
	/// Use the provided constructor to make a new cost.<br />
	/// (For requesting resources refer to ResourcesRequest)
	/// </summary>
	[System.Serializable]
	public class ResourcesStack
	{
		public List<NodeResourceType> NodeResourceTypes = new List<NodeResourceType>();
		public List<float> NodeResourceAmounts = new List<float>();
		public List<CraftedResourceType> CraftedResourceTypes = new List<CraftedResourceType>();
		public List<float> CraftedResourceAmounts = new List<float>();

		// Constructor
		public ResourcesStack (List<NodeResourceType> nodeResourceTypes = null, List<float> nodeResourceAmounts = null,
			List<CraftedResourceType> craftedResourceTypes = null, List<float> craftedResourceAmounts = null)
		{
			if (nodeResourceAmounts != null && nodeResourceAmounts.Count > 0)
			{
				NodeResourceTypes = new List<NodeResourceType>(nodeResourceTypes);
				NodeResourceAmounts = new List<float>(nodeResourceAmounts);
			}

			if (craftedResourceAmounts != null && craftedResourceAmounts.Count > 0)
			{
				CraftedResourceTypes = new List<CraftedResourceType>(craftedResourceTypes);
				CraftedResourceAmounts = new List<float>(craftedResourceAmounts);
			}
		}

		public bool PlayerCanBuy ()
		{
			return Game_Player_Resources.Instance.Holds(CraftedResourceTypes, CraftedResourceAmounts);
		}

		public bool NodeCanBeUsed(Game_Node_Stats nodeStats)
		{
			return nodeStats.HasResources(NodeResourceTypes, NodeResourceAmounts); ;
		}

		/// <summary>
		/// Buys this ResourcesCost if able.<br/>
		/// Returns True on Success.
		/// </summary>
		/// <param name="nodeStats">The node, if needed.</param>
		/// <returns></returns>
		public bool Buy (Game_Node_Stats nodeStats = null)
		{
			// Check
			bool canBuy = true;
			if (nodeStats != null)
			{
				canBuy &= this.NodeCanBeUsed(nodeStats);
			}
			canBuy &= this.PlayerCanBuy();

			// Apply
			if (canBuy)
			{
				canBuy &= Game_Player_Resources.Instance.Use(CraftedResourceTypes, CraftedResourceAmounts);
				if (canBuy)
				{
					nodeStats.GiveResources(NodeResourceTypes, NodeResourceAmounts, false);
					return true;
				}
			}
			return false;
		}
		public void PlayerGain ()
		{
			Game_Player_Resources.Instance.Gain(this.CraftedResourceTypes, this.CraftedResourceAmounts);
		}
	}
}