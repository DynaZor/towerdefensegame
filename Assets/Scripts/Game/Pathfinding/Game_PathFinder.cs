﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using Unity.Collections;
using UnityEngine;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Burst;

public class Game_PathFinder : MonoBehaviour
{
	#region Singleton
	public static Game_PathFinder Instance;
	private void Awake ()
	{
		if (Instance != null)
			Debug.Log("Game_PathFinder - More than one instance!\n" +
				"Registered Instance: " + Instance.gameObject.name +
				"This Instance: " + gameObject.name, this);
		else
			Instance = this;
	}
	#endregion
	#region Testing
#if UNITY_EDITOR
	[Header("Testing")]
	public bool RunTestNow = false;
	public int TestRepeats = 10;
	public bool TestChangeEachRepeat = false;
	private void OnValidate ()
	{
		if (RunTestNow)
		{
			RunTestNow = false;
			RunTest();
		}
	}

	private void RunTest ()
	{
		if (Game_Grid.Instance == null)
			return;
		float startTIme = Time.realtimeSinceStartup;
		float substractTime = 0f;
		
		Vector2Int startPos = getRandomPos();
		Vector2Int endPos =  getRandomPos();
		GetNewRandomPathPair(ref startPos, ref endPos, ref substractTime);

		int itemType = 1;
		if (Game_Grid.Instance.NodeAt(endPos).Enemy != null)
		{
			itemType = 0;
		}
		int success = 0;
		for (int i = 0; i < TestRepeats; i++)
		{
			Vector2Int[] result = FindPath(startPos, endPos, itemType, out _);
			if (result != null && result.Length > 0)
			{
				try
				{
					result[result.Length - 1].x = 0; // Make sure item is calculated and used
					success++;
				}
				catch
				{}
			}
			if (TestChangeEachRepeat && i < TestRepeats - 1) // Change start and end if Change is on and not last repeat
				GetNewRandomPathPair(ref startPos, ref endPos, ref substractTime);
		}

		float endTime = Time.realtimeSinceStartup;
		float calcTime = (endTime - startTIme - substractTime) * 1000f;
		Debug.Log("PF: Test Finished\n" +
			TestRepeats + "Repeats\n" +
			(TestChangeEachRepeat ? "Changing every Repeat" : "Constant") +
			"\nResults:\n" +
			"Found " + success + " // " + TestRepeats + " Paths\n" +
			"Total time " + calcTime + " ms\n" +
			"Average per PF " + (calcTime / TestRepeats) + " ms\n" +
			"Total GetNewRandomPathPair " + (substractTime * 1000f) + " ms");
	}

	private void GetNewRandomPathPair (ref Vector2Int startPos, ref Vector2Int endPos, ref float substractTime)
	{
		int rndTries = 0;
		float startTIme = Time.realtimeSinceStartup;
		while (startPos == endPos ||
					(Game_Grid.Instance.NodeAt(startPos).Item != null && Game_Grid.Instance.NodeAt(endPos).Item != null) ||
					(Game_Grid.Instance.NodeAt(startPos).Enemy != null && Game_Grid.Instance.NodeAt(endPos).Enemy != null))
		{
			startPos = getRandomPos();
			rndTries++;
			if (rndTries > 5)
			{
				endPos = getRandomPos();
			}
			if (rndTries > 100)
			{
				Debug.LogWarning("PF: GetNewRandomPathPair failed in 100 tries");
				break;
			}
		}
		float endTime = Time.realtimeSinceStartup;
		float calcTime = endTime - startTIme;
		substractTime += calcTime;
	}

	private Vector2Int getRandomPos ()
	{
		return new Vector2Int(UnityEngine.Random.Range(0, Game_Grid.Instance.GridSizeX), UnityEngine.Random.Range(0, Game_Grid.Instance.GridSizeY));
	}
#endif
	#endregion

	private const int moveCost = 1;
	private const int waitMoveCost = 51;

	#region Cached Values
	private static int[,] lastMapTypes;
	private static int[,] lastMapHealth;
	private static int[] lastConvertedMapTypes;
	private static int[] lastConvertedMapHealth;
	#endregion

	/// <summary>
	/// Get A* Path
	/// </summary>
	/// <param name="startPosition">Start position.</param>
	/// <param name="targetPosition">Target position.</param>
	/// <param name="itemType">Type of the item: 0 = Ally, 1 = Enemy</param>
	/// <param name="overrideDCostDiv">Optional: Override Destroy Cost Divider.</param>
	/// <returns>Returns Vector2Int Array starting with first move and ending with target node.</returns>
	public static Vector2Int[] FindPath (Vector2Int startPosition, Vector2Int targetPosition, int itemType, out int pathScore, int overrideDCostDiv = 1)
	{
		int2 GridSize = new int2(Game_Grid.Instance.GridSizeX, Game_Grid.Instance.GridSizeY);
		var finalPath = new NativeList<int2> (Allocator.TempJob);
		var mapType = Array2DToNativeArray(Game_Grid.Instance.GetItemMapForPF(), GridSize, true);
		var mapHealth = Array2DToNativeArray(Game_Grid.Instance.GetHealthMapForPF(), GridSize, false);
		FindPathJob job = new FindPathJob
		{
			startPosition = Vector2IntToInt2(startPosition),
			targetPosition = Vector2IntToInt2(targetPosition),
			itemType = itemType,
			mapType = mapType,
			mapHealth = mapHealth,
			overrideDCostDiv = overrideDCostDiv,
			GridSize = GridSize,
			finalPath = finalPath,
			pathScore = 0
		};
		JobHandle jobHandle = job.Schedule();
		jobHandle.Complete();

		pathScore = job.pathScore;

		Vector2Int[] convertedResult = new Vector2Int[finalPath.Length];
		for (int i = finalPath.Length - 1; i >= 0; i--)
		{
			convertedResult[i] = Int2ToVector2Int(finalPath[i]);
		}
		finalPath.Dispose();
		mapHealth.Dispose();
		mapType.Dispose();
		return convertedResult;
	}
	[BurstCompile]
	private struct FindPathJob : IJob
	{
		// Map Data
		public int itemType; // 0 - Ally, 1 - Enemy
		public NativeArray<int> mapType; // 0 - Free, 1 - Ally, 2 - Enemy, 3 - Obstacle, 4 - Ally Movable
		public NativeArray<int> mapHealth;
		public int2 GridSize;
		// Path Data
		public int2 startPosition;
		public int2 targetPosition;
		public int overrideDCostDiv;
		public int pathScore;

		// Result
		public NativeList<int2> finalPath;

		public void Execute ()
		{
			NativeArray<PathNode> pathNodeArray = new NativeArray<PathNode>(GridSize.x * GridSize.y, Allocator.Temp);

			// Grid Setup
			for (int y = 0; y < GridSize.y; y++)
			{
				for (int x = 0; x < GridSize.x; x++)
				{
					PathNode pathNode = new PathNode
					{
						x = x,
						y = y,
						index = CalculateNodeIndex(x, y, GridSize.x),

						gCost = int.MaxValue,
						hCost = CalculateHCost(new int2(x, y), targetPosition),
						isWalkable = false
					};

					// Implement data from maps
					if (mapType[pathNode.index] == 0) // Tile Free
					{
						pathNode.isWalkable = true;
					}
					else if (mapType[pathNode.index] == 3) // Tile has Obstacle
					{
						pathNode.SetisDestroyable(false);
					}
					else if (itemType == 1) // Enemy, Tile has Ally or Enemy
					{
						switch (mapType[pathNode.index])
						{
							case 1: // Static Ally
							case 4: // Movable Ally
								pathNode.SetisDestroyable(true, mapHealth[pathNode.index] / overrideDCostDiv);
								break;
							case 2: // Enemy
								pathNode.SetSameType();
								break;
							default:
								break;
						}
					}
					else if (itemType == 0) // Ally, Tile has Ally or Enemy
					{
						switch (mapType[pathNode.index])
						{
							case 1: // Static Ally
								pathNode.isWalkable = false;
								break;
							case 4: // Movable Ally
								pathNode.SetSameType();
								break;
							case 2: // Enemy
								pathNode.SetisDestroyable(true, mapHealth[pathNode.index] / overrideDCostDiv);
								break;
							default:
								break;
						}
					}

					pathNode.CalculateFinalCost();

					pathNode.cameFromNodeIndex = -1;

					pathNodeArray[pathNode.index] = pathNode;
				}
			}

			NativeArray<int2> neighbourOffsetArray = new NativeArray<int2>(4, Allocator.Temp);
			neighbourOffsetArray[0] = new int2(0, +1); // Up
			neighbourOffsetArray[1] = new int2(0, -1); // Down
			neighbourOffsetArray[2] = new int2(-1, 0); // Left
			neighbourOffsetArray[3] = new int2(+1, 0); // Right

			int endNodeIndex = CalculateNodeIndex(targetPosition.x, targetPosition.y, GridSize.x);

			PathNode startNode = pathNodeArray[CalculateNodeIndex(startPosition.x, startPosition.y, GridSize.x)];
			startNode.gCost = 0;
			startNode.CalculateFinalCost();
			pathNodeArray[startNode.index] = startNode;
			NativeArray<PathNode> pathNodeArray_NoSameType = new NativeArray<PathNode>(pathNodeArray, Allocator.Temp);
			NativeList<int> openList = new NativeList<int>(Allocator.Temp);
			NativeList<int> openList_NoSameType = new NativeList<int>(Allocator.Temp);
			NativeList<int> closedList = new NativeList<int>(Allocator.Temp);
			NativeList<int> closedList_NoSameType = new NativeList<int>(Allocator.Temp);

			openList.Add(startNode.index);
			openList_NoSameType.Add(startNode.index);

			// Loop
			LoopOpenList(ref pathNodeArray_NoSameType, ref neighbourOffsetArray, endNodeIndex,
				ref openList_NoSameType, ref closedList_NoSameType, true, out NativeList<int2> path2, out int score2);
			LoopOpenList(ref pathNodeArray, ref neighbourOffsetArray, endNodeIndex, 
				ref openList, ref closedList, false, out NativeList<int2> path1, out int score1);

			if (path1.Length > 0 && path2.Length > 0)
			{
				if (score1 > score2)
				{
					path1.Dispose();
					SetFinalValues(ref path2, ref score2);
				}
				else
				{
					path2.Dispose();
					SetFinalValues(ref path1, ref score1);
				}
			}
			else
			{
				if (path1.Length > 0)
				{
					path2.Dispose();
					SetFinalValues(ref path1, ref score1);
				}
				else
				{
					path1.Dispose();
					SetFinalValues(ref path2, ref score2);
				}
			}

			neighbourOffsetArray.Dispose();
			pathNodeArray.Dispose();
			pathNodeArray_NoSameType.Dispose();
			openList.Dispose();
			openList_NoSameType.Dispose();
			closedList.Dispose();
			closedList_NoSameType.Dispose();
		}

		private void SetFinalValues(ref NativeList<int2> path, ref int score)
		{
			pathScore = score;
			for (int i = 0; i < path.Length; i++)
			{
				finalPath.Add(path[i]);
			}
			path.Dispose();
		}

		private void LoopOpenList 
			(ref NativeArray<PathNode> pathNodeArray, ref NativeArray<int2> neighbourOffsetArray, 
			int endNodeIndex, ref NativeList<int> openList, ref NativeList<int> closedList, bool ignoreSameType,
			out NativeList<int2> path, out int score)
		{
			path = new NativeList<int2>(Allocator.Temp);
			score = 0;
			while (openList.Length > 0)
			{
				int currentNodeIndex = GetLowestCostFNodeIndex(openList, pathNodeArray);
				PathNode currentNode = pathNodeArray[currentNodeIndex];

				if (currentNodeIndex == endNodeIndex) // Reached our destination!
				{
					break;
				}

				// Remove current node from Open List
				for (int i = 0; i < openList.Length; i++)
				{
					if (openList[i] == currentNodeIndex)
					{
						openList.RemoveAtSwapBack(i);
						break;
					}
				}

				closedList.Add(currentNodeIndex);

				for (int i = 0; i < neighbourOffsetArray.Length; i++)
				{
					int2 neighbourOffset = neighbourOffsetArray[i];
					int2 neighbourPosition = new int2(currentNode.x + neighbourOffset.x, currentNode.y + neighbourOffset.y);

					if (!PositionExists(neighbourPosition, GridSize)) // Position doesn't exist
					{
						continue;
					}

					int neighbourNodeIndex = CalculateNodeIndex(neighbourPosition.x, neighbourPosition.y, GridSize.x);

					if (closedList.Contains(neighbourNodeIndex)) // Already searched this node
					{
						continue;
					}

					PathNode neighbourNode = pathNodeArray[neighbourNodeIndex];
					if (!neighbourNode.isWalkable) // Not walkable
					{
						continue;
					}

					if (ignoreSameType && neighbourNode.needQueue) // Same Type skip
					{
						continue;
					}

					int2 currentNodePosition = new int2(currentNode.x, currentNode.y);

					int tentativeGCost = currentNode.gCost + CalculateHCost(currentNodePosition, neighbourPosition);
					if (tentativeGCost < neighbourNode.gCost)
					{
						neighbourNode.cameFromNodeIndex = currentNodeIndex;
						neighbourNode.gCost = tentativeGCost;
						neighbourNode.CalculateFinalCost();
						pathNodeArray[neighbourNodeIndex] = neighbourNode;

						if (!openList.Contains(neighbourNode.index))
						{
							openList.Add(neighbourNode.index);
						}
					}

				}
			}

			PathNode endNode = pathNodeArray[endNodeIndex];
			if (endNode.cameFromNodeIndex == -1)
			{
				//Debug.Log("Didn't find a path!");
			}
			else // Found path
			{
				CalculatePath(pathNodeArray, endNode, out path, out score);
			}
		}

		private static void CalculatePath (NativeArray<PathNode> pathNodeArray,
									 PathNode endNode,
									 out NativeList<int2> finalPath,
									 out int pathScore)
		{
			finalPath = new NativeList<int2>(Allocator.Temp);
			pathScore = 0;
			if (endNode.cameFromNodeIndex == -1) // Couldn't find a path!
			{
				return;
			}
			else // Found a path
			{
				finalPath.Add(new int2(endNode.x, endNode.y));

				PathNode currentNode = endNode;
				while (currentNode.cameFromNodeIndex != -1)
				{
					PathNode cameFromNode = pathNodeArray[currentNode.cameFromNodeIndex];
					finalPath.Add(new int2(cameFromNode.x, cameFromNode.y));
					pathScore += cameFromNode.fCost;
					currentNode = cameFromNode;
				}
			}
		}
		#region Helpers
		private static bool PositionExists (int2 gridPosition, int2 gridSize)
		{
			return
				gridPosition.x >= 0 &&
				gridPosition.y >= 0 &&
				gridPosition.x < gridSize.x &&
				gridPosition.y < gridSize.y;
		}

		private int CalculateNodeIndex (int x, int y, int gridWidth) => x + (y * gridWidth);

		private static int CalculateHCost (int2 posA, int2 posB)
		{
			int xDistance = math.abs(posA.x - posB.x);
			int yDistance = math.abs(posA.y - posB.y);
			return (yDistance + xDistance) * moveCost;
		}

		private static int GetLowestCostFNodeIndex (NativeList<int> openList, NativeArray<PathNode> pathNodeArray)
		{
			PathNode lowestCostPathNode = pathNodeArray[openList[0]];
			for (int i = 1; i < openList.Length; i++)
			{
				PathNode testPathNode = pathNodeArray[openList[i]];
				if (testPathNode.fCost < lowestCostPathNode.fCost)
				{
					lowestCostPathNode = testPathNode;
				}
			}
			return lowestCostPathNode.index;
		}
		#endregion
		private struct PathNode
		{
			public int x;
			public int y;

			public int index;

			public int gCost;
			public int hCost; // Distance Cost
			public int? dCost; // Destroy Cost
			public int fCost; // Final Cost

			public bool isWalkable;
			public bool isDestroyable;

			public bool needQueue;

			public int cameFromNodeIndex;

			public void CalculateFinalCost ()
			{
				fCost = gCost + hCost + (dCost ?? 0);
			}

			public void SetIsWalkable (bool isWalkable)
			{
				this.isWalkable = isWalkable;
			}

			public void SetisDestroyable (bool isDestroyable, int? dCost = null)
			{
				this.isDestroyable = isDestroyable;
				this.dCost = dCost;

				this.isWalkable = isDestroyable;
			}

			public void SetSameType ()
			{
				needQueue = true;
				dCost = waitMoveCost;
			}
		}
	}
	#region Helpers
	private static NativeArray<int> Array2DToNativeArray (int[,] input, int2 GridSize, bool MapTypes)
	{
		if (MapTypes)
		{
			if (input == lastMapTypes)
			{
				return new NativeArray<int>(lastConvertedMapTypes, Allocator.TempJob);
			}
		}
		else
		{
			if (input == lastMapHealth)
			{
				return new NativeArray<int>(lastConvertedMapHealth, Allocator.TempJob);
			}
		}

		int[] Array1D = new int[GridSize.x * GridSize.y];
		for (int y = 0; y < GridSize.y; y++)
		{
			for (int x = 0; x < GridSize.x; x++)
			{
				Array1D[y * GridSize.x + x] = input[x, y];
			}
		}

		NativeArray<int> result = new NativeArray<int>(Array1D, Allocator.TempJob);

		if (MapTypes)
		{
			lastMapTypes = input;
			lastConvertedMapTypes = result.ToArray();
		}
		else
		{
			lastMapHealth = input;
			lastConvertedMapHealth = result.ToArray();
		}
		return result;
	}

	private static int2 Vector2IntToInt2 (Vector2Int input)
	{
		return new int2(input.x, input.y);
	}

	private static Vector2Int Int2ToVector2Int (int2 input)
	{
		return new Vector2Int(input.x, input.y);
	}
	#endregion
}