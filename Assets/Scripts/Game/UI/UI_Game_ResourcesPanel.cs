﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UI_Game_ResourcesPanel : MonoBehaviour
{
	[Header("Resources Panel")]
	[Header("References")]
	[SerializeField] private TextMeshProUGUI Label_Food = null;
	[SerializeField] private TextMeshProUGUI Label_Tech = null;
	[SerializeField] private TextMeshProUGUI Label_Electricity = null;
	[SerializeField] private TextMeshProUGUI Label_Age = null;

	private float last_Food = -1f;
	private float last_Tech = -1f;
	private float last_Electricity = -1f;
	private float last_Age = -1f;

	private bool Ready = false;

	#region Setup
	private void Awake ()
	{
		StartCoroutine(Setup());
	}

	private IEnumerator Setup ()
	{
		while (Game_Player_Resources.Instance == null)
			yield return null;
		Game_Player_Resources.Instance.PlayerResourcesUpdated += PlayerResourcesUpdated;
		Ready = true;
	}

	private void OnDestroy ()
	{
		if (Ready)
		{
			if (Game_Player_Resources.Instance != null)
				Game_Player_Resources.Instance.PlayerResourcesUpdated -= PlayerResourcesUpdated;
		}
	}
	#endregion

	#region Private Actions
	private void PlayerResourcesUpdated (Dictionary<ResourcesSystem.CraftedResourceType, float> newValues)
	{
		if (last_Food != newValues[ResourcesSystem.CraftedResourceType.Food])
		{
			Label_Food.SetText(((uint) newValues[ResourcesSystem.CraftedResourceType.Food]).GetNumberString(2));
			last_Food = newValues[ResourcesSystem.CraftedResourceType.Food];
		}
		if (last_Tech != newValues[ResourcesSystem.CraftedResourceType.Tech])
		{
			Label_Tech.SetText(((uint) newValues[ResourcesSystem.CraftedResourceType.Tech]).GetNumberString(2));
			last_Tech = newValues[ResourcesSystem.CraftedResourceType.Tech];
		}
		if (last_Age != newValues[ResourcesSystem.CraftedResourceType.Age])
		{
			Label_Age.SetText(((uint) newValues[ResourcesSystem.CraftedResourceType.Age]).GetNumberString(2));
			last_Age = newValues[ResourcesSystem.CraftedResourceType.Age];
		}
		if (last_Electricity != newValues[ResourcesSystem.CraftedResourceType.Electricity])
		{
			Label_Electricity.SetText(((uint) newValues[ResourcesSystem.CraftedResourceType.Electricity]).GetNumberString(2));
			last_Electricity = newValues[ResourcesSystem.CraftedResourceType.Electricity];
		}
	}
	#endregion
}