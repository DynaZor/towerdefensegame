﻿// Code written by Shahar DynaZor Alon Use of this code outside of this project
// without Shahar's permission is prohibited!
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ItemStatuses;
using System.Linq;

public class UI_Game_Item_StatusIconsManager : MonoBehaviour
{
	#region Global Data
	private static UI_Game_Item_StatusIconsParent iconPoolParent = null;
	private static bool alreadyWaitingForIconPoolParent = false;

	private static readonly Dictionary<ItemStatus, Vector3> iconLocalPositions = new Dictionary<ItemStatus, Vector3>()
	{
		{ItemStatus.InActive, new Vector3(0, 1, -2) },
		{ItemStatus.LowHealth10, new Vector3(0.75f, 1, -2) },
		{ItemStatus.LowHealth25, new Vector3(0.75f, 1, -2) },
		{ItemStatus.LowHealth50, new Vector3(0.75f, 1, -2) },
		{ItemStatus.Producing, new Vector3(0, 0.75f, -2) },
		{ItemStatus.NotEnoughGNDRes, new Vector3(0.75f, 0.25f, -2) },
		{ItemStatus.NotEnoughCraftedRes, new Vector3(0.50f, 0.25f, -2) },
		{ItemStatus.Moving, new Vector3(0.75f, 0.75f, -2) },
		{ItemStatus.WaitingToMove, new Vector3(0.75f, 0.75f, -2) },
		{ItemStatus.Attacking, new Vector3(0.75f, 0.5f, -2) },
		{ItemStatus.TookDamage, new Vector3(0.5f, 1, -2) },
		{ItemStatus.Healing, new Vector3(0, 0.50f, -2) },
		{ItemStatus.Upgraded, new Vector3(0, 0.25f, -2) },
	};

	private static readonly List<ItemStatus> activeOnlyStatuses = new List<ItemStatus>()
	{
		ItemStatus.Attacking, ItemStatus.Moving, ItemStatus.WaitingToMove,
		ItemStatus.Producing, ItemStatus.Healing, ItemStatus.NotEnoughCraftedRes, ItemStatus.NotEnoughGNDRes
	};
	#endregion
	#region My Data
	private Item_Information Item;
	private bool Ready = false;
	private bool inProgress = false;

	private int lastUpdate = 0;
	private int thisUpdate = 0;
	private Coroutine waiter;

	private Dictionary<ItemStatus, bool> Statuses = new Dictionary<ItemStatus, bool>()
	{
		{ItemStatus.InActive, false },
		{ItemStatus.LowHealth10, false },
		{ItemStatus.LowHealth25, false },
		{ItemStatus.LowHealth50, false },
		{ItemStatus.Healing, false },
		{ItemStatus.Producing, false },
		{ItemStatus.Moving, false },
		{ItemStatus.WaitingToMove, false },
		{ItemStatus.Attacking, false },
		{ItemStatus.TookDamage, false },
		{ItemStatus.NotEnoughGNDRes, false },
		{ItemStatus.NotEnoughCraftedRes, false },
		{ItemStatus.Upgraded, false },
	};
	private Dictionary<ItemStatus, bool> lastStatuses = new Dictionary<ItemStatus, bool>()
	{
		{ItemStatus.InActive, false },
		{ItemStatus.LowHealth10, false },
		{ItemStatus.LowHealth25, false },
		{ItemStatus.LowHealth50, false },
		{ItemStatus.Healing, false },
		{ItemStatus.Producing, false },
		{ItemStatus.Moving, false },
		{ItemStatus.WaitingToMove, false },
		{ItemStatus.Attacking, false },
		{ItemStatus.TookDamage, false },
		{ItemStatus.NotEnoughGNDRes, false },
		{ItemStatus.NotEnoughCraftedRes, false },
		{ItemStatus.Upgraded, false },
	};

	private Dictionary<ItemStatus, SpriteRenderer> icons = new Dictionary<ItemStatus, SpriteRenderer>()
	{
		{ItemStatus.InActive, null },
		{ItemStatus.LowHealth10, null },
		{ItemStatus.LowHealth25, null },
		{ItemStatus.LowHealth50, null },
		{ItemStatus.Healing, null },
		{ItemStatus.Producing, null },
		{ItemStatus.Moving, null },
		{ItemStatus.WaitingToMove, null },
		{ItemStatus.Attacking, null },
		{ItemStatus.TookDamage, null },
		{ItemStatus.NotEnoughGNDRes, null },
		{ItemStatus.NotEnoughCraftedRes, null },
		{ItemStatus.Upgraded, null },
	};

	private List<SpriteRenderer> toMoveToParent = new List<SpriteRenderer>();
	private List<ItemStatus> toGet = new List<ItemStatus>();
	private Dictionary<SpriteRenderer, ItemStatus> toUpdateIcon = new Dictionary<SpriteRenderer, ItemStatus>();
	#endregion

	#region Setup
	private void Awake ()
	{
		Item = GetComponent<Item_Information>();
		StartCoroutine(Setup());
	}

	private IEnumerator Setup ()
	{
		if (!alreadyWaitingForIconPoolParent)
		{
			alreadyWaitingForIconPoolParent = true;
			while (iconPoolParent == null)
			{
				//Debug.Log("still waiting0", this);
				yield return null;
				iconPoolParent = FindObjectOfType<UI_Game_Item_StatusIconsParent>();
			}
		}
		else
		{
			while (iconPoolParent == null)
			{
				yield return null;
				//Debug.Log("still waiting1", this);
			}
		}

		while (Item == null)
		{
			//Debug.Log("still waiting2", this);
			yield return null;
			Item = GetComponent<Item_Information>();
		}

		while (!Item.Ready)
		{
			//Debug.Log("still waiting3", this);
			yield return null;
		}

		Item.Item_Stats.ItemStateUpdatedEvent += UpdateItemStatus;
		Item.Item_Actions.ItemStatusUpdated += () => UpdateItemStatuses();
		Ready = true;

		CheckAll(true);
	}

	private void OnDestroy ()
	{
		if (Ready && Item != null)
		{
			Item.Item_Stats.ItemStateUpdatedEvent -= UpdateItemStatus;
			if (Item.Item_Actions != null)
			{
				Item.Item_Actions.ItemStatusUpdated -= () => UpdateItemStatuses();
			}
		}
	}

	#endregion

	private IEnumerator WaiterCoroutine ()
	{
		while (inProgress)
		{
			yield return null;
		}
		CheckAll();
	}

	private void UpdateItemStatus (bool active)
	{
		UpdateItemStatuses(true);
	}

	public void UpdateItemStatuses (bool force = true)
	{
		CheckAll(force);
	}

	private void CheckAll (bool firstTime = false)
	{
		if (inProgress)
		{
			if (waiter == null)
			{
				waiter = StartCoroutine(WaiterCoroutine());
			}
			return;
		}

		inProgress = true;
		lastUpdate = thisUpdate;
		lastStatuses = new Dictionary<ItemStatus, bool>(Statuses);
		var simpleItemStatus = Item.Item_Stats.ItemStatus;

		if (simpleItemStatus == Item_Stats.ItemStatusTypes.InActive)
		{
			Statuses[ItemStatus.InActive] = true;
			Statuses[ItemStatus.Producing] = false;
			Statuses[ItemStatus.Moving] = false;
			Statuses[ItemStatus.Attacking] = false;
			Statuses[ItemStatus.NotEnoughCraftedRes] = false;
			Statuses[ItemStatus.NotEnoughGNDRes] = false;
			Statuses[ItemStatus.WaitingToMove] = false;
			Statuses[ItemStatus.Healing] = false;
		}
		else // Active
		{
			Statuses[ItemStatus.InActive] = false;
			Statuses[ItemStatus.Producing] = simpleItemStatus == Item_Stats.ItemStatusTypes.Producing;
			Statuses[ItemStatus.Moving] = Item.Item_Actions.IsMoving;
			Statuses[ItemStatus.WaitingToMove] = Item.Item_Actions.IsWaiting;
			Statuses[ItemStatus.Attacking] = Item.Item_Actions.Attacked;
			Statuses[ItemStatus.Healing] = Item.Item_Actions.Healed;
			Statuses[ItemStatus.NotEnoughCraftedRes] = !Item.Item_Production.ProducedLastCycle; //!! 1Alpha Item Stats: Finish Implementing Needs(Not)Met Update
			Statuses[ItemStatus.NotEnoughGNDRes] = !Item.Item_Production.ProducedLastCycle; //!! 1Alpha Item Stats: Finish Implementing Needs(Not)Met Update
		}

		UpdateHealthStatuses();

		Statuses[ItemStatus.TookDamage] = Item.Item_Actions.TookDamage;
		Statuses[ItemStatus.Upgraded] = Item.Item_Actions.Upgraded;

		UpdateStatusIcons(firstTime);
	}

	private void UpdateHealthStatuses ()
	{
		float health = Item.Item_Stats.CurrentHealth / Item.Item_Stats.MaxHealth;
		if (health <= 0.25f)
		{
			if (health <= 0.10f)
			{
				Statuses[ItemStatus.LowHealth10] = true;
				Statuses[ItemStatus.LowHealth25] = false;
				Statuses[ItemStatus.LowHealth50] = false;
			}
			else if (health <= 0.25f)
			{
				Statuses[ItemStatus.LowHealth10] = false;
				Statuses[ItemStatus.LowHealth25] = true;
				Statuses[ItemStatus.LowHealth50] = false;
			}
			else if (health <= 0.50f)
			{
				Statuses[ItemStatus.LowHealth10] = false;
				Statuses[ItemStatus.LowHealth25] = false;
				Statuses[ItemStatus.LowHealth50] = true;
			}
		}
		else
		{
			Statuses[ItemStatus.LowHealth10] = false;
			Statuses[ItemStatus.LowHealth25] = false;
		}
	}

	private void UpdateStatusIcons (bool firstTime = false)
	{
		if (Statuses[ItemStatus.InActive] != lastStatuses[ItemStatus.InActive])
			UpdateStatusIcon(ItemStatus.InActive);

		if (Statuses[ItemStatus.InActive])
		{
			// Disable all ActiveOnly
			List<ItemStatus> toDisable = Statuses.Keys.ToList();
			toDisable.RemoveAll(x => lastStatuses[x] && activeOnlyStatuses.Contains(x));
			for (int i = 0; i < toDisable.Count; i++)
			{
				PrepareToMoveIconToParent(toDisable[i]);
			}
		}
		else // Active Only
		{
			for (int i = 0; i < activeOnlyStatuses.Count; i++)
			{
				if (lastStatuses[activeOnlyStatuses[i]] != Statuses[activeOnlyStatuses[i]])
				{
					UpdateStatusIcon(activeOnlyStatuses[i]);
				}
			}
		}

		// Update all non ActiveOnly
		List<ItemStatus> nonActiveOnly = Statuses.Keys.ToList();
		nonActiveOnly.RemoveAll(x => lastStatuses[x] != Statuses[x] && activeOnlyStatuses.Contains(x));
		for (int i = 0; i < nonActiveOnly.Count; i++)
		{
			UpdateStatusIcon(nonActiveOnly[i]);
		}

		GetIcons();
	}
	private void UpdateStatusIcon (ItemStatus status)
	{
		// Assuming status has changed
		if (Statuses[status])
		{
			PrepareToGetIcon(status);
		}
		else
		{
			PrepareToMoveIconToParent(status);
		}
	}
	private void PrepareToGetIcon (ItemStatus status)
	{
		if (icons[status] == null)
		{
			toGet.Add(status);
		}
	}

	private void PrepareToMoveIconToParent (ItemStatus status)
	{
		if (icons[status] != null)
		{
			toMoveToParent.Add(icons[status]);
			icons[status] = null;
		}
	}

	private void GetIcons ()
	{
		int count = toGet.Count;
		/*string log = "\n";
		for (int i = 0; i < Statuses.Keys.Count; i++)
		{
			log += ((ItemStatus) i).ToString() + " - " 
				+ lastStatuses[(ItemStatus) i].ToString() + "=>" 
				+ Statuses[(ItemStatus) i].ToString() 
				+ " (hasIcon=" + (icons[(ItemStatus) i] != null).ToString() + ")\n";
		}
		string log2 = "";
		for (int i = 0; i < toGet.Count; i++)
		{
			log2 += toGet[i].ToString();
		}
		Debug.Log("GetIcons called: toGet Count " + count 
			+ "(" + log2 + ")"
			+ "\nStatus:"	+ log, this);
		*/
		if (count > 0)
		{
			List<int> foundSamePlace = new List<int>();
			for (int i = 0; i < toMoveToParent.Count; i++)
			{
				SpriteRenderer samePlace = toMoveToParent.Find(x=>x.transform.localPosition == iconLocalPositions[toGet[0]]);
				if (samePlace != null)
				{
					icons[toGet[0]] = samePlace;
					if (icons[toGet[0]] != null)
						foundSamePlace.Add(toMoveToParent.IndexOf(samePlace));
				}
			}
			while (foundSamePlace.Count > 0)
			{
				toMoveToParent.RemoveAt(foundSamePlace[0]);
				foundSamePlace.RemoveAt(0);
			}

			int movetoparentCount = toMoveToParent.Count;
			while (count > 0)
			{
				if (movetoparentCount > 0)
				{
					icons[toGet[0]] = toMoveToParent[0];
					toMoveToParent.RemoveAt(0);
					toUpdateIcon.Add(icons[toGet[0]], toGet[0]);
					toGet.RemoveAt(0);
					movetoparentCount = toMoveToParent.Count;
				}
				else
				{
					icons[toGet[count - 1]] = iconPoolParent.GetIconSprite();
					icons[toGet[count - 1]].transform.SetParent(transform);
					toUpdateIcon.Add(icons[toGet[count - 1]], toGet[count - 1]);
					toGet.RemoveAt(count - 1);
				}
				count = toGet.Count;
			}
		}

		MoveIconsToParent();
	}

	private void MoveIconsToParent ()
	{
		int count = toMoveToParent.Count;
		if (count > 0)
		{
			while (count > 0)
			{
				iconPoolParent.ThrowIconSprite(toMoveToParent[count - 1]);
				toMoveToParent.RemoveAt(count - 1);
				count = toMoveToParent.Count;
			}
		}

		UpdateStatusIconPositions();
	}

	private void UpdateStatusIconPositions ()
	{
		var keys = Statuses.Keys.ToList();
		for (int i = 0; i < keys.Count; i++)
		{
			if (Statuses[keys[i]])
			{
				//Debug.Log("UpdateStatusIconPositions - " + keys[i] + " " + icons[keys[i]].ToString(), this);
				icons[keys[i]].transform.localPosition = iconLocalPositions[keys[i]];
			}
		}

		UpdateStatusIconSprites();
	}

	private void UpdateStatusIconSprites ()
	{
		List<SpriteRenderer> toupdate = toUpdateIcon.Keys.ToList();
		int count = toupdate.Count;
		if (count > 0)
		{
			while (count > 0)
			{
				toupdate[count - 1].sprite = Visual_Game_SpriteDatabase.Instance.ItemStatusIcons[toUpdateIcon[toupdate[count - 1]]];
				toupdate[count - 1].transform.name = "ItemStatusIcon_" + toUpdateIcon[toupdate[count - 1]].ToString(); 
				toUpdateIcon.Remove(toupdate[count - 1]);
				toupdate.RemoveAt(count - 1);
				count = toupdate.Count;
			}
		}

		inProgress = false;
	}
}