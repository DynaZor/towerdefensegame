﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using TMPro;
using UnityEngine;
using ResourcesSystem;
using System.Linq;
using UnityEngine.UI;
using System;

public class UI_Game_InfoPanel_Item : MonoBehaviour
{
	#region Singleton
	public static UI_Game_InfoPanel_Item Instance;
	private void MakeSingleton ()
	{
		if (Instance != null)
			Debug.Log("UI_Game_InfoPanel_Item" +
				" - More than one instance!\n" +
				"Registered Instance: " + Instance.gameObject.name +
				"This Instance: " + gameObject.name, this);
		else
			Instance = this;
	}
	#endregion

	[Header("Info Panel - Item")]
	[Header("References")]
	[Header("Canvas References")]
	[SerializeField] private Canvas InfoPanel_Canvas = null;
	[Header("Button References")]
	[SerializeField] private Button Button_Move = null;
	[SerializeField] private Image Button_Move_Icon = null;
	[SerializeField] private Button Button_Upgrade = null;
	[SerializeField] private Image Button_Upgrade_Icon = null;
	[SerializeField] private Button Button_Dismantle = null;
	[Header("Info Label References")]
	[SerializeField] private TextMeshProUGUI Label_Title = null;
	[SerializeField] private TextMeshProUGUI Label_Tier = null;
	[SerializeField] private Image Image_Age = null;
	//[SerializeField] private TextMeshProUGUI Label_Health = null;
	[Header("Slider References")]
	[SerializeField] private Slider Slider_Health = null;
	[Header("Need Icon References")]
	[SerializeField] private TextMeshProUGUI Label_NodeNeed_Fertility = null;
	[SerializeField] private TextMeshProUGUI Label_NodeNeed_Water = null;
	[SerializeField] private TextMeshProUGUI Label_NodeNeed_Vegetation = null;
	[SerializeField] private TextMeshProUGUI Label_CraftedNeed_Food = null;
	[SerializeField] private TextMeshProUGUI Label_CraftedNeed_Tech = null;
	[SerializeField] private TextMeshProUGUI Label_CraftedNeed_Electricity = null;
	[SerializeField] private Image Image_NodeNeed_Fertility = null;
	[SerializeField] private Image Image_NodeNeed_Water = null;
	[SerializeField] private Image Image_NodeNeed_Vegetation = null;
	[SerializeField] private Image Image_CraftedNeed_Food = null;
	[SerializeField] private Image Image_CraftedNeed_Tech = null;
	[SerializeField] private Image Image_CraftedNeed_Electricity = null;
	[Header("Production Icon References")]
	[SerializeField] private TextMeshProUGUI Label_Produce_Elect = null;
	[SerializeField] private TextMeshProUGUI Label_Produce_Food = null;
	[SerializeField] private TextMeshProUGUI Label_Produce_Tech = null;
	[SerializeField] private Image Image_Produce_Elect = null;
	[SerializeField] private Image Image_Produce_Food = null;
	[SerializeField] private Image Image_Produce_Tech = null;

	// Private Data
	private Item_Stats currentItem = null;
	
	#region Setup
	private void Awake ()
	{
		MakeSingleton();
		Panel_Hide();
	}

	private void OnDestroy ()
	{
		Panel_Hide();
	}
	#endregion

	#region Public Actions
	#region Buttons
	public void Button_Item_Move ()
	{
		UI_Game_Grid.Instance.Button_Item_Move(currentItem.GetComponent<Item_Actions>());
	}

	public void Button_Item_Dismantle ()
	{
		currentItem.Information.Item_Actions.Dismantle();
	}

	public void Button_Item_Upgrade ()
	{
		currentItem.Information.Item_Actions.Upgrade();
	}
	#endregion

	public void Panel_SetItem (Item_Stats newItem)
	{
		if (currentItem != null)
		{
			currentItem.HealthChangedEvent -= UpdateState_Health;
			currentItem.ItemStateNeedsUpdatedEvent -= UpdateState_NeedsMet;
			currentItem.ItemStateProductionUpdatedEvent -= UpdateState_Production;
			currentItem.HasTargetChangedEvent -= UpdateState_HasTarget;
			if (Game_Clock.Instance != null)
				Game_Clock.Instance.UIUpdateCallEvent -= FrequentUpdate;
		}
		currentItem = newItem;
		if (currentItem != null)
		{
			currentItem.HealthChangedEvent += UpdateState_Health;
			currentItem.ItemStateNeedsUpdatedEvent += UpdateState_NeedsMet;
			currentItem.ItemStateProductionUpdatedEvent += UpdateState_Production;
			currentItem.HasTargetChangedEvent += UpdateState_HasTarget;
			Game_Clock.Instance.UIUpdateCallEvent += FrequentUpdate;
			Update_All();
		}
		if (InfoPanel_Canvas != null)
			InfoPanel_Canvas.enabled = newItem != null? true : false;
	}

	public void Panel_Hide ()
	{
		if (InfoPanel_Canvas != null)
			InfoPanel_Canvas.enabled = false;
		if (currentItem != null)
			Panel_SetItem(null);
	}

	public void Event_ItemUpdgraded (Item_Stats item)
	{
		if (item == currentItem)
			Update_All();
	}
	#endregion

	#region Private Actions

	private void FrequentUpdate ()
	{
		UpdateState_UpgradePossible();
	}

	private void UpdateState_UpgradePossible ()
	{
		if (currentItem != null)
		{
			// Part of FrequentUpdate
			bool? upgradePossible = currentItem.Information.Item_Actions.UpgradePossible();

			Button_Upgrade.gameObject.SetActive(upgradePossible != null);

			if (upgradePossible == null)
				return;
			else if (upgradePossible == true)
			{
				Button_Upgrade.interactable = true;
				Button_Upgrade_Icon.sprite = Visual_Game_SpriteDatabase.Instance.UI_Item_Action_Upgrade_Possible;
			}
			else if (upgradePossible == false)
			{
				Button_Upgrade.interactable = false;
				Button_Upgrade_Icon.sprite = Visual_Game_SpriteDatabase.Instance.UI_Item_Action_Upgrade_ImPossible;
			}
		}
	}

	private void Update_All ()
	{
		if (currentItem != null)
		{
			Label_Title.text = currentItem.ItemName;
			Label_Tier.text = (currentItem.ItemTier + 1).ToString();
			Image_Age.sprite = Visual_Game_SpriteDatabase.Instance.UI_Age[(int) currentItem.Age];
			Button_Move.gameObject.SetActive(currentItem.Movable);
			Button_Dismantle.gameObject.SetActive(currentItem.Demolishable);

			UpdateInfo_Needs();
			UpdateInfo_Produce();

			UpdateState_Health();

			UpdateState_NeedsMet();
			UpdateState_Production();

			UpdateState_HasTarget();

			FrequentUpdate();
		}
	}

	private void UpdateState_HasTarget ()
	{
		if (currentItem.Information.Item_Actions.HasTarget)
			Button_Move_Icon.sprite = Visual_Game_SpriteDatabase.Instance.UI_Item_Action_Move_Active;
		else
			Button_Move_Icon.sprite = Visual_Game_SpriteDatabase.Instance.UI_Item_Action_Move_InActive;
	}

	private void UpdateInfo_Needs ()
	{
		if (currentItem.Information.Item_Needs.NodeNeedTypes != null)
		{
			var nodeNeeds = currentItem.Information.Item_Needs.NodeNeedTypes.ToList();
			var nodeValues = currentItem.Information.Item_Needs.NodeNeedValues.ToList();
			//!! 1Alpha ItemNeeds: Convert Resource Lists to Dictionaries (needs)

			bool need_Fert = nodeNeeds.Contains(NodeResourceType.GroundFertility)
				&& nodeValues[nodeNeeds.IndexOf(NodeResourceType.GroundFertility)] > 0f;
			Image_NodeNeed_Fertility.gameObject.SetActive(need_Fert);
			if (need_Fert) // excuse me
			{
				Label_NodeNeed_Fertility.text = 
					currentItem.Information.Item_Needs.NodeNeedValues[currentItem.Information.Item_Needs.NodeNeedTypes.ToList().IndexOf(NodeResourceType.GroundFertility)].GetNumberString(4);
			}

			bool need_Water = nodeNeeds.Contains(NodeResourceType.GroundWater)
				&& nodeValues[nodeNeeds.IndexOf(NodeResourceType.GroundWater)] > 0f;
			Image_NodeNeed_Water.gameObject.SetActive(need_Water);
			if (need_Water)
			{
				Label_NodeNeed_Water.text = 
					currentItem.Information.Item_Needs.NodeNeedValues[currentItem.Information.Item_Needs.NodeNeedTypes.ToList().IndexOf(NodeResourceType.GroundWater)].GetNumberString(4);
			}

			bool need_Veg = nodeNeeds.Contains(NodeResourceType.Vegetation)
				&& nodeValues[nodeNeeds.IndexOf(NodeResourceType.Vegetation)] > 0f;
			Image_NodeNeed_Vegetation.gameObject.SetActive(need_Veg);
			if (need_Veg)
			{
				Label_NodeNeed_Vegetation.text =
					currentItem.Information.Item_Needs.NodeNeedValues[currentItem.Information.Item_Needs.NodeNeedTypes.ToList().IndexOf(NodeResourceType.Vegetation)].GetNumberString(4);
			}

			var craftedNeeds = currentItem.Information.Item_Needs.CraftedNeedTypes.ToList();
			var craftedValues = currentItem.Information.Item_Needs.CraftedNeedValues.ToList();

			bool need_Food = craftedNeeds.Contains(CraftedResourceType.Food)
				&& craftedValues[craftedNeeds.IndexOf(CraftedResourceType.Food)] > 0f;
			Image_CraftedNeed_Food.gameObject.SetActive(need_Food);
			if (need_Food)
			{
				Label_CraftedNeed_Food.text = 
					currentItem.Information.Item_Needs.CraftedNeedValues[currentItem.Information.Item_Needs.CraftedNeedTypes.ToList().IndexOf(CraftedResourceType.Food)].GetNumberString(4);
			}

			bool need_Tech = craftedNeeds.Contains(CraftedResourceType.Tech)
				&& craftedValues[craftedNeeds.IndexOf(CraftedResourceType.Tech)] > 0f;
			Image_CraftedNeed_Tech.gameObject.SetActive(need_Tech);
			if (need_Tech)
			{
				Label_CraftedNeed_Tech.text =
					currentItem.Information.Item_Needs.CraftedNeedValues[currentItem.Information.Item_Needs.CraftedNeedTypes.ToList().IndexOf(CraftedResourceType.Tech)].GetNumberString(4);
			}

			bool need_Elect = craftedNeeds.Contains(CraftedResourceType.Electricity)
				&& craftedValues[craftedNeeds.IndexOf(CraftedResourceType.Electricity)] > 0f;
			Image_CraftedNeed_Electricity.gameObject.SetActive(need_Elect);
			if (need_Elect)
			{
				Label_CraftedNeed_Electricity.text = 
					currentItem.Information.Item_Needs.CraftedNeedValues[currentItem.Information.Item_Needs.CraftedNeedTypes.ToList().IndexOf(CraftedResourceType.Electricity)].GetNumberString(4);
			}
		}
		else // NodeNeedTypes == null
		{
			Image_NodeNeed_Fertility.gameObject.SetActive(false);
			Image_NodeNeed_Water.gameObject.SetActive(false);
			Image_NodeNeed_Vegetation.gameObject.SetActive(false);
			Image_CraftedNeed_Food.gameObject.SetActive(false);
			Image_CraftedNeed_Tech.gameObject.SetActive(false);
			Image_CraftedNeed_Electricity.gameObject.SetActive(false);
		}
	}

	private void UpdateInfo_Produce ()
	{
		if (currentItem.Information.Item_Production.ProductionTypes != null)
		{
			//!! 1Alpha ItemNeeds: Convert Resource Lists to Dictionaries (needs)
			var productionTypes = currentItem.Information.Item_Production.ProductionTypes.ToList();
			var productionValues = currentItem.Information.Item_Production.ProductionValues.ToList();

			bool prod_Elect = productionTypes.Contains(CraftedResourceType.Electricity)
				&& productionValues[productionTypes.IndexOf(CraftedResourceType.Electricity)] > 0f;
			Image_Produce_Elect.gameObject.SetActive(prod_Elect);
			if (prod_Elect)
			{
				Label_Produce_Elect.text = 
					currentItem.Information.Item_Production.ProductionValues[currentItem.Information.Item_Production.ProductionTypes.ToList().IndexOf(CraftedResourceType.Electricity)].GetNumberString(4);
			}

			bool prod_Food = productionTypes.Contains(CraftedResourceType.Food)
				&& productionValues[productionTypes.IndexOf(CraftedResourceType.Food)] > 0f;
			Image_Produce_Food.gameObject.SetActive(prod_Food);
			if (prod_Food)
			{
				Label_Produce_Food.text = 
					currentItem.Information.Item_Production.ProductionValues[currentItem.Information.Item_Production.ProductionTypes.ToList().IndexOf(CraftedResourceType.Food)].GetNumberString(4);
			}

			bool prod_Tech = productionTypes.Contains(CraftedResourceType.Tech)
				&& productionValues[productionTypes.IndexOf(CraftedResourceType.Tech)] > 0f;
			Image_Produce_Tech.gameObject.SetActive(prod_Tech);
			if (prod_Tech)
			{
				Label_Produce_Tech.text = 
					currentItem.Information.Item_Production.ProductionValues[currentItem.Information.Item_Production.ProductionTypes.ToList().IndexOf(CraftedResourceType.Tech)].GetNumberString(4);
			}
		}
		else // ProductionTypes == null
		{
			Image_Produce_Elect.gameObject.SetActive(false);
			Image_Produce_Food.gameObject.SetActive(false);
			Image_Produce_Tech.gameObject.SetActive(false);
		}
	}

	private void UpdateState_Health (float newHealth = -1f)
	{
		if (newHealth == -1f) // not set, read from item
		{
			Slider_Health.value = (int) (Slider_Health.maxValue * currentItem.CurrentHealth / currentItem.MaxHealth);
			//Label_Health.text = ((uint) Slider_Health.value).GetNumberString(3) + " / " + ((uint) Slider_Health.maxValue).GetNumberString(3);
		}
		else
		{
			Slider_Health.value = (int) (Slider_Health.maxValue * newHealth / currentItem.MaxHealth);
			//Label_Health.text = ((uint) newHealth).GetNumberString(3) + " / " + ((uint) Slider_Health.maxValue).GetNumberString(3);
		}
	}

	private void UpdateState_NeedsMet (bool state, NodeResourceType[] nodeNeedsNotMet, CraftedResourceType[] craftedNeedsNotMet)
	{
		//!! 1Alpha Item Panel: UpdateStateNeedsMet
		//!! 1Alpha UpdateStateNeedsMet: Store last state in item stats
		var nodeNeedsNotMetList = nodeNeedsNotMet.ToList();
		var craftedNeedsNotMetList = craftedNeedsNotMet.ToList();

		SetNeedMet(NodeResourceType.GroundFertility, !nodeNeedsNotMetList.Contains(NodeResourceType.GroundFertility));
		SetNeedMet(NodeResourceType.GroundWater, !nodeNeedsNotMetList.Contains(NodeResourceType.GroundWater));
		SetNeedMet(NodeResourceType.Vegetation, !nodeNeedsNotMetList.Contains(NodeResourceType.Vegetation));
		SetNeedMet(CraftedResourceType.Tech, !craftedNeedsNotMetList.Contains(CraftedResourceType.Tech));
		SetNeedMet(CraftedResourceType.Food, !craftedNeedsNotMetList.Contains(CraftedResourceType.Food));
		SetNeedMet(CraftedResourceType.Electricity, !craftedNeedsNotMetList.Contains(CraftedResourceType.Electricity));
	}

	private void SetNeedMet(NodeResourceType nodeResourceType, bool state)
	{
		var UI_Colors = Visual_Game_ColorDatabase.Instance.UI_Colors;
		var UI_Sprites = Visual_Game_SpriteDatabase.Instance;
		switch (nodeResourceType)
		{
			case NodeResourceType.GroundFertility:
				Image_NodeNeed_Fertility.sprite = state ? UI_Sprites.UI_Resource_GFertility : UI_Sprites.UI_Resource_GFertility_InActive;
				Label_NodeNeed_Fertility.color = state ? UI_Colors.Game_UI_Text_Normal : UI_Colors.Game_UI_Text_Alert;
				break;
			case NodeResourceType.GroundWater:
				Image_NodeNeed_Water.sprite = state ? UI_Sprites.UI_Resource_GWater : UI_Sprites.UI_Resource_GWater_InActive;
				Label_NodeNeed_Water.color = state ? UI_Colors.Game_UI_Text_Normal : UI_Colors.Game_UI_Text_Alert;
				break;
			case NodeResourceType.Vegetation:
				Image_NodeNeed_Vegetation.sprite = state ? UI_Sprites.UI_Resource_GVeg : UI_Sprites.UI_Resource_GVeg_InActive;
				Label_NodeNeed_Vegetation.color = state ? UI_Colors.Game_UI_Text_Normal : UI_Colors.Game_UI_Text_Alert;
				break;
		}
	}

	private void SetNeedMet (CraftedResourceType craftedResourceType, bool state)
	{
		var UI_Colors = Visual_Game_ColorDatabase.Instance.UI_Colors;
		var UI_Sprites = Visual_Game_SpriteDatabase.Instance;
		switch (craftedResourceType)
		{
			case CraftedResourceType.Tech:
				Image_CraftedNeed_Tech.sprite = state ? UI_Sprites.UI_Resource_CTech : UI_Sprites.UI_Resource_CTech_InActive;
				Label_CraftedNeed_Tech.color = state ? UI_Colors.Game_UI_Text_Normal : UI_Colors.Game_UI_Text_Alert;
				break;
			case CraftedResourceType.Food:
				Image_CraftedNeed_Food.sprite = state ? UI_Sprites.UI_Resource_CFood : UI_Sprites.UI_Resource_CFood_InActive;
				Label_CraftedNeed_Food.color = state ? UI_Colors.Game_UI_Text_Normal : UI_Colors.Game_UI_Text_Alert;
				break;
			case CraftedResourceType.Electricity:
				Image_CraftedNeed_Electricity.sprite = state ? UI_Sprites.UI_Resource_CElect : UI_Sprites.UI_Resource_CElect_InActive;
				Label_CraftedNeed_Electricity.color = state ? UI_Colors.Game_UI_Text_Normal : UI_Colors.Game_UI_Text_Alert;
				break;
		}
	}

	private void UpdateState_NeedsMet ()
	{
		//!! 1Alpha Item Panel: UpdateStateNeedsMet
	}

	private void UpdateState_Production (bool state)
	{
		//!! 1Alpha Item Panel: UpdateStateProduction
	}

	private void UpdateState_Production ()
	{
		//!! 1Alpha Item Panel: UpdateStateProduction
	}
	#endregion
}