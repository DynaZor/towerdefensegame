﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using AgesSystem;
using System;

public class UI_Game_Manager : MonoBehaviour
{
	#region Singleton
	public static UI_Game_Manager Instance;
	private void MakeSingleton ()
	{
		if (Instance != null)
			Debug.Log("UI_Game_Manager - More than one instance!\n" +
				"Registered Instance: " + Instance.gameObject.name +
				"This Instance: " + gameObject.name, this);
		else
			Instance = this;
	}
	#endregion

	[Header("Game State")]
	public Age CurrentAge = Age.Stone;

	#region Sprites
	[Space()]
	[Header("Sprites")]
	[SerializeField, Tooltip("Should match the Ages in order and number")] private List<SO_UI_InGame> UISpritesSO = new List<SO_UI_InGame>();
	#region Sprites Editor Helper
#if UNITY_EDITOR
	[SerializeField] private Age UpdateAllUISpritesTo = Age.Stone;
	[SerializeField] private bool UpdateAllUISprites = false;
	private void OnValidate ()
	{
		if (UpdateAllUISprites)
		{
			UpdateAllUISprites = false;
			var objects = FindObjectsOfType<UI_Game_SpriteUpdater>();
			for (int i = 0; i < objects.Length; i++)
			{
				objects[i].AgeChanged(UISpritesSO[(int) UpdateAllUISpritesTo]);
			}
		}
	}
#endif
	#endregion
	public List<SO_UI_InGame> UISprites
	{
		get
		{
			return UISpritesSO;
		}
	}

	public SO_UI_InGame CurrentUISprites
	{
		get
		{
			return UISpritesSO[(int) CurrentAge];
		}
	}
	
	public delegate void CurrentAgeChangedUIHandler (SO_UI_InGame newSprites);
	public event CurrentAgeChangedUIHandler CurrentAgeChangedUIEvent = delegate { };
	#endregion

	#region References
	[Space()]
	[Header("References")]
	[Header("Gameplay")]
	[SerializeField] private Image[] Image_DemolishOverlay = null;
	[Header("Pause")]
	[SerializeField] private Canvas Canvas_PauseMenu = null;
	[Header("Game Over")]
	[SerializeField] private Canvas Canvas_GameOver = null;
	[SerializeField] private TMPro.TextMeshProUGUI Label_GameOverSurvived = null;
	[SerializeField] private TMPro.TextMeshProUGUI Label_GameOverDifficulty = null;
	#endregion

	#region UI State
	public enum UI_State
	{
		Normal, Grid, Menu, GameOver
	}
	public UI_State State { get; private set; }
	private UI_State lastState;
	#endregion

	#region Private Auto References
	private UI_Game_Grid UI_Grid;
	private Game_Speed Game_Speed;
	#endregion
	#region Setup
	private void Awake ()
	{
		MakeSingleton();
		UIAction_DefaultStartState();
		CurrentAgeChangedUIEvent = delegate { };
		StartCoroutine(Setup());
	}

	private IEnumerator Setup ()
	{
		while (Game_Manager.Instance == null)
			yield return null;
		Game_Manager.Instance.CurrentAgeChangedEvent += SetCurrentAge;
		while (UI_Game_Grid.Instance == null)
			yield return null;
		UI_Grid = UI_Game_Grid.Instance;

		while (Game_Speed.Instance == null)
			yield return null;
		Game_Speed = Game_Speed.Instance;
	}

	private void UIAction_DefaultStartState ()
	{
		State = UI_State.Normal;
		lastState = UI_State.Normal;
		Canvas_GameOver.enabled = false;
		Canvas_PauseMenu.enabled = false;
		UIAction_ToggleDemolishOverlay(false);
	}

	private void OnDestroy ()
	{
		CurrentAgeChangedUIEvent = delegate { };
	}
#endregion

#region Game State
	public void SetCurrentAge (Age newAge)
	{
		CurrentAge = newAge;
		CurrentAgeChangedUIEvent.Invoke(CurrentUISprites);
	}
#endregion

#region UI States
	public void SetState(UI_State state)
	{
		lastState = State;
		State = state;
	}

	private void SetLastState ()
	{
		State = lastState;
	}
#endregion

#region Button Actions
	public void Button_ItemBuild (UI_Game_Button_ItemBuild button)
	{
		UI_Grid.BuildItemButton(button);
	}

	public void Button_DemolishToggle ()
	{
		UIAction_ToggleDemolishOverlay(UI_Grid.DemolishToggle());
	}

	public void Button_PauseMenu_Show ()
	{
		UIAction_TogglePauseMenu(true);
	}

	public void Button_PauseMenu_Hide ()
	{
		UIAction_TogglePauseMenu(false);
	}

	public void Button_MainMenu ()
	{
		// Save Before Quit Prompt
		SceneManager.LoadScene(0, LoadSceneMode.Single);
	}

	public void Button_Quit ()
	{
		// Save Before Quit Prompt
		Debug.Log("Quitting Game (Called from InGame UI_Manager)");
		Global_ApplicationActions.Quit();
	}
#endregion

#region UI Actions
	private void UIAction_TogglePauseMenu (bool state)
	{
		Canvas_PauseMenu.enabled = state;
		if (state)
		{
			SetState(UI_State.Menu);
			Game_Speed.Game_Pause();
		}
		else
		{
			SetLastState();
			Game_Speed.Game_UnPause();
		}
	}

	public void UIAction_ToggleDemolishOverlay (bool state)
	{
		for (int i = 0; i < Image_DemolishOverlay.Length; i++)
		{
			Image_DemolishOverlay[i].enabled = state;
		}
	}

	public void UIAction_GameOver ()
	{
		SetState(UI_State.GameOver);
		Game_Speed.Game_Pause();
		Label_GameOverSurvived.SetText(Game_Clock.Instance.CurrentTime.GetNumberString(7));
		Label_GameOverDifficulty.SetText(Difficulty_Manager.CurrentDifficulty.Name);
		Canvas_GameOver.enabled = true;
	}
#endregion
}