﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Game_Speed;

public class UI_Game_TimeSpeed : MonoBehaviour
{
	#region Singleton
	public static UI_Game_TimeSpeed Instance;
	private void MakeSingleton ()
	{
		if (Instance != null)
			Debug.Log("UI_Game_TimeSpeed - More than one instance!\n" +
				"Registered Instance: " + Instance.gameObject.name +
				"This Instance: " + gameObject.name, this);
		else
			Instance = this;
	}
	#endregion

	[Header("Button Settings")]
	[Header("Current Speed")]
	[SerializeField] private ColorBlock CurrentSpeedColors = new ColorBlock();
	[Header("Other Speeds")]
	[SerializeField] private ColorBlock OtherSpeedColors = new ColorBlock();

	// References
	[SerializeField] private Button ButtonPause = null;
	[SerializeField] private Button ButtonNormal = null;
	[SerializeField] private Button ButtonFast = null;
	[SerializeField] private Button ButtonSuperFast = null;

	#region Setup
	private void Awake ()
	{
		MakeSingleton();
		SpeedUpdated(Speed.Normal);
		StartCoroutine(Setup());
	}

	private IEnumerator Setup ()
	{
		while (Game_Speed.Instance == null)
			yield return null;
		Game_Speed.Instance.TimeSpeedUpdatedEvent += SpeedUpdated;
	}
	#endregion
	
	public void SetSpeed (int wantedSpeed)
	{
		Game_Speed.Instance.SetSpeed((Speed) wantedSpeed);
	}

	private void SpeedUpdated(Speed newSpeed)
	{
		List<Button> otherButtons = new List<Button>()
		{
			ButtonPause, ButtonNormal, ButtonFast, ButtonSuperFast
		};
		Button currentButton;
		switch (newSpeed)
		{
			case Speed.Pause:
				currentButton = ButtonPause;
				break;
			case Speed.Normal:
			default:
				currentButton = ButtonNormal;
				break;
			case Speed.Fast:
				currentButton = ButtonFast;
				break;
			case Speed.SuperFast:
				currentButton = ButtonSuperFast;
				break;
		}
		otherButtons.Remove(currentButton);
		currentButton.colors = CurrentSpeedColors;
		for (int i = 0; i < otherButtons.Count; i++)
		{
			otherButtons[i].colors = OtherSpeedColors;
		}
	}
}