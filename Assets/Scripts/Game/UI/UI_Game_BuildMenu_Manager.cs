﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using AgesSystem;
using System;

public class UI_Game_BuildMenu_Manager : MonoBehaviour
{
	[SerializeField] private Canvas Canvas_Items = null;
	[SerializeField] private Transform Grid_Items = null;
	[SerializeField] private GameObject itemButtonPrefab = null;
	[SerializeField] private Transform itemButtonPool = null;

	private int[] maxItemsPerAge = null;

	#region Setup
	private void Awake ()
	{
		HideCategory();
		StartCoroutine(Setup());
	}

	private IEnumerator Setup ()
	{
		while (Game_Item_Manager.Instance == null)
			yield return null;
		while (Game_Item_Manager.ItemsDB == null)
			yield return null;
		UpdateMaxItemsPerAge();
		Game_Manager.Instance.CurrentAgeChangedEvent += AgeUpdate;
	}
	#endregion

	#region UI Events
	public void ShowCategory (int type)
	{
		ShowCategory((ItemType) type);
	}

	public void ShowCategory (ItemType type) //!! 0POC BuildMenu: Finish implementing using ItemType
	{
		Canvas_Items.enabled = true;
	}
	public void HideCategory ()
	{
		Canvas_Items.enabled = false;
		ReturnAllButtons();
	}
	#endregion

	#region Pooling
	private void UpdateMaxItemsPerAge ()
	{
		maxItemsPerAge = Game_Item_Manager.ItemsDB.GetAllyMaxBuildingsSizeArray();
	}

	private void AgeUpdate (Age age)
	{
		PrepareItemButtonPool((int) age);
	}

	private void PrepareItemButtonPool (int ageIndex)
	{
		int targetAmount = maxItemsPerAge[ageIndex];
		int currentAmount = itemButtonPool.childCount;
		if (targetAmount > currentAmount)
		{
			for (int i = currentAmount; i < targetAmount; i++) //!! 0POC BuildMenu: Check if needs < or <=
			{
				Instantiate(itemButtonPrefab, itemButtonPool);
			}
		}
	}

	private void ReturnAllButtons ()
	{
		while (Grid_Items.childCount > 0)
		{
			Grid_Items.GetChild(0).SetParent(itemButtonPool);
		}
	}
	#endregion
}