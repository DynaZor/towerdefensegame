﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using System.Collections;
using UnityEngine;

public class UI_Game_Label_TimeSurvived : MonoBehaviour
{
    [SerializeField] private TMPro.TextMeshProUGUI Label = null;
    private bool Ready = false;
    private void Awake ()
    {
        StartCoroutine(Setup());
    }

    private IEnumerator Setup ()
    {
        while (Game_Clock.Instance == null)
            yield return null;
        Game_Clock.Instance.Tick_Group1 += TickUpdate;
        Ready = true;
    }

    private void OnDestroy ()
    {
        if (Ready) 
            Game_Clock.Instance.Tick_Group1 -= TickUpdate;
    }

    private void TickUpdate(int tickNumber)
    {
        if (tickNumber == 0)
        {
            Label.SetText(Game_Clock.Instance.CurrentTime.GetNumberString());
        }
    }
}