﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Collections;

public class UI_Game_InfoPanel_Node_SliderValue : MonoBehaviour
{
	[SerializeField] private TextMeshProUGUI Label_Value = null;
	[SerializeField] private Slider Slider = null;

	private void Awake ()
	{
		StartCoroutine(Setup());
	}

	private IEnumerator Setup ()
	{
		while (GetComponent<RectTransform>() == null)
			yield return null;
		while (GetComponent<RectTransform>().rect.width == 0)
			yield return null;
		Slider.maxValue = GetComponent<RectTransform>().rect.width;
	}

	public void SetValue(float newValue, float maxValue)
	{
		Label_Value.text = Mathf.RoundToInt(newValue).GetNumberString(4);
		Slider.value = Slider.maxValue * Mathf.Clamp01(newValue / maxValue);
	}
}