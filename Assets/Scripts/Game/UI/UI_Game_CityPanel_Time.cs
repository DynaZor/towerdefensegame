﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!

using System.Collections;
using UnityEngine;

public class UI_Game_CityPanel_Time : MonoBehaviour
{
	private readonly int TickUpdate_Text = 1;
	[SerializeField] private TMPro.TextMeshProUGUI Label_Time = null;

	private void Awake ()
	{
		StartCoroutine(Setup());
	}

	private IEnumerator Setup ()
	{
		while (Game_Clock.Instance == null)
			yield return null;
		Game_Clock.Instance.Tick_Group1 += TickUpdate;
	}

	private void TickUpdate(int tickNumber)
	{
		if (tickNumber == TickUpdate_Text)
		{
			UpdateText();
		}
	}

	private void UpdateText ()
	{
		Label_Time.SetText(Game_Clock.Instance.CurrentTime.GetNumberString(4));
	}
}