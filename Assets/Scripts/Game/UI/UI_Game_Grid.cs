﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using System;
using System.Collections;
using Unity.Mathematics;
using UnityEngine;

public class UI_Game_Grid : MonoBehaviour
{
	#region Singleton
	public static UI_Game_Grid Instance;
	private void MakeSingleton ()
	{
		if (Instance != null)
			Debug.Log("UI_Game_Grid - More than one instance!\n" +
				"Registered Instance: " + Instance.gameObject.name +
				"This Instance: " + gameObject.name, this);
		else
			Instance = this;
	}
	#endregion

	// State
	public enum GridUI_States
	{
		Normal, BuildingItem, MovingItem, Demolish
	}
	public GridUI_States GridUI_State { get; private set; }

	private bool Ready = false;

	// Moving
	private Item_Actions selectedItem = null;

	// Building
	private SO_Item build_selectedItem;
	private UI_Game_Button_ItemBuild build_currentSelectedButton = null;

	// Communication
	public static event Action<int2> OnGridMouseClick0;
	public static event Action<int2> OnGridMouseClick1;

	// References
	private UI_Game_Manager UI_Game_Manager;
	private Game_Grid Game_Grid;

	#region Setup
	private void Awake ()
	{
		MakeSingleton();
		StartCoroutine(Setup());
		GridUI_State = GridUI_States.Normal;
	}

	private IEnumerator Setup ()
	{
		while (UI_Game_Manager.Instance == null)
			yield return null;
		UI_Game_Manager = UI_Game_Manager.Instance;

		while (Game_Grid.Instance == null)
			yield return null;
		Game_Grid = Game_Grid.Instance;

		while (Visual_Game_NodeController.Instance == null)
			yield return null;
		Visual_Game_NodeController.Instance.ClearSelectedNode();

		while (Visual_Game_CameraController.Instance == null)
			yield return null;
		Visual_Game_CameraController.MouseClickEvent += MouseClick_Node;

		while (!Game_Grid.Ready)
			yield return null;
		Ready = true;
	}
	#endregion

	private void Update ()
	{
		if (Ready)
		{
			//!! 0POC Mouse Control: Move to Respective script
			//!! 1Alpha Keyboard Control: Move to Respective script
			if (Input.GetMouseButtonDown(1) | Input.GetKeyDown(KeyCode.Escape)) // Right Click / ESC
			{
				ExitCurrentMode();
			}
		}
	}

	private void MouseClick_Node(int mouseBTN)
	{
		if (Ready && mouseBTN == 0)
		{
			Game_Node node = Game_Grid.Instance.NodeAt(Visual_Game_NodeController.Instance.hoveredNode);
			if (node != null)
				NodeClicked(node);
		}
	}

	public void MouseClick0_Node (Vector2Int? selectedNode)
	{
		if (Ready)
		{
			Game_Node node = Game_Grid.Instance.NodeAt(selectedNode);
			if (node != null)
				NodeClicked(node);
		}
	}

	public void NodeClicked (Game_Node node) //!! 1Alpha UI States: Improve UI State Machine to be able to continue tasks and deselect only when needed
	{
		switch (GridUI_State)
		{
			case GridUI_States.Normal:
				if (Game_Grid.Instance.CheckNodeAt(node.NodePos, Game_Grid.CheckType.Visible) ?? false)
				{
					UI_Game_InfoPanel_Node.Instance.SetCurrentNode(node);
					return;
				}
				break;

			case GridUI_States.BuildingItem:
				if (Game_Grid.Instance.CheckNodeAt(node.NodePos, Game_Grid.CheckType.Buildable)?? false)
				{
					Game_Grid.CreateItem(node.GridPosX, node.GridPosY, build_selectedItem);
					return;
				}
				break;

			case GridUI_States.Demolish:
				if (Game_Grid.Instance.CheckNodeAt(node.NodePos, Game_Grid.CheckType.Demolishable) ?? false == true)
				{
					node.Item.Item_Actions.Dismantle();
					return;
				}
				break;

			case GridUI_States.MovingItem:
				if (Game_Grid.Instance.CheckNodeAt(node.NodePos, Game_Grid.CheckType.Navigatable) ?? false == true)
				{
					selectedItem.SetTarget(node); //!! 0POC Item Panel: Add 'follow item' system
					return;
				}
				break;
		}
	}

	private void ExitCurrentMode ()
	{
		build_currentSelectedButton = null;
		GridUI_State = GridUI_States.Normal;
		Visual_Game_NodeController.Instance.ClearSelectedNode();
		UI_Game_InfoPanel_Node.Instance.HidePanel();
		build_selectedItem = null;
		if (UI_Game_Manager.State == UI_Game_Manager.UI_State.Grid)
		{
			UI_Game_Manager.SetState(UI_Game_Manager.UI_State.Normal);
			UI_Game_Manager.UIAction_ToggleDemolishOverlay(false);
		}
	}

	public void Button_Item_Move (Item_Actions item)
	{
		ExitCurrentMode();
		selectedItem = item;
		GridUI_State = GridUI_States.MovingItem;
		UI_Game_Manager.SetState(UI_Game_Manager.UI_State.Grid);
	}

	public void BuildItemButton (UI_Game_Button_ItemBuild button)
	{
		if (build_currentSelectedButton != button)
		{
			ExitCurrentMode();
			build_currentSelectedButton = button;
			GridUI_State = GridUI_States.BuildingItem;
			build_selectedItem = button.ItemSO;
			UI_Game_Manager.SetState(UI_Game_Manager.UI_State.Grid);
		}
		else
		{
			ExitCurrentMode();
		}
	}

	public bool DemolishToggle ()
	{
		if (GridUI_State != GridUI_States.Demolish)
		{
			ExitCurrentMode();
			GridUI_State = GridUI_States.Demolish;
			UI_Game_Manager.SetState(UI_Game_Manager.UI_State.Grid);
			return true;
		}
		else
		{
			ExitCurrentMode();
			return false;
		}
	}
}