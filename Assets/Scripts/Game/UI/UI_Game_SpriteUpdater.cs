﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AgesSystem;
using UnityEngine.UI;

public class UI_Game_SpriteUpdater : MonoBehaviour
{
	[Header("UI Sprite")]
	[SerializeField] private SO_UI_InGame.UIElement ElementType = SO_UI_InGame.UIElement.Panel_Menu;

	private Image Image;

	//private bool Ready = false;

	#region Setup
	private void Awake()
    {
		StartCoroutine(Setup());
    }

	private IEnumerator Setup ()
	{
		while (UI_Game_Manager.Instance == null)
			yield return null;
		UI_Game_Manager.Instance.CurrentAgeChangedUIEvent += AgeChanged;
		Image = GetComponent<Image>();
		//Ready = true;
	}
	#endregion

	#region Events
	public void AgeChanged (SO_UI_InGame newSprites)
	{
		if (Image == null)
		{
			Image = GetComponent<Image>();
		}

		Sprite newSprite = null;
		switch (ElementType)
		{
			case SO_UI_InGame.UIElement.Button:
				newSprite = newSprites.Button;
				break;
			case SO_UI_InGame.UIElement.SmallButton:
				newSprite = newSprites.SmallButton;
				break;
			case SO_UI_InGame.UIElement.Panel_Menu:
				newSprite = newSprites.Panel_Menu;
				break;
			case SO_UI_InGame.UIElement.Panel_Info:
				newSprite = newSprites.Panel_Info;
				break;
			case SO_UI_InGame.UIElement.Panel_Top:
				newSprite = newSprites.Panel_Top;
				break;
			case SO_UI_InGame.UIElement.Demolish:
				newSprite = newSprites.Demolish;
				break;
		}

		Image.sprite = newSprite;
	}
	#endregion
}