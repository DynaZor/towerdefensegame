﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using ResourcesSystem;
using System.Collections.Generic;
using System.Collections;

public class UI_Game_InfoPanel_Node : MonoBehaviour
{
	#region Singleton
	public static UI_Game_InfoPanel_Node Instance;
	private void MakeSingleton ()
	{
		if (Instance != null)
			Debug.Log("UI_Game_InfoPanel_Node" +
				" - More than one instance!\n" +
				"Registered Instance: " + Instance.gameObject.name +
				"This Instance: " + gameObject.name, this);
		else
			Instance = this;
	}
	#endregion

	[Header("Info Panel - Node")]
	[Header("References")]
	[SerializeField] private Canvas InfoPanel_Canvas = null;
	[Header("Label References")]
	[SerializeField] private TextMeshProUGUI Label_Fertility = null;
	[SerializeField] private TextMeshProUGUI Label_Water = null;
	[SerializeField] private TextMeshProUGUI Label_Vegetation = null;
	[Header("Badge References")]
	[SerializeField] private Image Badge_Fertility = null;
	[SerializeField] private Image Badge_Water = null;
	[SerializeField] private Image Badge_Vegetation = null;
	[Header("Slider References")]
	[SerializeField] private UI_Game_InfoPanel_Node_SliderValue Slider_Fertility = null;
	[SerializeField] private UI_Game_InfoPanel_Node_SliderValue Slider_Water = null;
	[SerializeField] private UI_Game_InfoPanel_Node_SliderValue Slider_Vegetation = null;
	[SerializeField] private UI_Game_InfoPanel_Node_SliderValue Slider_Pollution = null;

	// Private Data
	private Game_Node currentNode = null;

	#region Setup
	private void Awake ()
	{
		MakeSingleton();
		HidePanel();
		StartCoroutine(Setup());
	}

	private IEnumerator Setup ()
	{
		while (Visual_Game_SpriteDatabase.Instance == null)
			yield return null;
		Badge_Fertility.sprite = Visual_Game_SpriteDatabase.Instance.UI_Resource_GFertility_Main;
		Badge_Water.sprite = Visual_Game_SpriteDatabase.Instance.UI_Resource_GWater_Main;
		Badge_Vegetation.sprite = Visual_Game_SpriteDatabase.Instance.UI_Resource_GVeg_Main;
	}
	#endregion

	#region Public Actions
	public void SetCurrentNode(Game_Node node)
	{
		if (currentNode != null)
		{
			currentNode.Stats.NodeStatsUpdatedEvent -= Update_Sliders;
		}
		currentNode = node;
		UpdateAllNodeInfo();
		InfoPanel_Canvas.enabled = true;
		currentNode.Stats.NodeStatsUpdatedEvent += Update_Sliders;

		if (node.Item != null)
			UI_Game_InfoPanel_Item.Instance.Panel_SetItem(node.Item.Item_Stats);
		else
			UI_Game_InfoPanel_Item.Instance.Panel_Hide();
	}

	public void HidePanel ()
	{
		InfoPanel_Canvas.enabled = false;
		if (currentNode != null)
		{
			currentNode.Stats.NodeStatsUpdatedEvent -= Update_Sliders;
			currentNode = null;
		}
		if (UI_Game_InfoPanel_Item.Instance != null)
			UI_Game_InfoPanel_Item.Instance.Panel_Hide();
	}
	#endregion

	#region Private Actions
	private void UpdateAllNodeInfo ()
	{
		Update_Main();
		Update_Sliders();
	}

	private void Update_Sliders (Dictionary<NodeResourceType, float> newNodeResources = null)
	{
		if (newNodeResources == null)
			newNodeResources = currentNode.Stats.NodeResources;

		Slider_Fertility.SetValue(newNodeResources[NodeResourceType.GroundFertility], currentNode.BaseStats.NodeBaseResources[NodeResourceType.GroundFertility]);

		Slider_Water.SetValue(newNodeResources[NodeResourceType.GroundWater], currentNode.BaseStats.NodeBaseResources[NodeResourceType.GroundWater]);

		Slider_Vegetation.SetValue(newNodeResources[NodeResourceType.Vegetation], currentNode.BaseStats.NodeBaseResources[NodeResourceType.Vegetation]);

		Slider_Pollution.SetValue(newNodeResources[NodeResourceType.Pollution], Gameplay_Values.Instance.NodeResourceRecoveryMaxPollution);
	}

	private void Update_Main ()
	{
		switch (currentNode.BaseStats.MainResourceType)
		{
			case NodeResourceType.GroundFertility:
				Label_Fertility.fontStyle = FontStyles.Bold;
				Label_Water.fontStyle = FontStyles.Normal;
				Label_Vegetation.fontStyle = FontStyles.Normal;
				Badge_Fertility.enabled = true;
				Badge_Water.enabled = false;
				Badge_Vegetation.enabled = false;
				break;
			case NodeResourceType.GroundWater:
				Label_Water.fontStyle = FontStyles.Bold;
				Label_Vegetation.fontStyle = FontStyles.Normal;
				Label_Fertility.fontStyle = FontStyles.Normal;
				Badge_Fertility.enabled = false;
				Badge_Water.enabled = true;
				Badge_Vegetation.enabled = false;
				break;
			case NodeResourceType.Vegetation:
				Label_Vegetation.fontStyle = FontStyles.Bold;
				Label_Water.fontStyle = FontStyles.Normal;
				Label_Fertility.fontStyle = FontStyles.Normal;
				Badge_Fertility.enabled = false;
				Badge_Water.enabled = false;
				Badge_Vegetation.enabled = true;
				break;
		}
	}
	#endregion
}