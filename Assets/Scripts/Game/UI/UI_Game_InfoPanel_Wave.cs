﻿// Code written by Shahar DynaZor Alon Use of this code outside of this project
// without Shahar's permission is prohibited!
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UI_Game_InfoPanel_Wave : MonoBehaviour
{
	#region Singleton
	public static UI_Game_InfoPanel_Wave Instance;
	private void MakeSingleton ()
	{
		if (Instance != null)
			Debug.Log("UI_Game_InfoPanel_Wave - More than one instance!\n" +
				"Registered Instance: " + Instance.gameObject.name +
				"This Instance: " + gameObject.name, this);
		else
			Instance = this;
	}
	#endregion
	#region Private Data
	[Header("References")]
	[SerializeField] private TMPro.TextMeshProUGUI Label_Waves = null;
	[SerializeField] private Slider Slider_Waves = null;
	#endregion
	#region Setup
	private void Awake ()
	{
		MakeSingleton();
		StartCoroutine(Setup());
	}

	private IEnumerator Setup ()
	{
		while (Slider_Waves.GetComponent<RectTransform>() == null)
			yield return null;
		while (Slider_Waves.GetComponent<RectTransform>().rect.width == 0)
			yield return null;
		Slider_Waves.maxValue = Slider_Waves.GetComponent<RectTransform>().rect.width;
	}
	#endregion
	#region Public Actions
	public static void SetNextWaveIn (uint timeLeft, uint waveTotal)
	{
		if (Instance != null)
		{
			Instance.Label_Waves.text = timeLeft.GetNumberString(4);
			Instance.Slider_Waves.value = Instance.Slider_Waves.maxValue * timeLeft / waveTotal;
		}
	}
	public static void SetWaveNow ()
	{
		if (Instance != null)
		{
			Instance.Label_Waves.text = "NOW";
			Instance.Slider_Waves.value = 0;
		}
	}
	#endregion
}