﻿// Code written by Shahar DynaZor Alon Use of this code outside of this project
// without Shahar's permission is prohibited!
using System.Collections;
using UnityEngine;
using ItemStatuses;
using System.Collections.Generic;
[ExecuteInEditMode]
public class UI_Game_Item_StatusIconsParent : MonoBehaviour
{
	[SerializeField] private SpriteRenderer iconPrefab = null;
	[SerializeField] private Vector3 inactivePosition = Vector3.zero;
	[SerializeField] private bool inactivePositionShowGizmo = true;
	[SerializeField] private int instantiateOnAwake = 10;
	[SerializeField] private List<ItemStatus> statuses_Type = new List<ItemStatus>();
	[SerializeField] private List<Sprite> statuses_Sprite = new List<Sprite>();
	//!! 1Alpha ItemStatusIcon: Future Optimization: Get requested type of icon. Less sprite changes.

	private Quaternion blankQuaternion = new Quaternion();

	#region Editor Only
#if UNITY_EDITOR
	[Header("Editor Only")]
	public bool InstantiateNow = false;
	public bool InstantiateDestroyFirst = false;
	private bool Editor_processingDestroy = false;
	private bool Editor_processingInstantiate = false;
	private bool Editor_processingPositions = false;
	private void OnDrawGizmos ()
	{
		if (inactivePositionShowGizmo)
			Gizmos.DrawWireCube(inactivePosition, new Vector3(0.5f, 0.5f, 0.5f));
	}

	private void OnValidate ()
	{
		if (InstantiateNow)
		{
			InstantiateNow = false;
			if (InstantiateDestroyFirst)
			{
				Editor_processingDestroy = true;
			}
			else
			{
				Editor_processingInstantiate = true;
			}
		}
	}

	private void Update ()
	{
		if (UnityEditor.EditorApplication.isPlaying == false)
		{
			if (Editor_processingDestroy)
			{
				Editor_processingDestroy = false;
				Editor_InstantiateIcons(1, InstantiateDestroyFirst);
			}
			else if (Editor_processingInstantiate)
			{
				Editor_processingInstantiate = false;
				Editor_InstantiateIcons(2);
			}
			else if (Editor_processingPositions)
			{
				Editor_processingPositions = false;
				Editor_InstantiateIcons(3);
			}
		}
	}

	private void Editor_InstantiateIcons (int part, bool removeFirst = false)
	{
		if (part == 1)
		{
			if (removeFirst)
			{
				for (int i = transform.childCount - 1; i >= 0; --i)
				{
					var child = transform.GetChild(i).gameObject;
					DestroyImmediate(child, false);
				}
			}
			Editor_processingInstantiate = true;
		}
		else if (part == 2)
		{
			for (int i = 0; i < instantiateOnAwake && transform.childCount < instantiateOnAwake; i++)
			{
				//InstantiateCommand();
				GameObject icon = UnityEditor.PrefabUtility.InstantiatePrefab(iconPrefab, transform) as GameObject;
			}
			Editor_processingPositions = true;
		}
		else if (part == 3)
		{
			for (int i = 0; i < transform.childCount; i++)
			{
				transform.GetChild(i).position = inactivePosition;
			}
		}
	}
#endif
	#endregion

	#region Global
	private void InstantiateCommand ()
	{
		Instantiate(iconPrefab, inactivePosition, blankQuaternion, transform);
	}
	#endregion

	#region Setup
	private void Awake ()
	{
#if UNITY_EDITOR
		if (UnityEditor.EditorApplication.isPlaying)
		{
#endif

			StartCoroutine(Setup());

#if UNITY_EDITOR
		}
#endif
	}

	private IEnumerator Setup ()
	{
		for (int i = 0; i < instantiateOnAwake && transform.childCount < instantiateOnAwake; i++)
		{
			yield return null;
			InstantiateCommand();
		}
	}

	#endregion

	#region Public Actions
	public void ThrowIconSprite (SpriteRenderer item)
	{
		item.transform.SetParent(transform);
		item.transform.position = inactivePosition;
	} 

	public SpriteRenderer GetIconSprite ()
	{
		if (transform.childCount > 0)
		{
			return transform.GetChild(0).GetComponent<SpriteRenderer>();
		}
		else
		{
			return Instantiate(iconPrefab, transform);
		}
	}
	#endregion
}