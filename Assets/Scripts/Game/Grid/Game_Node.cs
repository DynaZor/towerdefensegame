﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;
using System.Collections;

public class Game_Node
{
	// Properties
	public int GridPosX { get; private set; }
	public int GridPosY { get; private set; }
	public Vector2Int NodePos => new Vector2Int(GridPosX, GridPosY);
	public bool IsBlocked => Item != null || Enemy != null;
	public Vector3 NodeWorldPosition;
	public bool NodeWorldPosSet { get; private set; }

	// Node References
	public GameObject NodeGameObject { get; private set; }
	public Game_Node_Stats Stats { get; private set; }
	public Game_Node_BaseStats BaseStats { get; private set; }
	//public Game_Visual_Node Visual { get; private set; }

	// Item References
	public Item_Information Item { get; private set; }

	// Enemy References
	public Game_Enemy_Information Enemy { get; private set; }

	// Communication
	public delegate void ItemsUpdatedHandler ();
	public event ItemsUpdatedHandler ItemsUpdatedEvent = delegate { };


	public Game_Node (Vector3 nodePosition, int gridPosX, int gridPosY)
	{
		NodeWorldPosition = nodePosition;
		GridPosX = gridPosX;
		GridPosY = gridPosY;
		NodeWorldPosSet = true;
	}

	public void Setup(GameObject nodeGO)
	{
		NodeGameObject = nodeGO;
		NodeGameObject.name = "Node[" + GridPosX + "," + GridPosY + "]";
		Stats = nodeGO.GetComponent<Game_Node_Stats>();
		Stats.SetNode(this);
		BaseStats = nodeGO.GetComponent<Game_Node_BaseStats>();
	}

	public void SetItem(Item_Information item)
	{
		if (item != Item)
		{
			Item = item;
			ItemsUpdatedEvent.Invoke();
		}
	}

	public void SetEnemy(Game_Enemy_Information enemy)
	{
		if (enemy != Enemy)
		{
			Enemy = enemy;
			ItemsUpdatedEvent.Invoke();
		}
	}

	public void ItemDestroyed ()
	{
		Item = null;
		Enemy = null;
		ItemsUpdatedEvent.Invoke();
	}

}

public static class Game_Node_Extensions
{
	public enum ItemType
	{
		DoesntExist = -1, None, Ally, AllyMovable, Enemy, Obstacle
	}
	public static ItemType NodeItemType (this Game_Node node)
	{
		if (node == null)
			return ItemType.DoesntExist;

		if (!node.IsBlocked)
			return ItemType.None;

		if (node.Item != null)
		{
			if (node.Item.Data.Movable)
				return ItemType.AllyMovable;
			return ItemType.Ally;
		}

		if (node.Enemy != null)
		{
			return ItemType.Enemy;
		}
		return ItemType.Obstacle;
	}
}