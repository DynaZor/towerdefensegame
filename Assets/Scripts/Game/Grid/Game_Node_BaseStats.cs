﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;
using ResourcesSystem;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Game_Node_BaseStats : MonoBehaviour
{
	[Header("References")]
	[SerializeField] private Game_Node_Stats Grid_Node_Stats = null;

	// Public Data
	public Dictionary<NodeResourceType, float> NodeBaseResources { get; private set; }
	public NodeResourceType MainResourceType { get; private set; }

	// Private Data
	private bool FinishedGenerating = false;
	private Gameplay_Values gvalues => Gameplay_Values.Instance;

	#region Setup
	private void Start ()
	{
		NodeBaseResources = new Dictionary<NodeResourceType, float>
		{
			[NodeResourceType.GroundFertility] = 0f,
			[NodeResourceType.GroundWater] = 0f,
			[NodeResourceType.Vegetation] = 0f,
			[NodeResourceType.Pollution] = 0f
		};
		StartCoroutine(Setup());
	}

	private IEnumerator Setup ()
	{
		GenerateAndSetBaseStats();
		while (!FinishedGenerating)
			yield return null;
		Grid_Node_Stats.SetResources(GenerateStartStats());
	}

	#endregion

	#region Private Actions
	private void GenerateAndSetBaseStats ()
	{
		float baseResourceValue_Main = Difficulty_Manager.CurrentDifficulty.Node_Resources_Base_Main;
		float baseResourceValue_Sec = Difficulty_Manager.CurrentDifficulty.Node_Resources_Base_Secondary;
		float baseResourceMod_Main = Difficulty_Manager.CurrentDifficulty.Node_Resources_StartMod_Main;
		float baseResourceMod_Sec = Difficulty_Manager.CurrentDifficulty.Node_Resources_StartMod_Secondary;

		List<NodeResourceType> resourceTypes = Gameplay_Values.Instance.RenewableResources.ToList();
		// Choose Random Resource as the main resource for the node
		int mainResourceTypeIndex = Random.Range(0,resourceTypes.Count);
		MainResourceType = resourceTypes[mainResourceTypeIndex];
		
		// Generate Main Resource
		float mainResourceAmount = baseResourceValue_Main * UnityEngine.Random.Range(baseResourceMod_Main * 0.8f, baseResourceMod_Main * 1.2f);
		SetResource(MainResourceType, mainResourceAmount);

		// Generate Secondary Resources
		resourceTypes.Remove(MainResourceType);
		for (int i = 0; i < resourceTypes.Count; i++)
		{
			float secondaryResourceAmount = baseResourceValue_Sec * UnityEngine.Random.Range(baseResourceMod_Sec * 0.8f, baseResourceMod_Sec * 1.2f);
			SetResource(resourceTypes[i], secondaryResourceAmount);
		}

		FinishedGenerating = true;
	}

	private Dictionary<NodeResourceType, float> GenerateStartStats ()
	{
		var startStats = new Dictionary<NodeResourceType, float>();
		foreach (KeyValuePair<NodeResourceType, float> keyValuePair in NodeBaseResources)
		{
			startStats.Add(keyValuePair.Key, keyValuePair.Value * UnityEngine.Random.Range(gvalues.NRG_StartMin01, gvalues.NRG_StartMax01));
		}
		return startStats;
	}

	private void SetResource(NodeResourceType type, float amount)
	{
		NodeBaseResources[type] = amount;
	}
	#endregion
}