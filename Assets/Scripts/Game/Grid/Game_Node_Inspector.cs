﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;
using ResourcesSystem;
using System.Collections;
using UnityEditor;
using System.Linq;

public class Game_Node_Inspector : MonoBehaviour
{
#if UNITY_EDITOR

	private Game_Node_Stats Grid_Node_Stats;
	private Game_Node_BaseStats Grid_Node_BaseStats;
	
	private bool Ready = false;

	private void Awake ()
	{
		StartCoroutine(Setup());
	}

	private IEnumerator Setup ()
	{
		while (GetComponent<Game_Node_Stats>() == null)
			yield return null;
		Grid_Node_Stats = GetComponent<Game_Node_Stats>();

		while (GetComponent<Game_Node_BaseStats>() == null)
			yield return null;
		Grid_Node_BaseStats = GetComponent<Game_Node_BaseStats>();

		Ready = true;
	}

	[CustomEditor(typeof(Game_Node_Inspector))]
	public class Node_InspectorEditor : Editor
	{
		private bool resourcesFoldout = true;

		public override void OnInspectorGUI ()
		{
			if (EditorApplication.isPlaying && ((Game_Node_Inspector) target).Ready)
			{
				Game_Node_Inspector me = (Game_Node_Inspector)target;
				resourcesFoldout = EditorGUILayout.BeginFoldoutHeaderGroup(resourcesFoldout,
					"Resources");
				if (resourcesFoldout)
				{
					EditorGUILayout.LabelField("Main Resource Type",
							me.Grid_Node_BaseStats.MainResourceType.ToString());
					NodeResourceType[] types = me.Grid_Node_Stats.NodeResources.Keys.ToArray();
					for (int i = 0; i < types.Length; i++)
					{
						EditorGUILayout.BeginHorizontal();
						EditorGUILayout.LabelField(types[i].ToString(),
							me.Grid_Node_Stats.NodeResources[types[i]].ToString());
						EditorGUILayout.LabelField("Out of ",
							me.Grid_Node_BaseStats.NodeBaseResources[types[i]].ToString());
						EditorGUILayout.EndHorizontal();
					}
				}
				EditorGUILayout.EndFoldoutHeaderGroup();
	
				EditorUtility.SetDirty(target);
			}
			else
			{
				GUILayout.Label("Information about the node will be shown here in Play mode");
			}
		}
	}
#endif
}