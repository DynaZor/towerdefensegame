﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;
using ResourcesSystem;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System;

public class Game_Node_Stats : MonoBehaviour
{
	[Header("References")]
	[SerializeField] private Game_Node_BaseStats Grid_Node_BaseStats = null;

	// Public Data
	public Game_Node Node { get; private set; }
	public Dictionary<NodeResourceType, float> NodeResources { get; private set; }
	public bool Ready { get; private set; }

	// Timing
	private int Tick_Recovery = 0;
	private int Tick_RecordLastResources = 0;

	// Communication
	public delegate void NodeStatsUpdatedHandler (Dictionary<NodeResourceType, float> newNodeResources);
	public event NodeStatsUpdatedHandler NodeStatsUpdatedEvent = delegate { };

	// Private Data
	private Dictionary<NodeResourceType, float> lastNodeResources = null;
	private float ExtraPollution = 0f;

	// Private References
	private Gameplay_Values Gameplay_Values = null;

	#region Setup
	private void Awake ()
	{
		NodeResources = new Dictionary<NodeResourceType, float>
		{
			[NodeResourceType.GroundFertility] = 0f,
			[NodeResourceType.GroundWater] = 0f,
			[NodeResourceType.Vegetation] = 0f,
			[NodeResourceType.Pollution] = 0f,
		};
		StartCoroutine(Setup());
	}

	private IEnumerator Setup ()
	{
		while (Gameplay_Values.Instance == null)
			yield return null;
		Gameplay_Values = Gameplay_Values.Instance;

		while (Game_Clock.Instance == null)
			yield return null;

		while (!Ready)
			yield return null;

		Tick_RecordLastResources = Game_Clock.Instance.GetTickAssignment(4);
		Tick_Recovery = Game_Clock.Instance.GetTickAssignment(6);
		Game_Clock.Instance.Tick_Group4 += TickUpdate_Group4;
		Game_Clock.Instance.Tick_Group6 += TickUpdate_Group6;
	}

	public void SetResources (Dictionary<NodeResourceType, float> newNodeResources)
	{
		NodeResources = newNodeResources;
		lastNodeResources = NodeResources;
		Ready = true;
	}

	public void SetNode(Game_Node node)
	{
		Node = node;
	}

	private void OnDestroy ()
	{
		if (Ready)
		{
			Game_Clock.Instance.Tick_Group4 -= TickUpdate_Group4;
			Game_Clock.Instance.Tick_Group6 -= TickUpdate_Group6;
		}
	}
	#endregion

	#region Loops
	private void TickUpdate_Group4 (int currentTick)
	{
		if (Ready)
		{
			if (currentTick == Tick_RecordLastResources)
			{
				lastNodeResources = new Dictionary<NodeResourceType, float>(NodeResources);
			}
		}
	}

	private void TickUpdate_Group6 (int currentTick)
	{
		if (Ready)
		{
			if (currentTick == Tick_Recovery)
			{
				CheckForRecoveryAndRecover();
			}
		}
	}
	#endregion

	#region Public Actions
	public float GetPolluted(float amount, bool FromNeighbour = false, List<Game_Node> Ignore = null)
	{
		float spread = 0;
		NodeResources[NodeResourceType.Pollution] += amount;
		if (NodeResources[NodeResourceType.Pollution] > Difficulty_Manager.CurrentDifficulty.Node_Pollution_MaxPerTile)
		{
			spread = NodeResources[NodeResourceType.Pollution] - Difficulty_Manager.CurrentDifficulty.Node_Pollution_MaxPerTile;
			NodeResources[NodeResourceType.Pollution] = Difficulty_Manager.CurrentDifficulty.Node_Pollution_MaxPerTile;
			//if (Ignore == null)
			//	Ignore = new List<Game_Node>();
			//Ignore.Add(Node);
			//SpreadPollution(spread, Ignore);
		}

		CheckForChangesAndInvokeStatsUpdates();
		return spread;
	}

	public void GiveResources (List<NodeResourceType> types, List<float> amounts, bool Check)
	{ 
		if (!Check || CheckResources(types, amounts))
		{
			for (int i = 0; i < types.Count; i++)
			{
				CheckAndSubtractResource(types[i], amounts[i], true);
			}
		}
	}

	public bool CheckResources (List<NodeResourceType> types, List<float>amounts)
	{
		bool needsMet = true;
		for (int i = 0; i < types.Count; i++)
		{
			needsMet &= CheckAndSubtractResource(types[i], amounts[i], false);
		}
		return needsMet;
	}
	#endregion

	#region Private Actions
	private bool CheckAndSubtractResource (NodeResourceType type, float amount, bool substract)
	{   // Subtract only if needsMet! This way we save repeating methods and calculations
		if (substract && NodeResources[type] - amount >= 0f)
		{
			NodeResources[type] -= amount;
			CheckForChangesAndInvokeStatsUpdates();
		}

		else if (NodeResources[type] - amount >= 0f)
			return true;

		return false;
	}

	private void CheckForRecoveryAndRecover ()
	{
		UnityEngine.Profiling.Profiler.BeginSample("CheckForRecoveryAndRecover");
		if (Game_Grid.Instance.CheckNode(Node, Game_Grid.CheckType.Visible) ?? false)
		{
			float recoveryPollutionFactor =
			1f -
			Mathf.Clamp01(
			NodeResources[NodeResourceType.Pollution] /
			Gameplay_Values.NodeResourceRecoveryMaxPollution);

			if (recoveryPollutionFactor > 0f)
			{
				float recoveryPercent =
			recoveryPollutionFactor *
			Gameplay_Values.NodeResourceRecoveryPercent;

				List<NodeResourceType> resourceKeys = NodeResources.Keys.ToList();
				for (int i = 0; i < resourceKeys.Count; i++)
				{
					float maxValue = Grid_Node_BaseStats.NodeBaseResources[resourceKeys[i]];
					if (resourceKeys[i].IsRenewable() && NodeResources[resourceKeys[i]] < maxValue) // Renewable
					{
						float recoveryAmount = maxValue * recoveryPercent / 100f;
						if (NodeResources[resourceKeys[i]] + recoveryAmount < maxValue)
						{
							NodeResources[resourceKeys[i]] += recoveryAmount;
						}
						else
						{
							NodeResources[resourceKeys[i]] = maxValue;
						}
					}
				}
			}
			SubstractAndSpreadPollution();
			CheckForChangesAndInvokeStatsUpdates();
		}
		UnityEngine.Profiling.Profiler.EndSample();
	}

	public bool HasResources (List<NodeResourceType> nodeResourceTypes, List<float> nodeResourceAmounts)
	{
		bool result = true;
		for (int i = 0; i < nodeResourceAmounts.Count; i++)
		{
			result &= CheckAndSubtractResource(nodeResourceTypes[i], nodeResourceAmounts[i], false);
		}
		return result;
	}

	private void SubstractAndSpreadPollution ()
	{
		UnityEngine.Profiling.Profiler.BeginSample("SubstractAndSpreadPollution");
		if (NodeResources[NodeResourceType.Pollution] > 0f)
		{
			float sub = Gameplay_Values.NodePollutionRecoveryRate;
			float newValue = NodeResources[NodeResourceType.Pollution] - Gameplay_Values.NodePollutionRecoveryRate;

			if (newValue < 0f)
			{
				sub = NodeResources[NodeResourceType.Pollution];
				newValue = 0f;
			}
			NodeResources[NodeResourceType.Pollution] = newValue;
			//!! 1Alpha Pollution: Implement Pollution Spread
			//SpreadPollution(sub);
		}
		UnityEngine.Profiling.Profiler.EndSample();
	}

	private void SpreadPollution (float amount)//, List<Game_Node> Ignore = null)
	{
		var neighbours = Game_Grid.Instance.GetNodesFromPositions(Game_Grid.Instance.GetSurroundingNodePositions(Node.NodePos, 1, true));
		float remains = 0;
		List<Game_Node> maxedNodes = new List<Game_Node>();
		List<Game_Node> checkedNodes = new List<Game_Node>();
		for (int i = 0; i < neighbours.Count; i++)
		{
			float remain = neighbours[i].Stats.GetPolluted(amount / neighbours.Count * 0.5f);
			if (remain > 0)
			{
				remains += remain;
				maxedNodes.Add(neighbours[i]);
				maxedNodes = maxedNodes.Distinct().ToList();
			}
		}

		while (remains > 0)
		{
			if (maxedNodes.Count > checkedNodes.Count)
			{
				for (int i = 0; i < maxedNodes.Count; i++)
				{
					if (!checkedNodes.Contains(maxedNodes[i]))
					{
						float newRemains = remains;
						checkedNodes.Add(maxedNodes[i]);

						var maxedNeighbours = Game_Grid.Instance.GetNodesFromPositions(Game_Grid.Instance.GetSurroundingNodePositions(maxedNodes[i].NodePos, 1, true));
						for (int y = 0; y < maxedNeighbours.Count; y++)
						{
							if (!checkedNodes.Contains(maxedNeighbours[y]) && !maxedNodes.Contains(maxedNeighbours[y]))
							{
								float sub = remains / maxedNeighbours.Count * 0.5f;
								newRemains -= sub * 2;
								float remain = maxedNeighbours[y].Stats.GetPolluted(sub);
								if (remain > 0)
								{
									maxedNodes.Add(maxedNeighbours[y]);
									maxedNodes = maxedNodes.Distinct().ToList();
									newRemains += remain;
								}
							}
						}
						remains = newRemains;
					}
				}
			}
			else
			{
				continue;
			}
		}
	}

	private void CheckForChangesAndInvokeStatsUpdates ()
	{
		if (lastNodeResources != NodeResources)
		{
			NodeStatsUpdatedEvent.Invoke(NodeResources);
		}
	}
	#endregion
}