﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

public class Game_Grid : MonoBehaviour
{
	#region Singleton
	public static Game_Grid Instance;
	private void Awake ()
	{
		if (Instance != null)
			Debug.Log("Game_Grid - More than one instance!\n" +
				"Registered Instance: " + Instance.gameObject.name +
				"This Instance: " + gameObject.name, this);
		else
			Instance = this;
	}
	#endregion

	[Header("Gizmos")]
	[SerializeField] private bool EnableGizmos = false;

	[Header("Grid Settings")]
	public int GridSizeX = 20;
	public int GridSizeY = 20;
	[SerializeField] private int BasePosX = 10;
	[SerializeField] private int BasePosY = 10;
	[SerializeField] private SO_Item BaseSO = null;
	[SerializeField] private SO_Item BuildingSO = null;

	[Header("Prefabs")]
	[SerializeField] private GameObject NodePrefab = null;
	[SerializeField] private Item_Information ItemPrefab = null;

	[Header("References")]
	[SerializeField] private GameObject NodesParent = null;
	[SerializeField] private GameObject ItemsParent = null;
	[SerializeField] private GameObject PollutionGO = null;

	// Public Data
	public Game_Node[,] Nodes { get; private set; }
	public bool Ready { get; private set; }

	// Data
	private static bool data_UpdatedThisFrame = false;
	public static bool[,] Data_Blocked { get; private set; }
	public static bool[,] Data_Buildable { get; private set; }
	public static bool[,] Data_Visible { get; private set; }
	public static bool[,] Data_Ally { get; private set; }
	public static bool[,] Data_AllyMovable { get; private set; }
	public static bool[,] Data_Enemy { get; private set; }
	public static bool[,] Data_Navigatable { get; private set; }
	public static bool[,] Data_Demolishable { get; private set; }

	public static bool Data_Blocked_UpdatedThisFrame = false;
	public static bool Data_Buildable_UpdatedThisFrame = false;
	public static bool Data_Visible_UpdatedThisFrame = false;
	public static bool Data_Ally_UpdatedThisFrame = false;
	public static bool Data_AllyMovable_UpdatedThisFrame = false;
	public static bool Data_Enemy_UpdatedThisFrame = false;
	public static bool Data_Navigatable_UpdatedThisFrame = false;
	public static bool Data_Demolishable_UpdatedThisFrame = false;

	// Public Events
	public delegate void Loading_GridCreationEventHandler (float percentDone);
	public static event Loading_GridCreationEventHandler Loading_GridCreationEvent = delegate { };

	public delegate void GridItemsUpdatedHandler ();
	public static event GridItemsUpdatedHandler GridItemsUpdatedEvent = delegate { };
	public static event GridItemsUpdatedHandler GridItemsUpdatedEvent_ForceReRender = delegate { };
	public static event GridItemsUpdatedHandler GridItemsUpdatedEvent_ItemsQueueFinished = delegate { };

	public delegate void GridDataChangedHandler();
	public static event GridDataChangedHandler GridDataUpdate_Visibility = delegate { };

	// Private Data
	private DifficultySetting currentDifficulty => Difficulty_Manager.CurrentDifficulty;

	#region Setup
	public void NewGame ()
	{
		Ready = false;
		CreateGrid();
	}

	public void LoadGame ()
	{
		Ready = false;
		//!! 1Alpha LoadGame: Load Saved Game Data (Grid)
		//!! 1Alpha LoadGame: Notify Loader
		Ready = true;
	}

	private void CreateGrid ()
	{
		StartCoroutine(GridBuilder(0));
		UpdatePollution();
	}

	private void UpdatePollution ()
	{
		PollutionGO.transform.localScale = new Vector3(GridSizeX, 1f, GridSizeY);
		PollutionGO.GetComponent<Visual_Game_PollutionController>().IssueUpdate();
	}

	private IEnumerator GridBuilder (int waitFrames)
	{
		for (int i = 0; i < waitFrames; i++)
		{
			yield return null;
		}
		Nodes = new Game_Node[GridSizeX, GridSizeY];
		//Get the real world position of the bottom left of the grid.
		Vector3 bottomLeft = transform.position - (Vector3.right * (GridSizeX * 0.5f)) - (Vector3.up * (GridSizeY * 0.5f));
		int generatedNodes = 0;
		int totalNodes = Nodes.Length;
		float lastUpdate = 0f;
		for (int y = 0; y < GridSizeY; y++)
		{
			for (int x = 0; x < GridSizeX; x++)
			{
				//Get the world co ordinates of the bottom left of the graph
				Vector3 worldPoint = bottomLeft + (Vector3.right * x) + (Vector3.up * y);
				//Create a new node in the array
				Nodes[x, y] = new Game_Node(worldPoint, x, y);
				// Instantiate and Reference NodePrefab
				Nodes[x, y].Setup(Instantiate(NodePrefab, worldPoint, new Quaternion(), NodesParent.transform));
				// Add listener to node
				Nodes[x, y].ItemsUpdatedEvent += () => GridItemsUpdatedEvent.Invoke();
				// Loading Notifications
				generatedNodes++;
				if (Time.realtimeSinceStartup - lastUpdate > 0.025f)
				{
					Loading_GridCreationEvent.Invoke((float) generatedNodes / totalNodes);
					lastUpdate = Time.realtimeSinceStartup;
				}
				// Wait a frame every 100 nodes
				if (generatedNodes % 150 == 0)
					yield return null;
			}
		}
		Game_Speed.Instance.SetSpeed(Game_Speed.Speed.Normal);
		CreateItem(BasePosX, BasePosY, BaseSO, isBase: true);
		CreateItem(0, 5, BuildingSO);
		Game_Speed.Instance.SetSpeed(Game_Speed.Speed.Pause);
		StartCoroutine(Notifier());
	}

	public void GridItemsUpdated ()
	{
		GridItemsUpdatedEvent.Invoke();
	}

	private IEnumerator Notifier ()
	{
		while (Nodes[GridSizeX - 1, GridSizeY - 1] == null)
			yield return null;
		while (Nodes[GridSizeX - 1, GridSizeY - 1].Stats == null)
			yield return null;
		while (!Nodes[GridSizeX - 1, GridSizeY - 1].Stats.Ready)
			yield return null;
		Ready = true;
	}
	#endregion

	#region Loops
	private void Update ()
	{
		if (Ready)
		{
			data_UpdatedThisFrame = false;

			Data_Blocked_UpdatedThisFrame = false;
			Data_Buildable_UpdatedThisFrame = false;
			Data_Visible_UpdatedThisFrame = false;
			Data_Ally_UpdatedThisFrame = false;
			Data_AllyMovable_UpdatedThisFrame = false;
			Data_Enemy_UpdatedThisFrame = false;
			Data_Navigatable_UpdatedThisFrame = false;
			Data_Demolishable_UpdatedThisFrame = false;
			CheckAll_UpdateData();
		}
	}
	#endregion

	#region Public Actions
	#region UI
	/*public void ForceUpdateAllNodeVisuals ()
	{
		for (int x = 0; x < GridSizeX; x++)
		{
			for (int y = 0; y < GridSizeY; y++)
			{
				Nodes[x, y].Visual.ForceUpdateValues();
			}
		}
	}*/
	/*public void ToggleSelectionState (bool state)
	{
		for (int x = 0; x < GridSizeX; x++)
		{
			for (int y = 0; y < GridSizeY; y++)
			{
				Nodes[x, y].Visual.Hover(state);
			}
		}
	}*/
	#endregion
	#region Items
	private Queue<UnityAction> Queue_ItemCreation = new Queue<UnityAction>();
	private Coroutine Queue_ItemCreation_Routine = null;
	public void CreateItem (int gridX, int gridY, SO_Item item, bool ForceReRender = false, bool isBase = false)
	{
		UnityAction action = ()=>_CreateItem(gridX, gridY, item, ForceReRender, isBase);
		Queue_ItemCreation.Enqueue(action);

		if (Queue_ItemCreation_Routine == null)
			Queue_ItemCreation_Routine = StartCoroutine(ItemCreation_Routine());
	}
	public Item_Information _CreateItem (int gridX, int gridY, SO_Item item, bool ForceReRender = false, bool isBase = false)
	{
		Item_Information newItem = Instantiate(ItemPrefab, WorldPointFromNode(gridX, gridY, true), new Quaternion(), ItemsParent.transform);
		newItem.GetComponent<MeshRenderer>().enabled = item.Movable;
		newItem.SetItemSO(item);
		newItem.SetNode(Nodes[gridX, gridY].Stats);
		Nodes[gridX, gridY].SetItem(newItem);

		if (isBase)
		{
			//Item_PlayerBase playerBase;
			newItem.gameObject.AddComponent<Item_PlayerBase>();
			Debug.Log("playerBase created");
			//playerBase.Node = Nodes[gridX, gridY];
		}


		if (ForceReRender)
			GridItemsUpdatedEvent_ForceReRender.Invoke();
		return newItem;
	}
	private IEnumerator ItemCreation_Routine ()
	{
		bool lastFrameHadItems = false;
		while (true)
		{
			if (Queue_ItemCreation.Count > 0)
			{
				lastFrameHadItems = true;
				int maxAmount = 25;
				int amountToProcess = Mathf.Clamp(Queue_ItemCreation.Count, 0, maxAmount);
				for (int i = 0; i < amountToProcess; i++)
				{
					Queue_ItemCreation.Dequeue().Invoke();
				}
			}
			else
			{
				if (lastFrameHadItems)
					GridItemsUpdatedEvent_ItemsQueueFinished.Invoke();
				lastFrameHadItems = false;
			}
			yield return null;
		}
	}

	#endregion
	#region Nodes
	#region Node Checks
	#region Base Check
	public bool? CheckNode (Game_Node Node, CheckType check)
	{
		if (Node == null)
			return null;
		bool[,] grid = Nodes_GetGrid(check);
		return grid[Node.GridPosX, Node.GridPosY];
	}
	public bool? CheckNodeAt (Vector2Int nodePos, CheckType check)
	{
		if (!DoesNodeExistAt(nodePos))
			return null;
		bool[,] grid = Nodes_GetGrid(check);
		return grid[nodePos.x, nodePos.y];
	}
	#endregion
	#region Exist Checks
	public List<Vector2Int> ReturnOnlyExistingNodesAt (List<Vector2Int> nodePos)
	{
		List<Vector2Int> result = new List<Vector2Int>();
		for (int i = 0; i < nodePos.Count; i++)
		{
			if (DoesNodeExistAt(nodePos[i]))
			{
				result.Add(nodePos[i]);
			}
		}
		result = result.Distinct().ToList();
		return result;
	}
	public bool DoesNodeExistAt (Vector2Int? nodePos)
	{
		if (nodePos == null || nodePos.Value.x < 0 || nodePos.Value.x > GridSizeX - 1 || nodePos.Value.y < 0 || nodePos.Value.y > GridSizeY - 1)
		{
			return false;
		}
		return true;
	}
	public bool DoesNodeExistAt (int x, int y)
	{
		return DoesNodeExistAt(new Vector2Int(x, y));
	}
	#endregion
	public enum CheckType
	{
		Exist, Blocked, Buildable, Visible, Enemy, Ally, AllyMovable, Navigatable, Demolishable
	}

	public void CheckAll_UpdateData ()
	{
		if (data_UpdatedThisFrame)
			return;

		_ = Nodes_GetGrid(CheckType.Blocked);
		_ = Nodes_GetGrid(CheckType.Ally); // + Ally Movable
		_ = Nodes_GetGrid(CheckType.Enemy);
		_ = Nodes_GetGrid(CheckType.Visible);
		_ = Nodes_GetGrid(CheckType.Buildable);
		_ = Nodes_GetGrid(CheckType.Navigatable);
		_ = Nodes_GetGrid(CheckType.Demolishable);

		data_UpdatedThisFrame = true;
		Visual_Game_NodeController.Instance.UpdatePerFrameMaps();
	}

	/// <summary>
	/// Checks all nodes at the supplied list.
	/// Returns TRUE only if ALL the nodes comply to the desired check result.
	/// Returns FALSE if ANY of the nodes doesn't comply with the desired check result.
	/// </summary>
	/// <param name="nodePos">The node positions list.</param>
	/// <param name="check">The desired check.</param>
	/// <param name="desiredResult">The desired check result.</param>
	/// <returns></returns>
	public bool CheckAllNodesAt (List<Vector2Int> nodePos, CheckType check, bool desiredResult = true)
	{
		Nodes_GetGrid(check);
		nodePos = ReturnOnlyExistingNodesAt(nodePos);
		for (int i = 0; i < nodePos.Count; i++)
		{
			if (CheckNodeAt(nodePos[i], check) != desiredResult)
				return false;
		}
		return true;
	}

	/// <summary>
	/// Checks all nodes at the supplied list.
	/// Returns true if ANY of the nodes comply to the desired check result.
	/// Returns FALSE only if ALL the nodes don't comply with the desired check result.
	/// </summary>
	/// <param name="nodePos">The node positions list.</param>
	/// <param name="check">The desired check.</param>
	/// <param name="desiredResult">The desired check result.</param>
	/// <returns></returns>
	public bool CheckAnyNodesAt (List<Vector2Int> nodePos, CheckType check, bool desiredResult = true)
	{
		Nodes_GetGrid(check);
		nodePos = ReturnOnlyExistingNodesAt(nodePos);
		for (int i = 0; i < nodePos.Count; i++)
		{
			if (CheckNodeAt(nodePos[i], check) == desiredResult)
				return true;
		}
		return false;
	}
	public bool CheckAnySurroundingNodes (Vector2Int nodePos, int radius = 1, bool square = false, CheckType check = CheckType.Exist, bool desiredResult = true)
	{
		Nodes_GetGrid(check);
		for (int i = 1; i < radius; i++)
		{
			var nodes = GetSurroundingNodePositionsProgressive(nodePos, i, square);

			if (CheckAnyNodesAt(nodes, check) == desiredResult)
				return true;
		}
		return false;
	}
	public bool CheckAllSurroundingNodes (Vector2Int nodePos, int radius = 1, bool square = false, CheckType check = CheckType.Exist, bool desiredResult = true)
	{
		Nodes_GetGrid(check);
		for (int i = 1; i < radius; i++)
		{
			var nodes = GetSurroundingNodePositionsProgressive(nodePos, i, square);

			if (CheckAnyNodesAt(nodes, check) != desiredResult)
				return false;
		}
		return true;
	}
	public bool[,] Nodes_GetGrid (CheckType check)
	{
		bool[,] result = _Nodes_GetGrid(check);
		return result;
	}
	private bool[,] _Nodes_GetGrid (CheckType check)
	{
		bool[,] result = new bool[GridSizeX, GridSizeY];
		if (Ready)
		{
			switch (check)
			{
				case CheckType.Blocked:
					if (Data_Blocked_UpdatedThisFrame)
						return Data_Blocked;

					Data_Blocked = new bool[GridSizeX, GridSizeY];
					for (int Y = 0; Y < GridSizeY; Y++)
					{
						for (int X = 0; X < GridSizeX; X++)
						{
							if (Nodes[X, Y].IsBlocked)
								Data_Blocked[X, Y] = true;
						}
					}
					Data_Blocked_UpdatedThisFrame = true;
					return Data_Blocked;

				case CheckType.Enemy:
					if (Data_Enemy_UpdatedThisFrame)
						return Data_Enemy;

					if (!Data_Blocked_UpdatedThisFrame)
						_ = Nodes_GetGrid(CheckType.Blocked);

					Data_Enemy = new bool[GridSizeX, GridSizeY];
					for (int Y = 0; Y < GridSizeY; Y++)
					{
						for (int X = 0; X < GridSizeX; X++)
						{
							if (Data_Blocked[X, Y])
							{
								if (Nodes[X, Y].Enemy != null)
									Data_Enemy[X, Y] = true;
							}
						}
					}
					Data_Enemy_UpdatedThisFrame = true;
					return Data_Enemy;

				case CheckType.Ally:
				case CheckType.AllyMovable:
				{
					if (check == CheckType.Ally && Data_Ally_UpdatedThisFrame)
						return Data_Ally;
					else if (check == CheckType.AllyMovable && Data_AllyMovable_UpdatedThisFrame)
						return Data_AllyMovable;

					if (!Data_Blocked_UpdatedThisFrame)
						_ = Nodes_GetGrid(CheckType.Blocked);

					Data_Ally = new bool[GridSizeX, GridSizeY];
					Data_AllyMovable = new bool[GridSizeX, GridSizeY];

					for (int Y = 0; Y < GridSizeY; Y++)
					{
						for (int X = 0; X < GridSizeX; X++)
						{
							if (Data_Blocked[X, Y])
							{
								if (Nodes[X, Y].Item != null)
								{
									Data_Ally[X, Y] = true;
									Data_AllyMovable[X, Y] = Nodes[X, Y].Item.Data != null && Nodes[X, Y].Item.Data.Movable;
								}
								else
								{
									Data_Ally[X, Y] = false;
									Data_AllyMovable[X, Y] = false;
								}
							}
						}

					}

					Data_Ally_UpdatedThisFrame = true;
					Data_AllyMovable_UpdatedThisFrame = true;

					if (check == CheckType.Ally)
						return Data_Ally;
					else
						return Data_AllyMovable;
				}

				case CheckType.Visible:
				case CheckType.Buildable:
					bool build = true;
					bool[,] temp;
					if (check == CheckType.Buildable)
					{
						if (Data_Buildable_UpdatedThisFrame)
							return Data_Buildable;
						temp = new bool[GridSizeX, GridSizeY];
					}
					else //if (check == CheckType.Visible)
					{
						build = false;
						if (Data_Visible_UpdatedThisFrame)
							return Data_Visible;
						temp = new bool[GridSizeX, GridSizeY];
					}

					//!! 0POC GridCheck: Visibility: Replace radius with Gameplay Values (difficulty dependant)
					int visibleRadius = 3;
					//!! 0POC GridCheck: BuildRadius: Replace radius with Gameplay Values (difficulty dependant)
					int buildRadius = 2;
					int radius = check == CheckType.Visible? visibleRadius : buildRadius;

					if (!Data_Ally_UpdatedThisFrame)
						_ = Nodes_GetGrid(CheckType.Ally);

					List<Vector2Int> checkedNodes = new List<Vector2Int>();

					for (int Y = 0; Y < GridSizeY; Y++)
					{
						for (int X = 0; X < GridSizeX; X++)
						{
							if (Data_Ally[X, Y])
							{
								temp[X, Y] = true;

								Vector2Int currNodePos = new Vector2Int(X, Y);
								checkedNodes.Add(currNodePos);
								List<Vector2Int> surr = GetSurroundingNodePositions(currNodePos, radius, false);
								for (int i = 0; i < surr.Count; i++)
								{
									if (!checkedNodes.Contains(surr[i]))
									{
										temp[surr[i].x, surr[i].y] = true;

										checkedNodes.Add(surr[i]);
									}
								}
							}
						}
					}

					switch (check)
					{
						case CheckType.Visible:
							bool changed = Data_Visible == null || !Data_Visible.Equals(temp);
							Data_Visible = temp.Clone() as bool[,];
							Data_Visible_UpdatedThisFrame = true;
							if (changed)
								GridDataUpdate_Visibility.Invoke();
							return Data_Visible;

						case CheckType.Buildable:
							Data_Buildable = temp.Clone() as bool[,];
							Data_Buildable_UpdatedThisFrame = true;
							return Data_Buildable;
					}

					break;

				case CheckType.Navigatable:
					if (Data_Navigatable_UpdatedThisFrame)
						return Data_Navigatable;
					Data_Navigatable = new bool[GridSizeX, GridSizeY];

					if (!Data_Visible_UpdatedThisFrame)
						_ = Nodes_GetGrid(CheckType.Visible);
					if (!Data_Blocked_UpdatedThisFrame)
						_ = Nodes_GetGrid(CheckType.Blocked);

					for (int Y = 0; Y < GridSizeY; Y++)
					{
						for (int X = 0; X < GridSizeX; X++)
						{
							Data_Navigatable[X, Y] = Data_Visible[X, Y] 
								&& !Data_Blocked[X, Y];
						}
					}
					Data_Navigatable_UpdatedThisFrame = true;
					return Data_Navigatable;

				case CheckType.Demolishable:
					if (Data_Demolishable_UpdatedThisFrame)
						return Data_Demolishable;
					Data_Demolishable = new bool[GridSizeX, GridSizeY];

					if (!Data_Ally_UpdatedThisFrame)
						_ = Nodes_GetGrid(CheckType.Ally);

					for (int Y = 0; Y < GridSizeY; Y++)
					{
						for (int X = 0; X < GridSizeX; X++)
						{
							Data_Demolishable[X, Y] = 
								Data_Ally[X, Y] 
								&& Nodes[X, Y].Item != null
								&& Nodes[X, Y].Item.Item_Stats != null
								&& Nodes[X, Y].Item.Item_Stats.Demolishable;
						}
					}
					Data_Demolishable_UpdatedThisFrame = true;
					return Data_Demolishable;
			}
		}
		return result;
	}
	public int[,] GetItemMapForPF ()
	{
		CheckAll_UpdateData();
		int[,] result = new int[GridSizeX, GridSizeY];
		for (int y = 0; y < GridSizeY; y++)
		{
			for (int x = 0; x < GridSizeX; x++)
			{
				if (Data_Blocked[x, y])
				{
					if (Data_Ally[x, y])
					{
						if (Data_AllyMovable[x, y])
						{
							result[x, y] = 4;
							continue;
						}
						result[x, y] = 1;
						continue;
					}
					else if (Data_Enemy[x, y])
					{
						result[x, y] = 2;
						continue;
					}
					// Obstacle
					result[x, y] = 3;
				}
				else // Free
				{
					result[x, y] = 0;
				}
			}
		}
		return result;
	}
	public int[,] GetHealthMapForPF ()
	{
		int[,] result = new int[GridSizeX, GridSizeY];
		for (int y = 0; y < GridSizeY; y++)
		{
			for (int x = 0; x < GridSizeX; x++)
			{
				if (Nodes[x, y].Item != null)
				{
					if (Nodes[x, y].Item.Ready)
						result[x, y] = Mathf.CeilToInt(Nodes[x, y].Item.Item_Stats.CurrentHealth);
					else
						result[x, y] = 99999;
				}
				else if (Nodes[x, y].Enemy != null)
				{
					if (Nodes[x, y].Enemy.Ready)
						result[x, y] = Mathf.CeilToInt(Nodes[x, y].Enemy.Enemy_Stats.CurrentHealth);
					else
						result[x, y] = 99999;
				}
			}
		}
		return result;
	}
	#endregion
	#region Nodes Get
	public Game_Node NodeAt (Vector2Int? nodePos)
	{
		if (nodePos != null)
		{
			if (DoesNodeExistAt(nodePos))
			{
				return Nodes[nodePos.Value.x, nodePos.Value.y];
			}
			else
			{
				return null;
			}
		}
		return null;
	}
	public List<Vector2Int> GetSurroundingNodePositionsProgressive (Vector2Int nodePos, int checkRadius, bool Square = false)
	{
		List <Vector2Int> result = new List<Vector2Int>();
		if (checkRadius <= 0)
		{
			return result;
		}

		List<Vector2Int> toCheck = DynaZor.EditorUtilities.Helpers.GetCircleBres(nodePos.x, nodePos.y, checkRadius);
		toCheck = toCheck.Distinct().ToList();
		if (toCheck.Contains(nodePos))
		{
			toCheck.RemoveAll(x => x == nodePos);
		}
		result = ReturnOnlyExistingNodesAt(toCheck);

		return result;
	}
	public List<Vector2Int> GetSurroundingNodePositions (Vector2Int nodePos, int checkRadius, bool Square = false)
	{
		List <Vector2Int> result = new List<Vector2Int>();
		if (checkRadius <= 0)
		{
			return result;
		}
		List<Vector2Int> toCheck = new List<Vector2Int>();
		if (Square)
		{
			int minX = nodePos.x - checkRadius;
			int maxX = nodePos.x + checkRadius;
			int minY = nodePos.y - checkRadius;
			int maxY = nodePos.y + checkRadius;
			for (int x = minX; x <= maxX; x++)
			{
				for (int y = minY; y <= maxY; y++)
				{
					Vector2Int current = new Vector2Int(x, y);
					if (current != nodePos && !toCheck.Contains(current))
					{
						toCheck.Add(current);
					}
				}
			}
		}
		else // Circle
		{
			toCheck.AddRange(DynaZor.EditorUtilities.Helpers.DrawFilledCircle2(nodePos.x, nodePos.y, checkRadius));
		}

		if (result.Contains(nodePos))
		{
			result.RemoveAll(x => x == nodePos);
		}

		result = ReturnOnlyExistingNodesAt(toCheck);
		return result;
	}
	public List<Game_Node> GetNodesFromPositions (List<Vector2Int> nodePos)
	{
		List<Game_Node> result = new List<Game_Node>();
		nodePos.ForEach(x => result.Add(NodeAt(x)));
		return result;
	}
	#endregion
	#endregion
	#region Calculators
	public void GetGridBoundaries (out Vector3 min, out Vector3 max)
	{
		min = Nodes[0, 0].NodeWorldPosition;
		max = Nodes[GridSizeX - 1, GridSizeY - 1].NodeWorldPosition;
	}
	public Game_Node NodeFromWorldPoint (Vector3 a_vWorldPos)
	{
		float ixPos = Mathf.Clamp01((a_vWorldPos.x + GridSizeX / 2f) / GridSizeX);
		float iyPos = Mathf.Clamp01((a_vWorldPos.y + GridSizeY / 2f) / GridSizeY);

		int ix = Mathf.RoundToInt((GridSizeX - 1f) * ixPos);
		int iy = Mathf.RoundToInt((GridSizeY - 1f) * iyPos);

		return Nodes[ix, iy];
	}
	public Vector3 WorldPointFromNode (int x, int y, bool item)
	{
		if (item)
		{
			Vector3 pos = Nodes[x, y].NodeWorldPosition;
			pos.z = ItemsParent.transform.position.z;
			return pos;
		}
		else
			return Nodes[x, y].NodeWorldPosition;
	}
	#endregion
	#endregion

	#region Editor Only
#if UNITY_EDITOR
	[Header("Test Controls")]
	public bool Test_GenerateNewGrid = false;

	private void OnValidate ()
	{
		if (Test_GenerateNewGrid)
		{
			Test_GenerateNewGrid = false;
			StartCoroutine(GridBuilder(1));
		}
	}

	private void OnDrawGizmos ()
	{
		Gizmos.DrawWireCube(transform.position, new Vector3(GridSizeX, GridSizeY, 1f)); //Draw a wire cube with the given dimensions from the Unity inspector

		if (EnableGizmos && Nodes != null && Ready) //If the grid is not empty
		{
			foreach (Game_Node n in Nodes) //Loop through every node in the grid
			{
				bool showCube = false;
				if (n.IsBlocked) //If the current node is a wall node
				{
					Gizmos.color = new Color(1, 1, 1, 0.2f); //Set the color of the node
					showCube = true;
				}

				if (showCube)
					Gizmos.DrawCube(n.NodeWorldPosition, Vector3.one); //Draw the node at the position of the node.
			}
		}
	}
#endif
	#endregion
}