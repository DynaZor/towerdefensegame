﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;
using System.Collections;
using System.Linq;
using ResourcesSystem;

public class Game_Node_Sprites : MonoBehaviour
{
	[Header("References")]
	[SerializeField] private Game_Node_Stats Grid_Node_Stats = null;
	[SerializeField] private Gameplay_Values Gameplay_Values = null;

	// Private Data
	private bool Ready = false;

	// Timing
	private int Tick_SpriteUpdate = 0;

	// References
	[SerializeField] private MeshRenderer BackGroundSprite = null;
	[SerializeField] private MeshRenderer GroundWaterSprite = null;
	[SerializeField] private MeshRenderer GroundFertilitySprite = null;
	[SerializeField] private MeshRenderer VegetationSprite = null;
	//[SerializeField] private MeshRenderer PollutionSprite = null;
	
	#region Setup
	private void Awake ()
	{
		StartCoroutine(Setup());
	}

	private IEnumerator Setup ()
	{
		while (Gameplay_Values.Instance == null)
			yield return null;
		Gameplay_Values = Gameplay_Values.Instance;

		while (Visual_Game_SpriteDatabase.Instance == null)
			yield return null;
		SetSprites();

		while (!Grid_Node_Stats.Ready)
			yield return null;
		while (!Ready)
			yield return null;
		UpdateSprites();

		while (Game_Clock.Instance == null)
			yield return null;
		Tick_SpriteUpdate = Game_Clock.Instance.GetTickAssignment(3);
		Game_Clock.Instance.Tick_Group3 += TickUpdate;
	}

	private void OnDestroy ()
	{
		Game_Clock.Instance.Tick_Group3 -= TickUpdate;
	}
	#endregion

	#region Loops
	private void TickUpdate (int currentTick)
	{
		if (currentTick == Tick_SpriteUpdate)
		{
			UpdateSprites();
		}
	}
	#endregion

	#region Public Actions
	private void SetSprites ()
	{
		Visual_Game_SpriteDatabase db = Visual_Game_SpriteDatabase.Instance;
		int index = Random.Range(0, db.NodeSpritesSets.Length);
		SO_Node_Sprites sprites = db.NodeSpritesSets[index];

		BackGroundSprite.material.mainTexture = sprites.GroundDry;
		GroundWaterSprite.material.mainTexture = sprites.GroundWater;
		GroundFertilitySprite.material.mainTexture = sprites.GroundFertility;
		VegetationSprite.material.mainTexture = sprites.Vegetation;
		//PollutionSprite.material.mainTexture = sprites.Pollution;
		Ready = true;
	}
	#endregion

	#region Private Actions
	private void UpdateSprites ()
	{
		var keys = Grid_Node_Stats.NodeResources.Keys.ToList();
		for (int i = 0; i < keys.Count; i++)
		{
			float alpha = Mathf.Clamp01(Grid_Node_Stats.NodeResources[keys[i]] / Gameplay_Values.NodeResourceGenerationBaseValue);
			UpdateSprite(keys[i], alpha);
		}
	}

	private void UpdateSprite (NodeResourceType type, float alpha)
	{
		MeshRenderer currentSprite = GetSpriteByType(type);
		if (alpha > 0f)
		{
			currentSprite.enabled = true;
			currentSprite.material.color = new Color(1f, 1f, 1f, alpha);
		}
		else
		{
			currentSprite.enabled = false;
		}
	}

	private MeshRenderer GetSpriteByType (NodeResourceType type)
	{
		switch (type)
		{
			case NodeResourceType.GroundWater:
				return GroundWaterSprite;
			case NodeResourceType.GroundFertility:
				return GroundFertilitySprite;
			case NodeResourceType.Vegetation:
				return VegetationSprite;
			//case NodeResourceType.Pollution:
			//	return PollutionSprite;
		}
		return null;
	}
	#endregion
}