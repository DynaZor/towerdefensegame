﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using UnityEngine;
using System.Collections;
using UnityEditor;

public class Debug_Panel_Text : MonoBehaviour
{
	private TMPro.TextMeshProUGUI label;
	private void Awake ()
	{
		label = GetComponent<TMPro.TextMeshProUGUI>();
	}

	private void Update ()
	{
		string newText = "<u>Debug Info</u>\n";
		if (EditorApplication.isPlaying && Game_Clock.Instance != null)
		{
			newText += "Current Scene: " + UnityEditor.SceneManagement.EditorSceneManager.GetActiveScene().name + "\n";
			newText += "Current Time: " + Game_Clock.Instance.CurrentTime.ToString() + "\n";
			newText += "Tick_Group: " + Game_Clock.Instance.CurrentTickGroup.ToString() + "\n";
			newText += "Tick: " +
				Game_Clock.Instance.CurrentTick.ToString() + ":" + Game_Clock.Instance.CurrentTickOfGroup.ToString() + "\n";
		}
		else
		{
			newText += "Not Playing\\No Clock" + "Current Scene: " + UnityEditor.SceneManagement.EditorSceneManager.GetActiveScene().name;
		}

		if (label != null  && label.text != newText)
			label.text = newText;
	}
}