﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
using System;
using UnityEngine;
using UnityEditor;
using UnityEditor.EditorTools;
/*
public class Blobal_EditorTools
{
	static Game_Clock.TickEventHandler(int currentTick)
	{
		SceneView.onSceneGUIDelegate += OnScene;
	}
	static void OnScene (SceneView sceneView)
	{
		// Draw GUI stuff here for Scene window display
	}
}
/*
[EditorTool("TD Debug")]
class PlatformTool : EditorTool
{
	// Serialize this value to set a default value in the Inspector.
	[SerializeField]
	Texture2D m_ToolIcon;

	GUIContent m_IconContent;

	void OnEnable ()
	{
		m_IconContent = new GUIContent()
		{
			image = m_ToolIcon,
			text = "TD Debug",
			tooltip = "TD Debug"
		};
	}

	public override GUIContent toolbarIcon
	{
		get { return m_IconContent; }
	}

	// This is called for each window that your tool is active in. Put the functionality of your tool here.
	public override void OnToolGUI (EditorWindow window)
	{
		EditorGUI.BeginChangeCheck();
		if (EditorApplication.isPlaying)
		{
			EditorGUILayout.LabelField("Tick_Group: ", Game_Clock.Instance.CurrentTickGroup.ToString());
			EditorGUILayout.LabelField("Tick: ",
				Game_Clock.Instance.CurrentTick.ToString() + ":" + Game_Clock.Instance.CurrentTickOfGroup.ToString());
		}
		else
		{
			EditorGUILayout.LabelField("Not Playing", "Current Scene: " + UnityEditor.SceneManagement.EditorSceneManager.GetActiveScene().name);
		}

		if (EditorGUI.EndChangeCheck())
		{
		}
	}
}
*/
