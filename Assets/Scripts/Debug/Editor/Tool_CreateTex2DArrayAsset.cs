﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public static class Tool_CreateTex2DArrayAsset
{
	[MenuItem("DynaZor/Assets/Create Texture2DArray Asset", true)]
	private static bool CheckSelectionForTextures ()
	{
		object[] selection = Selection.objects;
		List<Texture2D> textures = new List<Texture2D>();
		for (int i = 0; i < selection.Length; i++)
		{
			if (selection[i].GetType() == typeof(Texture2D))
			{
				return true;
			}
		}
		return false;
	}
	[MenuItem("DynaZor/Assets/Create Texture2DArray Asset", false)]
	private static void GetTexturesFromSelection ()
	{
		object[] selection = Selection.objects;
		List<Texture2D> textures = new List<Texture2D>();
		for (int i = 0; i < selection.Length; i++)
		{
			if (selection[i].GetType() == typeof(Texture2D))
			{
				textures.Add((Texture2D) selection[i]);
			}
		}
		CreateAssetFromSelectedAssets(textures.ToArray());
	}

	private static void CreateAssetFromSelectedAssets (Texture2D[] textures)
	{
		Texture2DArray array = new Texture2DArray(textures[0].width, textures[0].height, textures.Length, textures[0].format, false);
		for (int i = 0; i < textures.Length; i++)
			array.SetPixels(textures[i].GetPixels(), i);
		array.Apply();
		AssetDatabase.CreateAsset(array, "Assets/Texture2DArray.asset");
	}
}
