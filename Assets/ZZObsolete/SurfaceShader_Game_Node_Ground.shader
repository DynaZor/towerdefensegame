﻿Shader "#DynaZor/SurfaceShader_Game_Node_Ground"
{
    Properties
    {
		_Textures("Textures", 2DArray) = "" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0

		_Value_GroundWater("Ground Water (0-1)", Range(0,1)) = 0.0
		_Value_Fertility("Fertility (0-1)", Range(0,1)) = 0.0
		_Value_Vegetation("Vegetation (0-1)", Range(0,1)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        #pragma surface surf Standard
        #pragma target 3.0

		UNITY_DECLARE_TEX2DARRAY(_Textures);

        struct Input
        {
            float2 uv_Textures;
        };

        half _Glossiness;
        half _Metallic;

        UNITY_INSTANCING_BUFFER_START(Props)
			UNITY_DEFINE_INSTANCED_PROP(half, _Value_Fertility)
			UNITY_DEFINE_INSTANCED_PROP(half, _Value_GroundWater)
			UNITY_DEFINE_INSTANCED_PROP(half, _Value_Vegetation)
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard output)
        {
			fixed4 water = UNITY_SAMPLE_TEX2DARRAY(_Textures, float3(IN.uv_Textures, 1));
			fixed4 fertility = UNITY_SAMPLE_TEX2DARRAY(_Textures, float3(IN.uv_Textures, 2));
			fixed4 vegetation = UNITY_SAMPLE_TEX2DARRAY(_Textures, float3(IN.uv_Textures, 3));

			fixed3 finalTexture = lerp(UNITY_SAMPLE_TEX2DARRAY(_Textures, float3(IN.uv_Textures, 0)).rgb, water.rgb, water.a * UNITY_ACCESS_INSTANCED_PROP(Props, _Value_GroundWater));
			finalTexture = lerp(finalTexture.rgb, fertility.rgb, fertility.a * UNITY_ACCESS_INSTANCED_PROP(Props, _Value_Fertility));
			finalTexture = lerp(finalTexture.rgb, vegetation.rgb, vegetation.a * UNITY_ACCESS_INSTANCED_PROP(Props, _Value_Vegetation));

            output.Albedo = finalTexture.rgb;
            output.Metallic = _Metallic;
            output.Smoothness = _Glossiness;
            output.Alpha = 1;
        }
        ENDCG
    }
    FallBack "Diffuse"
}