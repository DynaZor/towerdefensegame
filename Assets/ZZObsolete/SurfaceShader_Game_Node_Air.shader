﻿Shader "#DynaZor/SurfaceShader_Game_Node_Air"
{
	Properties
	{
		_TransparentBase("Transparent Base (Black, 0 Alpha)", Color) = (0,0,0,0)
		_Texture("Texture", 2DArray) = "" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0

		_Value_Pollution("Pollution (0-1)", Range(0,1)) = 0.0
	}
		SubShader
		{
			Tags { "RenderType" = "Transparent" }
			LOD 200

			CGPROGRAM
			#pragma surface surf Standard alpha 
			#pragma target 3.0

			UNITY_DECLARE_TEX2DARRAY(_Texture);
			float4 _TransparentBase;

			struct Input
			{
				float2 uv_Texture;
			};

			half _Glossiness;
			half _Metallic;

			UNITY_INSTANCING_BUFFER_START(Props)
				UNITY_DEFINE_INSTANCED_PROP(half, _Value_Pollution)
			UNITY_INSTANCING_BUFFER_END(Props)

			void surf(Input IN, inout SurfaceOutputStandard output)
			{
				//fixed4 pollution = UNITY_SAMPLE_TEX2DARRAY(_Texture, float3(IN.uv_Texture, 0));

				fixed4 finalTexture = UNITY_SAMPLE_TEX2DARRAY(_Texture, float3(IN.uv_Texture, 0));
				//finalTexture.rgb = pollution.rgb;
				finalTexture.a = lerp(0, finalTexture.a, UNITY_ACCESS_INSTANCED_PROP(Props, _Value_Pollution));

				output.Albedo = finalTexture.rgb;
				output.Metallic = _Metallic;
				output.Smoothness = _Glossiness;
				output.Alpha = finalTexture.a;
			}
			ENDCG
		}
			FallBack "Diffuse"
}