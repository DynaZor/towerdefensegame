﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
Shader "#DynaZor/SurfaceShader_Game_Node_Combined"
{
	Properties
	{
		_Color_Selection("Selection Color (Grid)", Color) = (0,0,0,0)
		_Textures("Textures", 2DArray) = "" {}

		_Value_GroundWater("Ground Water (0-1)", Range(0,1)) = 0.0
		_Value_Fertility("Fertility (0-1)", Range(0,1)) = 0.0
		_Value_Vegetation("Vegetation (0-1)", Range(0,1)) = 0.0
		//_Value_Pollution("Pollution (0-1)", Range(0,1)) = 0.0
		_Value_Selection("Selection Alpha (0-1)", Range(0,1)) = 0.0
	}
		SubShader
		{
			Tags { "RenderType" = "Opaque" }
			LOD 200

			CGPROGRAM
			#pragma surface surf Standard
			#pragma target 3.0
			#pragma enable_d3d11_debug_symbols

			UNITY_DECLARE_TEX2DARRAY(_Textures);

			struct Input
			{
				float2 uv_Textures;
			};

			UNITY_INSTANCING_BUFFER_START(Props)
				UNITY_DEFINE_INSTANCED_PROP(fixed4, _Color_Selection)
				UNITY_DEFINE_INSTANCED_PROP(half, _Value_Fertility)
				UNITY_DEFINE_INSTANCED_PROP(half, _Value_GroundWater)
				UNITY_DEFINE_INSTANCED_PROP(half, _Value_Vegetation)
				//UNITY_DEFINE_INSTANCED_PROP(half, _Value_Pollution)
				UNITY_DEFINE_INSTANCED_PROP(half, _Value_Selection)
			UNITY_INSTANCING_BUFFER_END(Props)

			void surf(Input IN, inout SurfaceOutputStandard output)
			{
				fixed4 water = UNITY_SAMPLE_TEX2DARRAY(_Textures, float3(IN.uv_Textures, 1));
				water.a = water.a * UNITY_ACCESS_INSTANCED_PROP(Props, _Value_GroundWater);

				fixed4 fertility = UNITY_SAMPLE_TEX2DARRAY(_Textures, float3(IN.uv_Textures, 2));
				fertility.a = fertility.a * UNITY_ACCESS_INSTANCED_PROP(Props, _Value_Fertility);

				fixed4 vegetation = UNITY_SAMPLE_TEX2DARRAY(_Textures, float3(IN.uv_Textures, 3));
				vegetation.a = vegetation.a * UNITY_ACCESS_INSTANCED_PROP(Props, _Value_Vegetation);

				//fixed4 pollution = UNITY_SAMPLE_TEX2DARRAY(_Textures, float3(IN.uv_Textures, 4));
				//pollution.a = pollution.a * UNITY_ACCESS_INSTANCED_PROP(Props, _Value_Pollution);

				fixed4 item = UNITY_SAMPLE_TEX2DARRAY(_Textures, float3(IN.uv_Textures, 5));
				fixed3 select = UNITY_ACCESS_INSTANCED_PROP(Props, _Color_Selection).rgb;

				fixed3 finalTexture = (water.rgb * water.a + UNITY_SAMPLE_TEX2DARRAY(_Textures, float3(IN.uv_Textures, 0)).rgb * (1 - water.a));
				finalTexture = (fertility.rgb * fertility.a + finalTexture * (1 - fertility.a));
				finalTexture = (vegetation.rgb * vegetation.a + finalTexture * (1 - vegetation.a));

				finalTexture = (item.rgb * item.a + finalTexture * (1 - item.a));

				//finalTexture = (pollution.rgb * pollution.a + finalTexture * (1 - pollution.a));

				finalTexture = (select.rgb * _Value_Selection + finalTexture * (1 - _Value_Selection));

				output.Albedo = finalTexture.rgb;
				output.Metallic = 0;
				output.Smoothness = 0;
				output.Alpha = 1;
			}
			ENDCG
		}
			FallBack "Diffuse"
}