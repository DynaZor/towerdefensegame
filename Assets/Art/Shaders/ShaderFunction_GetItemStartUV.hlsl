﻿//UNITY_SHADER_NO_UPGRADE
void ShaderFunction_GetItemUV_float (bool TheresAnItem,
SamplerState ss,
float2 currentUV, int4 dispIntFour, 
float4 _IntDataPack2, float4 dataMap2, 
Texture2D _Ally_Info, Texture2D _ItemTextures_AllyBases, Texture2D _ItemTextures_AllyBuildings,
out float4 col,
out float2 UVStart,
out float2 UVEnd)
{
	col = float4(0, 0, 0, 0);
	if (TheresAnItem)
	{
		float4 dispFixedFour = float4(0, 0, 0, 0);
		float2 dispFixedTwo = float2(0, 0);
		float4 dispFixedFour2 = float4(0, 0, 0, 0);
		float4 dispFixedFour3 = float4(0, 0, 0, 0);
		UVStart = float2(0, 0);
		UVEnd = float2(0, 0);
	// Raise UNROLL # if more than current # textures. 
	// * If more than 100 textures: 
	// Separate to another texture, continue count but subtract the index by 100 if index > 100
	[unroll(50)]
		while (dispIntFour.a < dispIntFour.b) // Add each Item's Tiers count to skip it
		{
			dispFixedFour.r = (float) dispIntFour.a / (float) _IntDataPack2.a; //  0.1 / (float)_IntDataPack2.a; // x
						//dispFixedFour.r = 1.0 - dispFixedFour.r;
			dispFixedFour.g = dispIntFour.g + 0.1; // *1.0 - 0.1; // y
			dispFixedFour.g = dispFixedFour.g / 10.0;
			dispFixedTwo.x = dispFixedFour.x;
			dispFixedTwo.y = dispFixedFour.y;
			dispFixedFour3 = float4(_Ally_Info.SampleLevel(ss, dispFixedTwo, 0));
			dispFixedFour2.r = dispFixedFour3.r * 255.0 - 1.0;
			dispFixedFour.b = dispFixedFour2.r;
			dispIntFour.r = dispIntFour.r + (int) dispFixedFour.b; // Add Sampled to Texture Index
			dispIntFour.a = dispIntFour.a + 1.0; // i++
		}
	
		UVStart.y = (float) dispIntFour.x; // Item Start = texture index
		UVEnd.y = (float) dispIntFour.x + 1.0; // Item End = texture index +1

		if ((int) dispIntFour.g == 9)
		{ // (Cat) Base
			UVStart.x = UVStart.x / (float) _IntDataPack2.r; // Item Start to UV
			UVEnd.x = UVEnd.x / (float) _IntDataPack2.r; // Item End to UV
		}
		else
		{ // Building
			UVStart.x = UVStart.x / (float) _IntDataPack2.g; // Item Start to UV
			UVEnd.x = UVEnd.x / (float) _IntDataPack2.g; // Item End to UV
		}

		dispIntFour.r = dataMap2.r; // Status
		dispFixedFour.b = (float) dispIntFour.r; // Status Start
		dispFixedFour.a = (float) dispIntFour.r + 1; // Status End
		UVStart.y = dispFixedFour.b / (float) _IntDataPack2.b; // Status Start To UV
		UVEnd.y = dispFixedFour.a / (float) _IntDataPack2.b; // Status End to UV

		float2 itemUV = float2(lerp(UVStart.x, UVEnd.x, 1.0 - currentUV.x),
					lerp(1.0 - UVStart.y, 1.0 - UVEnd.y, 1.0 - currentUV.y)); // Calculate Item Texture UV using Node UV

		// Get Item Texture
		if (dispIntFour.g == 9)
		{ // (Cat) Base
			col = float4(_ItemTextures_AllyBases.SampleLevel(ss, itemUV, 0)); //UNITY_SAMPLE_TEX2D_SAMPLER(_ItemTextures_AllyBases, //_DataTex, currentUV);

		}
		else
		{ // Building
			col = float4(_ItemTextures_AllyBuildings.SampleLevel(ss, itemUV, 0));
		}
	}
}