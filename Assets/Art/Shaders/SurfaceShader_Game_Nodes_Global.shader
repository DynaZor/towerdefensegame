﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
Shader "#DynaZor/SurfaceShader_Game_Nodes_Global"
{
	Properties
	{
	_IntDataPack1("_IntDataPack1", Vector) = (0,0,0,0) 
		// rg: GridSize.XY, b: _NodeVarBase, a: None

	_IntDataPack2("_IntDataPack2", Vector) = (0,0,0,0)
		// r: _ItemTextures_AllyBases_NumTotalVars, g: _ItemTextures_AllyBuildings_NumTotalVars,
		// b: _Ally_Info_NumStatuses, a: _Ally_Info_NumItems

	//_GridSizeX("GridSizeX", Range(0,1000)) = 30
	//_GridSizeY("GridSizeY", Range(0,1000)) = 30

	//_NodeVarBase("NodeVariation Multiplier", int) = 10
	//_Ally_Info_NumItems("_Ally_Info_NumItems", int) = 1


	//_ItemTextures_AllyBases_NumTotalVars("_ItemTextures_AllyBases_NumTotalVars", int) = 1
	//_ItemTextures_AllyBuildings_NumTotalVars("_ItemTextures_AllyBuildings_NumTotalVars", int) = 1
	//_Ally_Info_NumStatuses("_Ally_Info_NumStatuses", int) = 1

	[NoScaleOffset]
	_NodeVarTex("NodeVariation (RGBA)", 2D) = "black" {}

	[NoScaleOffset]
	_DataTex("Data (RGBA)", 2D) = "black" {}

	[NoScaleOffset]
	_DataTex2("Data2 (RGBA)", 2D) = "black" {}

	[NoScaleOffset]
	_DataTex_PerFrame("Data Per Frame (RGBA)", 2D) = "black" {}

	[NoScaleOffset]
	_SelectionColors("_SelectionColors", 2D) = "black" {}

	[NoScaleOffset]
	_Ally_Info("_Ally_Info (R)", 2D) = "black" {}

	[NoScaleOffset]
	_ItemTextures_AllyBases("_ItemTextures_AllyBases", 2D) = "black" {}

	[NoScaleOffset]
	_ItemTextures_AllyBuildings("_ItemTextures_AllyBuildings", 2D) = "black" {}

	[NoScaleOffset]
	_NodeTextures_DryGround("_NodeTextures_DryGround", 2DArray) = "" {}

	[NoScaleOffset]
	_NodeTextures_Fertility("_NodeTextures_Fertility", 2DArray) = "" {}

	[NoScaleOffset]
	_NodeTextures_GroundWater("_NodeTextures_GroundWater", 2DArray) = "" {}

	[NoScaleOffset]
	_NodeTextures_Vegetation("_NodeTextures_Vegetation", 2DArray) = "" {}

	[NoScaleOffset]
	_NodeTextures_Fog("_NodeTextures_Fog", 2DArray) = "" {}
	}
		SubShader
	{
	LOD 200

	CGPROGRAM
	#pragma surface surf Standard 
	//!! Remove on final build
	#pragma enable_d3d11_debug_symbols

	// Use shader model 3.0 target, to get nicer looking lighting
	#pragma target 3.0

	UNITY_DECLARE_TEX2D(_DataTex);
	// Receiving fixed: 
	// R: GroundFertility 0-1f
	// G: GroundWater 0-1f
	// B: Vegetation 0-1f
	// A: Visible 0/1

	UNITY_DECLARE_TEX2D_NOSAMPLER(_NodeVarTex);
	//sampler2D _NodeVarTex;
	// Receiving: 
	// R: GroundDry \ FoW: int / _NodeVarBase
	// G: GroundFertility: int / _NodeVarBase
	// B: GroundWater: int / _NodeVarBase
	// A: Vegetation: int / _NodeVarBase

	UNITY_DECLARE_TEX2D_NOSAMPLER(_DataTex2);
	// Receiving bytes (0-255): 
	// R: Item Status
	// G: Item Tier
	// B: Item Index
	// A: Item Category

	UNITY_DECLARE_TEX2D_NOSAMPLER(_DataTex_PerFrame);
	// Receiving: 
	// R: Node Selection Type:
	//	0 - Not Selected
	//	10 - Game_Grid_Selection_Normal_Free
	//	20 - Game_Grid_Selection_Normal_Ally
	//	30 - Game_Grid_Selection_Normal_AllyMovable
	//	40 - Game_Grid_Selection_Normal_Enemy
	//	50 - Game_Grid_Selection_Action_Free
	//	60 - Game_Grid_Selection_Action_Blocked
	//  70 - Not Selected
	// G: Selection Power 0-255
	// B: Visibility
	// A: none

	UNITY_DECLARE_TEX2D_NOSAMPLER(_SelectionColors);
	// Receiving Colors for _DataTex_PerFrame Selection 
	// (x = _DataTex_PerFrame.r, y = 0)

	UNITY_DECLARE_TEX2D_NOSAMPLER(_Ally_Info);
	// y 0 - #tiers for each building (Cat 0)
	// y 1 - #tiers for each base (Cat 2)

	//sampler2D _ItemTextures_AllyBuildings;	// Cat 0
	//sampler2D _ItemTextures_AllyBases;		// Cat 2

	UNITY_DECLARE_TEX2DARRAY(_NodeTextures_DryGround);		// R
	UNITY_DECLARE_TEX2DARRAY(_NodeTextures_Fertility);		// G
	UNITY_DECLARE_TEX2DARRAY(_NodeTextures_GroundWater);	// B
	UNITY_DECLARE_TEX2DARRAY(_NodeTextures_Vegetation);		// A

	UNITY_DECLARE_TEX2DARRAY(_NodeTextures_Fog);			// R

	UNITY_DECLARE_TEX2D_NOSAMPLER(_ItemTextures_AllyBuildings);
	UNITY_DECLARE_TEX2D_NOSAMPLER(_ItemTextures_AllyBases);
	// 0 - _ItemTextures_AllyBuildings
	// 1 - _ItemTextures_AllyBases

	struct Input
	{
		fixed2 uv_DataTex;
		fixed3 worldPos;
	};


	int4 _IntDataPack1; 
		// rg: GridSize, b: _NodeVarBase, a: None

	int4 _IntDataPack2; 
		// r: _ItemTextures_AllyBases_NumTotalVars, g: _ItemTextures_AllyBuildings_NumTotalVars,
		// b: _Ally_Info_NumStatuses, a: _Ally_Info_NumItems

	void surf(Input IN, inout SurfaceOutputStandard o)
	{
		// Pre-Allocation
		int4 dispIntFour = int4(12, 13, 14, 15);
		fixed4 dispFixedFour = fixed4(9.1, 9.11, 9.111, 9.1111);
		fixed4 dispFixedFour2 = fixed4(8.1, 8.11, 8.111, 8.1111);
		fixed4 dispFixedFour3 = fixed4(7.1, 7.11, 7.111, 7.1111);
		fixed2 dispFixedTwo = fixed2(6.1, 6.11);
		fixed4 col = fixed4(123, 132, 321, 312);
		fixed3 finalColor = fixed3 (0.123, 0.1234, 0.12345);
		// UV Helpers
		//fixed currentX = IN.worldPos.x + (fixed)_GridSizeX * 0.5;
		//fixed currentY = IN.worldPos.y + (fixed)_GridSizeY * 0.5;
		fixed2 _current = fixed2(
			IN.worldPos.x + (fixed)_IntDataPack1.r * 0.5,
			IN.worldPos.y + (fixed)_IntDataPack1.g * 0.5);

		_current = fixed2(
			clamp(_current.x / (fixed)_IntDataPack1.r, 0.0, 1.0),
			clamp(_current.y / (fixed)_IntDataPack1.g, 0.0, 1.0)); // Map UV

		fixed2 currentUV = fixed2(
				IN.uv_DataTex.x * (fixed)_IntDataPack1.r,
				IN.uv_DataTex.y * (fixed)_IntDataPack1.g); // Node UV

		if (currentUV.x > 1.0)
			currentUV.x = currentUV.x % 1.0;
		if (currentUV.y > 1.0)
			currentUV.y = currentUV.y % 1.0;

		// PF Map - Determines Visibility
		dispFixedFour.rgba = UNITY_SAMPLE_TEX2D_SAMPLER(_DataTex_PerFrame, _DataTex, _current).rgba;
		dispFixedFour.rgba = dispFixedFour.rgba * fixed4(255.0, 255.0, 255.0, 255.0);
		int4 dataMap_pf = (int4)dispFixedFour;
		dataMap_pf  = dataMap_pf - int4(1, 1, 1, 1);

		fixed4 varMap = UNITY_SAMPLE_TEX2D_SAMPLER(_NodeVarTex, _DataTex, _current); // Variation Map
		int var = varMap.r * _IntDataPack1.b; // Also used for Fog of War
		// Visibility
		if (dataMap_pf.b > 0)
		{
			dispFixedFour = UNITY_SAMPLE_TEX2D(_DataTex, _current); // Data Map 1 - Resources
			
			// Resources
			col = // Dry
				UNITY_SAMPLE_TEX2DARRAY(_NodeTextures_DryGround, fixed3(currentUV, var));
			finalColor = col;

			if (dispFixedFour.r > 0) { // Fertility
				var = varMap.g * _IntDataPack1.b;
				col =
					UNITY_SAMPLE_TEX2DARRAY(_NodeTextures_Fertility, fixed3(currentUV, var));
				col.a = col.a * dispFixedFour.r;
				finalColor = col.rgb * col.a + finalColor * (1 - col.a);
			}

			if (dispFixedFour.g > 0) { // Water
				var = varMap.b * _IntDataPack1.b;
				col =
					UNITY_SAMPLE_TEX2DARRAY(_NodeTextures_GroundWater, fixed3(currentUV, var));
				col.a = col.a * dispFixedFour.g;
				finalColor = col.rgb * col.a + finalColor * (1 - col.a);
			}

			if (dispFixedFour.b > 0) { // Vegetation
				var = varMap.a * _IntDataPack1.b;
				col =
					UNITY_SAMPLE_TEX2DARRAY(_NodeTextures_Vegetation, fixed3(currentUV, var));
				col.a = col.a * dispFixedFour.b;
				finalColor = col.rgb * col.a + finalColor * (1 - col.a);
			}

			// Data Map 2 Preparation
			dispFixedFour = UNITY_SAMPLE_TEX2D_SAMPLER(_DataTex2, _DataTex, _current);
			dispFixedFour = dispFixedFour * fixed4(255.0, 255.0, 255.0, 255.0);
			int4 dataMap2 = (int4)dispFixedFour;
			dispIntFour = int4(0, 0, 0, 0);

			// Selection
			if (dataMap_pf.g > 0) // Selection Power
			{
				dispFixedFour.g = dataMap_pf.r;  // Get Selection Type int
				dispFixedFour.g = dispFixedFour.g * 0.0125; // Selection Type to UV X => X / 8 / 10
				dispFixedFour.g = dispFixedFour.g + 0.0625; // Selection Type UV Prep X to pixel center
				col.rgb = UNITY_SAMPLE_TEX2D_SAMPLER(_SelectionColors, _DataTex, fixed2(dispFixedFour.g, 0.5)).rgb; // Get Selection Color
				col.a = dataMap_pf.g / 255.0; // Set Selection Alpha => Power 255 -> 1f
				finalColor = saturate(col.rgb * finalColor + finalColor * (1 - col.a)); // Apply Selection Color
			}

			// Ally Static Items
			if (!all(dataMap2 == dispIntFour)) // If NOT (All == 0) there's an item
			{
				dispIntFour = int4(1, 1, 1, 1);
				dataMap2 = dataMap2 - dispIntFour;
				dispIntFour = int4 (dataMap2.g, dataMap2.a, dataMap2.b, 0);
				// Texture Index Calculation

				// dispIntFour:
				// r: TextureIndex-Current (add Tier)
				// g: Category (-1)
				// b: (Start)Index
				// a: i (loop)

				// _IntDataPack2:
				// r: _ItemTextures_AllyBases_NumTotalVars, 
				// g: _ItemTextures_AllyBuildings_NumTotalVars,
				// b: _Ally_Info_NumStatuses
				// a: _Ally_Info_NumItems

				if (dispIntFour.g != -1) {
					[unroll(50)]	// Raise UNROLL # if more than current # textures. 
									// * If more than 100 textures: 
									// Separate to another texture, continue count but subtract the index by 100 if index > 100
					while (dispIntFour.a < dispIntFour.b) // Add each Item's Tiers count to skip it
					{
						dispFixedFour.r = (fixed)dispIntFour.a / (fixed)_IntDataPack2.a;//  0.1 / (fixed)_IntDataPack2.a; // x
						//dispFixedFour.r = 1.0 - dispFixedFour.r;
						dispFixedFour.g = dispIntFour.g + 0.1;// *1.0 - 0.1; // y
						dispFixedFour.g = dispFixedFour.g / 10.0;
						dispFixedTwo.r = dispFixedFour.r;
						dispFixedTwo.g = dispFixedFour.g;
						dispFixedFour3 = UNITY_SAMPLE_TEX2D_SAMPLER(_Ally_Info, _DataTex, dispFixedTwo);
						dispFixedFour2.r = dispFixedFour3.r * 255.0 - 1.0;
						dispFixedFour.b = dispFixedFour2.r;
						//dispFixedFour.b = dispFixedFour.b * 255.0;
						//dispFixedFour.b = UNITY_SAMPLE_TEX2D_SAMPLER(_Ally_Info, _DataTex, dispFixedFour.rg).r * 255.0 - 0.9; // Sampled
						dispIntFour.r = dispIntFour.r + (int)dispFixedFour.b; // Add Sampled to Texture Index
						dispIntFour.a = dispIntFour.a + 1; // i++
					}
				}
				dispFixedFour.r = (fixed)dispIntFour.r; // Item Start = texture index
				dispFixedFour.g = (fixed)dispIntFour.r + 1; // Item End = texture index +1

				if (dispIntFour.g == 9) { // (Cat) Base
					dispFixedFour.r = dispFixedFour.r / (fixed)_IntDataPack2.r; // Item Start to UV
					dispFixedFour.g = dispFixedFour.g / (fixed)_IntDataPack2.r; // Item End to UV
				}
				else { // Building
					dispFixedFour.r = dispFixedFour.r / (fixed)_IntDataPack2.g; // Item Start to UV
					dispFixedFour.g = dispFixedFour.g / (fixed)_IntDataPack2.g; // Item End to UV
				}

				dispIntFour.r = dataMap2.r; // Status
				dispFixedFour.b = (fixed) dispIntFour.r; // Status Start
				dispFixedFour.a = (fixed) dispIntFour.r + 1; // Status End
				dispFixedFour.b = dispFixedFour.b / (fixed) _IntDataPack2.b; // Status Start To UV
				dispFixedFour.a = dispFixedFour.a / (fixed) _IntDataPack2.b; // Status End to UV

				currentUV = fixed2(
					lerp(dispFixedFour.b, dispFixedFour.a, 1.0 - currentUV.x),
					lerp(1.0 - dispFixedFour.r, 1.0 - dispFixedFour.g, 1.0 - currentUV.y)); // Calculate Item Texture UV using Node UV

				// Get Item Texture
				if (dispIntFour.g == 9) { // (Cat) Base
					col = UNITY_SAMPLE_TEX2D_SAMPLER(_ItemTextures_AllyBases, _DataTex, currentUV);
				}
				else { // Building
					col = UNITY_SAMPLE_TEX2D_SAMPLER(_ItemTextures_AllyBuildings, _DataTex, currentUV);
				}

				// Selection
				if (dataMap_pf.g > 0) // Selection Power
				{
					dispFixedFour.g = dataMap_pf.r;  // Get Selection Type int
					dispFixedFour.g = dispFixedFour.g * 0.0125; // Selection Type to UV X => X / 8 / 10
					dispFixedFour.g = dispFixedFour.g + 0.0625; // Selection Type UV Prep X to pixel center
					dispFixedFour.rgb = UNITY_SAMPLE_TEX2D_SAMPLER(_SelectionColors, _DataTex, fixed2(dispFixedFour.g, 0.5)).rgb; // Get Selection Color
					dispFixedFour.a = saturate(dataMap_pf.g / 255.0 - (1 - 0.33 * (col.r + col.g + col.b))); // Set Selection Alpha => Power 255 -> 1f
					col.rgb = (dispFixedFour.rgb * dispFixedFour.a + col.rgb * (1 - dispFixedFour.a)); // Apply Selection Color
				}
				finalColor = col.rgb * col.a + finalColor * (1 - col.a); // Apply Item Texture
			}
		}
		else // If Invisible
		{
			col = UNITY_SAMPLE_TEX2DARRAY(_NodeTextures_Fog, fixed3(currentUV, var)); // Fog Color
			if (dataMap_pf.g > 0) // if selected\hovered
			{
				col.rgb = col.rgb * 0.8;
			}
			finalColor = col;
		}

		o.Smoothness = 0;
		o.Metallic = 0;
		o.Albedo = finalColor;
		}

		ENDCG
	}

	FallBack "Diffuse"
}