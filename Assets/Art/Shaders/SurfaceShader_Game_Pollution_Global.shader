﻿// Code written by Shahar DynaZor Alon
// Use of this code outside of this project without Shahar's permission is prohibited!
Shader "#DynaZor/SurfaceShader_Game_Pollution_Global"
{
    Properties
    {
        _DataTex ("Pollution Data (R)", 2D) = "black" {}
		_NoiseTex("Pollution Texture", 2D) = "black" {}
		_GridSizeX("GridSizeX", Range(0,1000)) = 30
		_GridSizeY("GridSizeY", Range(0,1000)) = 30
		_Radius("Alpha Blending Radius", Range(0,5)) = 1.5
		_MaximumAlpha("Maximum Alpha", Range(0,1)) = 0.75
    }
    SubShader
    {
        Tags { "Queue" = "Transparent" "RenderType" = "Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha // Alpha blend
        LOD 200

        CGPROGRAM
        #pragma surface surf Standard alpha:fade fullforwardshadows noambient 
        #pragma target 3.0

        sampler2D _DataTex;
        sampler2D _NoiseTex;
		
        struct Input
        {
            float2 uv_DataTex;
			float2 uv_NoiseTex;
			float3 worldPos;
        };

		int _GridSizeX;
		int _GridSizeY;
		fixed _Radius;
		fixed _MaximumAlpha;

		float2 NodeToUV(int X, int Y)
		{
			float2 output = float2((float)X / (float)_GridSizeX, (float)Y / (float)_GridSizeY);
			return output;
		}

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
			float centerX = (fixed)_GridSizeX * 0.5;
			float centerY = (fixed)_GridSizeY * 0.5;
			float currentX = centerX - IN.worldPos.x;
			float currentY = centerY - IN.worldPos.y;
			float _currentX = clamp((IN.worldPos.x + (fixed)_GridSizeX * 0.5) / (fixed)_GridSizeX, 0, 1);
			float _currentY = clamp((IN.worldPos.y + (fixed)_GridSizeY * 0.5) / (fixed)_GridSizeY, 0, 1);
			float currentGridExactX = (fixed)_GridSizeX * _currentX;
			float currentGridExactY = (fixed)_GridSizeY * _currentY;
			int currentGridX = floor(currentGridExactX);
			int currentGridY = floor(currentGridExactY);
			float currentGridXWorld = (fixed)currentGridX - centerX;
			float currentGridYWorld = (fixed)currentGridY - centerY;

			
			float strMap_CRNT = tex2D(_DataTex, NodeToUV(currentGridX, currentGridY)).r;

			float strMap_UP = tex2D(_DataTex, NodeToUV(currentGridX, currentGridY + 1)).r;
			float strMap_DWN = tex2D(_DataTex, NodeToUV(currentGridX, currentGridY - 1)).r;
			float strMap_RT = tex2D(_DataTex, NodeToUV(currentGridX + 1, currentGridY)).r;
			float strMap_LFT = tex2D(_DataTex, NodeToUV(currentGridX - 1, currentGridY)).r;

			fixed4 noiseMap = tex2D(_NoiseTex, fixed2(IN.uv_NoiseTex.x * _GridSizeX, IN.uv_NoiseTex.y * _GridSizeY));
			
			fixed calcStrength = 0;

			fixed radius = _Radius;

			fixed C_distance = distance(fixed2(currentGridExactX, currentGridExactY), fixed2(currentGridX + 0.5, currentGridY + 0.5));
			fixed C_power = (1 - saturate(C_distance * radius)) * strMap_CRNT;
			calcStrength += C_power;

			fixed U_distance = distance(fixed2(currentGridExactX, currentGridExactY), fixed2(currentGridX + 0.5, currentGridY + 1.5));
			fixed U_power = (1 - saturate(U_distance * radius)) * strMap_UP;
			calcStrength += U_power;

			fixed D_distance = distance(fixed2(currentGridExactX, currentGridExactY), fixed2(currentGridX + 0.5, currentGridY - 0.5));
			fixed D_power = (1 - saturate(D_distance * radius)) * strMap_DWN;
			calcStrength += D_power;

			fixed L_distance = distance(fixed2(currentGridExactX, currentGridExactY), fixed2(currentGridX - 0.5, currentGridY + 0.5));
			fixed L_power = (1 - saturate(L_distance * radius)) * strMap_LFT;
			calcStrength += L_power;

			fixed R_distance = distance(fixed2(currentGridExactX, currentGridExactY), fixed2(currentGridX + 1.5, currentGridY + 0.5));
			fixed R_power = (1 - saturate(R_distance * radius)) * strMap_RT;
			calcStrength += R_power;

			calcStrength = saturate(calcStrength);

			o.Alpha = calcStrength;
			o.Alpha *= noiseMap.a;
			o.Alpha = clamp(o.Alpha * _MaximumAlpha, 0, _MaximumAlpha);
			o.Albedo = noiseMap.rgb;

            o.Metallic = 0;
            o.Smoothness = 0;
        }
        ENDCG
    }
    FallBack "Diffuse"
}