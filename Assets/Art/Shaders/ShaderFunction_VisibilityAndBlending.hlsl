﻿//UNITY_SHADER_NO_UPGRADE
void ShaderFunction_VisibilityAndBlending_float(int4 dataMap_pf, float4 _DataTex_current, 
float4 _Fog_currentUV, float4 _Dry_currentUV, float4 _Fertility_currentUV, float4 _GroundWater_currentUV, float4 _Vegetation_currentUV,
float4 _Item_currentUV, bool ThereIsAnItem,
Texture2D _SelectionColors, SamplerState ss,
out float4 finalColor)
{
	float4 col = float4(0, 0, 0, 0);
	finalColor = float4(col);
	if (dataMap_pf.b > 0) // If Visible
	{
		float4 dispFixedFour = _DataTex_current; // Data Map 1 - Resources
			
		// Resources
		// Dry
		finalColor = _Dry_currentUV; 

		if (dispFixedFour.r > 0)
		{ // Fertility
			col = _Fertility_currentUV;
			col.a = col.a * dispFixedFour.r;
			finalColor = float4(col.rgb * col.a, 0) + finalColor.rgba * (1 - col.a);
		}

		if (dispFixedFour.g > 0)
		{ // Water
			col = _GroundWater_currentUV;
			col.a = col.a * dispFixedFour.g;
			finalColor = float4(col.rgb * col.a, 0) + finalColor.rgba * (1 - col.a);
		}

		if (dispFixedFour.b > 0)
		{ // Vegetation
			col = _Vegetation_currentUV;
			col.a = col.a * dispFixedFour.b;
			finalColor = float4(col.rgb * col.a, 0) + finalColor.rgba * (1 - col.a);
		}
		
		if (ThereIsAnItem)
		{
			col = _Item_currentUV;
			finalColor = float4(col.rgb * col.a, 0) + finalColor.rgba * (1 - col.a);
		}
		
		if (dataMap_pf.g > 0) // Selection Power
		{
			dispFixedFour.g = dataMap_pf.r; // Get Selection Type int
			dispFixedFour.g = dispFixedFour.g * 0.0125; // Selection Type to UV X => X / 8 / 10
			dispFixedFour.g = dispFixedFour.g + 0.0625; // Selection Type UV Prep X to pixel center
			dispFixedFour.rgb = _SelectionColors.SampleLevel(ss, float2(dispFixedFour.g, 0.5), 0).rgb;
			//UNITY_SAMPLE_TEX2D_SAMPLER(_SelectionColors, _DataTex, fixed2(dispFixedFour.g, 0.5)).rgb; // Get Selection Color
			dispFixedFour.a = saturate(dataMap_pf.g / 255.0 - (1 - 0.33 * (col.r + col.g + col.b))); // Set Selection Alpha => Power 255 -> 1f
			finalColor.rgb = (dispFixedFour.rgb * dispFixedFour.a + finalColor.rgb * (1 - dispFixedFour.a)); // Apply Selection Color
		}
	}
	else // If Invisible
	{
		col = _Fog_currentUV; // Fog Color
		if (dataMap_pf.g > 0) // if selected\hovered
		{
			col.rgb = col.rgb * 0.8;
		}
		finalColor = float4(col.rgb, 1);
	}
}