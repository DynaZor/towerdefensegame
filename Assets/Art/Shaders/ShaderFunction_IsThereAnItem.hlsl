//UNITY_SHADER_NO_UPGRADE
void IsThereAnItem_float(int4 DataTex2_current, out bool TheresAnItem)
{
	int4 zeros = int4(0, 0, 0, 0);
	TheresAnItem = !all(DataTex2_current == zeros);
}