﻿// Library made by Shahar DynaZor Alon, please give credit if used in a project.
using UnityEngine;
using UnityEditor;
using DynaZor.EditorUtilities.SemanticVersioning;
using System;

namespace DynaZor.EditorUtilities.EUScriptableObjects
{
	public static class AddonInformation
	{
		public static readonly string AddonName = "Editor Utilities (DynaZor Edition)";
		public static readonly string AddonShortName = "Editor Utilities";
		public static readonly string AddonVersion = "v0.00.005";
		public static readonly string AddonBy = "Shahar DynaZor (Original by Vitaliy Zasadnyy)";
	}

	[System.Serializable]
	[CreateAssetMenu(fileName = "Settings", menuName = "App Settings", order = 51)]
	public class AppSettings : ScriptableObject
	{
		#region AddonInformation
		[SerializeField] public string AddonName;
		[SerializeField] public string AddonVersion;
		[SerializeField] public string AddonBy;
		#endregion

		#region Static Fields
		public static AppSettings Current;
		#endregion

		#region enums
		public enum DevStage
		{
			Prototype, POC, Alpha, Beta, Final
		}

		public enum BuildNameType
		{
			Date, Version, State_Version, Version_Date, Date_Version
		}
		#endregion

		#region MainWindow
		[SerializeField] public string LastBuildName;
		[SerializeField] public DevStage LastState;

		[SerializeField] public BuildNameType currentBuildNameType;
		[SerializeField] public DevStage currentState;
		#endregion

		#region SettingsWindow
		[SerializeField]
		public string currentAppName;
		[SerializeField]
		public string currentCompName;
		[SerializeField]
		public string Copyright = "2019";
		[SerializeField]
		public string currentEXEName;
		[SerializeField]
		public int MinMajorDigits = 1;
		[SerializeField]
		public int MinMinorDigits = 2;
		[SerializeField]
		public int MinPatchDigits = 3;
		[SerializeField]
		public bool currentSingleInstance;
		[SerializeField] public string BuildsPath;
		#endregion

		#region Versions
		[SerializeField] public VersionNumber currentAppVer => new VersionNumber(Major, Minor, Patch);
		[SerializeField]
		public int Major = 1;
		[SerializeField]
		public int Minor = 1;
		[SerializeField]
		public int Patch = 1;

		[SerializeField] public VersionNumber LastBuildAppVer => new VersionNumber(LastMajor, LastMinor, LastPatch);
		[SerializeField]
		public int LastMajor = 1;
		[SerializeField]
		public int LastMinor = 1;
		[SerializeField]
		public int LastPatch = 1;

		public static string GenerateVersionString (AppSettings a, bool last)
		{
			if (last)
				return a.LastBuildAppVer.String(a.MinMajorDigits, a.MinMinorDigits, a.MinPatchDigits, flatten: false);
			else
				return a.currentAppVer.String(a.MinMajorDigits, a.MinMinorDigits, a.MinPatchDigits, flatten: false);
		}

		public void SetLastVersion (VersionNumber version)
		{
			LastMajor = version.Major;
			LastMinor = version.Minor;
			LastPatch = version.Patch;
		}
		#endregion

		#region Events
		public void Validate ()
		{
#if UNITY_EDITOR
			VersionNumber ver = new VersionNumber(Major, Minor, Patch);
			string versionStr = ver.String(MinMajorDigits, MinMinorDigits, MinPatchDigits, false);
			int versionInt = ver.Int(MinMajorDigits, MinMinorDigits, MinPatchDigits);
			PlayerSettings.bundleVersion = versionStr;
			PlayerSettings.iOS.buildNumber = versionStr;
			PlayerSettings.macOS.buildNumber = versionStr;
			PlayerSettings.Android.bundleVersionCode = versionInt;
			PlayerSettings.Lumin.versionCode = versionInt;
			PlayerSettings.PS4.appVersion = versionStr;
			PlayerSettings.Switch.displayVersion = versionStr;
			PlayerSettings.Switch.releaseVersion = versionStr;
			PlayerSettings.tvOS.buildNumber = versionStr;
			PlayerSettings.XboxOne.Version = versionStr;

			PlayerSettings.forceSingleInstance = currentSingleInstance;
			PlayerSettings.productName = currentAppName;
			PlayerSettings.companyName = currentCompName;

			UpdateAddonInfo();
#endif
		}

		public void UpdateAddonInfo ()
		{
			AddonName = AddonInformation.AddonName;
			AddonBy = AddonInformation.AddonBy;
			AddonVersion = AddonInformation.AddonVersion;
		}

#if UNITY_EDITOR
		public bool ProjectInformationMatches ()
		{
			bool correct = true;
			if (PlayerSettings.forceSingleInstance != currentSingleInstance)
			{
				//Debug.Log("AppSettings - currentSingleInstance and AppSettings' forceSingleInstance are not matching, please update");
				correct = false;
			}
			if (PlayerSettings.productName != currentAppName)
			{
				//Debug.Log("AppSettings - currentAppName and AppSettings' productName are not matching, please update");
				correct = false;
			}
			if (PlayerSettings.companyName != currentCompName)
			{
				//Debug.Log("AppSettings - currentCompName and AppSettings' companyName are not matching, please update");
				correct = false;
			}

			return correct;
		}
#endif
		#endregion
	}

	#region Editor GUI
#if UNITY_EDITOR
	[CustomEditor(typeof(AppSettings))]
	[CanEditMultipleObjects]
	public class AppSettingsEditor : Editor
	{
		SerializedProperty currentBuildNameType;
		SerializedProperty currentState;
		SerializedProperty currentAppName;
		SerializedProperty currentCompName;
		SerializedProperty Copyright;
		SerializedProperty currentSingleInstance;
		SerializedProperty currentEXEName;
		SerializedProperty BuildsPath;
		SerializedProperty MinMajorDigits;
		SerializedProperty MinMinorDigits;
		SerializedProperty MinPatchDigits;
		SerializedProperty Major;
		SerializedProperty Minor;
		SerializedProperty Patch;
		SerializedProperty LastBuildName;
		SerializedProperty LastState;
		SerializedProperty LastMajor;
		SerializedProperty LastMinor;
		SerializedProperty LastPatch;

		void OnEnable ()
		{
			currentBuildNameType = serializedObject.FindProperty("currentBuildNameType");
			currentState = serializedObject.FindProperty("currentState");
			currentAppName = serializedObject.FindProperty("currentAppName");
			currentCompName = serializedObject.FindProperty("currentCompName");
			Copyright = serializedObject.FindProperty("Copyright");
			currentSingleInstance = serializedObject.FindProperty("currentSingleInstance");
			currentEXEName = serializedObject.FindProperty("currentEXEName");
			BuildsPath = serializedObject.FindProperty("BuildsPath");
			MinMajorDigits = serializedObject.FindProperty("MinMajorDigits");
			MinMinorDigits = serializedObject.FindProperty("MinMinorDigits");
			MinPatchDigits = serializedObject.FindProperty("MinPatchDigits");
			Major = serializedObject.FindProperty("Major");
			Minor = serializedObject.FindProperty("Minor");
			Patch = serializedObject.FindProperty("Patch");
			LastMajor = serializedObject.FindProperty("LastMajor");
			LastMinor = serializedObject.FindProperty("LastMinor");
			LastPatch = serializedObject.FindProperty("LastPatch");
			LastState = serializedObject.FindProperty("LastState");
			LastBuildName = serializedObject.FindProperty("LastBuildName");
		}

		public override void OnInspectorGUI ()
		{
			serializedObject.Update();

			EditorGUILayout.LabelField("Project Information", EditorStyles.boldLabel);
			EditorGUILayout.PropertyField(currentAppName);
			EditorGUILayout.PropertyField(currentCompName);
			EditorGUILayout.PropertyField(Copyright);

			EditorGUILayout.LabelField("Build Settings", EditorStyles.boldLabel);
			EditorGUILayout.PropertyField(currentSingleInstance);
			EditorGUILayout.PropertyField(currentEXEName);
			EditorGUILayout.PropertyField(BuildsPath);
			EditorGUILayout.PropertyField(currentBuildNameType);

			EditorGUILayout.LabelField("Versioning Settings", EditorStyles.boldLabel);
			EditorGUILayout.LabelField("Minimum Digits");
			GUILayout.BeginHorizontal();
			EditorGUILayout.PropertyField(MinMajorDigits, GUIContent.none, GUILayout.Width(50f));
			EditorGUILayout.LabelField(".", GUILayout.Width(5f));
			EditorGUILayout.PropertyField(MinMinorDigits, GUIContent.none, GUILayout.Width(50f));
			EditorGUILayout.LabelField(".", GUILayout.Width(5f));
			EditorGUILayout.PropertyField(MinPatchDigits, GUIContent.none, GUILayout.Width(50f));
			GUILayout.EndHorizontal();

			EditorGUILayout.LabelField("Current Version", EditorStyles.boldLabel);
			GUILayout.BeginHorizontal();
			EditorGUILayout.PropertyField(currentState, new GUIContent(""), GUILayout.Width(70f));
			EditorGUILayout.PropertyField(Major, GUIContent.none, GUILayout.Width(50f));
			EditorGUILayout.LabelField(".", GUILayout.Width(5f));
			EditorGUILayout.PropertyField(Minor, GUIContent.none, GUILayout.Width(50f));
			EditorGUILayout.LabelField(".", GUILayout.Width(5f));
			EditorGUILayout.PropertyField(Patch, GUIContent.none, GUILayout.Width(50f));
			GUILayout.EndHorizontal();

			EditorGUILayout.LabelField("Last Build Information", EditorStyles.boldLabel);
			EditorGUILayout.PropertyField(LastBuildName);
			GUILayout.BeginHorizontal();
			EditorGUILayout.PropertyField(LastState, new GUIContent(""), GUILayout.Width(70f));
			EditorGUILayout.PropertyField(LastMajor, GUIContent.none, GUILayout.Width(50f));
			EditorGUILayout.LabelField(".", GUILayout.Width(5f));
			EditorGUILayout.PropertyField(LastMinor, GUIContent.none, GUILayout.Width(50f));
			EditorGUILayout.LabelField(".", GUILayout.Width(5f));
			EditorGUILayout.PropertyField(LastPatch, GUIContent.none, GUILayout.Width(50f));
			GUILayout.EndHorizontal();

			serializedObject.ApplyModifiedProperties();
		}
	}

	[ExecuteInEditMode]
	[CustomPropertyDrawer(typeof(AppSettings))]
	public class AppSettingsDrawerUIE : PropertyDrawer
	{
		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
		{
			// Using BeginProperty / EndProperty on the parent property means that
			// prefab override logic works on the entire property.
			EditorGUI.BeginProperty(position, label, property);
			position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

			// Don't make child fields be indented
			var indent = EditorGUI.indentLevel;
			EditorGUI.indentLevel = 0;

			var normalRect = new Rect(position.x, position.y, position.width, position.height);
			var applyButton  = new Rect(position.x, position.y + 20f, position.width * 0.5f, position.height);
			var autoFindButton  = new Rect(position.x + (position.width * 0.5f), position.y + 20f, position.width * 0.5f, position.height);

			EditorGUI.PropertyField(normalRect, property, GUIContent.none);

			// Right Click Menu
			AppSettingsGUI.SORightClickMenu(property, normalRect);

			// Set indent back to what it was
			EditorGUI.indentLevel = indent;
			EditorGUI.EndProperty();
		}
	}

	[ExecuteInEditMode]
	public static class AppSettingsGUI
	{
		public static void SORightClickMenu (SerializedProperty property, Rect rect)
		{
			Event current = Event.current;
			if (rect.Contains(current.mousePosition) && current.type == EventType.ContextClick)
			{
				GenericMenu menu = new GenericMenu();
				AppSettings val = (AppSettings)property.objectReferenceValue;
				if (val != null)
				{
					if (!val.ProjectInformationMatches())
					{
						menu.AddItem(new GUIContent("Apply Information to Project", "Applies the information from this AppSettings asset to the project."), false, val.Validate);
					}
				}
				bool sameAsCurrent = property.objectReferenceValue == AppSettings.Current;
				menu.AddItem(new GUIContent("Get from Editor Utilites", "References the AppSettings asset used in Editor Utilities."), sameAsCurrent, SetProperty);
				
				void SetProperty ()
				{
					property.objectReferenceValue = AppSettings.Current;
					property.serializedObject.ApplyModifiedProperties();
					property.serializedObject.Update();
				}

				menu.ShowAsContext();

				current.Use();
			}
		}

		public static void AutoRightClickMenu<T> (T obj, string propertyName, EventType current, bool FromSettings = false)
		{
			if (current == EventType.ContextClick)
			{
				Type type = obj.GetType();
				var field = type.GetField(propertyName);
				if (field == null)
				{
					return;
				}
				GenericMenu menu = new GenericMenu();
				AppSettings val = (AppSettings)field.GetValue(obj);
				if (val != null)
				{
					if (!val.ProjectInformationMatches())
					{
						menu.AddItem(new GUIContent("Apply Information to Project", "Applies the information from this AppSettings asset to the project."), false, val.Validate);
					}
				}
				bool sameAsCurrent = val == AppSettings.Current;

				if (!FromSettings)
				{
					menu.AddItem(new GUIContent("Get from Editor Utilites", "References the AppSettings asset used in Editor Utilities."), sameAsCurrent, SetValue);

					void SetValue ()
					{
						field.SetValue(obj, AppSettings.Current);
					}
				}

				menu.ShowAsContext();
			}
		}
	}
#endif
	#endregion
}