﻿// Custom version by Shahar DynaZor Alon, please give credit if used in a project.
#region Original Credits
/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2015 Vitaliy Zasadnyy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
#endregion
#region Using
using System;
using System.IO;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEditor.SceneManagement;
using UnityEditor.Build.Reporting;
using UnityEngine.SceneManagement;
using UnityEditor.AnimatedValues;
using DynaZor.EditorUtilities.EUScriptableObjects;
using DynaZor.EditorUtilities.EUScriptableObjects.EUSettings;
#endregion
namespace DynaZor.EditorUtilities.EUWindows
{
	public class AddonInformation
	{
		public static readonly string AddonName = "Editor Utilities (DynaZor Edition)";
		public static readonly string AddonShortName = "Editor Utilities";
		public static readonly string AddonVersion = "v0.00.005";
		public static readonly string AddonBy = "Shahar DynaZor (Original by Vitaliy Zasadnyy)";
	}

    public class EditorUtilitiesWindow : EditorWindow
	{
		#region Variables
		private const string SETTINGS_FILE_NAME = "EditorUtilitiesSettings.asset";
        private const string SETTINGS_DIRECTORY = "Assets/Settings/Editor/";
		private const string APPSETTINGS_DIRECTORY = "Assets/Settings/";

		private EditorUtilitiesSettings _settings;
		private bool _isInSettingsMode;
        private string _currentState = "";
		private string BuildStatus = "";
		private float currentWidth = 0;
		private float maxWidth => currentWidth - 20f;

		#region Utilities UI States
		private Vector2 _scrollViewPosition_Utilities;
		private Vector2 _scrollViewPosition_SceneShortcuts_Launch;
		private Vector2 _scrollViewPosition_SceneShortcuts_Load;
		#endregion
		#region Settings UI States
		private Vector2 _scrollViewPosition_Settings;
		#region From _settings
		private bool _Settings_AddonInfo_Open
		{
			get
			{
				if (_settings != null)
					return _settings._Settings_AddonInfo_Open;
				return true;
			}
			set
			{
				_settings._Settings_AddonInfo_Open = value;
			}
		}
		private bool _Settings_AppSettings_Open
		{
			get
			{
				if (_settings != null)
					return _settings._Settings_AppSettings_Open;
				return true;
			}
			set
			{
				_settings._Settings_AppSettings_Open = value;
			}
		}
		private bool _Settings_QuickBuild_Open
		{
			get
			{
				if (_settings != null)
					return _settings._Settings_QuickBuild_Open;
				return true;
			}
			set
			{
				_settings._Settings_QuickBuild_Open = value;
			}
		}
		private bool _Settings_Preload_Open
		{
			get
			{
				if (_settings != null)
					return _settings._Settings_Preload_Open;
				return true;
			}
			set
			{
				_settings._Settings_Preload_Open = value;
			}
		}
		private bool _Settings_MoreInfo_Open
		{
			get
			{
				if (_settings != null)
					return _settings._Settings_MoreInfo_Open;
				return true;
			}
			set
			{
				_settings._Settings_MoreInfo_Open = value;
			}
		}
		#endregion
		private AnimBool _Settings_SceneShortcuts_Fold;
		private AnimBool _Settings_SceneShortcuts_Launch_Fold;
		private AnimBool _Settings_SceneShortcuts_Load_Fold;
		#endregion
		#region GUIStyles
		private GUIStyle Style_EditorStateLabel = new GUIStyle();
		private GUIStyle Style_CenterMe = new GUIStyle();
		private GUIStyle Style_SceneButton = new GUIStyle();
		private GUIStyle Style_TestBounds = new GUIStyle();
		private GUIStyle Style_MiniLabel_CenterMe
		{
			get
			{
				GUIStyle temp = new GUIStyle(EditorStyles.miniLabel);
				temp.alignment = TextAnchor.UpperCenter;
				temp.padding = new RectOffset(0, 0, 0, 0);
				return temp;
			}
		}
		private GUIStyle Style_FoldoutHeader (float subWidth = 0f)
		{
			GUIStyle temp = new GUIStyle(EditorStyles.foldoutHeader);
			temp.fixedWidth = maxWidth - subWidth;
			temp.stretchWidth = true;
			return temp;
		}
		private GUIStyle Style_Box
		{
			get
			{
				GUIStyle temp = new GUIStyle(GUI.skin.box);
				temp.overflow = new RectOffset();
				temp.margin = new RectOffset(1,0,0,0);
				temp.padding = new RectOffset(0,0,0,0);
				temp.contentOffset = Vector2.zero;
				return temp;
			}
		}
		private GUIStyle Style_MiniButton
		{
			get
			{
				GUIStyle temp = new GUIStyle(EditorStyles.miniButton);
				temp.overflow = new RectOffset();
				temp.margin = new RectOffset(0, 0, 0, 0);
				temp.padding = new RectOffset(0, 0, 0, 0);
				temp.contentOffset = Vector2.zero;
				return temp;
			}
		}
		private GUIStyle Style_NoPaddingNoMargins
		{
			get
			{
				GUIStyle temp = new GUIStyle();
				temp.overflow = new RectOffset();
				temp.margin = new RectOffset(0, 0, 0, 0);
				temp.padding = new RectOffset(0, 0, 0, 0);
				temp.contentOffset = Vector2.zero;
				return temp;
			}
		}
		private GUIStyle FoldoutHeader_def => Style_FoldoutHeader();
		#endregion
		#endregion
		#region Registeration
		[MenuItem("Window/Editor Utilities (DynaZor Edition)", priority = 1)]
        public static void ShowWindow()
        {
			EditorWindow.GetWindow<EditorUtilitiesWindow>(false, AddonInformation.AddonShortName, true);
		}
		#endregion
		#region Lifecycle
		private void OnEnable()
		{
			_settings = LoadSettings();
			ApplyWindowProperties();
			Style_CenterMe.alignment = TextAnchor.UpperCenter;
			Style_CenterMe.padding = new RectOffset(0, 0, 0, 0);
			Style_SceneButton.alignment = TextAnchor.MiddleLeft;
			Style_SceneButton.padding = new RectOffset(0, 0, 0, 0);
			Style_SceneButton.stretchWidth = true;
			Style_SceneButton.margin = new RectOffset(0, 0, 0, 0);
			Texture2D bluePixel = new Texture2D(1,1);
			bluePixel.SetPixel(0, 0, Color.blue);
			bluePixel.Apply();
			Style_TestBounds.normal.background = bluePixel;

			UnityEngine.Events.UnityAction repaintEvent = new UnityEngine.Events.UnityAction(base.Repaint);

			_Settings_SceneShortcuts_Fold = new AnimBool(true);
			_Settings_SceneShortcuts_Fold.valueChanged.AddListener(repaintEvent);

			_Settings_SceneShortcuts_Launch_Fold = new AnimBool(false);
			_Settings_SceneShortcuts_Launch_Fold.valueChanged.AddListener(repaintEvent);

			_Settings_SceneShortcuts_Load_Fold = new AnimBool(false);
			_Settings_SceneShortcuts_Load_Fold.valueChanged.AddListener(repaintEvent);

			ApplyCurrentAppSettings();

			EUEditorOnly.EditorHelpers.CheckVersionConsistency();
		}

		private void ApplyCurrentAppSettings ()
		{
			if (AppSettings.Current != _settings.AppSettings)
			{
				AppSettings.Current = _settings.AppSettings;
			}
		}

		private void OnGUI()
		{
			currentWidth = EditorGUIUtility.currentViewWidth;
			GetCurrentState();
			if (_settings == null)
			{
				_settings = LoadSettings();
				if (_settings == null)
				{
					Helpers.DebugLog("Setting are not loaded yet");
					return;
				}
			}
			ApplyWindowProperties();
			if (_settings.AppSettings != null)
			{
				_settings.AppSettings.ProjectInformationMatches();
			}
			DrawToolbar();
			DrawWindowContent();
			ApplyCurrentAppSettings();
		}

		private void Update()
        {
			string current = GetCurrentState();
			if (_currentState != current)
			{
				_currentState = current;
				Repaint();
			}
		}

		private void ApplyWindowProperties ()
		{
			this.minSize = new Vector2(_settings.Panel_Width_Min, _settings.Panel_Height_Min);
		}
		#endregion
		#region UI Draw
		#region Toolbars
		private void DrawToolbar()
        {
            if (_isInSettingsMode)
            {
                DrawSettingsToolbar();
            }
            else
            {
                DrawUtilitiesToolbar();
            }
		}

		private void DrawUtilitiesToolbar ()
		{
			GUILayout.BeginHorizontal(EditorStyles.toolbar);
			GUILayout.FlexibleSpace();
			if (GUILayout.Button(new GUIContent("Recent Projects Editor", "Remove unneeded projects with one click"), EditorStyles.toolbarButton))
			{
				EURecents.EditRecentProjectsWindow.ShowWindow();
			}

			if (GUILayout.Button("Settings", EditorStyles.toolbarButton))
			{
				_isInSettingsMode = true;
			}
			GUILayout.EndHorizontal();
		}

		private void DrawSettingsToolbar ()
		{
			GUILayout.BeginHorizontal(EditorStyles.toolbar);

			GUILayout.FlexibleSpace();

			var backUpColor = GUI.color;
			GUI.color = Color.green;
			if (GUILayout.Button("Done", EditorStyles.toolbarButton))
			{
				EditorUtility.SetDirty(_settings);
				AssetDatabase.SaveAssets();
				_isInSettingsMode = false;
			}
			GUI.color = backUpColor;

			GUILayout.EndHorizontal();
		}
		#endregion
		private void DrawWindowContent()
        {
            if (_isInSettingsMode)
            {
                DrawSettings();
            }
            else
            {
                DrawUtilities();
            }
        }
		#region Utilities
		private void DrawUtilities ()
		{
			using (var s = new EditorGUILayout.ScrollViewScope(_scrollViewPosition_Utilities, GUILayout.MaxWidth(currentWidth - 2f)))
			{
				_scrollViewPosition_Utilities = s.scrollPosition;
				bool appSettingsPresent = _settings.AppSettings != null;
				if (appSettingsPresent)
				{
					DrawInfo();
				}

				if (_settings.showQuickShortcuts | _settings.showEditorState)
				{
					using (new EditorGUILayout.HorizontalScope(GUILayout.Height(40f), GUILayout.Width(currentWidth - 20f)))
					{
						GUILayout.FlexibleSpace();
						if (_settings.showQuickShortcuts)
						{
							GUILayout.Space(2f);
							DrawQuickShortcuts();
							GUILayout.Space(2f);
						}
						if (_settings.showEditorState)
						{
							DrawEditorState();
						}
						GUILayout.FlexibleSpace();
					}
				}
				if (appSettingsPresent && _settings.showVersion)
				{
					DrawVersionInformation();
				}

				if (appSettingsPresent && _settings.showQuickBuild)
				{
					DrawQuickBuild();
				}

				if (_settings.showSceneShortcuts)
				{
					DrawSceneShortcuts();
				}
			}
		}

		private void DrawInfo ()
		{
			using (new EditorGUILayout.HorizontalScope(GUILayout.MaxWidth(currentWidth - 20f)))
			{
				GUILayout.Space(2.5f);
				GUILayout.FlexibleSpace();
				using (new EditorGUILayout.HorizontalScope(GUILayout.MinWidth(_settings.Info_Field_Width_Min * 2 + 20f), GUILayout.MaxWidth(_settings.Info_Field_Width_Max * 2 + 20f)))
				{
					using (var cc = new EditorGUI.ChangeCheckScope())
					{
						GUILayout.Space(2.5f);
						string newAppName = EditorGUILayout.TextField(_settings.AppSettings.currentAppName, GUILayout.ExpandWidth(true), GUILayout.MinWidth(_settings.Info_Field_Width_Min), GUILayout.MaxWidth(_settings.Info_Field_Width_Max));
						EditorGUILayout.LabelField("by", GUILayout.Width(17.5f));
						string newCompName = EditorGUILayout.TextField(_settings.AppSettings.currentCompName, GUILayout.ExpandWidth(true), GUILayout.MinWidth(_settings.Info_Field_Width_Min), GUILayout.MaxWidth(_settings.Info_Field_Width_Max));
						if (cc.changed)
						{
							_settings.AppSettings.currentAppName = newAppName;
							_settings.AppSettings.currentCompName = newCompName;
						}
					}
				}
				GUILayout.FlexibleSpace();
			}
			DrawAppSettingsWarnningErrors(true);
		}

		private void DrawEditorState ()
		{
			//float sub = (_settings.showQuickShortcuts? _settings.Button_Quick_Play_Width : 0) + (_settings.showQuickShortcuts && _settings.LoadPreloadScene? _settings.Button_Quick_Preload_Width : 0);
			using (new EditorGUILayout.HorizontalScope(GUILayout.Height(40f), GUILayout.Width(90f)))
			{
				GUILayout.FlexibleSpace();
				EditorGUILayout.LabelField(_currentState, Style_EditorStateLabel, GUILayout.Height(40f), GUILayout.Width(90f));
				GUILayout.FlexibleSpace();
			}
		}

		private void DrawQuickShortcuts ()
		{
			using (new EditorGUILayout.HorizontalScope(GUILayout.Height(40f), GUILayout.Width(_settings.Button_Quick_Play_Width + (_settings.LoadPreloadScene? _settings.Button_Quick_Preload_Width : 0) - 30f)))
			{
				GUILayout.FlexibleSpace();
				if (GUILayout.Button(new GUIContent("Play", (_settings.LoadPreloadScene? "Preload and then s" : "S") + "tart Play Mode\nRight Click: Stop Play Mode"), GUILayout.Height(40f), GUILayout.Width(_settings.Button_Quick_Play_Width)))
				{
					int type = Event.current.button;
					if (type == 0)
					{
						if (!EditorApplication.isPlaying)
						{
							if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
							{
								//!! 9EU: Implement Play Scene setting and uncomment below
								//CheckAndLoadPlayScene();
								CheckAndAddLoadPreloadScene();
								EditorApplication.isPlaying = true;
							}
						}
					}
					else if (type == 1)
					{
						//!! 9EU: Implement Last Loaded Scenes memory restore
						EditorApplication.isPlaying = false;
					}
				}

				if (_settings.LoadPreloadScene)
				{
					if (GUILayout.Button(new GUIContent("Preload", "Additive Load the Preload Scene\nWill not start Play Mode\nRight Click: Unload Preload Scene"), GUILayout.Height(40f), GUILayout.Width(_settings.Button_Quick_Preload_Width)))
					{
						if (!EditorApplication.isPlaying)
						{
							int type = Event.current.button;

							if (type == 0)
							{
								CheckAndAddLoadPreloadScene();
							}
							else if (type == 1)
							{
								CheckAndUnloadPreloadScene();
							}
						}
					}
				}
				GUILayout.FlexibleSpace();
			}
		}

		private void CheckAndLoadPlayScene ()
		{
			CheckAndLoadScene(_settings._playScene);
		}

		private void DrawVersionInformation ()
		{
			using (new EditorGUILayout.HorizontalScope(GUILayout.MaxWidth(currentWidth - 5f)))
			{
				using (var cc = new EditorGUI.ChangeCheckScope())
				{
					GUILayout.FlexibleSpace();
					var newState = (AppSettings.DevStage) EditorGUILayout.EnumPopup(_settings.AppSettings.currentState, GUILayout.Width(70f));
					var newMajor = EditorGUILayout.IntField(_settings.AppSettings.Major, GUILayout.Width(_settings.VersionField_Width));
					EditorGUILayout.LabelField(".", GUILayout.Width(5f));
					var newMinor = EditorGUILayout.IntField(_settings.AppSettings.Minor, GUILayout.Width(_settings.VersionField_Width));
					EditorGUILayout.LabelField(".", GUILayout.Width(5f));
					var newPatch = EditorGUILayout.IntField(_settings.AppSettings.Patch, GUILayout.Width(_settings.VersionField_Width));
					GUILayout.FlexibleSpace();

					if (cc.changed)
					{
						_settings.AppSettings.currentState = newState;
						_settings.AppSettings.Major = newMajor;
						_settings.AppSettings.Minor = newMinor;
						_settings.AppSettings.Patch = newPatch;
					}
				}
			}
		}

		private void DrawQuickBuild ()
		{
			if (_settings.AppSettings != null)
			{
				using (new EditorGUILayout.HorizontalScope(GUILayout.MaxWidth(currentWidth - 5f)))
				{
					using (var cc = new EditorGUI.ChangeCheckScope())
					{
						GUILayout.FlexibleSpace();
						EditorGUILayout.LabelField("Name Format:", GUILayout.Width(80f));
						var newSett = (AppSettings.BuildNameType) EditorGUILayout.EnumPopup(_settings.AppSettings.currentBuildNameType, GUILayout.Width(100f));
						GUILayout.FlexibleSpace();

						if (cc.changed)
						{
							_settings.AppSettings.currentBuildNameType = newSett;
						}
					}
				}

				using (new EditorGUILayout.HorizontalScope(GUILayout.MinWidth(110f), GUILayout.MaxWidth(currentWidth - 5f), GUILayout.Height(35f)))
				{
					GUILayout.FlexibleSpace();

					if (GUILayout.Button(new GUIContent("Build", "Quick Build to:\n" + (GetBuildPath(false, out _, true)??"Error") + "\nRight Click: Build To... (Browse)"), GUILayout.Height(30f), GUILayout.Width(50f)))
					{
						int type = Event.current.button;
						if (type == 0)
						{
							IssueBuild(false);
						}
						else if (type == 1)
						{
							IssueBuild(true);
						}
					}

					if (_settings.showQuickBuildSettings | _settings.showQuickBuildTarget)
					{
						using (new EditorGUILayout.VerticalScope(GUILayout.Height(35f), GUILayout.MinWidth(60f), GUILayout.MaxWidth(150f)))
						{
							GUILayout.FlexibleSpace();
							if (_settings.showQuickBuildSettings)
							{
								using (var cc = new EditorGUI.ChangeCheckScope())
								{
									var newSett = (EditorUtilitiesSettings.BuildSettingsType) EditorGUILayout.EnumFlagsField(new GUIContent("", "Quick Build Options"), _settings.BuildSettings, GUILayout.Height(15f), GUILayout.MinWidth(60f), GUILayout.MaxWidth(150f));
									if (cc.changed)
									{
										_settings.BuildSettings = newSett;
									}
								}
							}
							if (_settings.showQuickBuildTarget)
							{
								var newSett = (BuildTarget) EditorGUILayout.EnumPopup(new GUIContent("", "Quick Build Target"), _settings.selectedBuildTarget, null, false, GUILayout.Height(15f), GUILayout.MinWidth(60f), GUILayout.MaxWidth(150f));

								using (var cc = new EditorGUI.ChangeCheckScope())
								{
									if (cc.changed)
									{
										_settings.selectedBuildTarget = newSett;
									}
								}
							}
							GUILayout.FlexibleSpace();
						}
					}

					GUILayout.FlexibleSpace();
				}
				
				_settings.AppSettings.Validate();

				if (_settings.showLastBuild)
				{
					DrawLastBuild();
				}
			}
			else
			{
				BuildStatus = "Build Status: No App Settings asset selected!";
				EditorGUILayout.LabelField(BuildStatus, EditorStyles.boldLabel);
			}
		}

		private void IssueBuild (bool BrowseFirst)
		{
			_settings.AppSettings.Validate();
			// Starting BuildPlayerOptions
			string buildPath = GetBuildPath(BrowseFirst, out string folderName);
			if (string.IsNullOrEmpty(buildPath) | string.IsNullOrWhiteSpace(buildPath))
			{
				return;
			}
			BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
			buildPlayerOptions.locationPathName = buildPath;

			List<EditorBuildSettingsScene> editorBuildSettingsScenes = new List<EditorBuildSettingsScene>();
			editorBuildSettingsScenes.AddRange(EditorBuildSettings.scenes);
			List<string> ScenePaths = new List<string>();
			foreach (EditorBuildSettingsScene scene in editorBuildSettingsScenes)
				ScenePaths.Add(scene.path);

			buildPlayerOptions.target = _settings.selectedBuildTarget;

			buildPlayerOptions.scenes = ScenePaths.ToArray();

			buildPlayerOptions.options = _settings.selectedBuildOptions;

			bool isFinal = _settings.AppSettings.currentState == AppSettings.DevStage.Final;
			bool alwaysDevOn = _settings.BuildSettings.HasFlag(EditorUtilitiesSettings.BuildSettingsType.AlwaysDev);
			bool autoDevOn = _settings.BuildSettings.HasFlag(EditorUtilitiesSettings.BuildSettingsType.AutoDev);
			if (alwaysDevOn || (autoDevOn && isFinal))
			{
				buildPlayerOptions.options |= BuildOptions.Development;
			}

			if (buildPlayerOptions.options.HasFlag(BuildOptions.Development))
			{
				Helpers.DebugLog("Starting Dev Build");
			}
			else
			{
				Helpers.DebugLog("Starting Non-Dev Build");
			}

			_settings.AppSettings.LastState = _settings.AppSettings.currentState;
			_settings.AppSettings.SetLastVersion(_settings.AppSettings.currentAppVer);
			_settings.AppSettings.LastBuildName = folderName;
			_settings.AppSettings.Validate();

			// Start build
			BuildResult buildResult = BuildPipeline.BuildPlayer(buildPlayerOptions).summary.result;
			bool buildSucess = buildResult == BuildResult.Succeeded;
			if (buildSucess && _settings.BuildSettings.HasFlag(EditorUtilitiesSettings.BuildSettingsType.OpenFolder))
			{
				EditorUtility.RevealInFinder(buildPlayerOptions.locationPathName);
			}
			Helpers.DebugLog("Build " + buildResult.ToString());
		}

		private void DrawLastBuild ()
		{
			using (new EditorGUILayout.HorizontalScope(GUILayout.MaxWidth(currentWidth - 5f)))
			{
				EditorGUILayout.LabelField("Last Build:\n" + _settings.AppSettings.LastState.ToString()
								+ " v" + AppSettings.GenerateVersionString(_settings.AppSettings, true)
								+ " (\"" + _settings.AppSettings.LastBuildName + "\")", Style_MiniLabel_CenterMe, GUILayout.Height(20f));
			}
		}

		private void DrawSceneShortcuts ()
		{
			float maxHeight = 25f * Mathf.Clamp((_settings.launchSceneShortcuts.Count > _settings.loadSceneShortcuts.Count? _settings.launchSceneShortcuts.Count : _settings.loadSceneShortcuts.Count) + 1,2,10);
			float minHeight = 25f * Mathf.Clamp((_settings.launchSceneShortcuts.Count > _settings.loadSceneShortcuts.Count? _settings.launchSceneShortcuts.Count : _settings.loadSceneShortcuts.Count) + 1,1,3);
			using (var v1 = new EditorGUILayout.VerticalScope(GUILayout.MinHeight(minHeight), GUILayout.MaxHeight(maxHeight), GUILayout.ExpandHeight(true)))
			{
				using (var h1 = new EditorGUILayout.HorizontalScope(GUILayout.MinWidth(_settings.Button_LaunchScene_Width_Min + _settings.Button_LoadScene_Width_Min + _settings.Button_LoadSceneAdditive_Width_Min + 5f), GUILayout.MaxWidth(currentWidth - 15f)))
				{
					GUILayout.FlexibleSpace();
					using (var h2 = new EditorGUILayout.VerticalScope(GUILayout.MinWidth(_settings.Button_LaunchScene_Width_Min - 15f), GUILayout.MaxWidth(currentWidth - 20f)))
					{
						EditorGUILayout.LabelField("Launch Scene:", EditorStyles.boldLabel, GUILayout.Width(95f), GUILayout.Height(15f));
						if (_settings.launchSceneShortcuts.Count > 0)
						{
							using (var s = new EditorGUILayout.ScrollViewScope(_scrollViewPosition_SceneShortcuts_Launch, GUILayout.MinHeight(minHeight), GUILayout.MaxHeight(maxHeight), GUILayout.MinWidth(_settings.Button_LaunchScene_Width_Min), GUILayout.MaxWidth(currentWidth - 5f)))
							{
								_scrollViewPosition_SceneShortcuts_Launch = s.scrollPosition;
								for (int i = 0; i < _settings.launchSceneShortcuts.Count; i++)
								{
									using (var h3 = new EditorGUILayout.HorizontalScope(GUILayout.MinWidth(_settings.Button_LaunchScene_Width_Min - 15f), GUILayout.MaxWidth(currentWidth - 20f)))
									{
										Scene currentScene = SceneManager.GetSceneByPath(_settings.launchSceneShortcuts[i]);
										string sceneName = _settings.launchSceneShortcuts[i];
										string[] sceneNameParts = sceneName.Split('/');
										sceneName = sceneNameParts[sceneNameParts.Length - 1].Replace(".unity", "");
										if (GUILayout.Button(sceneName + (currentScene.buildIndex == -1 ? "" : " (" + currentScene.buildIndex + ")"),
											GUILayout.MaxWidth(300f),
											GUILayout.MinWidth(_settings.Button_LaunchScene_Width_Min)))
										{
											if (!EditorApplication.isPlaying)
											{
												if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
												{
													EditorSceneManager.OpenScene(_settings.launchSceneShortcuts[i], OpenSceneMode.Single);
													CheckAndAddLoadPreloadScene();
													EditorApplication.isPlaying = true;
												}
											}
										}
									}
								}
							}
						}
						else
						{
							EditorGUILayout.HelpBox("No scenes added. Go to setting to add one.", MessageType.Info);
						}
					}

					GUILayout.FlexibleSpace();

					using (var h2 = new EditorGUILayout.VerticalScope(GUILayout.MinWidth(_settings.Button_LoadScene_Width_Min + _settings.Button_LoadSceneAdditive_Width_Min - 15f), GUILayout.MaxWidth(currentWidth - 20f)))
					{
						EditorGUILayout.LabelField("Load Scene:", EditorStyles.boldLabel, GUILayout.Width(80f), GUILayout.Height(15f));
						if (_settings.loadSceneShortcuts.Count > 0)
						{
							using (var s = new EditorGUILayout.ScrollViewScope(_scrollViewPosition_SceneShortcuts_Load, GUILayout.MinHeight(minHeight), GUILayout.MaxHeight(maxHeight), GUILayout.MinWidth(_settings.Button_LoadScene_Width_Min + _settings.Button_LoadSceneAdditive_Width_Min - 20f), GUILayout.MaxWidth(currentWidth - 20f)))
							{
								_scrollViewPosition_SceneShortcuts_Load = s.scrollPosition;
								for (int i = 0; i < _settings.loadSceneShortcuts.Count; i++)
								{
									using (var h3 = new EditorGUILayout.HorizontalScope(GUILayout.MinWidth(_settings.Button_LoadScene_Width_Min + _settings.Button_LoadSceneAdditive_Width_Min - 20f), GUILayout.MaxWidth(currentWidth - 20f)))
									{
										Scene currentScene = EditorSceneManager.GetSceneByPath(_settings.loadSceneShortcuts[i]);
										string sceneName = _settings.loadSceneShortcuts[i];
										string[] sceneNameParts = sceneName.Split('/');
										sceneName = sceneNameParts[sceneNameParts.Length - 1].Replace(".unity", "");
										if (GUILayout.Button(sceneName + (currentScene.buildIndex == -1 ? "" : " (" + currentScene.buildIndex + ")"),
											GUILayout.MaxWidth(300f),
											GUILayout.MinWidth(_settings.Button_LoadScene_Width_Min)))
										{
											if (!EditorApplication.isPlaying)
											{
												if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
												{
													EditorSceneManager.OpenScene(_settings.loadSceneShortcuts[i], OpenSceneMode.Single);
												}
											}
										}

										if (GUILayout.Button(new GUIContent("+", "Additive Load Scene\nRight Click: Unload Scene"),
											GUILayout.MinWidth(_settings.Button_LoadSceneAdditive_Width_Min),
											GUILayout.MaxWidth(_settings.Button_LoadSceneAdditive_Width_Max)))
										{
											if (!EditorApplication.isPlaying | _settings.additiveLoadSceneWhilePlaying)
											{ 
												int type = Event.current.button;
												if (type == 0)
												{
													CheckAndAddLoadScene(_settings.loadSceneShortcuts[i]);
												}
												else if (type == 1)
												{
													if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
													{
														CheckAndUnloadScene(_settings.loadSceneShortcuts[i]);
													}
												}
											}
										}
									}
								}
							}
						}
						else
						{
							EditorGUILayout.HelpBox("No scenes added. Go to setting to add one.", MessageType.Info);
						}
					}

					GUILayout.FlexibleSpace();
				}
			}
		}
		#endregion
		#region Settings
		private void DrawSettings ()
		{
			using (var s = new EditorGUILayout.ScrollViewScope(_scrollViewPosition_Settings, false, false, GUI.skin.horizontalScrollbar, GUI.skin.verticalScrollbar, GUI.skin.scrollView))
			{
				_scrollViewPosition_Settings = s.scrollPosition;
				using (new EditorGUILayout.VerticalScope(GUILayout.MaxWidth(maxWidth)))
				{
					DrawAddonInfo();

					DrawSettingsAppSettings();
					EditorGUILayout.Space();

					using (var cc = new EditorGUI.ChangeCheckScope())
					{
						var newEdState = GUILayout.Toggle(_settings.showEditorState, "Show Editor State");
						var newQuick = GUILayout.Toggle(_settings.showQuickShortcuts, "Show Play Button");
						if (cc.changed)
						{
							_settings.showEditorState = newEdState;
							_settings.showQuickShortcuts = newQuick;
						}
					}

					EditorGUILayout.Space();
					EditorGUI.BeginDisabledGroup(!_settings.showQuickShortcuts);
					DrawSettingsToggles();
					DrawSettingsPreload();

					EditorGUI.EndDisabledGroup();

					DrawSettingsSceneShortcuts();
					DrawSettingsQuickBuild();

					EditorGUILayout.Space();
					DrawSettingsAdvanced();
					EditorGUILayout.Space();
				}
			}
		}

		private void DrawAddonInfo ()
		{
			_Settings_AddonInfo_Open = EditorGUILayout.BeginFoldoutHeaderGroup(_Settings_AddonInfo_Open, AddonInformation.AddonName, FoldoutHeader_def);
			if (_Settings_AddonInfo_Open)
			{
				using (new EditorGUILayout.VerticalScope(Style_Box))
				{
					EditorGUILayout.LabelField("Version" + AddonInformation.AddonVersion.Replace('v', ' '));
					EditorGUILayout.LabelField("by " + AddonInformation.AddonBy);
					_Settings_MoreInfo_Open = EditorGUILayout.ToggleLeft(new GUIContent("Show Advanced Information"), _Settings_MoreInfo_Open, EditorStyles.miniLabel);
					if (_Settings_MoreInfo_Open)
					{
						EditorGUILayout.LabelField("Unity " + Application.unityVersion, EditorStyles.miniLabel);
						EditorGUILayout.LabelField("C# " + System.Environment.Version.ToString(), EditorStyles.miniLabel);
						EditorGUILayout.LabelField("Detected System Language: " + Application.systemLanguage.ToString(), EditorStyles.miniLabel);
					}
				}
			}
			EditorGUILayout.EndFoldoutHeaderGroup();
		}

		private void DrawSettingsPreload ()
		{
			EditorGUILayout.Space();

			using (var cc = new EditorGUI.ChangeCheckScope())
			{
				var newSett = EditorGUILayout.BeginFoldoutHeaderGroup(_Settings_Preload_Open, "Additive Preload Scene " + (_settings.LoadPreloadScene ? "(On)" : "(Off)"), FoldoutHeader_def);
				if (cc.changed)
				{
					_Settings_Preload_Open = newSett;
				}
			}
			
			if (_Settings_Preload_Open)
			{
				using (var h = new EditorGUILayout.VerticalScope(Style_Box))
				{
					using (var cc = new EditorGUI.ChangeCheckScope())
					{
						var preload = GUILayout.Toggle(_settings.LoadPreloadScene, "Enable");
						EditorGUI.BeginDisabledGroup(!_settings.LoadPreloadScene);
						var scene = DrawScenePathField(_settings._preloadScene, false);
						EditorGUI.EndDisabledGroup();
						if (cc.changed)
						{
							_settings.LoadPreloadScene = preload;
							_settings._preloadScene = scene;
						}
					}
				}
			}
			EditorGUILayout.EndFoldoutHeaderGroup();
		}

		private void DrawSettingsQuickBuild ()
		{
			if (_settings.AppSettings != null)
			{
				GUILayout.Space(5f);

				using (var cc = new EditorGUI.ChangeCheckScope())
				{
					var newSett = EditorGUILayout.BeginFoldoutHeaderGroup(_Settings_QuickBuild_Open, "Quick Build Settings " + (_settings.showQuickBuild ? "(On)" : " (Off)"), FoldoutHeader_def);
					if (cc.changed)
					{
						_Settings_QuickBuild_Open = newSett;
					}
				}
				
				EditorGUI.BeginDisabledGroup(!_settings.showQuickBuild);
				if (_Settings_QuickBuild_Open)
				{
					using (new EditorGUILayout.VerticalScope(Style_Box, GUILayout.MinWidth(120f), GUILayout.MaxWidth(maxWidth)))
					{
						using (var cc = new EditorGUI.ChangeCheckScope())
						{
							float prevWidth = EditorGUIUtility.labelWidth;
							EditorGUIUtility.labelWidth = 120f;

							var options = (BuildOptions) EditorGUILayout.EnumFlagsField("Persistent Options", _settings.selectedBuildOptions, GUILayout.MinWidth(80f), GUILayout.MaxWidth(maxWidth));
							var settings = (EditorUtilitiesSettings.BuildSettingsType) EditorGUILayout.EnumFlagsField("Quick Settings", _settings.BuildSettings, GUILayout.MinWidth(80f), GUILayout.MaxWidth(maxWidth));
							var showQuick = GUILayout.Toggle(_settings.showQuickBuildSettings, "Show Quick Settings");

							GUILayout.Space(5f);

							var selTarget = (BuildTarget) EditorGUILayout.EnumPopup(new GUIContent("Selected Target"), _settings.selectedBuildTarget, null, false, GUILayout.MinWidth(80f), GUILayout.MaxWidth(maxWidth));
							var showTarget = GUILayout.Toggle(_settings.showQuickBuildTarget, "Show Quick Target");

							EditorGUIUtility.labelWidth = prevWidth;
							if (cc.changed)
							{
								_settings.selectedBuildOptions = options;
								_settings.BuildSettings = settings;
								_settings.showQuickBuildSettings = showQuick;
								_settings.selectedBuildTarget = selTarget;
								_settings.showQuickBuildTarget = showTarget;
							}
						}
						
					}
				}
				EditorGUI.EndDisabledGroup();
				EditorGUILayout.EndFoldoutHeaderGroup();
			}
		}

		private void DrawSettingsToggles ()
		{
			using (var cc = new EditorGUI.ChangeCheckScope())
			{
				EditorGUI.BeginDisabledGroup(_settings.AppSettings == null);
				var showInfo = GUILayout.Toggle(_settings.showInfoSettings, "Show Quick Info");
				var showVer = GUILayout.Toggle(_settings.showVersion, "Show Version Control");
				var showBuild = GUILayout.Toggle(_settings.showQuickBuild, "Show Quick Build");

				EditorGUI.BeginDisabledGroup(!_settings.showQuickBuild);
				var showLast = GUILayout.Toggle(_settings.showLastBuild, "Show Last Build Information");
				EditorGUI.EndDisabledGroup();

				EditorGUI.EndDisabledGroup();

				var showShortcuts = GUILayout.Toggle(_settings.showSceneShortcuts, "Show Scene Schortcuts");
				if (cc.changed)
				{
					_settings.showInfoSettings = showInfo;
					_settings.showVersion = showVer;
					_settings.showQuickBuild = showBuild;
					_settings.showLastBuild = showLast;
					_settings.showSceneShortcuts = showShortcuts;
				}
			}
		}

		private void DrawSettingsAdvanced ()
		{
			using (var cc = new EditorGUI.ChangeCheckScope())
			{
				var showAdv = EditorGUILayout.BeginFoldoutHeaderGroup(_settings.showAdvancedSettings, "Advanced Settings", FoldoutHeader_def);
				if (cc.changed)
				{
					_settings.showAdvancedSettings = showAdv;
				}
			}
			

			if (_settings.showAdvancedSettings)
			{
				using (var cc = new EditorGUI.ChangeCheckScope())
				{
					float prevWidth = EditorGUIUtility.labelWidth;
					EditorGUIUtility.labelWidth = _settings.Settings_Advanced_LabelWidth;
					var a1 = EditorGUILayout.Toggle("additiveLoadSceneWhilePlaying", _settings.additiveLoadSceneWhilePlaying, GUILayout.ExpandWidth(false));
					var a2 = EditorGUILayout.FloatField("Panel_Height_Min", _settings.Panel_Height_Min, GUILayout.ExpandWidth(false));
					var a3 = EditorGUILayout.FloatField("Panel_Width_Min", _settings.Panel_Width_Min, GUILayout.ExpandWidth(false));
					var a4 = EditorGUILayout.FloatField("Settings_Advanced_LabelWidth", _settings.Settings_Advanced_LabelWidth, GUILayout.ExpandWidth(false));
					var a5 = EditorGUILayout.FloatField("Button_LaunchScene_Width_Min", _settings.Button_LaunchScene_Width_Min, GUILayout.ExpandWidth(false));
					var a6 = EditorGUILayout.FloatField("SceneShortcuts_Height_Min", _settings.SceneShortcuts_Height_Min, GUILayout.ExpandWidth(false));
					var a7 = EditorGUILayout.FloatField("Button_LoadScene_Width_Min", _settings.Button_LoadScene_Width_Min, GUILayout.ExpandWidth(false));
					var a8 = EditorGUILayout.FloatField("Button_LoadSceneAdditive_Width_Min", _settings.Button_LoadSceneAdditive_Width_Min, GUILayout.ExpandWidth(false));
					var a9 = EditorGUILayout.FloatField("Button_LoadSceneAdditive_Width_Max", _settings.Button_LoadSceneAdditive_Width_Max, GUILayout.ExpandWidth(false));
					var b1 = EditorGUILayout.FloatField("SceneLists_Width_Max", _settings.SceneLists_Width_Max, GUILayout.ExpandWidth(false));
					var b2 = EditorGUILayout.FloatField("VersionField_Width", _settings.VersionField_Width, GUILayout.ExpandWidth(false));
					var b3 = EditorGUILayout.FloatField("Info_Field_Width_Min", _settings.Info_Field_Width_Min, GUILayout.ExpandWidth(false));
					var b4 = EditorGUILayout.FloatField("Info_Field_Width_Max", _settings.Info_Field_Width_Max, GUILayout.ExpandWidth(false));
					var b5 = EditorGUILayout.FloatField("Settings_SceneLists_SpaceAfterItem", _settings.Settings_SceneLists_SpaceAfterItem, GUILayout.ExpandWidth(false));
					EditorGUIUtility.labelWidth = prevWidth;
					if (cc.changed)
					{
						_settings.additiveLoadSceneWhilePlaying = a1;
						_settings.Panel_Height_Min = a2;
						_settings.Panel_Width_Min = a3;
						_settings.Settings_Advanced_LabelWidth = a4;
						_settings.Button_LaunchScene_Width_Min = a5;
						_settings.SceneShortcuts_Height_Min = a6;
						_settings.Button_LoadScene_Width_Min = a7;
						_settings.Button_LoadSceneAdditive_Width_Min = a8;
						_settings.Button_LoadSceneAdditive_Width_Max = a9;
						_settings.SceneLists_Width_Max = b1;
						_settings.VersionField_Width = b2;
						_settings.Info_Field_Width_Min = b3;
						_settings.Info_Field_Width_Max = b4;
						_settings.Settings_SceneLists_SpaceAfterItem = b5;
					}
				}
			}
			EditorGUILayout.EndFoldoutHeaderGroup();
		}

		private void DrawSettingsAppSettings ()
		{
			GUILayout.Space(2f);

			using (var cc = new EditorGUI.ChangeCheckScope())
			{
				var Sett = EditorGUILayout.BeginFoldoutHeaderGroup(_Settings_AppSettings_Open, "App Settings", FoldoutHeader_def);
				if (cc.changed)
				{
					_Settings_AppSettings_Open = Sett;
				}
			}
			EditorGUILayout.BeginVertical(Style_Box);

			EditorGUILayout.LabelField("App Settings Asset");
			int id = GUIUtility.GetControlID(FocusType.Keyboard);

			using (var cc = new EditorGUI.ChangeCheckScope())
			{
				var a1 = EditorGUILayout.ObjectField(_settings.AppSettings, typeof(AppSettings), false) as AppSettings;
				if (cc.changed)
				{
					_settings.AppSettings = a1;
				}
			}

			EUScriptableObjects.AppSettingsGUI.AutoRightClickMenu(_settings, "AppSettings", Event.current.GetTypeForControl(id), true);

			if (_settings.AppSettings == null)
			{
				if (GUILayout.Button("Create"))
				{
					_settings.AppSettings = AssetDatabase.LoadAssetAtPath<AppSettings>(CreateAsset<AppSettings>(APPSETTINGS_DIRECTORY, "AppSettings"));
					UpdateInformation();
				}
			}
			else
			{
				if (AppSettings.Current != _settings.AppSettings)
				{
					AppSettings.Current = _settings.AppSettings;
					UpdateInformation();
					DrawAppSettingsWarnningErrors(false);
				}
			}

			if (_Settings_AppSettings_Open)
			{
				if (_settings.AppSettings != null)
				{
					using (var cc = new EditorGUI.ChangeCheckScope())
					{
						GUILayout.Space(5f);
						var a1 =  EditorGUILayout.TextField(new GUIContent("Product Name"), _settings.AppSettings.currentAppName);
						var a2 =  EditorGUILayout.TextField(new GUIContent("Copyright"), _settings.AppSettings.Copyright);
						var a3 =  EditorGUILayout.TextField(new GUIContent("Company Name"), _settings.AppSettings.currentCompName);
						var a4 =  EditorGUILayout.TextField(new GUIContent("EXE Name"), _settings.AppSettings.currentEXEName);
						var a5 =  EditorGUILayout.Toggle(new GUIContent("Force Single Instance"), _settings.AppSettings.currentSingleInstance);

						GUILayout.BeginHorizontal();
						GUILayout.Label(new GUIContent("Version Minimum Digits"));
						var a6 = _settings.AppSettings.MinMajorDigits = EditorGUILayout.IntField(_settings.AppSettings.MinMajorDigits, GUILayout.Width(20f));
						EditorGUILayout.LabelField(".", GUILayout.Width(5f));
						var a7 =  EditorGUILayout.IntField(_settings.AppSettings.MinMinorDigits, GUILayout.Width(20f));
						EditorGUILayout.LabelField(".", GUILayout.Width(5f));
						var a8 =  EditorGUILayout.IntField(_settings.AppSettings.MinPatchDigits, GUILayout.Width(20f));
						GUILayout.EndHorizontal();
						GUILayout.Space(5f);

						GUILayout.BeginHorizontal();
						EditorGUILayout.PrefixLabel("Build Path");
						GUILayout.FlexibleSpace();
						DrawBrowseButton(ref _settings.AppSettings.BuildsPath, false, false, "Choose Build Path");
						GUILayout.EndHorizontal();

						EditorGUILayout.HelpBox("Current Path:\n" + _settings.AppSettings.BuildsPath, string.IsNullOrEmpty(_settings.AppSettings.BuildsPath) ? MessageType.Error : MessageType.None);
						
						if (cc.changed)
						{
							_settings.AppSettings.currentAppName = a1;
							_settings.AppSettings.Copyright = a2;
							_settings.AppSettings.currentCompName = a3;
							_settings.AppSettings.currentEXEName = a4;
							_settings.AppSettings.currentSingleInstance = a5;
							_settings.AppSettings.MinMajorDigits = a6;
							_settings.AppSettings.MinMinorDigits =  a7;
							_settings.AppSettings.MinPatchDigits = a8;
							_settings.AppSettings.Validate();
						}
					}
				}
				else
				{
					EditorGUILayout.LabelField("Please select App Settings asset");
				}
			}

			EditorGUILayout.EndVertical();
			EditorGUILayout.EndFoldoutHeaderGroup();
		}

		private void DrawSettingsSceneShortcuts ()
		{
			EditorGUILayout.Space();
			using (new EditorGUILayout.VerticalScope())
			{
				_Settings_SceneShortcuts_Fold.target = EditorGUILayout.Foldout(_Settings_SceneShortcuts_Fold.target, "Scenes Shortcut Settings " + (_settings.showSceneShortcuts? "(On)" : "(Off)"), true, FoldoutHeader_def);
				using (var f = new EditorGUILayout.FadeGroupScope(_Settings_SceneShortcuts_Fold.faded))
				{
					if (f.visible)
					{
						using (var v = new EditorGUILayout.VerticalScope(Style_Box))
						{
							_Settings_SceneShortcuts_Launch_Fold.target = EditorGUILayout.Foldout(_Settings_SceneShortcuts_Launch_Fold.target, "Quick Launch Scenes", true, FoldoutHeader_def);
							using (var f2 = new EditorGUILayout.FadeGroupScope(_Settings_SceneShortcuts_Launch_Fold.faded))
							{
								if (f2.visible)
								{
									EditorGUI.BeginDisabledGroup(!_settings.showSceneShortcuts);
									DrawList(null, ref _settings.launchSceneShortcuts,
											 drawListItem: (scene, last) => DrawScenePathField(scene, last),
											 createListItem: () => "Assets/Scenes/");
									EditorGUI.EndDisabledGroup();
								}
							}

							GUILayout.Space(2f);

							_Settings_SceneShortcuts_Load_Fold.target = EditorGUILayout.Foldout(_Settings_SceneShortcuts_Load_Fold.target, "Quick Load Scenes", true, FoldoutHeader_def);
							using (var f2 = new EditorGUILayout.FadeGroupScope(_Settings_SceneShortcuts_Load_Fold.faded))
							{
								if (f2.visible)
								{
									EditorGUI.BeginDisabledGroup(!_settings.showSceneShortcuts);
									DrawList(null, ref _settings.loadSceneShortcuts,
										 drawListItem: (scene, last) => DrawScenePathField(scene, last),
										 createListItem: () => "Assets/Scenes/");
									EditorGUI.EndDisabledGroup();
								}
							}
						}
					}
				}
			}
		}

		private void DrawSettingsSceneListMenu_Launch(Rect position)
		{
			DrawSettingsSceneListMenu(position, true);
		}

		private void DrawSettingsSceneListMenu_Load (Rect position)
		{
			DrawSettingsSceneListMenu(position, false);
		}

		private void DrawSettingsSceneListMenu(Rect position, bool Launch)
		{
			var menu = new GenericMenu();
			menu.AddItem(new GUIContent("Clear List"), false, OnItemClicked);
			menu.DropDown(new Rect());

			void OnItemClicked ()
			{
				if (Launch)
					_settings.launchSceneShortcuts.Clear();
				else
					_settings.loadSceneShortcuts.Clear();
			}
		}

		private string DrawScenePathField(string path, bool last)
        {
            EditorGUILayout.BeginVertical();
			string newPath;
			SceneAsset oldScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(path);
			SceneAsset newScene = EditorGUILayout.ObjectField(oldScene, typeof(SceneAsset), false) as SceneAsset;
			if (oldScene != newScene)
			{
				newPath = AssetDatabase.GetAssetPath(newScene);
			}
			else
			{
				newPath = path;
			}
			
			bool isSceneValid = File.Exists(newPath);
			if (!isSceneValid)
			{
				newPath = "";
			}

			if (!last)
			{
				GUILayout.Space(_settings.Settings_SceneLists_SpaceAfterItem);
			}
			EditorGUILayout.EndVertical();
            return newPath;
        }
		#endregion
		#region General
		private void DrawAppSettingsWarnningErrors (bool ShowAppSettingsPrefix)
		{
			MessageType messageType = _settings.showQuickBuild ? MessageType.Error : MessageType.Warning;
			string prefix = ShowAppSettingsPrefix? "AppSettings: " : "";

			if (string.IsNullOrEmpty(_settings.AppSettings.BuildsPath))
			{
				EditorGUILayout.HelpBox(prefix + "Build Path not set!", messageType);
			}

			if (string.IsNullOrEmpty(_settings.AppSettings.currentEXEName))
			{
				EditorGUILayout.HelpBox(prefix + "EXE Name not set!", messageType);
			}

			if (string.IsNullOrEmpty(_settings.AppSettings.currentAppName))
			{
				EditorGUILayout.HelpBox(prefix + "Product Name not set!", messageType);
			}

			if (string.IsNullOrEmpty(_settings.AppSettings.currentCompName))
			{
				EditorGUILayout.HelpBox(prefix + "Company Name not set!", messageType);
			}

			if (string.IsNullOrEmpty(_settings.AppSettings.Copyright))
			{
				EditorGUILayout.HelpBox(prefix + "Copyright not set!", messageType);
			}
		}
		#endregion
		#endregion
		#region Helpers
		private EditorUtilitiesSettings LoadSettings()
		{
			if (!File.Exists(SETTINGS_DIRECTORY + SETTINGS_FILE_NAME))
			{
				CreateAsset<EditorUtilitiesSettings>(SETTINGS_DIRECTORY, SETTINGS_FILE_NAME);
			}
			EditorUtilitiesSettings settings = AssetDatabase.LoadAssetAtPath(SETTINGS_DIRECTORY + SETTINGS_FILE_NAME, typeof(EditorUtilitiesSettings)) as EditorUtilitiesSettings;
			UpdateInformation(settings);

			return settings;
		}

		private void UpdateInformation (EditorUtilitiesSettings settings = null)
		{
			if (settings == null)
			{
				settings = _settings;
				if (settings == null)
				{
					return;
				}
			}
			settings.UpdateAddonInfo();

			if (settings.AppSettings != null)
			{
				settings.AppSettings.UpdateAddonInfo();
			}
		}

		private void DrawList<T>(string title, ref List<T> list, Func<T,bool,T> drawListItem, Func<T> createListItem)
        {
			using (var v = new EditorGUILayout.VerticalScope(Style_Box))
			{
				if (!string.IsNullOrWhiteSpace(title) & !string.IsNullOrEmpty(title))
				{
					EditorGUILayout.LabelField(title);
				}

				GUILayout.Space(3f);

				for (int i = 0; i < list.Count; i++)
				{
					using (var h = new EditorGUILayout.HorizontalScope())
					{
						list[i] = drawListItem(list[i], (i == list.Count - 1));
						if (GUILayout.Button("x", Style_MiniButton, GUILayout.Width(17f), GUILayout.Height(17f)))
						{
							list.RemoveAt(i);
						}
					}
				}

				GUILayout.Space(3f);

				using (new EditorGUILayout.HorizontalScope())
				{
					GUILayout.FlexibleSpace();
					if (GUILayout.Button("+", Style_MiniButton, GUILayout.Width(17f), GUILayout.Height(17f)))
					{
						list.Add(createListItem());
					}
				}

				GUILayout.Space(1f);
			}
        }

		private string CreateAsset<T> (string directory, string name) where T : ScriptableObject
		{
			var conf = ScriptableObject.CreateInstance<T>();

			if (!Directory.Exists(directory))
			{
				Directory.CreateDirectory(directory);
			}
			string tempName = name;

			if (!name.EndsWith(".asset"))
				name = name + ".asset";

			if (!File.Exists(directory + name))
			{ 
				AssetDatabase.CreateAsset(conf, directory + name);
				AssetDatabase.Refresh();
			}
			else
			{
				try
				{
					T test = AssetDatabase.LoadAssetAtPath<T>(directory + name);
					if (test.GetType() != typeof(T))
					{
						throw new Exception();
					}
				}
				catch
				{
					while (File.Exists(directory + name))
					{
						name = "_" + name;
					}
					AssetDatabase.CreateAsset(conf, directory + name);
					AssetDatabase.Refresh();
				}
			}
            
            EditorUtility.FocusProjectWindow();
			return directory + name;
		}

        private string GetCurrentState()
        {
            string state = "Idle";
            Style_EditorStateLabel.normal.textColor = Color.green;
			Style_EditorStateLabel.fontStyle = FontStyle.Normal;
			Style_EditorStateLabel.fontSize = 32;
			Style_EditorStateLabel.alignment = TextAnchor.MiddleCenter;

			if (EditorApplication.isCompiling)
            {
                state = "Compiling";
                Style_EditorStateLabel.normal.textColor = Color.red;
				Style_EditorStateLabel.fontStyle = FontStyle.Bold;
				Style_EditorStateLabel.fontSize = 24;
			}
            else if (EditorApplication.isPaused)
            {
                state = "Paused";
                Style_EditorStateLabel.normal.textColor = Color.yellow;
				Style_EditorStateLabel.fontSize = 24;
			}
            else if (EditorApplication.isPlaying)
            {
                state = "Playing";
                Style_EditorStateLabel.normal.textColor = Color.blue;
				Style_EditorStateLabel.fontSize = 24;
			}
            else if (EditorApplication.isUpdating)
            {
                state = "Updating\nAssetDatabase";
                Style_EditorStateLabel.normal.textColor = Color.red;
				Style_EditorStateLabel.fontStyle = FontStyle.Bold;
				Style_EditorStateLabel.fontSize = 18;
			}

			if (_settings.showQuickShortcuts)
			{
				Style_EditorStateLabel.fontSize -= 7;
				if (_settings.LoadPreloadScene)
				{
					Style_EditorStateLabel.fontSize -= 0;
				}
			}

			return state;
        }

		private string Number2String(int number, bool isCaps)
		{
			char c = (char)((isCaps ? 65 : 97) + (number - 1));
			return c.ToString();
		}

		private void CheckAndAddLoadPreloadScene ()
		{
			if (_settings.LoadPreloadScene)
			{
				CheckAndAddLoadScene(_settings._preloadScene);
			}
		}

		private void CheckAndAddLoadScene (string path)
		{
			var openScenes = EditorSceneManager.sceneCount;
			bool SceneIsLoaded = false;
			Scene target = EditorSceneManager.GetSceneByPath(path);
			for (int i = 0; i < openScenes; i++)
			{
				if (EditorSceneManager.GetSceneAt(i) == target)
				{
					SceneIsLoaded = true;
					continue;
				}
			}

			if (!SceneIsLoaded)
			{
				EditorSceneManager.OpenScene(path, OpenSceneMode.Additive);
			}
		}

		private void CheckAndLoadScene (string path)
		{
			var openScenes = EditorSceneManager.sceneCount;
			bool SceneIsLoaded = false;
			Scene target = EditorSceneManager.GetSceneByPath(path);
			for (int i = 0; i < openScenes; i++)
			{
				if (EditorSceneManager.GetSceneAt(i) == target)
				{
					SceneIsLoaded = true;
					continue;
				}
			}

			if (!SceneIsLoaded)
			{
				EditorSceneManager.OpenScene(path, OpenSceneMode.Single);
			}
		}

		private void CheckAndUnloadPreloadScene ()
		{
			if (_settings.LoadPreloadScene)
			{
				CheckAndUnloadScene(_settings._preloadScene);
			}
		}

		private void CheckAndUnloadScene (string path)
		{
			if (_settings.LoadPreloadScene)
			{
				var openScenes = EditorSceneManager.sceneCount;
				bool SceneIsLoaded = false;
				Scene target = EditorSceneManager.GetSceneByPath(path);
				for (int i = 0; i < openScenes; i++)
				{
					if (EditorSceneManager.GetSceneAt(i) == target)
					{
						SceneIsLoaded = true;
						continue;
					}
				}

				if (SceneIsLoaded && openScenes > 1)
				{
					Scene[] targets = new Scene[1]{target };
					EditorSceneManager.SaveModifiedScenesIfUserWantsTo(targets);
					EditorSceneManager.CloseScene(target, true);
				}
			}
		}

		private void DrawBrowseButton (ref string Target, bool Local, bool File, string Title, string Format = "unity")
		{
			string controlName = "button" + UnityEngine.Random.Range(0,99999).ToString();
			GUI.SetNextControlName(controlName);
			if (GUILayout.Button("Browse", GUILayout.Width(50f)))
			{
				Target = FileBrowser_Open(Local, File, Title, Format);
				EditorGUI.FocusTextInControl(controlName);
			}
		}

		private static string FileBrowser_Open (bool Local, bool File, string Title, string Format)
		{
			string Target;
			string newPath;
			if (File)
				newPath = EditorUtility.OpenFilePanel(Title, "", Format);
			else
				newPath = EditorUtility.OpenFolderPanel(Title, "", "");
			Target = Helpers.HandleFilePath(newPath, Local, File);
			return Target;
		}

		private static string FileBrowser_Save (bool Local, bool File, string Title, string Format)
		{
			string Target;
			string newPath;
			if (File)
				newPath = EditorUtility.SaveFilePanel(Title, "", "", Format);
			else
				newPath = EditorUtility.SaveFolderPanel(Title, "", "");
			Target = Helpers.HandleFilePath(newPath, Local, File);
			return Target;
		}

		private string GetBuildPath (bool Browse, out string folderName, bool ForceNoBrowse = false)
		{
			folderName = "";
			if (!Browse & (_settings.AppSettings == null || (_settings.AppSettings != null && String.IsNullOrEmpty(_settings.AppSettings.BuildsPath))))
			{
				if (ForceNoBrowse)
				{
					return null;
				}

				if (_settings.AppSettings != null)
				{
					EUEditorOnly.EditorHelpers.DebugWarning("Build Path not set in AppSettings!");
				}
				Browse = true;
			}

			string calcBuildPath = Browse? FileBrowser_Save(false, true, "Select Build Path", "exe") : _settings.AppSettings.BuildsPath;
			string finalPath;
			if (!Browse)
			{
				// Set current data to date
				string date = DateTime.Now.ToString("yyyyMMdd");
				// Folder Name
				string calcBuildPath_Folder = "";
				switch (_settings.AppSettings.currentBuildNameType)
				{
					case AppSettings.BuildNameType.Date:
						calcBuildPath_Folder = date;
						break;

					case AppSettings.BuildNameType.Date_Version:
						calcBuildPath_Folder = date + "_" + AppSettings.GenerateVersionString(_settings.AppSettings, false);
						break;

					case AppSettings.BuildNameType.State_Version:
						calcBuildPath_Folder = _settings.AppSettings.currentState
							+ "_" + AppSettings.GenerateVersionString(_settings.AppSettings, false);
						break;

					case AppSettings.BuildNameType.Version:
						calcBuildPath_Folder = AppSettings.GenerateVersionString(_settings.AppSettings, false);
						break;

					case AppSettings.BuildNameType.Version_Date:
						calcBuildPath_Folder = AppSettings.GenerateVersionString(_settings.AppSettings, false) + "_" + date;
						break;
				}

				string originalPath = calcBuildPath;
				string originalPath_Folder = calcBuildPath_Folder;
				int letterIndex = 0;
				calcBuildPath = Path.Combine(originalPath, calcBuildPath_Folder);
				while (Directory.Exists(calcBuildPath))
				{
					letterIndex++;
					calcBuildPath_Folder = originalPath_Folder + Number2String(letterIndex, true);
					calcBuildPath = Path.Combine(originalPath, calcBuildPath_Folder);
				}
				folderName = calcBuildPath_Folder;

				finalPath = Path.Combine(calcBuildPath, _settings.AppSettings.currentEXEName + ".exe");
			}
			else
			{
				folderName = Path.GetDirectoryName(calcBuildPath);
				finalPath = calcBuildPath;
			}
			return finalPath;
		}
		#endregion
	}
}