﻿// Custom version by Shahar DynaZor Alon, please give credit if used in a project.
#region Original Credits
/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2015 Vitaliy Zasadnyy
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
#endregion
using UnityEngine;
using System.Collections.Generic;

namespace DynaZor.EditorUtilities.EUScriptableObjects.EUSettings
{
	public class AddonInformation
	{
		public static readonly string AddonName = "Editor Utilities (DynaZor Edition)";
		public static readonly string AddonShortName = "Editor Utilities";
		public static readonly string AddonVersion = "v0.00.005";
		public static readonly string AddonBy = "Shahar DynaZor (Original by Vitaliy Zasadnyy)";
	}

	[System.Serializable]
	public class EditorUtilitiesSettings : ScriptableObject
	{
		#region AddonInformation
		public string AddonName = AddonInformation.AddonName;
		public string AddonVersion = AddonInformation.AddonVersion;
		public string AddonBy = AddonInformation.AddonBy;
		#endregion

		#region References
		public AppSettings AppSettings;
		#endregion

		#region Settings
		public bool showQuickBuild = true;
		public bool showQuickBuildSettings = true;
		public bool showEditorState = true;
		public bool showSceneShortcuts = true;
		public bool showQuickShortcuts = true;
		public bool showInfoSettings = true;
		public bool showLastBuild = true;
		public bool showVersion = true;
		public bool LoadPreloadScene = false;
		public bool _Settings_MoreInfo_Open = false;
		public string _preloadScene;
		public bool _setPlayScene = false;
		public string _playScene;
		public List<string> launchSceneShortcuts = new List<string>();
		public List<string> loadSceneShortcuts = new List<string>();
		#region Quick Build Settings
		[System.Flags]
		public enum BuildSettingsType
		{
			None = 0,
			AlwaysDev = 1,
			AutoDev = 2,
			OpenFolder = 4,
			All = ~0
		}
		public BuildSettingsType BuildSettings = BuildSettingsType.OpenFolder | BuildSettingsType.AutoDev;
		public bool showQuickBuildTarget = true;
		public UnityEditor.BuildOptions selectedBuildOptions = UnityEditor.BuildOptions.CompressWithLz4HC;
		public UnityEditor.BuildTarget selectedBuildTarget = UnityEditor.BuildTarget.StandaloneWindows64;
		#endregion
		#endregion

		#region UI
		public bool _Settings_AddonInfo_Open = true;
		public bool _Settings_AppSettings_Open = true;
		public bool _Settings_QuickBuild_Open = true;
		public bool _Settings_Preload_Open = true;
		#endregion

		#region Advanced Settings
		public bool showAdvancedSettings = false;
		public bool additiveLoadSceneWhilePlaying = false;
		public float Panel_Width_Min = 250f;
		public float Panel_Height_Min = 95f;
		public float SceneShortcuts_Height_Min = 50f;
		public float Button_LaunchScene_Width_Min = 90f;
		public float Button_LoadScene_Width_Min = 90f;
		public float Button_LoadSceneAdditive_Width_Min = 20f;
		public float Button_LoadSceneAdditive_Width_Max = 35f;
		public float SceneLists_Width_Max = 500f;
		public float Settings_Advanced_LabelWidth = 250f;
		public float VersionField_Width = 35f;
		public float Info_Field_Width_Min = 60f;
		public float Info_Field_Width_Max = 180f;
		public float Button_Quick_Play_Width = 55f;
		public float Button_Quick_Preload_Width = 55f;
		public float Settings_SceneLists_SpaceAfterItem = 8f;
		#endregion

		#region Events
		public void UpdateAddonInfo ()
		{
			AddonName = AddonInformation.AddonName;
			AddonVersion = AddonInformation.AddonVersion;
			AddonBy = AddonInformation.AddonBy;
		}
		#endregion
	}
}