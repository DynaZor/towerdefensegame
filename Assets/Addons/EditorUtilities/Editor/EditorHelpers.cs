﻿// Library made by Shahar DynaZor Alon, please give credit if used in a project.
using UnityEditor;
using UnityEngine;

namespace DynaZor.EditorUtilities.EUEditorOnly
{
	public class AddonInformation
	{
		public static readonly string AddonName = "Editor Utilities (DynaZor Edition)";
		public static readonly string AddonShortName = "Editor Utilities";
		public static readonly string AddonVersion = "v0.00.005";
		public static readonly string AddonBy = "Shahar DynaZor (Original by Vitaliy Zasadnyy)";
	}

	///<summary>
	///Helpers accessible in Edit mode
	///For Runtime and Play Helpers, use EditorUtilities.Helpers
	///</summary>
	public static class EditorHelpers
	{
		public static class EditorUI
		{
			public static void DrawUILine (Color color, int thickness = 2, int padding = 10)
			{
				Rect r = EditorGUILayout.GetControlRect(GUILayout.Height(padding+thickness));
				r.height = thickness;
				r.y += padding / 2;
				r.x -= 2;
				r.width += 6;
				EditorGUI.DrawRect(r, color);
			}
		}
		public static void CheckVersionConsistency ()
		{
			if (			EditorUtilities.AddonInformation.AddonVersion != EUWindows.AddonInformation.AddonVersion ||
							EditorUtilities.AddonInformation.AddonVersion != SemanticVersioning.AddonInformation.AddonVersion ||
							EditorUtilities.AddonInformation.AddonVersion != EUScriptableObjects.EUSettings.AddonInformation.AddonVersion ||
							EditorUtilities.AddonInformation.AddonVersion != EUScriptableObjects.AddonInformation.AddonVersion ||
							EditorUtilities.AddonInformation.AddonVersion != EURecents.AddonInformation.AddonVersion ||
							EditorUtilities.AddonInformation.AddonVersion != EUEditorOnly.AddonInformation.AddonVersion ||
							EditorUtilities.AddonInformation.AddonVersion != EUExtensions.AddonInformation.AddonVersion)
			{
				string errors = "\n";

				if (EditorUtilities.AddonInformation.AddonVersion != EUWindows.AddonInformation.AddonVersion)
				{
					errors += "EUWindows.AddonInformation\n";
				}

				if (EditorUtilities.AddonInformation.AddonVersion != SemanticVersioning.AddonInformation.AddonVersion)
				{
					errors += "SemanticVersioning.AddonInformation\n";
				}
				
				if (EditorUtilities.AddonInformation.AddonVersion != EUScriptableObjects.EUSettings.AddonInformation.AddonVersion)
				{
					errors += "EUScriptableObjects.EUSettings.AddonInformation\n";
				}

				if (EditorUtilities.AddonInformation.AddonVersion != EUScriptableObjects.AddonInformation.AddonVersion)
				{
					errors += "EUScriptableObjects.AddonInformation\n";
				}

				if (EditorUtilities.AddonInformation.AddonVersion != EURecents.AddonInformation.AddonVersion)
				{
					errors += "EURecents.AddonInformation\n";
				}

				if (EditorUtilities.AddonInformation.AddonVersion != EUEditorOnly.AddonInformation.AddonVersion)
				{
					errors += "EUEditorOnly.AddonInformation\n";
				}

				if (EditorUtilities.AddonInformation.AddonVersion != EUExtensions.AddonInformation.AddonVersion)
				{
					errors += "EUExtensions.AddonInformation\n";
				}

				DebugWarning("Version Inconsistency, please reimport addon.\nIf warning insists, please report." + errors);
			}
		}

		///<summary>
		///For logging as the Editor Utilities. 
		///Don't use outside Editor Utilities to prevent confusion!
		///</summary>
		public static void DebugLog (string message, UnityEngine.Object obj = null)
		{
			Debug.Log(AddonInformation.AddonName + " - " + message, obj);
		}

		///<summary>
		///For logging as the Editor Utilities. 
		///Don't use outside Editor Utilities to prevent confusion!
		///</summary>
		public static void DebugWarning (string message, UnityEngine.Object obj = null)
		{
			Debug.LogWarning(AddonInformation.AddonName + " - " + message, obj);
		}
	}
}