﻿// Library made by Shahar DynaZor Alon, please give credit if used in a project.

using UnityEngine;
using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using Unity.Mathematics;

namespace DynaZor.EditorUtilities
{
	public static class AddonInformation
	{
		public static readonly string AddonName = "Editor Utilities (DynaZor Edition)";
		public static readonly string AddonShortName = "Editor Utilities";
		public static readonly string AddonVersion = "v0.00.005";
		public static readonly string AddonBy = "Shahar DynaZor (Original by Vitaliy Zasadnyy)";
	}

	///<summary>
	///Helpers accessible in Play mode
	///For Editor Only Helpers, use EditorOnly.EditorHelpers
	///</summary>
	public static class Helpers
	{
		#region Data
		#region RectTransform
		public static void MatchRectTransform (this RectTransform targetRect, RectTransform fromRect)
		{
			targetRect.anchorMin = fromRect.anchorMin;
			targetRect.anchorMax = fromRect.anchorMax;
			targetRect.anchoredPosition = fromRect.anchoredPosition;
			targetRect.sizeDelta = fromRect.sizeDelta;
		}
		#endregion
		#region Bool
		public static void FlipBoolValue (this ref bool var)
		{
			var = !var;
		}
		#endregion
		#region List
		public static void MoveItemInList <T>(List<T> list, int oldIndex, int newIndex) where T : UnityEngine.Object
		{
			T item = list[oldIndex];
			list.RemoveAt(oldIndex);
			list.Insert(newIndex, item);
		}
		#endregion
		#endregion
		#region Logging
		///<summary>
		///For logging as the Editor Utilities. 
		///Don't use outside Editor Utilities to prevent confusion!
		///</summary>
		public static void DebugLog (string message, UnityEngine.Object obj = null)
		{
			Debug.Log(AddonInformation.AddonName + " - " + message, obj);
		}

		///<summary>
		///For logging as the Editor Utilities. 
		///Don't use outside Editor Utilities to prevent confusion!
		///</summary>
		public static void DebugWarning (string message, UnityEngine.Object obj = null)
		{
			Debug.LogWarning(AddonInformation.AddonName + " - " + message, obj);
		}
		#endregion
		#region Files
		public static string HandleFilePath (string Path, bool Local, bool File, bool ForLog = false)
		{
			if (!String.IsNullOrEmpty(Path) && !String.IsNullOrWhiteSpace(Path))
			{
				Path = Path.Replace(System.IO.Path.AltDirectorySeparatorChar, System.IO.Path.DirectorySeparatorChar);
				if (!File && Path[Path.Length - 1] != System.IO.Path.DirectorySeparatorChar)
				{
					Path += System.IO.Path.DirectorySeparatorChar;
				}

				if (Local)
				{
					string codeBase = Assembly.GetExecutingAssembly().CodeBase;
					UriBuilder projecturi = new UriBuilder(codeBase);
					string projectpath1 = Uri.UnescapeDataString(projecturi.Path);
					string projectPath2 = System.IO.Path.GetFullPath(new Uri(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(projectpath1), "../../")).AbsolutePath);
					Path = Path.Replace(projectPath2, "");
					Path = Path.Replace(System.IO.Path.DirectorySeparatorChar, System.IO.Path.AltDirectorySeparatorChar);
				}

				if (ForLog)
				{
					Path += ":1";
					Path = Path.Replace(System.IO.Path.DirectorySeparatorChar, System.IO.Path.AltDirectorySeparatorChar );
				}

				return Path;
			}
			return null;
		}
		#endregion
		#region Visual Data
		public static Vector2Int ToVector2Int(this int2 input)
		{
			return new Vector2Int(input.x, input.y);
		}
		public static List<Vector2Int> DrawFilledCircle2 (int x, int y, int r)
		{
			List<Vector2Int> result = new List<Vector2Int>();
			int r2 = r * r;
			int area = r2 << 2;
			int rr = r << 1;

			for (int i = 0; i < area; i++)
			{
				int tx = (i % rr) - r;
				int ty = (i / rr) - r;

				if ((tx * tx) + (ty * ty) <= r2)
					result.Add(new Vector2Int(x + tx, y + ty));
			}

			result = result.Distinct().ToList();
			return result;
		}
		public static List<Vector2Int> DrawFilledMidpointCircleSinglePixelVisit (int centerX, int centerY, int radius)
		{
			List<Vector2Int> result = new List<Vector2Int>();
			int x = radius;
			int y = 0;
			int radiusError = 1 - x;

			while (x >= y)  // iterate to the circle diagonal
			{
				// use symmetry to draw the two horizontal lines at this Y with a special case to draw
				// only one line at the centerY where y == 0
				int startX = -x + centerX;
				int endX = x + centerX;
				result.AddRange(drawHorizontalLine(startX, endX, y + centerY));
				if (y != 0)
				{
					result.AddRange(drawHorizontalLine(startX, endX, -y + centerY));
				}

				// move Y one line
				y++;

				// calculate or maintain new x
				if (radiusError < 0)
				{
					radiusError += (2 * y) + 1;
				}
				else
				{
					// we're about to move x over one, this means we completed a column of X values, use
					// symmetry to draw those complete columns as horizontal lines at the top and bottom of the circle
					// beyond the diagonal of the main loop
					if (x >= y)
					{
						startX = -y + 1 + centerX;
						endX = y - 1 + centerX;
						result.AddRange(drawHorizontalLine(startX, endX, x + centerY));
						result.AddRange(drawHorizontalLine(startX, endX, -x + centerY));
					}
					x--;
					radiusError += 2 * (y - x + 1);
				}
			}

			result = result.Distinct().ToList();
			return result;
		}

		public static List<Vector2Int> drawHorizontalLine (int startX, int endX, int y)
		{
			List<Vector2Int> result = new List<Vector2Int>();
			for (int i = startX; i < endX; i++)
			{
				result.Add(new Vector2Int(i, y));
			}
			return result;
		}

		/// <summary>
		/// Function for (non-filled) circle-generation using Bresenham's algorithm.
		/// </summary>
		/// <param name="CenterX">The circle's center x.</param>
		/// <param name="CenterY">The circle's center y.</param>
		/// <param name="Radius">The radius.</param>
		public static List<Vector2Int> GetCircleBres (int CenterX, int CenterY, int Radius)
		{
			List<Vector2Int> result = new List<Vector2Int>();
			int x = 0, y = Radius;
			int d = 3 - (2 * Radius);
			result.AddRange(GetCirclePart(CenterX, CenterY, x, y));
			while (y >= x)
			{
				// for each pixel we will 
				// draw all eight pixels 
				x++;

				// check for decision parameter 
				// and correspondingly  
				// update d, x, y 
				if (d > 0)
				{
					y--;
					d = d + (4 * (x - y)) + 10;
				}
				else
				{
					d = d + (4 * x) + 6;
				}

				result.AddRange(GetCirclePart(CenterX, CenterY, x, y));
			}

			result = result.Distinct().ToList();
			return result;
		}
		public static List<Vector2Int> GetCirclePart (int CenterX, int CenterY, int x, int y)
		{
			List<Vector2Int> result = new List<Vector2Int>
			{
				new Vector2Int(CenterX + x, CenterY + y),
				new Vector2Int(CenterX - x, CenterY + y),
				new Vector2Int(CenterX + x, CenterY - y),
				new Vector2Int(CenterX - x, CenterY - y),
				new Vector2Int(CenterX + y, CenterY + x),
				new Vector2Int(CenterX - y, CenterY + x),
				new Vector2Int(CenterX + y, CenterY - x),
				new Vector2Int(CenterX - y, CenterY - x)
			};
			return result;
		}
		#endregion
	}

	namespace EUScriptableObjects
	{
	}
}