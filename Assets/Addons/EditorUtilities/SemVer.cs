﻿// Library made by Shahar DynaZor Alon, please give credit if used in a project.
// Made to correspond with Semantic Versioning 2.0.0
using UnityEngine;
namespace DynaZor.EditorUtilities.SemanticVersioning
{
	public static class AddonInformation
	{
		public static readonly string AddonName = "Editor Utilities (DynaZor Edition)";
		public static readonly string AddonVersion = "v0.00.005";
		public static readonly string AddonBy = "Shahar DynaZor (Original by Vitaliy Zasadnyy)";
	}

	[SerializeField, System.Serializable]
	public class VersionNumber
	{
		public int Major = 0;
		public int Minor = 0;
		public int Patch = 0;
		
		public VersionNumber(int major = 0, int minor = 0, int patch = 0)
		{
			Major = major;
			Minor = minor;
			Patch = patch;
		}

		public string String (int minMajorDigits = 1, int minMinorDigits = 1, int minPatchDigits = 1, bool flatten = false, char seperator = '.')
		{
			string major = AddZeros(Major, minMajorDigits);
			string minor = AddZeros(Minor, minMinorDigits);
			string patch = AddZeros(Patch, minPatchDigits);

			string output = major;
			if (!flatten) output += seperator;
			output += minor;
			if (!flatten) output += seperator;
			output += patch;
			return output;
		}

		public int Int (int minMajorDigits = 1, int minMinorDigits = 1, int minPatchDigits = 1)
		{
			string str = String(minMajorDigits: minMajorDigits, minMinorDigits: minMinorDigits, minPatchDigits: minPatchDigits, flatten: true);
			int.TryParse(str, out int output);
			return output;
		}

		private static string AddZeros (int number, int minDigits)
		{
			string output = number.ToString();
			if (minDigits > 0)
			{
				for (int i = output.ToCharArray().Length; i < minDigits; i++)
				{
					output = "0" + output;
				}
			}
			return output;
		}
	}
}