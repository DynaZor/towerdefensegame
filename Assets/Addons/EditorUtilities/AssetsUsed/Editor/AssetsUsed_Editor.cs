﻿// Library made by Shahar DynaZor Alon, please give credit if used in a project.

using UnityEditor;
using UnityEngine;
using DynaZor.EditorUtilities.AssetsUsed;

namespace DynaZor.EditorUtilities.AssetsUsed.Editor
{
	public class AssetsUsed_Editor : EditorWindow
	{
		public static readonly string WindowTitle = "SO_AssetsUsed Editor";

		public SO_AssetsUsed Items = null;
		private SO_AssetsUsed ItemsBackup = null;

		private bool Changed = false;
		#region Editor Integration
		[MenuItem("DynaZor/Assets Used")]
		public static void CreateWindow ()
		{
			var window = EditorWindow.GetWindow<AssetsUsed_Editor>(false, WindowTitle, true);

			if (Selection.activeObject != null && Selection.activeObject.GetType() == typeof(SO_AssetsUsed))
			{
				window.Items = (SO_AssetsUsed) Selection.activeObject;
			}
		}

		public static void CreateWindow(SO_AssetsUsed asset)
		{
			var window = EditorWindow.GetWindow<AssetsUsed_Editor>(false, WindowTitle, true);
			window.Items = asset;
			window.Repaint();
		}
		#endregion
		private void OnGUI ()
		{
			using (var ChangeCheck = new EditorGUI.ChangeCheckScope())
			{
				var itemsBU = Items;
				Items = EditorGUILayout.ObjectField(new GUIContent("SO_AssetsUsed"), Items, typeof(SO_AssetsUsed), false) as SO_AssetsUsed;

				if (ChangeCheck.changed)
				{
					if (Changed)
					{
						if (!EditorUtility.DisplayDialog("Data Loss Warning",
					"You are about to revert this asset\nAre you sure? This Cannot be Undone!", "Continue", "Cancel"))
						{
							Items = itemsBU;
							Repaint();
							return;
						}
					}
					CreateBackupItems();
					Changed = false;
					Repaint();
				}

				if (Items == null)
				{
					if (GUILayout.Button(new GUIContent("Create", "Automatically create and reference a new SO_AssetsUsed asset\n" +
						"New asset will be placed in Assets\\")))
					{
						AssetDatabase.CreateAsset(new SO_AssetsUsed(), "Assets/AssetsUsed.asset");
						var newAsset = AssetDatabase.LoadAssetAtPath("Assets/AssetsUsed.asset", typeof(SO_AssetsUsed)) as SO_AssetsUsed;
						Items = newAsset;
						CreateBackupItems();
						Changed = false;
						Repaint();
					}
				}

			}

			if (Items != null)
			{
				using (var ChangeCheck = new EditorGUI.ChangeCheckScope())
				{
					DrawToolbar();

					DrawItems();
				}
			}
		}

		private void DrawItems ()
		{
			for (int i = 0; i < Items.Assets.Count; i++)
			{
				using (new EditorGUILayout.HorizontalScope())
				{
					GUILayout.Label(new GUIContent(Items.Assets[i].AssetName, Items.Assets[i].AssetWebsite));
					GUILayout.Label(new GUIContent(Items.Assets[i].AssetProfile.ToString(), ""));
					GUILayout.Label(new GUIContent(Items.Assets[i].AssetType.ToString(), ""));
					GUILayout.Label(new GUIContent(Items.Assets[i].CreatorName, Items.Assets[i].CreatorName + "\n" + Items.Assets[i].CreatorContact + "\n" + Items.Assets[i].CreatorWebsite));
					GUILayout.Label(new GUIContent(Items.Assets[i].LicenseName, ""));
					GUILayout.Label(new GUIContent("Comments (Public)", Items.Assets[i].CommentsPublic));
					GUILayout.Label(new GUIContent("Comments (Private)", Items.Assets[i].CommentsPrivate));
					if (GUILayout.Button("Edit"))
					{
						AssetsUsed_SingularEditor.CreateWindow(Items, Items.Assets[i], this);
					}
					if (GUILayout.Button("Remove"))
					{
						if (EditorUtility.DisplayDialog("Data Loss Warning",
				"You are about to delete this entry\nAre you sure? This Cannot be Undone!", "Delete", "Cancel"))
						{
							Items.Assets.RemoveAt(i);
							SetChanged();
						}
					}
				}
			}
			using (new EditorGUILayout.HorizontalScope())
			{
				GUILayout.FlexibleSpace();
				if (GUILayout.Button("+"))
				{
					Items.Assets.Add(new AssetInfo());
					SetChanged();
				}
				GUILayout.FlexibleSpace();
			}
		}

		#region GUI Draw
		private void DrawToolbar ()
		{
			using (new EditorGUILayout.HorizontalScope(EditorStyles.toolbar))
			{
				GUILayout.FlexibleSpace();
				GUI.enabled = Changed;
				if (GUILayout.Button(new GUIContent("Revert"), EditorStyles.toolbarButton))
				{
					Items.Apply(ItemsBackup);
					SetChanged(false);
				}
				if (GUILayout.Button(new GUIContent("Save"), EditorStyles.toolbarButton))
				{
					AssetDatabase.SaveAssets();
					CreateBackupItems();
					SetChanged(false);
				}
				GUI.enabled = true;
			}
		}

		#endregion

		#region Actions
		public void SetChanged (bool state = true)
		{
			Changed = state;
			if (state)
				EditorUtility.SetDirty(Items);
			Repaint();
		}

		private void CreateBackupItems (SO_AssetsUsed newAsset = null)
		{
			if (newAsset == null)
			{
				ItemsBackup = ScriptableObject.CreateInstance<SO_AssetsUsed>();
				ItemsBackup.Apply(Items);
			}
			else
			{
				ItemsBackup = ScriptableObject.CreateInstance<SO_AssetsUsed>();
				ItemsBackup.Apply(newAsset);
			}
		}
		#endregion

	}

	public class AssetsUsed_SingularEditor : EditorWindow
	{
		public static readonly string WindowTitle = "SO_AssetsUsed\\AssetInfo Editor";

		private AssetInfo inputData = null;
		private AssetInfo workData = null;

		private SO_AssetsUsed asset = null;

		private bool changed = false;

		private AssetsUsed_Editor sender = null;

		public static void CreateWindow (SO_AssetsUsed asset, AssetInfo data, AssetsUsed_Editor sender = null)
		{
			var window = EditorWindow.GetWindow<AssetsUsed_SingularEditor>(true, WindowTitle, true);

			if (window.changed)
			{
				if (!window.CloseUnsavedConfirm())
				{
					return;
				}
			}
			window.workData = new AssetInfo(data);
			window.inputData = data;
			window.asset = asset;
			window.sender = sender;
		}

		private void OnGUI ()
		{
			if (inputData != null && workData != null && asset != null)
			{
				using (var changeCheck = new EditorGUI.ChangeCheckScope())
				{
					DrawToolbar();

					DrawFields();
				}
			}
			else
			{
				EditorGUILayout.HelpBox("Error\nPlease relaunch this editor window", MessageType.Error);
			}
		}

		private void OnLostFocus ()
		{
			Close();
		}

		private void OnDestroy ()
		{
			if (changed)
			{
				if (!CloseUnsavedConfirm())
				{
					return;
				}
			}
		}
		#region GUI Draw
		private void DrawToolbar ()
		{
			using (new EditorGUILayout.HorizontalScope(EditorStyles.toolbar))
			{
				GUILayout.FlexibleSpace();
				GUI.enabled = changed;
				if (GUILayout.Button(new GUIContent("Revert"), EditorStyles.toolbarButton))
				{
					EditorGUIUtility.editingTextField = false;
					Revert();
				}
				if (GUILayout.Button(new GUIContent("Apply"), EditorStyles.toolbarButton))
				{
					EditorGUIUtility.editingTextField = false;
					ApplyChanges();
				}
				GUI.enabled = true;
			}
		}
		private void DrawFields ()
		{
			using (var ChangeCheck = new EditorGUI.ChangeCheckScope())
			{
				workData.AssetName = EditorGUILayout.DelayedTextField("Asset Name", workData.AssetName);
				workData.AssetProfile = (AssetProfile) EditorGUILayout.EnumPopup(new GUIContent("Asset Profile"), workData.AssetProfile);
				workData.AssetType = (AssetType) EditorGUILayout.EnumPopup(new GUIContent("Asset Profile"), workData.AssetType);
				workData.AssetWebsite = EditorGUILayout.DelayedTextField("Asset Website", workData.AssetWebsite);

				workData.ShortCreditLine = EditorGUILayout.DelayedTextField("Short Credit Line", workData.ShortCreditLine);

				EditorGUILayout.PrefixLabel(new GUIContent("Comments (Private)"));
				workData.CommentsPrivate = EditorGUILayout.TextArea(workData.CommentsPrivate);
				EditorGUILayout.PrefixLabel(new GUIContent("Comments (Public)"));
				workData.CommentsPublic = EditorGUILayout.TextArea(workData.CommentsPublic);

				workData.CreatorName = EditorGUILayout.DelayedTextField("Creator Name", workData.CreatorName);
				workData.CreatorWebsite = EditorGUILayout.DelayedTextField("Creator Website", workData.CreatorWebsite);
				workData.CreatorContact = EditorGUILayout.DelayedTextField("Creator Contact", workData.CreatorContact);

				workData.LicenseName = EditorGUILayout.DelayedTextField("License Name", workData.LicenseName);
				EditorGUILayout.PrefixLabel(new GUIContent("License"));
				workData.License = EditorGUILayout.TextArea(workData.License);

				if (ChangeCheck.changed)
					SetChanged();
			}
		}
		#endregion
		#region Actions
		public void SetChanged (bool state = true)
		{
			changed = state;
			Repaint();
		}
		private bool CloseUnsavedConfirm ()
		{
			int choice = EditorUtility.DisplayDialogComplex("Unsaved Data Warning", 
				"You are about to change or close this window\nDo you want to apply the changes first?",
				"Yes", "No", "Cancel");
			bool result = true;
			switch (choice)
			{
				case 0:
					ApplyChanges();
					break;
				case 2:
					result = false;
					break;
			}
			return result;
		}

		private void Revert ()
		{
			workData = new AssetInfo(inputData);
			SetChanged(false);
		}

		private void ApplyChanges ()
		{
			inputData.Apply(workData);
			SetChanged(false);
			sender.SetChanged();
		}
		#endregion
	}

	public class AssetsUsed_Inspector : UnityEditor.Editor
	{
		public override void OnInspectorGUI ()
		{
			if (GUILayout.Button(new GUIContent("Open Editor")))
			{
				AssetsUsed_Editor.CreateWindow(target as SO_AssetsUsed);
			}
			base.OnInspectorGUI();
		}
	}

	#region Attributes (Editor)
	[CustomPropertyDrawer(typeof(ShortCreditLineAttribute))]
	public class ShortCreditLineAttributeDrawer : PropertyDrawer
	{
		SerializedProperty currentProperty;
		#region OnGUI
		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
		{
			property.serializedObject.Update();
			Event e = Event.current;

			if (e.type == EventType.MouseDown && e.button == 1 && position.Contains(e.mousePosition))
			{
				GenericMenu context = new GenericMenu ();
				currentProperty = property;

				context.AddItem(new GUIContent("\"Asset Name\" by Creator Name"), true, Fill_AssetByCreator);
				context.AddItem(new GUIContent("\"Asset Name\" by Creator Name, under License Name"), true, Fill_AssetByCreatorUnderLicense);
				context.AddItem(new GUIContent("Asset Name (Creator Name)"), true, Fill_AssetBracketCreator);
				context.AddItem(new GUIContent("Asset Name (License Name)"), true, Fill_AssetBracketLicense);

				context.ShowAsContext();
			}
			EditorGUI.PropertyField(position, property);
			
			property.serializedObject.ApplyModifiedProperties();
		}
		#endregion
		#region Fill Methods
		private void Fill_AssetByCreator ()
		{
			GetInfo(out string AssetName, out string CreatorName, out string LicenseName);
			Fill("\"" + AssetName + "\"" + " by " + CreatorName);
		}
		private void Fill_AssetByCreatorUnderLicense ()
		{
			GetInfo(out string AssetName, out string CreatorName, out string LicenseName);
			Fill("\"" + AssetName + "\"" + " by " + CreatorName + "under " + LicenseName);
		}
		private void Fill_AssetBracketCreator ()
		{
			GetInfo(out string AssetName, out string CreatorName, out string LicenseName);
			Fill(AssetName + " (" + CreatorName + ")");
		}
		private void Fill_AssetBracketLicense ()
		{
			GetInfo(out string AssetName, out string CreatorName, out string LicenseName);
			Fill(AssetName + " (" + LicenseName + ")");
		}
		#endregion
		#region Helper Methods
		private void Fill (string text)
		{
			currentProperty.stringValue = text;
		}
		private void GetInfo(out string AssetName, out string CreatorName, out string LicenseName)
		{
			AssetName = currentProperty.serializedObject.FindProperty("AssetName").stringValue;
			CreatorName = currentProperty.serializedObject.FindProperty("CreatorName").stringValue;
			LicenseName = currentProperty.serializedObject.FindProperty("LicenseName").stringValue;
		}
		#endregion
	}
	#endregion
}