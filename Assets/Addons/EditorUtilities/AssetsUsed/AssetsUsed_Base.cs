﻿// Library made by Shahar DynaZor Alon, please give credit if used in a project.

using UnityEngine;

namespace DynaZor.EditorUtilities.AssetsUsed
{
	public enum AssetProfile
	{
		Free, Paid, Demo
	}

	public enum AssetType
	{
		Addon,
		Script,
		AI,
		Visual,
		Audio,
		UI
	}

	public class ShortCreditLineAttribute : PropertyAttribute
	{
		public ShortCreditLineAttribute ()
		{
		}
	}
}