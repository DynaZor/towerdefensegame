﻿// Library made by Shahar DynaZor Alon, please give credit if used in a project.

using UnityEngine;

namespace DynaZor.EditorUtilities.AssetsUsed
{
	[System.Serializable]
	public class AssetInfo
	{
		public string AssetName = "Asset Name";
		[ShortCreditLine] public string ShortCreditLine = "Short Credit Line";

		public AssetProfile AssetProfile = AssetProfile.Free;
		public AssetType AssetType = AssetType.Addon;

		public string AssetWebsite = "www";
		public string CreatorName = "Creator Name";
		public string CreatorContact = "Creator Contact";
		public string CreatorWebsite = "www";

		public string LicenseName = "Asset License Name";
		[TextArea] public string License = "Asset License";

		[TextArea] public string CommentsPublic = "Public comments";
		[TextArea] public string CommentsPrivate = "Private comments";

		public AssetInfo ()
		{
		}

		public AssetInfo (AssetInfo asset)
		{
			Apply(asset);
		}

		public void Apply (AssetInfo asset)
		{
			AssetName = asset.AssetName;
			ShortCreditLine = asset.ShortCreditLine;
			AssetProfile = asset.AssetProfile;
			AssetWebsite = asset.AssetWebsite;
			CreatorName = asset.CreatorName;
			CreatorContact = asset.CreatorContact;
			CreatorWebsite = asset.CreatorWebsite;
			LicenseName = asset.LicenseName;
			License = asset.License;
			CommentsPublic = asset.CommentsPublic;
			CommentsPrivate = asset.CommentsPrivate;
		}
	}
}