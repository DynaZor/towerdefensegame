Presented as part of Tower Defense Game by DynaZor

# **Editor Utilities (DynaZor Edition)** 

[main-project]:https://bitbucket.org/DynaZor/towerdefensegame/src/master/
[main-project-index]:https://bitbucket.org/DynaZor/towerdefensegame/src/master/INDEX.md
[semver]:https://bitbucket.org/DynaZor/towerdefensegame/src/master/SemVer.md

## Back to the Project's [README.md][main-project]\\[INDEX.md][main-project-index]

## Introduction

An asset made to ease casual working with Unity.

## Table of Contents

[TOC]

## Disclaimer

Heavily extended upon the original "Unity Editor Utilities" by Vitaliy Zasadnyy ([Link](https://assetstore.unity.com/packages/tools/utilities/unity-editor-utilities-37101)).



## Features

As of v0.00.003

Features marked with (D) were implemented by DynaZor.

Features marked with (t) were tweaked by DynaZor.

Features not marked are Vitaliy Zasadnyy's original code.



### (t) Big Colored Editor State Label

Easily see the editor's state, useful when waiting for it to compile.



### (t) Scene Shortcuts

Load (Normal or Additive) or Play a Scene from user-defined lists.

Set the desired scenes for each list in the Settings.



### (D) Quick Project Settings

Quickly change the project's Name, Company, Dev-Stage and Version.

No need to dig into Project Settings anymore ~,~



### (D) Big "Play" Button

Harder to miss, easier to find and Single Purpose!

It's hard to market a single purpose button, you know...



### (D) Additive Preloading a Scene Before Play

Now you don't need to remember to add that scene before play,

just click the Play button ;)

There's also a "Preload" button for just additively loading the Preload Scene without playing.

Set the desired Preload Scene in the Settings.



### (D) Quick Build and No Overwrites!

Build by filling in the version, dev-stage and clicking "Build".

You can also choose from a number of Folder Naming options;

[Date] \ [Version] \ [State]\_[Version] \ [Version]\_[Date] \ [Date]\_[Version]

Set the desired build location and versioning preferences in the Settings.

Also, if there's already a folder with the same name,

then Editor Utilities will add a letter to the end of the build name and increment it until there isn't a folder with that name.



### (D) Semantic Versioning + Dev-Stage

Implemented a library for easy and standardized versioning control.



### Recent Projects Editor

Clean the Projects appearing in Unity's Recent Projects.



([Back to Table Of Contents](#markdown-header-table-of-contents))



## Setup

Import\Copy the "EditorUtilities" folder into your Unity project.

*(I recommend to make an "Addons" folder to store it)*

Create an "App Settings" asset either by using EU, or create yourself.

*(I recommend to use the generated "Settings" folder to store it)*

Open the addon's main panel via: Window \ Editor Utilities (DynaZor Edition)

In the Editor Utilities panel's top bar, click "Settings" 

If you made an AppSettings asset yourself, reference it.

Adjust the settings to your liking and click "Done".



([Back to Table Of Contents](#markdown-header-table-of-contents))



## Additional Usage Examples

* Reference the assigned App Settings asset and read its info into your project. 

    (Currently Editor Only, use Right Click - Get from Editor Utilities)

    ```Code:
    AppSettings.Current
    ```

    Example 1: Show the Version and Dev-Stage on the game's Main Menu.

    Example 2: For a project with a name changing a lot, show the Project Name on the Main Menu.

     

* *Have ideas for additional usage of this asset?*

    *Suggest it in the [**Issues**](https://bitbucket.org/DynaZor/towerdefensegame/issues) page!*



([Back to Table Of Contents](#markdown-header-table-of-contents))



## Planned Features

* **Quick Build - Settings: "v Prefix" Checkbox**

    (currently no "v" prefix is added to build folder name before the version number)

* **Quick Build: Build Without AppSettings**

* **Scene Shortcuts - Main and Secondary Lists for each action's list**

    ​     

* **Have an idea for an additional feature everyone will enjoy?**

    *Suggest it in the [**Issues**](https://bitbucket.org/DynaZor/towerdefensegame/issues) page!*



([Back to Table Of Contents](#markdown-header-table-of-contents))



## Known Issues

* **Features section is not updated to latest version**

* Scene Shortcuts: Only some (maybe only active) scenes have their Build Index

* AppSettings.Current is not set on Play and Build

    *Workaround: Make an AppSettings variable in a singleton GameObject,* 

    *set from the editor by Right Click - Get from Editor Utilities*

      

* *Found issues with the addon?*

    *Please report them in the [**Issues**](https://bitbucket.org/DynaZor/towerdefensegame/issues) page :)*



([Back to Table Of Contents](#markdown-header-table-of-contents))



## Coming Soon

* Updated Features section
* Detailed "Usage.md" file
* Screenshot\s
* Unity Asset Store page?



([Back to Table Of Contents](#markdown-header-table-of-contents))



## Versioning

We use [SemVer](http://semver.org/) for versioning.

See **[SemVer.md][semver]** for more information about Versioning.



([Back to Table Of Contents](#markdown-header-table-of-contents))



## Change Log

**v0.00.006**

* AppSettings Property Drawer: Right Click - Get from Editor Utilities: Now works with private serialized variables, too
* Scene Shortcuts\Play - Prompt Save if there are Unsaved Scenes



([Back to Change Log Title](#markdown-header-change-log))

([Back to Table Of Contents](#markdown-header-table-of-contents))



**v0.00.005**

* New: Version Consistency Check between addon's files
* New: AppSettings property drawers now have Context Menu containing two options:
    * "Get from Editor Utilities" - to reference the Editor Utilities' Current AppSettings (var needs to be public!)
    * "Apply Information to Project" - Appears only when selected AppSettings' information is different from the project's
* New: Different (sub) Namespaces for differentiating parts of Editor Utilities
* New: More Info panel in Settings showing Unity and C# versions, Detected System Language
* New: "Recent Projects Editor" Toolbar Button
* New: Warnings\Errors for missing needed AppSettings properties
* New: Right Click Button Functions:
    * Play Button: Right Click to Stop
    * Preload Button: Right Click to Unload
    * Build Button: Right Click to Build To... (Browse)
    * Scene Lists: "+" Button: Right Click to Unload
* New: Scene Management works only when not playing (can change in Advanced Settings)
* New: Base.cs: Helpers for Play Mode (Currently only EU Debug)
* New: EUExtensions: Get and Set SerializedProperty easily
* New: EUEditorOnly: Editor Only Helper Methods for Editor Utilities
* Script Refactoring and Maintenance



([Back to Change Log Title](#markdown-header-change-log))

([Back to Table Of Contents](#markdown-header-table-of-contents))



**v0.00.004**

* New: Quick Build Settings + Quick Build Target + Persistent Quick Build Settings 

    Including: Open Folder, Auto Dev, Always Dev, Unity's Build Options, Target Selection

* New: Create and Auto Reference "App Settings" Asset from the Settings panel

* Easier Scene Referencing in Settings

* Settings GUI Overhaul: Now categorized and collapsible

* Settings: Build Path: Browse to set path

* AppSettings Implementation Overhaul: Now without SerializedObject

* **(Editor Only)** New: Static reference to the assigned AppSettings through 

    ```Code:
    AppSettings.Current
    ```

* Window Refactoring

* SemVer Refactoring

* Label fixes

* Minor fixes



([Back to Change Log Title](#markdown-header-change-log))

([Back to Table Of Contents](#markdown-header-table-of-contents))



**v0.00.003**

* GUI Overhaul: Expanding width and height, Launch and Load Scene side by side,

    Launch and Load Scene scroll bars, Whole Panel Min Size and Scroll Bars, 

    Better Sizing and Placement of Play, Preload and Editor State, 

    New Interface is now capable of being super small

* New: Folder Naming Format: [Date]\_[Version]
* New: Advanced Settings: Currently only Visual Adjustments 
* New: Custom Inspector for AppSettings Assets
* Fix: Last Build Version
* Tidied up and Organized the Scripts better
* Moved "Edit Recent Projects" to Window \ Editor Utilities \ Edit Recent Projects
* Changed Namespace to DynaZor.EditorUtilities to differentiate from original
* Small fixes
* Created this README.md



([Back to Change Log Title](#markdown-header-change-log))

([Back to Table Of Contents](#markdown-header-table-of-contents))