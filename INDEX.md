# Tower Defense Game by DynaZor

## Index

### Documents (.md)

[README.md](https://bitbucket.org/DynaZor/towerdefensegame/src/master) - Project's Main Readme document, including current Dev-Stage

[INDEX.md](https://bitbucket.org/DynaZor/towerdefensegame/src/master/INDEX.md) - Project's Index document, containing links to all documents (This document)

[DevHistory.md](https://bitbucket.org/DynaZor/towerdefensegame/src/master/DevHistory.md) - The Project's Detailed Development History, excluding Dev-Stage

[Thanks.md](https://bitbucket.org/DynaZor/towerdefensegame/src/master/Thanks.md) - Credits and Thanks

[IDEA.md](https://bitbucket.org/DynaZor/towerdefensegame/src/master/IDEA.md) - The Original Idea note from DynaZor's Google Keep

[SemVer.md](https://bitbucket.org/DynaZor/towerdefensegame/src/master/SemVer.md) - Details about Semantic Versioning in this project

[LICENSE.md](https://bitbucket.org/DynaZor/towerdefensegame/src/master/LICENSE.md) - The license used for this project

[Info\Documents\GameDesign-1Pager.md](https://bitbucket.org/DynaZor/towerdefensegame/src/master/Info/Documents/GameDesign-1Pager.md) - Game Design 1 Pager for the game

[Assets\Addons\EditorUtilities\README.md](https://bitbucket.org/DynaZor/towerdefensegame/src/master/Assets/Addons/EditorUtilities/README.md) - Editor Utilities (DynaZor Edition) Main Readme document



### Interesting Folders

[Media\Concepts](https://bitbucket.org/DynaZor/towerdefensegame/src/master/Media/Concepts) - The original Concept Art, Sketches, Visualizations made for the game

[Media\Screenshots](https://bitbucket.org/DynaZor/towerdefensegame/src/master/Media/Screenshots) - Screenshots from different versions of the game

[Info\Art Charts](https://bitbucket.org/DynaZor/towerdefensegame/src/master/Info/Art Charts) - Chronological Collection of the Art Iterations of in-game graphics [(*A)][#markdown-header-notes]

[Info\Flow Charts](https://bitbucket.org/DynaZor/towerdefensegame/src/master/Info/Flow Charts) - Architectural, UI and (not yet) Gameplay Plans of the game [(*A)][#markdown-header-notes]

[Info\Documents](https://bitbucket.org/DynaZor/towerdefensegame/src/master/Info/Documents/) - Documents made for the game (Example: [Game Design 1 Pager](https://bitbucket.org/DynaZor/towerdefensegame/src/master/Info/Documents/GameDesign-1Pager.md))

[Assets\Addons\EditorUtilities](https://bitbucket.org/DynaZor/towerdefensegame/src/master/Assets/Addons/EditorUtilities/) - Editor Utilities (DynaZor Edition) - Unity Addon Asset by DynaZor



### Notes

**(*A)** - Open .graphml files with yEd Graph Editor, updated JPG exports of the graphs are regularly uploaded.