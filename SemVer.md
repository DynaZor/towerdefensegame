# Tower Defense Game by DynaZor

## Back to [README.md](https://bitbucket.org/DynaZor/towerdefensegame/src)\\[INDEX.md](https://bitbucket.org/DynaZor/towerdefensegame/src/INDEX.md)

We use **[Semantic Versioning 2.0.0](http://semver.org/)** (AKA SemVer) for versioning.

## SemVer - The Basic Concept

> MAJOR.MINOR.PATCH
>
> 1. MAJOR version when you make incompatible API changes
> 2. MINOR version when you add functionality in a backwards compatible manner
> 3. PATCH version when you make backwards compatible bug fixes

(Above is a quoted and a bit changed section from the [SemVer](https://semver.org/) page)

In this project the Save Files Compatibility is treated as the "API Compatibility".

Difference in Major version means save files won't be compatible at all, mostly likely to cause a game crash.

Difference in Minor version means the game should handle the conversion of the save files.

Difference in Patch version means the save files should be 100% compatible.

As soon as we realize that we’ve broken the Semantic Versioning spec, we'll fix the problem and release a new minor version that corrects the problem and restores backwards compatibility as soon as possible. 

## Dev-Stages

Throughout the development of the game, the project will go through several Development Stages,

namely: Prototype, Proof Of Concept, Alpha, Beta, Release, Patch

These won't affect the versioning of the project, but will act as an additional guideline of what to expect from said version.

You can follow the Development History of the game and get more information regarding the different stages in the [DevHistory.md](DevHistory.md) file.