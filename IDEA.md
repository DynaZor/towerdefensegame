# Tower Defense Game by DynaZor

## Back to [README.md](https://bitbucket.org/DynaZor/towerdefensegame/src/master/)\\[INDEX.md](https://bitbucket.org/DynaZor/towerdefensegame/src/master/INDEX.md)\\[DevHistory.md](https://bitbucket.org/DynaZor/towerdefensegame/src/master/DevHistory.md)

## The Original Idea note from DynaZor's Google Keep

Open source (read only) and public trello

\>Write all ideas in a trello board, group by subjects
After all is written and sorted,

make a visual code architecture flow for each system
 (find suitable free software)
Always prefer Interfaces, then Abstract Classes
unique classes are last resort or one time use only
Make a board for considerations and fill;
Visuals+Art atyles, coding, gameplay, audio

Use scriptable objects for unit and building parameters
(interface) general placeable type - health and needs
(Interface for so) building/unit/movable unit
(scriptable object) items - specific stats, upgrades, etc
this way enemy can find and hurt any item,
all items will require the needs on the basic code,


\>Grid based world, top down ortho or isometric

\>Empire Earth\War of Ages age of war (flash) inspired

\>at least 3 different ages; Stone age, bronze age, laser age

\>Get resources by placing farms, barns and factories

\>Buildings can be demolished for % of the resources used to build them

\>Enemies
spawn outside the screen, each time at a different out of screen tile
This forces players to change formations unlike standard td games
Most enemies will try to reach the base, some (random chance) will just seek to destroy a number of buildings and escape (if none left, destroy base)
Need Path finding scripts:
*minimum obstacles to base
*find closest and easiest to reach building
Path finder needs to update on grid changes -
*if already close to base, go shortest and minimum obstacle path

\>Map will expand on each age advance

\>*Optional - tiles will have properties in percent:
Ground Fertility for farms
Grassy (copy from kingdoms and castles) for barns
Underground water מי תהום for factories
changes are over time except zeroing out
\>Building a farm will decrease fertility but increase grassy
\>Building a barn will decrease grassy but increase fertility
\>Buildimg a factory will zero out grassy and fertility and decrease water

\>*Optional - Factories will have a radius at which farms and barns will be less effective

\>Defenses and Units
walls, units of soldiers, unit behind cover, tower units
Moveable crusade units, like Age Of War (clubman up to laser shooting mech)

\>Electrical ages will need power factories that will give a gnerous radius electricity
Different buildings will require diff amounts of elect
Soldier units without cover dont need elect
Towers need minimal food but elect

\>Defenses are upgradeable, like Age Of War units,
cannot upgrade to diff age, need to demolish first

\>Downtime between waves of enemeis

\>Base
Center block will be the hut for the tribe
to upgrade to bronze age, need more blocks free and will vecome a town, then a castle
Laser age distopian city with more blocks
*Additional town/homes blocks for bonus attack and Defense on all units at cost of resourcrs, mostly food (both farms and barns)
Base will have health and needs,
if needs are not met there will be a continues health penalty

\>Enemies:
Stone age
cows and buffallos, snakes, wolves, lions, cheetahs,
people, tribes

Bronze age
Dragons,
peasants, crusades, small armies

Laser age
nano machine flocks, stray\rabid robots,
people resistance, robot armies, tanks

\>choose what triggers enemy age advance, connect with difficulty setting

\>if needs not met for an item - it shuts down, if unit then it will demolish without refund after some time
walls will lose health gradually if not "maintained"

*Optional - wood, stone and iron resources

\>Age advance will need points gathered by (from most to least)
killing enemies, building and getting units, upgrading buildings and units (using resources), gathering resources

*Optional - god powers by age (Like Age Of War)

\>pathfinder
check each of 4 adj blocks if there's a obst, make a list of paths, choose one with least obst and shortest
*for non base attck enemies - find closest least obst out of view block and path to it - first by obst