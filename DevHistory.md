# Tower Defense Game by DynaZor

## Back to [README.md](https://bitbucket.org/DynaZor/towerdefensegame/src/master/)\\[INDEX.md](https://bitbucket.org/DynaZor/towerdefensegame/src/master/INDEX.md)

[toc-link]:#markdown-header-table-of-contents
[original-idea]:IDEA.md



## The Project's Development History

### [Table of Contents](#toc)

[TOC]

### Stage 1 - Prototype v0.00.000 - [v0.00.149](https://bitbucket.org/DynaZor/towerDefensegame/downloads/DynaZorTD_0.00.149.exe)

Started 07/10/2019

Ended 05/11/2019

#### Stage Description

The Prototype was meant to test DynaZor's abilities for the creation of the game.

The Prototype's practical goal is to have the Main Basic Systems of the game working.

#### Stage Missions

- [x] Planning the basic systems
- [x] Building the basic systems, testing and expanding
- [x] Putting it all together, adding minimal playability
- [x] Optimization Run
- [x] Reassessment - Coming up with missing features and then Planning the next steps

#### Team Members in this Stage

Shahar DynaZor



([Back to Table Of Contents][toc-link])



### Stage 0 - The Idea

#### Stage Description

Coming up with an idea for a game was a huge struggle.

Until one night;

DynaZor saw a YouTube compilation video about upcoming strategy games,

and was disappointed he couldn't find a Simplistic yet Strategical casual game.

Then DynaZor came up with the idea, and wrote it as fast as he could,

so he wouldn't forget it.

(The Original Idea from DynaZor's Google Keep note written that night - [Link][original-idea])

#### Stage Missions

* [x] Find an idea for a game to develop and then enjoy
* [x] Write the idea down, as detailed as possible
* [x] Consider the idea as the next project

#### Team Members in this Stage

Shahar DynaZor