# DynaZor TowerDefense

## Introduction

Free Open Source Simplistic 2D Top-Down Real-Time Strategy **Tower Defense** game,

combining elements from [RTS, TD and Age of War games][inspire].

A passion project lead by Shahar DynaZor :)

![DynaZorTD](Media/Screenshots/v0.00.368-InGame.png)

## Terminology

**RTS** - Real-Time Strategy

**TD** - Tower Defense

**Dev-Stage** - Development Stage

[src]: https://bitbucket.org/DynaZor/towerdefensegame/src/master/
[trello]:https://trello.com/dynazortowerdefense	"Trello"



[unity-current]:https://unity3d.com/de/unity/whats-new/2019.3.0	"Unity 2019.3.0f6"



[toc-link]:#markdown-header-table-of-contents	"Table of Contents"
[inspire]:#markdown-header-inspiration	"Inspiration"
[extras]:#markdown-header-extras	"Extras"
[license]:https://bitbucket.org/DynaZor/towerdefensegame/src/master/LICENSE.md	"LICENSE.md"
[semver]:https://bitbucket.org/DynaZor/towerdefensegame/src/master/SemVer.md	"SemVer.md"
[dev-history]:https://bitbucket.org/DynaZor/towerdefensegame/src/master/DevHistory.md	"DevHistory.md"
[thanks]: https://bitbucket.org/DynaZor/towerdefensegame/src/master/Thanks.md""	"Thanks.md"
[issues]:https://bitbucket.org/DynaZor/towerdefensegame/issues	"Issues"
[downloads]:https://bitbucket.org/DynaZor/towerdefensegame/downloads/	"Downloads"
[eu-src]: https://bitbucket.org/DynaZor/towerdefensegame/src/master/Assets/Addons/EditorUtilities	"(Asset) Editor Utilities (DynaZor Edition) - Source"
[eu-store]:#	"(Asset) Editor Utilities (DynaZor Edition) - Unity Asset Store"
[media-screenshots]: https://bitbucket.org/DynaZor/towerdefensegame/src/master/Media/Screenshots/	"(Media) Screenshots"
[media-concepts]: https://bitbucket.org/DynaZor/towerdefensegame/src/master/Media/Concepts/	"(Media) Visual Concepts"
[media-charts]:https://bitbucket.org/DynaZor/towerdefensegame/src/master/Info/Flow%20Charts/	"(Media) Flow Charts"
[media-art-charts]:https://bitbucket.org/DynaZor/towerdefensegame/src/master/Info/Art%20Charts/	"(Media) Art Charts"

[media-documents]:https://bitbucket.org/DynaZor/towerdefensegame/src/master/Info/Documents/ "(Media) Documents"
[last-prototype]:https://bitbucket.org/DynaZor/towerdefensegame/downloads/DynaZorTD_0.00.149.exe	"Prototype v0.00.149"

[renderdoc]:https://renderdoc.org/ "RenderDoc"



## Table of Contents

[TOC]

[INDEX.md](https://bitbucket.org/DynaZor/towerdefensegame/src/master/INDEX.md) - Project's Index document, containing links to all documents

## Playing The Game

### Quick Download

**[v0.00.149][last-prototype] - Prototype** - the Main (most) Basic Systems of the game working

**[Downloads][downloads]** - Including newer builds



Non-Release builds are available for **testing** purposes.

```
Provided Files are SFX Archives (https://en.wikipedia.org/wiki/Self-extracting_archive).

Download and execute the wanted file,

choose where to extract the game to and click "Extract".

A new folder, named after the version, will be created at the desired location.

Execute the "TD.exe" executable file from inside the folder to play!
```

If and when you find bugs, ideas for features or anything else,

you are welcome to Create an Issue in the **[Issues][issues]** page! (Now open for anonymous submissions, too)

Thanks in advance :)



([Back to Table Of Contents][toc-link])




## Dev-Stage: Proof Of Concept

**[Watch the project's Trello Boards here!][trello]**

**Bold Missions** are the missions that are currently in progress



The Proof Of Concept's goal is to plan, conceptualize and begin the creation of the final game.

It will *(hopefully)* include all the gameplay features, which will be refined in the next stages.



- [x] Prototype of the Basic Main Systems (for details look at **[DevHistory.md][dev-history]**)
- [x] Optimization Run
    - [x] Optimize the Game Clock
    - [x] Optimize Pathfinding
        - [x] Optimize Existing Simple System - *Failed, Resorted to using an existing system*
        - [X] Implementation of Aron Granberg's "A* Pathfinding Project" - *'Free' didn't meet requirements*
        - [x] Consider Implementation of Unity's Tile system - *Rejected, made Single Quad Node Shader*
        - [x] Implementation of own Custom A* Pathfinding 
    - [x] Optimize Graphics for Big Grids (up to 100x100 tiles)
        - [x] Single Quad Pollution for the whole map
        - [x] Single Quad Ground for the whole map
        - [x] Render Static Items on Node's Quad
    - [x] Optimize other Main Systems
        - [x] Reduce Amount of GO's in scene
        - [x] Fix Major Bugs
            - [x] Camera Controls
            - [x] Info Panels
- [ ] **Basic Concepts**
    - [x] Game Design
        - [x] List Gameplay Features ("TD - Features" board in our Trello)
        - [x] Plan Feedback Loops
        - [x] Game Design 1-Pager
        - [x] Difficulties
        - [x] Fog Of War
    - [ ] **Art**
        - [ ] **Art Style**
            - [x] Nodes *(The world itself - Ground and Pollution)*
            - [ ] **Lighting**
            - [ ] Movement and Direction of Characters
            - [ ] Attacking Visualization Type and Style
            - [ ] Color Palette Specification \ Limiting
    - [x] UI
        - [x] Features
        - [x] Basic Layout
    - [ ] Sound
        - [ ] SFX
        - [ ] UI Sounds for each age
        - [ ] BGM for each age
- [ ] **First Iteration**
    - [ ] **Game Design and Planning**
        - [ ] Items and Enemies
            - [ ] Items Sheet - Basic Naming and Progression
            - [ ] Items Sheet - Descriptions
            - [ ] Items Sheet - Stats
        - [x] Difficulties
        - [ ] **Age Progression**
            - [ ] Progress Button
            - [ ] Progress Conditions System
        - [ ] **Program the Advanced Systems**
            - [x] Global Items Database + Editor
            - [ ] **Enemy Waves**
                - [x] Basic Spawning
                - [ ] **Difficulty Based Spawning**
                - [ ] Enemy Progression
            - [x] Fog Of War
    - [ ] **Systems**
        - [x] Game Grid System
        - [x] Nodes Ecosystem
        - [x] Pathfinding System
        - [x] Items System
        - [ ] **Centralized Mouse Control System**
        - [ ] Base of Centralized Keyboard Control System
        - [x] Buildable Area only near Existing Buildings
        - [ ] Base of Pollution Expansion System
    - [ ] **Art**
        - [x] Nodes Variation 1
        - [ ] **First Buildings for each age (Base, Farm, Wall)**
        - [ ] First Enemies for each age
        - [ ] First Ally Movables for each age
        - [ ] **Fog Of War**
        - [ ] **Lighting**
    - [x] **UI**
        - [x] Most Basic UI Iteration 1 for each age
        - [x] Age System UI Implementation
        - [x] Centralized UI System - *Postponed to Alpha stage*
        - [x] Pixelization Overhaul
        - [ ] **Item Status Visualization**
            - [x] System
            - [ ] **Icons**
        - [ ] **Build Menu Base**
            - [ ] **System**
            - [ ] Icons
    - [ ] Sound
        - [ ] Basic SFX System
        - [ ] Basic SFX + Placeholders
        - [ ] Basic UI Sounds System
        - [ ] Basic UI Sounds for first age
        - [ ] Basic BGM System
        - [ ] Basic BGM for first age
- [ ] Testing
    - [ ] Systems
    - [ ] UI
    - [ ] Art
    - [ ] Sound
    - [ ] Gameplay
- [ ] Finalize Concepts
    - [ ] Game Design
    - [ ] Art
    - [ ] UI
    - [ ] Sound
- [ ] **Fix Major Bugs**
- [ ] Reassessment - Come up with missing features and then Plan the next steps

 

([Back to Table Of Contents][toc-link])



## Made With

* [Unity][unity-current] (Hover for exact version) and Unity Hub - Game Engine
* [Microsoft Visual Studio 2019 Community](https://visualstudio.microsoft.com/) - IDE
* [yEd Graph Editor](https://www.yworks.com/products/yed) - Graph Editor for Plans (see [Extras](#markdown-header-extras))
* [SourceTree](https://www.sourcetreeapp.com/) (Software) and [Bitbucket](https://bitbucket.org/) (Provider) - GIT Source Control
* [Typora](https://typora.io/) - Editor for making this and other *.md files
* [RenderDoc][renderdoc] - Custom Shader Debugging

 

([Back to Table Of Contents][toc-link])



## Development - Getting Started

These instructions will get you a copy of the project up 

and running on your local machine for development and testing purposes.

### Prerequisites

[Unity][unity-current] (Hover for exact version)

An IDE, we use Microsoft Visual Studio 2019 Community provided with Unity

### Installing

Get a copy of the code using a Source Control application.
Open the root folder with Unity.

Optional - Open Editor Utilities via Window\Editor Utilities.



### Playing

Load both "MainMenu" and "_persistantData" scenes,

then Play.



### Contributing

As always contributions are welcome,

but currently only through Creating an Issue on the **[Issues][issues]** page.

In the future, the repository will be open for forking.



([Back to Table Of Contents][toc-link])



## Versioning

We use [SemVer](http://semver.org/) for versioning.

See **[SemVer.md][semver]** for more information about Versioning.



([Back to Table Of Contents][toc-link])



## Authors

* **Shahar DynaZor** - *Initial work* - [me@DynaZor.com](mailto:me@DynaZor.com)
* Want to join? Contact DynaZor! ^_^



([Back to Table Of Contents][toc-link])

 

## License

This project is licensed under **The 3-Clause BSD License**,
see the **[LICENSE.md][license]** file for details.



Used assets' licenses will be included in the near future.



([Back to Table Of Contents][toc-link])

 

## Assets Used

### Unity

* SerializableDictionary - [Link](https://github.com/azixMcAze/Unity-SerializableDictionary) - Under MIT license

### Fonts

* OpenDyslexic v3 - [Link](https://gumroad.com/l/OpenDyslexic) - Under OFL license
* pixeldroid's Console (Regular) - [Link](https://github.com/pixeldroid/fonts/tree/master/console) - Under OFL license
*  CodeMan38's PressStart2P - [Link](https://fonts.google.com/specimen/Press+Start+2P) - Under OFL license

All non-pixel-based fonts were pixelized using TextMeshPro

([Back to Table Of Contents][toc-link])

 

## Acknowledgments

This is a passion project, started out of the desire to play such a game :)

Credits and Thanks are available in [Thanks.md][thanks].

([Back to Table Of Contents][toc-link])

 

## Inspiration

[Age of War](https://armorgames.com/play/616/age-of-war)

[Kingdoms and Castles](http://kingdomsandcastles.com/)

[Banished](https://store.steampowered.com/app/242920/Banished/)

[Empire Earth](https://en.wikipedia.org/wiki/Empire_Earth)

and generally Flash and Android TD games



([Back to Table Of Contents][toc-link])

 

## Extras

### *(Media)* Visual Concepts ([Link][media-concepts])

```
The original Concept Art, Sketches, Visualizations made for the game
```



([Back to Table Of Contents][toc-link])



### *(Media)* Flow Charts ([Link][media-charts]) 

```
Architectural, UI and (not yet) Gameplay Plans of the game

*Open .graphml files with yEd Graph Editor
*Updated JPG exports of the graphs are regularly uploaded
```



([Back to Extras][extras])

([Back to Table Of Contents][toc-link])



###  *(Media)*  Art Charts ([Link][media-art-charts]) 

```
Chronological Collection of the Art Iterations of in-game graphics

*Open .graphml files with yEd Graph Editor
*Updated JPG exports of the graphs are regularly uploaded
```



([Back to Extras][extras])

([Back to Table Of Contents][toc-link])



### *(Media)*  Screenshots ([Link][media-screenshots]) 

```
Screenshots from different versions of the game
and some screenshots from the Editor
```



([Back to Extras][extras])

([Back to Table Of Contents][toc-link])



### *(Media)*  Documents ([Link][media-documents]) 

```
Documents made for the game
Example: Game Design 1 Pager (GameDesign-1Pager.md)
```



([Back to Extras][extras])

([Back to Table Of Contents][toc-link])



### *(Asset)* Editor Utilities (DynaZor Edition) ([Link][eu-src])

```
An asset made to ease casual working with Unity.
```



([Back to Extras][extras])

([Back to Table Of Contents][toc-link])