# Tower Defence Game by DynaZor

## Back to [README.md](https://bitbucket.org/DynaZor/towerdefencegame/src/master/)\\[INDEX.md](https://bitbucket.org/DynaZor/towerdefencegame/src/master/INDEX.md)

# Tower Defence Game

*One-page design document*

## Game Identity / Mantra: 

Free Open Source Simplistic 2D Top-Down RTS Tower Defense game

## Design Pillars:

* Creativity
* Urgency
* Easy to learn, Hard to Master

## Genre/Story/Mechanics Summary:

Tower Defence game with Age Progression and Basic Resource Management in an Expanding World Map.

## Features: 

* Standard Tower Defence Controls
* Age Progression - For both the Player and the Enemies. (+Per Age UI and Sound)
* Map expands over time
* Natural Resources deplete from use. Renew unless there’s a lot of pollution from buildings.

## Interface: 

The game is built first for Windows with either Mouse, Keyboard, or Both.

**Time controls** - Speed up and pause the game world

**Camera controls** - Pan, Zoom, Center Base Building

**Construction\Ally controls** - Build, Upgrade, Demolish, Move

**Age controls** - Progress to Next Age

## Art Style: 

2.5D Simplistic Pixel Art - Medium Resolution

Top Down View - Wide at the bottom and thin at the top

Shaded yet Cartoonish - Cutesy yet Serious vibe, Low-Medium Detail

## Music/Sound: 

Responsibility and Leadership

Stone Age - Nature, Folklore

Glory Age - Chivalry, Generosity

Malignant Age - Dread, Depression

Mended Age - Futuristic, Utopia

## Development Roadmap / Launch Criteria: 

**Audience**:   18-24 years old, gamers who want to chill to a tactical game on a daily basis.

**Platforms**: 

* **Open Source - Windows Standalone**
* \*Steam - Windows
* \**Web Player
* \**Play Store - Android
* \***AppStore - iOS

**Milestones:**

* **Milestone 1**: Prototype complete

    Planned: Not Planned

    *Achieved: 05/11/2019*

* **Milestone 2**: Proof Of Concept complete

    Planned: 01/07/2020

    *Achieved: Not Yet*

* **Milestone 3**: Alpha complete

    Planned: 01/12/2020

    *Achieved: Not Yet*

* **Milestone 4**: Beta complete

    Planned: 01/03/2020

    *Achieved: Not Yet*

* **Milestone 5**: Polish (Release) complete

    Planned: 01/06/2020

    *Achieved: Not Yet*

* **Launch Day**:

    Planned: 03/10/2020 (DynaZor’s 24th BDay)

    *Achieved: Not Yet*